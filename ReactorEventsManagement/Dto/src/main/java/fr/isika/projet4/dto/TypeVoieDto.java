package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * <b>TypeVoieDto est la classe . C'est une Dto de la classe TypeVoie.</b>
 * @author stagiaire
 *
 */
public class TypeVoieDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id attribut de TypeVoieDto.
     */
    private Integer id;

    /**
     * libelle attribut de TypeVoieDto.
     */
    private String libelle;

    /**
     * Constructeur vide.
     */
    public TypeVoieDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

}
