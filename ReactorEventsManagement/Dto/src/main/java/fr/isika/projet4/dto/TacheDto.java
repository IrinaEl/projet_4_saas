package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * @author stagiaire class tacheDto
 */
public class TacheDto implements Serializable {

    /**
     * constante dans le contexte du seriaizable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * attribut id tache.
     */
    private Integer id;

    /**
     * attribut deadline tache.
     */
    private Integer deadline;

    /**
     * attribut type tache.
     */
    private TypeTacheDto typeTaches;

    /**
     * attribut statut de la tâche.
     */
    private StatutTacheDto statut;

    /**
     * le bénévole affectée par le RA à a tâche (en tant que ressoutce ->
     * intervenant externe de type bénévole.
     */
    private RessourceDto benevole;

    /**
     * Default constructor.
     */
    public TacheDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the deadline
     */
    public Integer getDeadline() {
        return deadline;
    }

    /**
     * @param paramDeadline the deadline to set
     */
    public void setDeadline(Integer paramDeadline) {
        deadline = paramDeadline;
    }

    /**
     * @return the typeTaches
     */
    public TypeTacheDto getTypeTaches() {
        return typeTaches;
    }

    /**
     * @param paramTypeTaches the typeTaches to set
     */
    public void setTypeTaches(TypeTacheDto paramTypeTaches) {
        typeTaches = paramTypeTaches;
    }

    /**
     * @return the statut
     */
    public StatutTacheDto getStatut() {
        return statut;
    }

    /**
     * @param paramStatut the statut to set
     */
    public void setStatut(StatutTacheDto paramStatut) {
        statut = paramStatut;
    }

    /**
     * @return the benevole
     */
    public RessourceDto getBenevole() {
        return benevole;
    }

    /**
     * @param paramBenevole the benevole to set
     */
    public void setBenevole(RessourceDto paramBenevole) {
        benevole = paramBenevole;
    }

}