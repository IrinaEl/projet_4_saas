package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe représentant l'entité {@link RecetteDto}.
 * @author stagiaire
 */
public class RecetteDto extends FluxDto implements Serializable {

    /**
     * Default serial version Id.
     */
    private static final long serialVersionUID = 1L;

}
