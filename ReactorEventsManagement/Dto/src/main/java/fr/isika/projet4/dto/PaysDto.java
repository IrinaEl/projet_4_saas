package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * <b>PaysDto est une classe sérializable. C'est une Dto de la classe Pays.</b>
 * @author stagiaire
 *
 */
public class PaysDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id de la classe PaysDto.
     */
    private Integer id;

    /**
     * libelle de la classe PaysDto.
     */
    private String libelle;

    /**
     * Default constructor.
     */
    public PaysDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

}
