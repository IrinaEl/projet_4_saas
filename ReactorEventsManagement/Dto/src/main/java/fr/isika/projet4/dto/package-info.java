/**
 * Package contenant toutes les classes imitant les entités et qui pourront être
 * transportées dans toutes les couches de l'application.
 */
package fr.isika.projet4.dto;