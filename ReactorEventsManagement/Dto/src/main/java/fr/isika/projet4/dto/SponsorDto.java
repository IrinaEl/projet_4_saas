package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe définissant la DTO {@link SponsorDto}.
 * @author stagiaire
 *
 */
public class SponsorDto extends UtilisateurDto implements Serializable {

    /**
     * num serialisable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * logo du sponsor à afficher dans l'évènement.
     */
    private String logo;

    /**
     * raison sociale de la société sponsor.
     */
    private String raisonSociale;

    /**
     * default constructor.
     */
    public SponsorDto() {
        super();
    }

    /**
     * @param paramId
     * @param paramMail
     * @param paramMdp
     * @param paramNom
     * @param paramPrenom
     * @param paramLogo
     * @param paramRaisonSociale
     */
    public SponsorDto(Integer paramId, String paramMail, String paramMdp, String paramNom, String paramPrenom, String paramLogo,
            String paramRaisonSociale) {
        super(paramId, paramMail, paramMdp, paramNom, paramPrenom);
        logo = paramLogo;
        raisonSociale = paramRaisonSociale;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param paramLogo the logo to set
     */
    public void setLogo(String paramLogo) {
        logo = paramLogo;
    }

    /**
     * @return the raisonSociale
     */
    public String getRaisonSociale() {
        return raisonSociale;
    }

    /**
     * @param paramRaisonSociale the raisonSociale to set
     */
    public void setRaisonSociale(String paramRaisonSociale) {
        raisonSociale = paramRaisonSociale;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
