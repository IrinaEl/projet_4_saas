package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Class type d'activite Dto.
 */
public class TypeActiviteDto implements Serializable {

    /**
     * attribut génerer par le serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * attribut identifiant.
     */
    private Integer id;

    /**
     * attribut intitule du type d'activite.
     */
    private String intitule;

    /**
     * attribut description du type d'activite.
     */
    private String description;

    /**
     * Default Constructor.
     */
    public TypeActiviteDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        this.intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        this.description = paramDescription;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
