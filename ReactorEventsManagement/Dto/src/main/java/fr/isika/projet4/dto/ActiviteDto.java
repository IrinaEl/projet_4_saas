package fr.isika.projet4.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Classe ActiviteDto serializable.
 * @author stagiaire
 */
public class ActiviteDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * attribut id activiteDto.
     */
    private Integer id;

    /**
     * Attribut date de debut.
     */
    private Date dateDebut;

    /**
     * Attribut date de fin.
     */
    private Date dateFin;

    /**
     * Attribut intitule activite.
     */
    private String intitule;

    /**
     * Attribut description axctivite.
     */
    private String description;

    /**
     * Attribut nombe de place pour l'activite.
     */
    private Integer nombrePlace;

    /**
     * Attribut prix de l'activite.
     */
    private double prix;

    /**
     * Attribut budget de l'activite.
     */
    private double budget;

    /**
     * attribut type d'activite.
     */
    private TypeActiviteDto typeActivite;

    /**
     * attribut responsable d'activite.
     */
    private ResponsableActiviteDto responsableActivite;

    /**
     * attribut evenement.
     */
    private EvenementDto evenement;

    /**
     * Default Constructor.
     */
    public ActiviteDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the dateDebut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * @param paramDateDebut the dateDebut to set
     */
    public void setDateDebut(Date paramDateDebut) {
        dateDebut = paramDateDebut;
    }

    /**
     * @return the dateFin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * @param paramDateFin the dateFin to set
     */
    public void setDateFin(Date paramDateFin) {
        dateFin = paramDateFin;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

    /**
     * @return the nombrePlace
     */
    public Integer getNombrePlace() {
        return nombrePlace;
    }

    /**
     * @param paramNombrePlace the nombrePlace to set
     */
    public void setNombrePlace(Integer paramNombrePlace) {
        nombrePlace = paramNombrePlace;
    }

    /**
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * @param paramPrix the prix to set
     */
    public void setPrix(double paramPrix) {
        prix = paramPrix;
    }

    /**
     * @return the budget
     */
    public double getBudget() {
        return budget;
    }

    /**
     * @param paramBudget the budget to set
     */
    public void setBudget(double paramBudget) {
        budget = paramBudget;
    }

    /**
     * @return the typeActivite
     */
    public TypeActiviteDto getTypeActivite() {
        return typeActivite;
    }

    /**
     * @param paramTypeActivite the typeActivite to set
     */
    public void setTypeActivite(TypeActiviteDto paramTypeActivite) {
        typeActivite = paramTypeActivite;
    }

    /**
     * @return the responsableActivite
     */
    public ResponsableActiviteDto getResponsableActivite() {
        return responsableActivite;
    }

    /**
     * @param paramResponsableActivite the responsableActivite to set
     */
    public void setResponsableActivite(ResponsableActiviteDto paramResponsableActivite) {
        responsableActivite = paramResponsableActivite;
    }

    /**
     * @return the evenement
     */
    public EvenementDto getEvenement() {
        return evenement;
    }

    /**
     * @param paramEvenement the evenement to set
     */
    public void setEvenement(EvenementDto paramEvenement) {
        evenement = paramEvenement;
    }

}
