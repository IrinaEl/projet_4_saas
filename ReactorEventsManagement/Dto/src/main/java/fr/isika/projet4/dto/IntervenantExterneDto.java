package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Une personne ou une société qui participe à un {@link EvenementDto} ou à une
 * {@link ActiviteDto}.
 * @author stagiaire
 *
 */
public class IntervenantExterneDto extends RessourceDto implements Serializable {

    /**
     * La version de l'intervenant externe.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le genre en fonction de la profession de l'intervenant.
     */
    private TypeIntervenantDto typeIntervenant;

    /**
     * Constructeur par défaut.
     */
    public IntervenantExterneDto() {
        super();
    }

    /**
     * @return the typeIntervenant
     */
    public TypeIntervenantDto getTypeIntervenant() {
        return typeIntervenant;
    }

    /**
     * @param paramTypeIntervenant the typeIntervenant to set
     */
    public void setTypeIntervenant(TypeIntervenantDto paramTypeIntervenant) {
        typeIntervenant = paramTypeIntervenant;
    }
}
