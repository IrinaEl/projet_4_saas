package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * class type de tache dto serializable.
 */
public class TypeTacheDto implements Serializable {

    /**
     * constante serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * attribut identifiant type de tache.
     */
    private Integer id;

    /**
     * attribut intitule type de tache.
     */
    private String intitule;

    /**
     * attribut description type de tache.
     */
    private String description;

    /**
     * Default constructor.
     */
    public TypeTacheDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

}
