package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * <b>AdresseDto est la classe . C'est une Dto de la classe Adresse.</b>
 * @author stagiaire
 *
 */
public class AdresseDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id attribut de AdresseDto.
     */
    private Integer id;

    /**
     * numeroVoie attribut de AdresseDto.
     */
    private String numeroVoie;

    /**
     * nomVoie attribut de AdresseDto.
     */
    private String nomVoie;

    /**
     * typeVoie attribut de AdresseDto.
     */
    private TypeVoieDto typeVoie;

    /**
     * ville attribut de AdresseDto.
     */
    private VilleDto ville;

    /**
     * Constructeur de la classe AdresseDto.
     */
    public AdresseDto() {
        // Do nothing because c'est le constructeur par défaut.
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the numeroVoie
     */
    public String getNumeroVoie() {
        return numeroVoie;
    }

    /**
     * @param paramNumeroVoie the numeroVoie to set
     */
    public void setNumeroVoie(String paramNumeroVoie) {
        numeroVoie = paramNumeroVoie;
    }

    /**
     * @return the nomVoie
     */
    public String getNomVoie() {
        return nomVoie;
    }

    /**
     * @param paramNomVoie the nomVoie to set
     */
    public void setNomVoie(String paramNomVoie) {
        nomVoie = paramNomVoie;
    }

    /**
     * @return the typeVoie
     */
    public TypeVoieDto getTypeVoie() {
        return typeVoie;
    }

    /**
     * @param paramTypeVoie the typeVoie to set
     */
    public void setTypeVoie(TypeVoieDto paramTypeVoie) {
        typeVoie = paramTypeVoie;
    }

    /**
     * @return the ville
     */
    public VilleDto getVille() {
        return ville;
    }

    /**
     * @param paramVille the ville to set
     */
    public void setVille(VilleDto paramVille) {
        ville = paramVille;
    }

}
