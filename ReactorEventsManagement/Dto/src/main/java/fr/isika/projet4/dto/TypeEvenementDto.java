package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe définit le type d'un évenement.
 * @author stagiaire
 *
 */
public class TypeEvenementDto implements Serializable {

    /**
     * Pour la serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id du type d'evenement.
     */
    private int id;

    /**
     * libelle de type d'évenement.
     */
    private String libelle;

    /**
     * Constructor default.
     */
    public TypeEvenementDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(int paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

}
