package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe définissant la DTO {@link MembreAdministrationDto}.
 * @author stagiaire
 *
 */
public class MembreAdministrationDto extends UtilisateurDto implements Serializable {

    /**
     * num serialisable.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MembreAdministrationDto() {
        super();
    }

}
