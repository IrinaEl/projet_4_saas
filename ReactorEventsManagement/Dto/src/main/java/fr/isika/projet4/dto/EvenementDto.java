package fr.isika.projet4.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * class EvenementDto serilizable.
 * @author stagiaire
 *
 */
public class EvenementDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id de evenement.
     */
    private Integer id;

    /**
     * date debut de l'évenement.
     */
    private Date dateDebut;

    /**
     * date fin de l'évenement.
     */
    private Date dateFin;

    /**
     * intitule de l'évenement.
     */
    private String intitule;

    /**
     * description de l'evenement.
     */
    private String description;

    /**
     * le prix de l'evenement.
     */
    private Double prix;

    /**
     * budget de l'evenement.
     */
    private Double budget;

    /**
     * adresse de l'evenement.
     */
    private AdresseDto adresse;

    /**
     * attribut de type d'evenement.
     */
    private TypeEvenementDto typeEvenement;

    /**
     * Default constructor.
     */
    public EvenementDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the dateDebut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * @param paramDateDebut the dateDebut to set
     */
    public void setDateDebut(Date paramDateDebut) {
        dateDebut = paramDateDebut;
    }

    /**
     * @return the dateFin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * @param paramDateFin the dateFin to set
     */
    public void setDateFin(Date paramDateFin) {
        dateFin = paramDateFin;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

    /**
     * @return the prix
     */
    public Double getPrix() {
        return prix;
    }

    /**
     * @param paramPrix the prix to set
     */
    public void setPrix(Double paramPrix) {
        prix = paramPrix;
    }

    /**
     * @return the budget
     */
    public Double getBudget() {
        return budget;
    }

    /**
     * @param paramBudget the budget to set
     */
    public void setBudget(Double paramBudget) {
        budget = paramBudget;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the adresse
     */
    public AdresseDto getAdresse() {
        return adresse;
    }

    /**
     * @param paramAdresse the adresse to set
     */
    public void setAdresse(AdresseDto paramAdresse) {
        adresse = paramAdresse;
    }

    /**
     * @return the typeEvenement
     */
    public TypeEvenementDto getTypeEvenement() {
        return typeEvenement;
    }

    /**
     * @param paramTypeEvenement the typeEvenement to set
     */
    public void setTypeEvenement(TypeEvenementDto paramTypeEvenement) {
        typeEvenement = paramTypeEvenement;
    }

}
