package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe CodePostalDto serializable.
 * @author stagiaire
 *
 */
public class CodePostalDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

}
