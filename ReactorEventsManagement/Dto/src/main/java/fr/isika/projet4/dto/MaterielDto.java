package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Type de ressource qui peut être utilisée pour un {@link EvenementDto} ou pour
 * une {@link ActiviteDto}.
 * @author stagiaire
 *
 */
public class MaterielDto extends RessourceDto implements Serializable {

    /**
     * La version du matériel.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le nom du matériel.
     */
    private String intitule;

    /**
     * Constructeur par défaut.
     */
    public MaterielDto() {
        super();
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

}
