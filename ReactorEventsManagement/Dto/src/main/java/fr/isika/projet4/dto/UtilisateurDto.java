package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe définissant la DTO {@link UtilisateurDto}.
 * @author stagiaire
 *
 */
public class UtilisateurDto implements Serializable {

    /**
     * num serialisable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id de l'{@link UtilisateurDto}.
     */
    private Integer id;

    /**
     * mail de l'{@link UtilisateurDto}.
     */

    private String mail;

    /**
     * mdp de l'{@link UtilisateurDto}.
     */

    private String mdp;

    /**
     * nom de l'{@link UtilisateurDto}.
     */
    private String nom;

    /**
     * prenom de l'{@link UtilisateurDto}.
     */
    private String prenom;

    /**
     * type d'utilisateur.
     */
    private String type;

    /**
     * constructeur vide lié à l'Utilisateur.
     */
    public UtilisateurDto() {
    }

    /**
     * @param paramId     .
     * @param paramMail   .
     * @param paramMdp    .
     * @param paramNom    .
     * @param paramPrenom .
     */
    public UtilisateurDto(Integer paramId, String paramMail, String paramMdp, String paramNom, String paramPrenom) {
        super();
        id = paramId;
        mail = paramMail;
        mdp = paramMdp;
        nom = paramNom;
        prenom = paramPrenom;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param paramMail the mail to set
     */
    public void setMail(String paramMail) {
        mail = paramMail;
    }

    /**
     * @return the mdp
     */
    public String getMdp() {
        return mdp;
    }

    /**
     * @param paramMdp the mdp to set
     */
    public void setMdp(String paramMdp) {
        mdp = paramMdp;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param paramNom the nom to set
     */
    public void setNom(String paramNom) {
        nom = paramNom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param paramPrenom the prenom to set
     */
    public void setPrenom(String paramPrenom) {
        prenom = paramPrenom;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param paramType the type to set
     */
    public void setType(String paramType) {
        type = paramType;
    }

}
