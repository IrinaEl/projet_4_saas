package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe représentant les flux financiers qui circulent pendant la création et
 * l'organisation d'un {@link Evenement} ou d'une {@link Activite}.
 * @author stagiaire
 *
 */
public class FluxDto implements Serializable {

    /**
     * La version du flux.
     */
    private static final long serialVersionUID = 1L;

    /**
     * L'id du flux.
     */
    private Integer id;

    /**
     * Le montant du flux.
     */
    private double montant;

    /**
     * La facture dans laquelle se retrouve le flux.
     */
    private FactureDto facture;

    /**
     * L'activité concernée par le flux.
     */
    private ActiviteDto activite;

    /**
     * L'{@link EvenementDto} lié au flux.
     */
    private EvenementDto evenment;

    /**
     * Default constructor.
     */
    public FluxDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the montant
     */
    public double getMontant() {
        return montant;
    }

    /**
     * @param paramMontant the montant to set
     */
    public void setMontant(double paramMontant) {
        montant = paramMontant;
    }

    /**
     * @return the facture
     */
    public FactureDto getFacture() {
        return facture;
    }

    /**
     * @param paramFacture the facture to set
     */
    public void setFacture(FactureDto paramFacture) {
        facture = paramFacture;
    }

    /**
     * @return the activite
     */
    public ActiviteDto getActivite() {
        return activite;
    }

    /**
     * @param paramActivite the activite to set
     */
    public void setActivite(ActiviteDto paramActivite) {
        activite = paramActivite;
    }

    /**
     * @return the evenment
     */
    public EvenementDto getEvenment() {
        return evenment;
    }

    /**
     * @param paramEvenment the evenment to set
     */
    public void setEvenment(EvenementDto paramEvenment) {
        evenment = paramEvenment;
    }

}
