package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe définissant la DTO {@link ResponsableActiviteDto}.
 * @author stagiaire {
 */
public class ResponsableActiviteDto extends UtilisateurDto implements Serializable {

    /**
     * num serialisable.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ResponsableActiviteDto() {
        super();
    }

}
