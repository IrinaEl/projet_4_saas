package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe représentant les différentes ressources utilisées pour organiser un
 * événement ou une activité.
 * @author stagiaire
 *
 */
public class RessourceDto implements Serializable {

    /**
     * Version de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * L'id de la ressource.
     */
    private Integer id;

    /**
     * Le montant à payer pour ladite ressource.
     */
    private double prix;

    /**
     * Le nombre de ressources utilisées avec cette société.
     */
    private double quantite;

    /**
     * La raison sociale de la ressource (nom de société ou personne morale).
     */
    private String raisonSociale;

    /**
     * Le numéro d'identification de la société.
     */
    private String numeroRCS;

    /**
     * Le numéro de téléphone de la ressource.
     */
    private String numeroTelephone;

    /**
     * Le mail de la ressource.
     */
    private String mail;

    /**
     * type de ressource (intervenant ou matériel), peut-être besoin pour angular.
     */
    private String type;

    /**
     * Constructor.
     */
    public RessourceDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * @param paramPrix the prix to set
     */
    public void setPrix(double paramPrix) {
        prix = paramPrix;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the raisonSociale
     */
    public String getRaisonSociale() {
        return raisonSociale;
    }

    /**
     * @param paramRaisonSociale the raisonSociale to set
     */
    public void setRaisonSociale(String paramRaisonSociale) {
        raisonSociale = paramRaisonSociale;
    }

    /**
     * @return the numeroRCS
     */
    public String getNumeroRCS() {
        return numeroRCS;
    }

    /**
     * @param paramNumeroRCS the numeroRCS to set
     */
    public void setNumeroRCS(String paramNumeroRCS) {
        numeroRCS = paramNumeroRCS;
    }

    /**
     * @return the numeroTelephone
     */
    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    /**
     * @param paramNumeroTelephone the numeroTelephone to set
     */
    public void setNumeroTelephone(String paramNumeroTelephone) {
        numeroTelephone = paramNumeroTelephone;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param paramMail the mail to set
     */
    public void setMail(String paramMail) {
        mail = paramMail;
    }

    /**
     * @return the quantite
     */
    public double getQuantite() {
        return quantite;
    }

    /**
     * @param paramQuantite the quantite to set
     */
    public void setQuantite(double paramQuantite) {
        quantite = paramQuantite;
    }

    public String getType() {
        return type;
    }

    public void setType(String paramType) {
        type = paramType;
    }

}
