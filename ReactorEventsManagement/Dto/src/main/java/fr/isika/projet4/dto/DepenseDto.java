package fr.isika.projet4.dto;

/**
 * Type de {@link FluxDto} représentant le montant réglé ou à régler. Objet
 * transportable représentant la {@link Depense} de l'unité de persistence.
 * @author stagiaire
 *
 */
public class DepenseDto extends FluxDto {

    /**
     * La version de la dépense.
     */
    private static final long serialVersionUID = 1L;

}
