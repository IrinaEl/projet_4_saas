package fr.isika.projet4.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * class InscriptionDto serilizable.
 * @author stagiaire
 *
 */
public class InscriptionDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id de InscriptionDto.
     */

    private Integer id;

    /**
     * le nom de l'inscrit.
     */
    private String nom;

    /**
     * prenom de l'inscrit.
     */
    private String prenom;

    /**
     * date naissance de l'inscrit.
     */
    private Date dateNaissance;

    /**
     * mail de l'inscrit.
     */
    private String mail;

    /**
     * montant regle de l'inscription.
     */
    private double montantRegle;

    /**
     * evenement de l'inscrit.
     */
    private EvenementDto evenement;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * Default constructor.
     */
    public InscriptionDto() {
        super();
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param paramNom the nom to set
     */
    public void setNom(String paramNom) {
        nom = paramNom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param paramPrenom the prenom to set
     */
    public void setPrenom(String paramPrenom) {
        prenom = paramPrenom;
    }

    /**
     * @return the dateNaissance
     */
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * @param paramDateNaissance the dateNaissance to set
     */
    public void setDateNaissance(Date paramDateNaissance) {
        dateNaissance = paramDateNaissance;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param paramMail the mail to set
     */
    public void setMail(String paramMail) {
        mail = paramMail;
    }

    /**
     * @return the montantRegle
     */
    public double getMontantRegle() {
        return montantRegle;
    }

    /**
     * @param paramMontantRegle the montantRegle to set
     */
    public void setMontantRegle(double paramMontantRegle) {
        montantRegle = paramMontantRegle;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the evenement
     */
    public EvenementDto getEvenement() {
        return evenement;
    }

    /**
     * @param paramEvenement the evenement to set
     */
    public void setEvenement(EvenementDto paramEvenement) {
        evenement = paramEvenement;
    }

}
