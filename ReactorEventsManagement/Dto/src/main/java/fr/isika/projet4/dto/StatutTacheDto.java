package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * classe représentant le dto {@StatutTacheDto}.
 * @author stagiaire
 *
 */
public class StatutTacheDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant du statut.
     */
    private Integer id;

    /**
     * l'intitulé du statut (1 - en cours, 2 - terminée).
     */
    private String intitule;

    /**
     * Default constructor.
     */
    public StatutTacheDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

}
