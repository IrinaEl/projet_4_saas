package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe définissant la DTO {@link LeadPoleDto}.
 * @author stagiaire
 *
 */
public class LeadPoleDto extends UtilisateurDto implements Serializable {

    /**
     * num serialisable.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public LeadPoleDto() {
        super();
    }

}
