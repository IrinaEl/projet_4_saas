package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe représentant les différentes factures.
 * @author stagiaire
 *
 */
public class FactureDto implements Serializable {

    /**
     * La version de la facture.
     */
    private static final long serialVersionUID = 1L;

}
