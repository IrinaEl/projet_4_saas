package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Le genre d'intervenants qui peuvent participer à un {@link EvenementDto} ou à
 * une {@link ActiviteDto}.
 * <ul>
 * <li>Bénévole</li>
 * <li>Conférencier</li>
 * <li>Vendeur</li>
 * <li>etc.</li>
 * </ul>
 * @author stagiaire
 *
 */
public class TypeIntervenantDto implements Serializable {

    /**
     * La version du type d'intervenant.
     */
    private static final long serialVersionUID = 1L;

    /**
     * L'id de l'intervenant.
     */
    private Integer id;

    /**
     * Le genre d'intervenant.
     */
    private String intitule;

    /**
     * La description d'un genre d'intervenant, peut être utile pour un type
     * d'intervenant atypique.
     */
    private String description;

    /**
     * Constructeur par défaut.
     */
    public TypeIntervenantDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

}
