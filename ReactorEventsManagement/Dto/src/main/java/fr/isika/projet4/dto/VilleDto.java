package fr.isika.projet4.dto;

import java.io.Serializable;

/**
 * Classe VilleDto serializable.
 * @author stagiaire
 *
 */
public class VilleDto implements Serializable {

    /**
     * constante fournit dans le contexte du serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id attribut de villeDto.
     */
    private Integer id;

    /**
     * libelle attribut de villeDto.
     */
    private String libelle;

    /**
     * pays attribut de ville dto.
     */
    private PaysDto pays;

    /**
     * constructeur vide.
     */
    public VilleDto() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

    /**
     * @return the pays
     */
    public PaysDto getPays() {
        return pays;
    }

    /**
     * @param paramPays the pays to set
     */
    public void setPays(PaysDto paramPays) {
        pays = paramPays;
    }

}
