package fr.isika.projet4.bu.rest.retour.activite;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * Retours de la méthode
 * {@link fr.isika.projet4.data.api.IDaoActivite#addResponsableActivity(ResponsableActiviteDto, ActiviteDto)}.
 * @author stagiaire
 *
 */
public class RetourAddResponsableActivity {

    /**
     * Retour une activiteDto si aucun erreur n'est rencontrée.
     */
    private ActiviteDto activiteDto;

    /**
     * retourne une exception si une exception a été interceptée.
     */
    private ActiviteException activiteException;

    /**
     * @return the activiteDto
     */
    public ActiviteDto getActiviteDto() {
        return activiteDto;
    }

    /**
     * @param paramActiviteDto the activiteDto to set
     */
    public void setActiviteDto(ActiviteDto paramActiviteDto) {
        activiteDto = paramActiviteDto;
    }

    /**
     * @return the activiteException
     */
    public ActiviteException getActiviteException() {
        return activiteException;
    }

    /**
     * @param paramActiviteException the activiteException to set
     */
    public void setActiviteException(ActiviteException paramActiviteException) {
        activiteException = paramActiviteException;
    }

}
