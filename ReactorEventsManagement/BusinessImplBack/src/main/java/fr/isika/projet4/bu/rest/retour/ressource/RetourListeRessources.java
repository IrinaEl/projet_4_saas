package fr.isika.projet4.bu.rest.retour.ressource;

import java.util.List;

import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe représentant le type de retour pour une liste de ressources.
 * @author stagiaire
 *
 */
public class RetourListeRessources {

    /**
     * liste retournée en cas de succès.
     */
    private List<RessourceDto> ressources;

    /**
     * exception retournée en cas d'échec.
     */
    private RessourceException exception;

    public List<RessourceDto> getRessources() {
        return ressources;
    }

    public void setRessources(List<RessourceDto> paramRessources) {
        ressources = paramRessources;
    }

    public RessourceException getException() {
        return exception;
    }

    public void setException(RessourceException paramException) {
        exception = paramException;
    }

    public RetourListeRessources() {

    }

    public RetourListeRessources(List<RessourceDto> paramRessources, RessourceException paramException) {

        ressources = paramRessources;
        exception = paramException;
    }

}
