package fr.isika.projet4.bu.rest.retour.activite;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * classe représentant l'objet retourné par le service REST permettant de
 * récupérer un dto {@link ActiviteDto} par son identifiant.
 * @author stagiaire
 *
 */
public class RetourGetActiviteById {

    /**
     * l'activité retournée en cas de succès.
     */
    private ActiviteDto activite;

    /**
     * exception retournée en cas d'échec.
     */
    private ActiviteException exception;

    /**
     * constructeur vide.
     */
    public RetourGetActiviteById() {

    }

    /**
     * constructeur parametré.
     * @param paramActivite  - activité
     * @param paramException - exception
     */
    public RetourGetActiviteById(ActiviteDto paramActivite, ActiviteException paramException) {

        activite = paramActivite;
        exception = paramException;
    }

    /**
     * @return the activite
     */
    public ActiviteDto getActivite() {
        return activite;
    }

    /**
     * @param paramActivite the activite to set
     */
    public void setActivite(ActiviteDto paramActivite) {
        activite = paramActivite;
    }

    /**
     * @return the exception
     */
    public ActiviteException getException() {
        return exception;
    }

    /**
     * @param paramException the exception to set
     */
    public void setException(ActiviteException paramException) {
        exception = paramException;
    }

}
