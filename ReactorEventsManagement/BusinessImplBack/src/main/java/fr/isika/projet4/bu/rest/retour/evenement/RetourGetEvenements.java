package fr.isika.projet4.bu.rest.retour.evenement;

import java.io.Serializable;
import java.util.List;

import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * Classe représentant l'objet retourné par le service REST.
 * @author stagiaire
 *
 */
public class RetourGetEvenements implements Serializable {

    /**
     * La liste d'évenements retournée en cas de succès.
     */
    private List<EvenementDto> evenements;

    /**
     * L'exception retournée en cas d'échec.
     */
    private EventException exception;

    /**
     * Constructeur par défaut.
     */
    public RetourGetEvenements() {
        super();
    }

    /**
     * @param paramEvenements - la liste d'{@link EvenementDto}.
     * @param paramException - l'exception levée en cas d'erreur.
     */
    public RetourGetEvenements(List<EvenementDto> paramEvenements, EventException paramException) {
        super();
        evenements = paramEvenements;
        exception = paramException;
    }

    /**
     * @return the evenements
     */
    public List<EvenementDto> getEvenements() {
        return evenements;
    }

    /**
     * @param paramEvenements the evenements to set
     */
    public void setEvenements(List<EvenementDto> paramEvenements) {
        evenements = paramEvenements;
    }

    /**
     * @return the exception
     */
    public EventException getException() {
        return exception;
    }

    /**
     * @param paramException the exception to set
     */
    public void setException(EventException paramException) {
        exception = paramException;
    }

}
