package fr.isika.projet4.bu.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.bu.api.IBusinessResponsableActivite;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.RAException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * classe impléménentant les méthodes métier relatives au dto.
 * {@link ResponsableActiviteDto}
 * @author stagiaire
 *
 */
@Service
public class BusinessResponsableActivite implements IBusinessResponsableActivite {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * proxy de la classe exposant les services DAO.
     */
    private IDaoService proxy;

    /**
     * remplissage du proxy à chaque appel grâce à une classe utilitaire.
     */
    public BusinessResponsableActivite() {

        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    @Override
    public ResponsableActiviteDto getRaById(Integer paramIdRa) throws RAException {

        return proxy.getRaById(paramIdRa);
    }

    @Override
    public List<ResponsableActiviteDto> getAllResponsableActivite() throws RAException {
        return proxy.getAllResponsableActivite();
    }

}
