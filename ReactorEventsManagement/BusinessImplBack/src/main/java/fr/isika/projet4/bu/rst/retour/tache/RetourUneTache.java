package fr.isika.projet4.bu.rst.retour.tache;

import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * classe représentant l'objet retourné par le serivce REST permettant
 * d'affecter un bénévole à une tâche.
 * @author stagiaire
 *
 */
public class RetourUneTache {

    /**
     * liste de tâches retournée en cas de succès.
     */
    private TacheDto tache;

    /**
     * exception retournée en cas d'échec.
     */
    private TacheException exception;

    /**
     *
     */
    public RetourUneTache() {
        // Constructeur connard
    }

    /**
     * @return the tache
     */
    public TacheDto getTache() {
        return tache;
    }

    /**
     * @param paramTache the tache to set
     */
    public void setTache(TacheDto paramTache) {
        tache = paramTache;
    }

    /**
     * @return the exception
     */
    public TacheException getException() {
        return exception;
    }

    /**
     * @param paramException the exception to set
     */
    public void setException(TacheException paramException) {
        exception = paramException;
    }

}
