package fr.isika.projet4.bu.rest.retour.evenement;

import java.util.List;

import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * Retour de la Méthode
 * {@link fr.isika.projet4.data.api.IDaoResponsableActivite#getAllResponsableActivite()}.
 * @author stagiaire
 *
 */
public class RetourGetAllResponsableActivite {

    /**
     * Retour une liste {@link ResponsableActiviteDto} si aucun erreur n'est
     * rencontrée.
     */
    private List<ResponsableActiviteDto> responsableActivites;

    /**
     * Retour une Exception si une exception a été interceptée.
     */
    private RAException raException;

    /**
     * @return the responsableActivites
     */
    public List<ResponsableActiviteDto> getResponsableActivites() {
        return responsableActivites;
    }

    /**
     * @param paramResponsableActivites the responsableActivites to set
     */
    public void setResponsableActivites(List<ResponsableActiviteDto> paramResponsableActivites) {
        responsableActivites = paramResponsableActivites;
    }

    /**
     * @return the raException
     */
    public RAException getRaException() {
        return raException;
    }

    /**
     * @param paramRaException the raException to set
     */
    public void setRaException(RAException paramRaException) {
        raException = paramRaException;
    }

}
