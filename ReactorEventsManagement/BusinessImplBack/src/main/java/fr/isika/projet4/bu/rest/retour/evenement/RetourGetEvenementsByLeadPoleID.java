package fr.isika.projet4.bu.rest.retour.evenement;

import java.util.List;

import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.exceptions.LeadPoleException;

/**
 * Retours de la méthode
 * {@link fr.isika.projet4.data.api.IDaoEvenement#getEvenementsByLeadPoleID(int)}.
 * @author stagiaire
 *
 */
public class RetourGetEvenementsByLeadPoleID {

    /**
     * Retourne une liste {@link EvenementDto} si aucune erreur n'est rencontrée.
     */
    private List<EvenementDto> evenements;

    /**
     * Retourne une {@link fr.isika.projet4.exceptions.LeadPoleException} si une
     * exception a été interceptée.
     */
    private LeadPoleException leadPoleException;

    /**
     * @return the evenements
     */
    public List<EvenementDto> getEvenements() {
        return evenements;
    }

    /**
     * @param paramEvenements the evenements to set
     */
    public void setEvenements(List<EvenementDto> paramEvenements) {
        evenements = paramEvenements;
    }

    /**
     * @return the leadPoleException
     */
    public LeadPoleException getLeadPoleException() {
        return leadPoleException;
    }

    /**
     * @param paramLeadPoleException the leadPoleException to set
     */
    public void setLeadPoleException(LeadPoleException paramLeadPoleException) {
        leadPoleException = paramLeadPoleException;
    }

}
