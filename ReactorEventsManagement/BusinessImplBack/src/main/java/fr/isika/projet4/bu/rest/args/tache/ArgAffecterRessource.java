package fr.isika.projet4.bu.rest.args.tache;

/**
 * classe argument pour ma méthode affectation de ressources.
 * @author Irina El
 *
 */
public class ArgAffecterRessource {
    /**
     * id activité à laquelle on affecte un bénévole.
     */
    private Integer idActivite;
    /**
     * ressource affectée à l'activité.
     */
    private Integer idRessource;
    
   
    /**
     * constructeur vide
     */
    public ArgAffecterRessource() {
        
    }

    /**
     * @param paramIdActivite
     * @param paramIdRessource
     */
    public ArgAffecterRessource(Integer paramIdActivite, Integer paramIdRessource) {
        super();
        idActivite = paramIdActivite;
        idRessource = paramIdRessource;
    }
    
    /**
     * @return the idActivite
     */
    public Integer getIdActivite() {
        return idActivite;
    }
    
    /**
     * @param paramIdActivite the idActivite to set
     */
    public void setIdActivite(Integer paramIdActivite) {
        idActivite = paramIdActivite;
    }
    
    /**
     * @return the idRessource
     */
    public Integer getIdRessource() {
        return idRessource;
    }
    
    /**
     * @param paramIdRessource the idRessource to set
     */
    public void setIdRessource(Integer paramIdRessource) {
        idRessource = paramIdRessource;
    }
   
}
