package fr.isika.projet4.bu.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.bu.api.IBusinessRessource;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * classe impléménetnant les méthodes métier relatives au dto Ressource.
 * @author stagiaire
 *
 */
@Service
public class BusinessRessource implements IBusinessRessource {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * proxy de la classe exposant les services DAO.
     */
    private IDaoService proxy;

    /**
     * remplissage du proxy à chaque appel grâce à une classe utilitaire.
     */
    public BusinessRessource() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    /**
     * identifiant du type d'intervenant correspondant au liellé : "bénévole".
     */
    private static final Integer ID_TYPE_INTERVENANT_BENEVOLE = 10;

    @Override
    public List<IntervenantExterneDto> getAllBenevoles() throws RessourceException {

        return proxy.getAllBenevoles(ID_TYPE_INTERVENANT_BENEVOLE);
    }

    @Override
    public List<RessourceDto> getRessourceByActivite(Integer paramIdActivite) throws RessourceException {
        
        return proxy.getRessourcesByActivite(paramIdActivite);
    }

    @Override
    public List<RessourceDto> getAllRessources() throws RessourceException {
        
        return proxy.getAllRessources();
    }

}
