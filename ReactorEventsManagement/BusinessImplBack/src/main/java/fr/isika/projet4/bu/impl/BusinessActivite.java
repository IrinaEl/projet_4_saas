package fr.isika.projet4.bu.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.bu.api.IBusinessActivite;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * Claase impléménetant les méthodes métier liées au DTO {@link Activite}.
 * @author stagiaire
 *
 */
@Service
public class BusinessActivite implements IBusinessActivite {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * proxy de la classe exposant les services DAO.
     */
    private IDaoService proxy;

    /**
     * remplissage du proxy à chaque appel grâce à une classe utilitaire.
     */
    public BusinessActivite() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    @Override
    public List<ActiviteDto> getActivitesByRa(Integer paramIdRa) throws ActiviteException {

        return proxy.getActivitesByRa(paramIdRa);
    }

    @Override
    public List<ActiviteDto> getAllActiviteByEvent(Integer paramIdEvent) throws ActiviteException {
        return proxy.getAllActiviteByEvent(paramIdEvent);
    }

    @Override
    public ActiviteDto getActiviteById(Integer paramIdActivite) throws ActiviteException {
        return proxy.getActiviteById(paramIdActivite);
    }

    /**
     * @return the proxy
     */
    public IDaoService getProxy() {
        return proxy;
    }

    /**
     * @param paramProxy the proxy to set
     */
    public void setProxy(IDaoService paramProxy) {
        proxy = paramProxy;
    }

    @Override
    public ActiviteDto addResponsableActivity(ResponsableActiviteDto paramResponsableActiviteDto,
                                              ActiviteDto paramActiviteDto) throws ActiviteException {

        return proxy.addResponsableActivity(paramResponsableActiviteDto, paramActiviteDto);
    }

    @Override
    public ActiviteDto affecterRessource(Integer paramIdRessource, Integer paramIdActivite) throws ActiviteException {
        
        return proxy.affecterRessource(paramIdRessource, paramIdActivite);
    }
    
    
}
