package fr.isika.projet4.bu.rest.retour.evenement;

import java.util.List;

import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * Retours de la méthode
 * {@link fr.isika.projet4.data.api.IDaoEvenement#getByRA(int)}.
 * @author ludwig
 *
 */
public class RetourGetByRA {

    /**
     * Retourne un {@link EvenementDto} si aucune erreur n'est rencontrée.
     */
    private List<EvenementDto> events;

    /**
     * Retourne une {@link fr.isika.projet4.exceptions.RAException} si une exception
     * à été interceptée.
     */
    private RAException exception;

    /**
     * @return the exception
     */
    public RAException getException() {
        return exception;
    }

    /**
     * @param paramException the exception to set
     */
    public void setException(RAException paramException) {
        exception = paramException;
    }

    /**
     * @return the events
     */
    public List<EvenementDto> getEvents() {
        return events;
    }

    /**
     * @param paramEvents the event to set
     */
    public void setEvents(List<EvenementDto> paramEvents) {
        events = paramEvents;
    }

}
