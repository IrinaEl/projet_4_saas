package fr.isika.projet4.bu.rest.retour.activite;

import java.io.Serializable;
import java.util.List;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * objet retourné par les méthodes qui retournent une liste d'activité ou une exception.
 * @author stagiaire
 *
 */
public class RetourGetActivites implements Serializable {

    /**
     * retour de la méthode en cas de succès.
     */
    private List<ActiviteDto> activites;

    /**
     * retour de la méthode en cas d'échec.
     */
    private ActiviteException exception;

    /**
     *
     * @param paramActivites - la liste d'{link {@link ActiviteDto}.
     * @param paramException -l'exception levée en cas d'erreur.
     */
    public RetourGetActivites(List<ActiviteDto> paramActivites, ActiviteException paramException) {
        super();
        activites = paramActivites;
        exception = paramException;
    }

    /**
     * constructeur vide.
     */
    public RetourGetActivites() {
        super();
    }

    /**
     * @return the activites
     */
    public List<ActiviteDto> getActivites() {
        return activites;
    }

    /**
     * @param paramActivites the activites to set
     */
    public void setActivites(List<ActiviteDto> paramActivites) {
        activites = paramActivites;
    }

    /**
     * @return the exception
     */
    public ActiviteException getException() {
        return exception;
    }

    /**
     * @param paramException the exception to set
     */
    public void setException(ActiviteException paramException) {
        exception = paramException;
    }

}
