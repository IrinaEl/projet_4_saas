package fr.isika.projet4.bu.rest.args.tache;

import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.TacheDto;

/**
 * classe représentant l'objet passé en paramètre de la méthode
 * affecterBénévole.
 * @author stagiaire
 *
 */
public class ArgAffecterBenevole {

    
    /**
     * tache à laquelle on affecte un bénévole.
     */
    private TacheDto tache;

    /**
     * bénévole affecté à la tâche.
     */

    private RessourceDto benevole;
    

  
    /**
     * @return the tache
     */
    public TacheDto getTache() {
        return tache;
    }

    /**
     * @param paramTache the tache to set
     */
    public void setTache(TacheDto paramTache) {
        tache = paramTache;
    }

    /**
     * @return the benevole
     */
    public RessourceDto getBenevole() {
        return benevole;
    }

    /**
     * @param paramBenevole the benevole to set
     */
    public void setBenevole(RessourceDto paramBenevole) {
        benevole = paramBenevole;
    }

}
