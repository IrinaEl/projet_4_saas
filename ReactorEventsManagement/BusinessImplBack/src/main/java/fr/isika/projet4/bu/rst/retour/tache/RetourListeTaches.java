package fr.isika.projet4.bu.rst.retour.tache;

import java.util.List;

import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * classe représentant l'objet retourné par le serivce REST permettant de
 * récupérer une liste de tâches à partir de l'identifiant de l'activité.
 * @author stagiaire
 *
 */
public class RetourListeTaches {

    /**
     * liste de tâches retournée en cas de succès.
     */
    private List<TacheDto> taches;

    /**
     * exception retournée en cas d'échec.
     */
    private TacheException exception;

    /**
     * constructeur vide.
     */
    public RetourListeTaches() {

    }

    /**
     * constructeur paramétré.
     * @param paramTaches    - liste de tâches
     * @param paramException - l'exception
     */
    public RetourListeTaches(List<TacheDto> paramTaches, TacheException paramException) {
        super();
        taches = paramTaches;
        exception = paramException;
    }

    /**
     * @return the taches
     */
    public List<TacheDto> getTaches() {
        return taches;
    }

    /**
     * @param paramTaches the taches to set
     */
    public void setTaches(List<TacheDto> paramTaches) {
        taches = paramTaches;
    }

    /**
     * @return the exception
     */
    public TacheException getException() {
        return exception;
    }

    /**
     * @param paramException the exception to set
     */
    public void setException(TacheException paramException) {
        exception = paramException;
    }

}
