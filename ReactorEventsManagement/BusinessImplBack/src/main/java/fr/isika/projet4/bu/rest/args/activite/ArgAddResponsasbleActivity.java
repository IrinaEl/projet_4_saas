package fr.isika.projet4.bu.rest.args.activite;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;

/**
 * classe représentant l'objet passé en paramètre de la méthode
 * addResponsableActivity.
 * @author stagiaire
 *
 */
public class ArgAddResponsasbleActivity {

    /**
     * responsable activite à affecter à activité.
     */
    private ResponsableActiviteDto raDto;

    /**
     * responsable activite à affecter à cette activité.
     */
    private ActiviteDto activiteDto;

    /**
     * @return the raDto
     */
    public ResponsableActiviteDto getRaDto() {
        return raDto;
    }

    /**
     * @param paramRaDto the raDto to set
     */
    public void setRaDto(ResponsableActiviteDto paramRaDto) {
        raDto = paramRaDto;
    }

    /**
     * @return the activiteDto
     */
    public ActiviteDto getActiviteDto() {
        return activiteDto;
    }

    /**
     * @param paramActiviteDto the activiteDto to set
     */
    public void setActiviteDto(ActiviteDto paramActiviteDto) {
        activiteDto = paramActiviteDto;
    }

}
