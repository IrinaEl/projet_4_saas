package fr.isika.projet4.bu.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.projet4.bu.api.IBusinessActivite;
import fr.isika.projet4.bu.api.IBusinessEvenement;
import fr.isika.projet4.bu.api.IBusinessResponsableActivite;
import fr.isika.projet4.bu.api.IBusinessRessource;
import fr.isika.projet4.bu.api.IBusinessTache;
import fr.isika.projet4.bu.impl.BusinessActivite;
import fr.isika.projet4.bu.impl.BusinessEvenement;
import fr.isika.projet4.bu.impl.BusinessResponsableActivite;
import fr.isika.projet4.bu.impl.BusinessTache;
import fr.isika.projet4.bu.rest.args.activite.ArgAddResponsasbleActivity;
import fr.isika.projet4.bu.rest.args.tache.ArgAffecterBenevole;
import fr.isika.projet4.bu.rest.args.tache.ArgAffecterRessource;
import fr.isika.projet4.bu.rest.retour.activite.RetourAddResponsableActivity;
import fr.isika.projet4.bu.rest.retour.activite.RetourGetActiviteById;
import fr.isika.projet4.bu.rest.retour.activite.RetourGetActivites;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetAllResponsableActivite;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetByRA;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetEvenements;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetEvenementsByLeadPoleID;
import fr.isika.projet4.bu.rest.retour.ressource.RetourListeBenevoles;
import fr.isika.projet4.bu.rest.retour.ressource.RetourListeRessources;
import fr.isika.projet4.bu.rst.retour.tache.RetourListeTaches;
import fr.isika.projet4.bu.rst.retour.tache.RetourUneTache;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.LeadPoleDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.ActiviteException;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.LeadPoleException;
import fr.isika.projet4.exceptions.RAException;
import fr.isika.projet4.exceptions.RessourceException;
import fr.isika.projet4.exceptions.TacheException;

/**
 * classe exposant les services REST back-office de la couche métier.
 * @author stagiaire
 *
 */
@RestController()
@RequestMapping("/back-rest")
@CrossOrigin("*")
public class BackRestServices {

//---------------------------------------GET ACTIVITES BY RA------------------------------------//
    /**
     * classe contenant les méthodes métier back-office relatives au dto
     * {@link ActiviteDto}.
     */
    @Autowired
    private IBusinessActivite businessActivite;

    /**
     * classe contenant les méthodes métier back-office relatives au dto
     * {@link ResponsableActiviteDto}.
     */
    @Autowired
    private IBusinessResponsableActivite busRa;

    /**
     * classe contenant les méthodes métier back-office relatives au dto
     * {@link EvenementDto}.
     */
    @Autowired
    private IBusinessEvenement businessEvent;

    /**
     * Instanciation de {@link fr.isika.projet4.bu.impl.BusinessEvenement} contenant
     * les méthodes métier du {@link EvenementDto}.
     */
    @Autowired
    private IBusinessEvenement buEvenement;

    /**
     * injection de la classe contenant les méthodes métier relatives au dto
     * {@link TacheDto}.
     */
    @Autowired
    private IBusinessTache buTache;

    /**
     * injection de la classe contenant les méthodes métier relatives au dto
     * {@link RessourceDto}.
     */
    @Autowired
    private IBusinessRessource buRessource;

    /**
     * exposition de la méthode getActivitesByRa.
     * @param idRa - identifiant du RA à retourner
     * @return {@link RetourGetActivites}
     */
    @GetMapping(path = "/getActivitesByRa/{idRa}", produces = "application/json")
    public RetourGetActivites getActivitesById(@PathVariable("idRa") Integer idRa) {
        RetourGetActivites retour = new RetourGetActivites();
        try {
            retour.setActivites(businessActivite.getActivitesByRa(idRa));
        } catch (ActiviteException e) {

            retour.setException(e);
        }
        return retour;
    }

    /**
     * exposition de la méthode getAllActivitesByEvent.
     * @param idEvent - identifiant de l'évenment .
     * @return {@link RetourGetActivites}
     */
    @GetMapping(path = "/getActivitesByEvent/{idEvent}", produces = "application/json")
    public RetourGetActivites getActivitesByEvent(@PathVariable("idEvent") Integer idEvent) {
        RetourGetActivites retour = new RetourGetActivites();
        try {
            retour.setActivites(businessActivite.getAllActiviteByEvent(idEvent));
        } catch (ActiviteException e) {
            retour.setException(e);
        }
        return retour;
    }

    /**
     * exposition de la méthode getAllEvent.
     * @return {@link RetourGetEvenements}
     */
    @GetMapping(path = "/getAllEvent", produces = "application/json")
    public RetourGetEvenements getAllEvent() {
        RetourGetEvenements retour = new RetourGetEvenements();
        try {
            retour.setEvenements(businessEvent.getAll());
        } catch (EventException e) {
            retour.setException(e);
        }
        return retour;
    }

    /**
     * exposition de la méthode {@link IBusinessEvenement#getAllDoneEvents()}.
     * @return {@link RetourGetEvenements}
     */
    @GetMapping(path = "/get-all-events", produces = "application/json")
    public RetourGetEvenements getAllDoneEvents() {
        RetourGetEvenements retour = new RetourGetEvenements();
        try {
            retour.setEvenements(businessEvent.getAllDoneEvents());
        } catch (EventException e) {
            retour.setException(e);
        }
        return retour;
    }

    /**
     * exposition permettant de récupére un dto {@link ActiviteDto} par son
     * identifiant.
     * @param idActivite - identifiant de l'activité recherchée.
     * @return un dto {@link ActiviteDto}
     */
    @GetMapping(path = "/getActiviteById/{idActivite}", produces = "application/json")
    public RetourGetActiviteById getActiviteById(@PathVariable("idActivite") Integer idActivite) {
        RetourGetActiviteById retour = new RetourGetActiviteById();
        try {
            retour.setActivite(businessActivite.getActiviteById(idActivite));
        } catch (ActiviteException e) {
            retour.setException(e);
        }
        return retour;
    }

    /**
     * Exposition en service rest de la méthode
     * {@link BusinessEvenement#getByRA(int)}.
     * @param idRA - L'id du {@link ResponsableActiviteDto}
     * @return {@link RetourGetByRA}
     */
    @GetMapping(path = "/getByRA/{id_responsable_activite}", produces = "application/json")
    public RetourGetByRA getByRA(@PathVariable("id_responsable_activite") int idRA) {
        RetourGetByRA retour = new RetourGetByRA();
        try {
            retour.setEvents(buEvenement.getByRA(idRA));
        } catch (RAException e) {
            retour.setException(e);
        }
        return retour;
    }

    /**
     * Exposition en service rest de la méthode
     * {@link BusinessEvenement#getEvenementsByLeadPoleID(int)}.
     * @param idLeadPole idLeadPole - l'id du {@link LeadPoleDto}.
     * @return {@link RetourGetEvenementsByLeadPoleID}
     */
    @GetMapping(path = "/getEvenementsByLeadPoleID/{leadPoleID}", produces = "application/json")
    public RetourGetEvenementsByLeadPoleID getEvenementsByLeadPoleID(@PathVariable("leadPoleID") int idLeadPole) {
        RetourGetEvenementsByLeadPoleID retour = new RetourGetEvenementsByLeadPoleID();
        try {
            retour.setEvenements(buEvenement.getEvenementsByLeadPoleID(idLeadPole));
        } catch (LeadPoleException e) {
            retour.setLeadPoleException(e);
        }
        return retour;
    }

    /**
     * Exposition du service REST de la méthode
     * {@link BusinessTache#getTachesByActivite(Integer)}.
     * @param idActivite - identifiant de l'activité dont les tâches sont
     *                   recherchées.
     * @return une liste de tâches de l'activité
     */

    @GetMapping(path = "/getTachesByActivite/{idActivite}", produces = "application/json")
    public RetourListeTaches getTachesByActivite(@PathVariable("idActivite") Integer idActivite) {
        RetourListeTaches retour = new RetourListeTaches();
        try {
            retour.setTaches(buTache.getTachesByActivite(idActivite));
        } catch (TacheException e) {
            retour.setException(e);
        }

        return retour;
    }

    /**
     * EXposition du service REST de la méthode d'ffectation d'un bénévole à une
     * tâche.
     * @param argAffecterBenevole - paramètre complexe contenant la tâche et le
     *                            bénévole à y affecter
     * @return {@link TacheDto} modifiée
     */
    @PostMapping(path = "/affecterBenevole", produces = "application/json", consumes = "application/json")
    public RetourUneTache affecterBenevole(@RequestBody ArgAffecterBenevole argAffecterBenevole) {
        RetourUneTache retour = new RetourUneTache();
        try {
            retour.setTache(buTache.affecterBenevole(argAffecterBenevole.getTache(), argAffecterBenevole.getBenevole()));
        } catch (TacheException e) {
            retour.setException(e);
        }
        return retour;
    }

    /**
     * Exposition du service REST de récupération de tous les bénévoles.
     * @return liste de tous les bénévoles
     */
    @GetMapping(path = "/getAllBenevoles", produces = "application/json")
    public RetourListeBenevoles getAllBenevoles() {
        RetourListeBenevoles retour = new RetourListeBenevoles();
        try {
            retour.setBenevoles(buRessource.getAllBenevoles());
        } catch (RessourceException e) {
            retour.setException(e);
        }
        return retour;
    }

    /**
     * Exposition du service REST de la méthode
     * {@link BusinessResponsableActivite#getAllResponsableActivite()}.
     * @return {@link RetourGetAllResponsableActivite}.
     */
    @GetMapping(path = "/getAllResponsableActivite", produces = "application/json")
    public RetourGetAllResponsableActivite getAllResponsableActivite() {
        RetourGetAllResponsableActivite retour = new RetourGetAllResponsableActivite();
        try {
            retour.setResponsableActivites(busRa.getAllResponsableActivite());
        } catch (RAException e) {
            retour.setRaException(e);
        }
        return retour;
    }

    /**
     * Exposition du service REST de la méthode
     * {@link BusinessActivite#addResponsableActivity(ResponsableActiviteDto, ActiviteDto)}.
     * @param argAddResponsableActivite - pour affecter ce responsableActivite à
     *                                  cette activiteDto.
     * @return {@link RetourAddResponsableActivity}.
     */
    @PostMapping(path = "/addResponsableActivity", produces = "application/json", consumes = "application/json")
    public RetourAddResponsableActivity addResponsableActivity(@RequestBody ArgAddResponsasbleActivity argAddResponsableActivite) {
        RetourAddResponsableActivity retour = new RetourAddResponsableActivity();
        try {
            retour.setActiviteDto(businessActivite.addResponsableActivity(argAddResponsableActivite.getRaDto(),
                    argAddResponsableActivite.getActiviteDto()));
        } catch (ActiviteException e) {
            retour.setActiviteException(e);
        }
        return retour;
    }

    @PostMapping(path = "/affecterRessource", produces = "application/json", consumes = "application/json")
    public RetourGetActiviteById affecterRessource(@RequestBody ArgAffecterRessource arg) {
        RetourGetActiviteById retour = new RetourGetActiviteById();
        try {
            retour.setActivite(businessActivite.affecterRessource(arg.getIdRessource(), arg.getIdActivite()));
        } catch (ActiviteException e) {
            retour.setException(e);
        }
        return retour;
    }

    @GetMapping(path = "/getRessourcesByActivite/{idAct}", produces = "application/json")
    public RetourListeRessources getRessourcesByActivite(@PathVariable(name = "idAct") Integer idActivite) {
        RetourListeRessources retour = new RetourListeRessources();
        try {
            retour.setRessources(buRessource.getRessourceByActivite(idActivite));
        } catch (RessourceException e) {
            retour.setException(e);
        }
        return retour;
    }

    @GetMapping(path = "/getAllRessources", produces = "application/json")
    public RetourListeRessources getAllRessources() throws RessourceException {
        RetourListeRessources retour = new RetourListeRessources();
        try {
            retour.setRessources(buRessource.getAllRessources());
        } catch (RessourceException e) {
            retour.setException(e);
        }
        return retour;
    }
}
