package fr.isika.projet4.bu.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.bu.api.IBusinessTache;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * classe impléménetant les émthodes métier liées au DTO {@link TacheDto}.
 * @author stagiaire
 *
 */
@Service
public class BusinessTache implements IBusinessTache {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * proxy de la classe exposant les services DAO.
     */
    private IDaoService proxy;

    /**
     * remplissage du proxy à chaque appel grâce à une classe utilitaire.
     */
    public BusinessTache() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.debug(e);
        }
    }

    @Override
    public List<TacheDto> getTachesByActivite(Integer paramIdActivite) throws TacheException {

        return proxy.getTachesByActivite(paramIdActivite);
    }

    @Override
    public TacheDto affecterBenevole(TacheDto paramIdTache, RessourceDto paramBenevole) throws TacheException {

        return proxy.affecterBenevole(paramIdTache, paramBenevole);
    }

    /**
     * @return the proxy
     */
    public IDaoService getProxy() {
        return proxy;
    }

    /**
     * @param paramProxy the proxy to set
     */
    public void setProxy(IDaoService paramProxy) {
        proxy = paramProxy;
    }

}
