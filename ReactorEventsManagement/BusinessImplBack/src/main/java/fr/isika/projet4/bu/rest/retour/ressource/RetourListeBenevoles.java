package fr.isika.projet4.bu.rest.retour.ressource;

import java.util.List;

import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe représentant le type de retour pour une liste de bénévoles.
 * @author stagiaire
 *
 */
public class RetourListeBenevoles {

    /**
     * liste retournée en cas de succès.
     */
    private List<IntervenantExterneDto> benevoles;

    /**
     * exception retournée en cas d'échec.
     */
    private RessourceException exception;

    /**
     *
     */
    public RetourListeBenevoles() {

    }

    /**
     * @param paramBenevoles liste de bénévoles
     * @param paramException exception
     */
    public RetourListeBenevoles(List<IntervenantExterneDto> paramBenevoles, RessourceException paramException) {
        super();
        benevoles = paramBenevoles;
        exception = paramException;
    }

    /**
     * @return the benevoles
     */
    public List<IntervenantExterneDto> getBenevoles() {
        return benevoles;
    }

    /**
     * @param paramBenevoles the benevoles to set
     */
    public void setBenevoles(List<IntervenantExterneDto> paramBenevoles) {
        benevoles = paramBenevoles;
    }

    /**
     * @return the exception
     */
    public RessourceException getException() {
        return exception;
    }

    /**
     * @param paramException the exception to set
     */
    public void setException(RessourceException paramException) {
        exception = paramException;
    }

}
