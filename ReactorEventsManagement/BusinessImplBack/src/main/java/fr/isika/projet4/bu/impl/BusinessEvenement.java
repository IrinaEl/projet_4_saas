package fr.isika.projet4.bu.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.bu.api.IBusinessEvenement;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.LeadPoleException;
import fr.isika.projet4.exceptions.RAException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * Classe d'implémentation des services métier pour un {@link EvenementDto}.
 * @author ludwig
 *
 */
@Service
public class BusinessEvenement implements IBusinessEvenement {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * Instanciation de notre proxy.
     */
    private IDaoService proxy;

    /**
     * Constructeur permettant de créer notre proxy.
     */
    public BusinessEvenement() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    @Override
    public List<EvenementDto> getByRA(int paramIdRA) throws RAException {
        return proxy.getByRA(paramIdRA);
    }

    @Override
    public List<EvenementDto> getAll() throws EventException {
        return proxy.getAllEvent();
    }

    @Override
    public List<EvenementDto> getEvenementsByLeadPoleID(int paramLeadPoleID) throws LeadPoleException {
        return proxy.getEvenementsByLeadPoleID(paramLeadPoleID);
    }

    @Override
    public List<EvenementDto> getAllDoneEvents() throws EventException {
        return proxy.getAllDoneEvents();
    }

}
