package fr.isika.projet4.bu.impl.test.evenement;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessEvenement;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetByRA;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * Test des sevices métier pour la méthode
 * {@link fr.isika.projet4.bu.impl.BusinessEvenement#getByRA(int))}.
 * @author ludwig
 *
 */
public class TestBusinessGetByRA {

    private static Logger log = Logger.getLogger(TestBusinessGetByRA.class);

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessEvenement bu;

    /**
     * Mock des dépendences du service.
     */
    private static IDaoService mock;

    /**
     * L'id de test dans le cas nominal.
     */
    private static final int ID_RA_NOMINAL = 1;

    /**
     * L'évenement attendu.
     */
    private static EvenementDto eventNominal = new EvenementDto();

    /**
     * Initialisation du {@link ClassPathXmlApplicationContext}.
     */
    private static ClassPathXmlApplicationContext cpx;

    /**
     * L'id du {@link fr.isika.projet4.dto.ResponsableActiviteDto} pour le
     * {@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static final Integer ID_EVENT_NOMINAL = 1;

    /**
     * Le prix de l'{@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static final double PRIX_EVENT_NOMINAL = 5000;

    /**
     * Le budget de l'{@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static final double BUDGET_EVENT_NOMINAL = 200;

    /**
     * L'id de l'adresse l'{@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 3;

    /**
     * L'id du type de voie lié à l'adresse de
     * l'{@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 3;

    /**
     * L'id de la ville liée à l'adresse de
     * l'{@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static final Integer ID_VILLE_EVENT = 2;

    /**
     * L'id du pays de l'{@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static final Integer ID_PAYS_EVENT = 2;

    /**
     * La liste attendue contenant {@link TestBusinessGetByRA#expectedEvent1}.
     */
    private static List<EvenementDto> expectedListe = new ArrayList<>();

    /**
     * Le wrap {@link RetourGetByRA} contenant la {@link RAException}.
     */
    private static RetourGetByRA expectedWrapException = new RetourGetByRA();

    /**
     * L'évenement attendu contenu dans {@link TestBusinessGetByRA#expectedListe}.
     */
    private static EvenementDto expectedEvent1 = new EvenementDto();

    /**
     * La liste attendue contenant plusieurs {@link EvenementDto}. La valeur 2 sera
     * prise pour test.
     */
    private static List<EvenementDto> expectedListeSize = new ArrayList<>();

    /**
     * Le wrap {@link RetourGetByRA} contenant la liste
     * {@link TestBusinessGetByRA#expectedListeSize}.
     */
    private static RetourGetByRA expectedWrapListSize = new RetourGetByRA();

    /**
     * L'id du {@link fr.isika.projet4.dto.ResponsableActiviteDto} pour le
     * {@link TestBusinessGetByRA#testTailleListe()}.
     */
    private static final int ID_RA_TEST_TAILLE_LISTE = 2;

    /**
     * L'id du {@link fr.isika.projet4.dto.ResponsableActiviteDto} pour le
     * {@link TestBusinessGetByRA#testEchecListeVide()}.
     */
    private static final int ID_RA_ECHEC_LISTE_VIDE = 3;

    /**
     * Initialisation du {@link TestBusinessGetByRA#mock} et des évenements de test.
     * @throws Exception -
     */
    @BeforeClass
    public static void beforeAllTests() throws Exception {
        initEventNominal();
        initExpectedEvent();

        expectedListe.add(expectedEvent1);

        expectedListeSize.add(new EvenementDto());
        expectedListeSize.add(new EvenementDto());

        expectedWrapListSize.setEvents(expectedListeSize);
        expectedWrapException.setException(new RAException("Vous n'êtes affecté à aucun évenement", null));

        mock = EasyMock.createMock(IDaoService.class);
        EasyMock.expect(mock.getByRA(ID_RA_NOMINAL)).andReturn(expectedListe);
        EasyMock.expect(mock.getByRA(ID_RA_TEST_TAILLE_LISTE)).andReturn(expectedWrapListSize.getEvents());
        EasyMock.expect(mock.getByRA(ID_RA_ECHEC_LISTE_VIDE)).andThrow(expectedWrapException.getException());
        EasyMock.replay(mock);

        cpx = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        bu = cpx.getBean(IBusinessEvenement.class);

        Class<? extends IBusinessEvenement> clazz = bu.getClass();
        Field chp = clazz.getDeclaredField("proxy");
        chp.setAccessible(true);
        chp.set(bu, mock);
        chp.setAccessible(false);

    }

    /**
     * Test de la liste d'{@link EvenementDto}.
     * @throws RAException - Si la liste est vide.
     */
    @Test
    public void testNominal() throws RAException {
        RetourGetByRA wrap = new RetourGetByRA();
        wrap.setEvents(mock.getByRA(ID_RA_NOMINAL));
        for (EvenementDto e : wrap.getEvents()) {
            AssertionEntities.assertEntity(eventNominal, e);
        }
    }

    /**
     * Test de la taille de la liste d'{@link EvenementDto}.
     * @throws RAException - Si la liste est vide.
     */
    @Test
    public void testTailleListe() throws RAException {
        Assert.assertEquals(expectedWrapListSize.getEvents().size(), mock.getByRA(ID_RA_TEST_TAILLE_LISTE).size());
    }

    /**
     * Test d'échec dans le cas d'une liste vide.
     */
    @Test
    public void testEchecListeVide() {
        try {
            mock.getByRA(ID_RA_ECHEC_LISTE_VIDE);
            Assert.fail("Echec du test");
        } catch (RAException e) {
            Assert.assertEquals(expectedWrapException.getException().getMessage(), e.getMessage());
        }
    }

    /**
     * Après tests.
     */
    @AfterClass
    public static void afterAllTests() {
        EasyMock.verify(mock);
        cpx.close();
    }

    /**
     * Méthode pour set les valeurs de {@link TestBusinessGetByRA#eventNominal}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initEventNominal() throws ParseException {
        eventNominal.setId(ID_EVENT_NOMINAL);
        eventNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy").parse("18-11-2019"));
        eventNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy").parse("19-11-2019"));
        eventNominal.setIntitule("Intitule event test 3");
        eventNominal.setDescription("Description event test 3");
        eventNominal.setPrix(PRIX_EVENT_NOMINAL);
        eventNominal.setBudget(BUDGET_EVENT_NOMINAL);
        eventNominal.setAdresse(new AdresseDto());
        eventNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        eventNominal.getAdresse().setNomVoie("Gringo mas");
        eventNominal.getAdresse().setNumeroVoie("36");
        eventNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        eventNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        eventNominal.getAdresse().getTypeVoie().setLibelle("impasse");
        eventNominal.getAdresse().setVille(new VilleDto());
        eventNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        eventNominal.getAdresse().getVille().setLibelle("Madrid");
        eventNominal.getAdresse().getVille().setPays(new PaysDto());
        eventNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        eventNominal.getAdresse().getVille().getPays().setLibelle("Espagne");
        eventNominal.setTypeEvenement(new TypeEvenementDto());
        eventNominal.getTypeEvenement().setId(1);
        eventNominal.getTypeEvenement().setLibelle("Majeur");
    }

    /**
     * Méthode pour set les valeurs de {@link TestBusinessGetByRA#expectedEvent1}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initExpectedEvent() throws ParseException {
        expectedEvent1.setId(ID_EVENT_NOMINAL);
        expectedEvent1.setDateDebut(new SimpleDateFormat("dd-MM-yyyy").parse("18-11-2019"));
        expectedEvent1.setDateFin(new SimpleDateFormat("dd-MM-yyyy").parse("19-11-2019"));
        expectedEvent1.setIntitule("Intitule event test 3");
        expectedEvent1.setDescription("Description event test 3");
        expectedEvent1.setPrix(PRIX_EVENT_NOMINAL);
        expectedEvent1.setBudget(BUDGET_EVENT_NOMINAL);
        expectedEvent1.setAdresse(new AdresseDto());
        expectedEvent1.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        expectedEvent1.getAdresse().setNomVoie("Gringo mas");
        expectedEvent1.getAdresse().setNumeroVoie("36");
        expectedEvent1.getAdresse().setTypeVoie(new TypeVoieDto());
        expectedEvent1.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        expectedEvent1.getAdresse().getTypeVoie().setLibelle("impasse");
        expectedEvent1.getAdresse().setVille(new VilleDto());
        expectedEvent1.getAdresse().getVille().setId(ID_VILLE_EVENT);
        expectedEvent1.getAdresse().getVille().setLibelle("Madrid");
        expectedEvent1.getAdresse().getVille().setPays(new PaysDto());
        expectedEvent1.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        expectedEvent1.getAdresse().getVille().getPays().setLibelle("Espagne");
        expectedEvent1.setTypeEvenement(new TypeEvenementDto());
        expectedEvent1.getTypeEvenement().setId(1);
        expectedEvent1.getTypeEvenement().setLibelle("Majeur");
    }
}
