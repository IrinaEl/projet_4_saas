/**
 * package contenant les classes de test des méthodes métiers relatives au DTO
 * {@link Activite}.
 */
package fr.isika.projet4.business.back.impl.test.activite;
