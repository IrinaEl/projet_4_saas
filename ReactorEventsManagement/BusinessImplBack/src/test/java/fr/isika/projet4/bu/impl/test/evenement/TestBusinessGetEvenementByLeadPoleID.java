package fr.isika.projet4.bu.impl.test.evenement;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessEvenement;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetEvenementsByLeadPoleID;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.LeadPoleException;

/**
 * Test des services métier pour la méthode
 * {@link fr.isika.projet4.bu.impl.BusinessEvenement#getEvenementsByLeadPoleID(int)}.
 * @author Stagiaire
 *
 */
public class TestBusinessGetEvenementByLeadPoleID {

    /**
     * log pour debuger.
     */
    private static Logger log = Logger.getLogger(TestBusinessGetEvenementByLeadPoleID.class);

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessEvenement bu;

    /**
     * Mock des dépendences du service.
     */
    private static IDaoService mock;

    /**
     * identificat du LP dans le test nominal.
     */
    private static final Integer ID_LP_NOMINAL = 15;

    /**
     * L'évenement attendu.
     */
    private static EvenementDto evenementNominal = new EvenementDto();

    /**
     * Initialisation du {@link ClassPathXmlApplicationContext}.
     */
    private static ClassPathXmlApplicationContext cpx;

    /**
     * L'id de l'{@link TestEventByLeadPoleId#eventNominal}.
     */
    private static final Integer ID_EVENEMENT_EXPECTED = 1;

    /**
     * Le budget de l'{@link TestEventByLeadPoleId#eventNominal}.
     */
    private static final double BUDGET_EVENEMENT_EXPECTED = 50000;

    /**
     * Le prix de l'{@link TestEventByLeadPoleId#eventNominal}.
     */
    private static final double PRIX_EVENEMENT_EXPECTED = 30;

    /**
     * L'id de l'adresse l'{@link TestEventByLeadPoleId#eventNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 8;

    /**
     * L'id du type de voie lié à l'adresse de
     * l'{@link TestEventByLeadPoleId#eventNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 2;

    /**
     * L'id de la ville liée à l'adresse de
     * l'{@link TestEventByLeadPoleId#eventNominal}.
     */
    private static final Integer ID_VILLE_EVENT = 6;

    /**
     * L'id du pays de l'{@link TestEventByLeadPoleId#eventNominal}.
     */
    private static final Integer ID_PAYS_EVENT = 9;

    /**
     * La liste attendue contenant {@link TestEventByLeadPoleId#expectedEvent1}.
     */
    private static List<EvenementDto> expectedListe = new ArrayList<>();

    /**
     * Le wrap {@link RetourGetEvenementsByLeadPoleID} contenant la
     * {@link LeadPoleException}.
     */
    private static RetourGetEvenementsByLeadPoleID expectedWrapException = new RetourGetEvenementsByLeadPoleID();

    /**
     * L'évenement attendu contenu dans
     * {@link TestBusinessGetEvenementByLeadPoleID#expectedListe}.
     */
    private static EvenementDto expectedEvent1 = new EvenementDto();

    /**
     * La liste attendue contenant plusieurs {@link EvenementDto}. La valeur 2 sera
     * prise pour test.
     */
    private static List<EvenementDto> expectedListeSize = new ArrayList<>();

    /**
     * Le wrap {@link RetourGetEvenementsByLeadPoleID} contenant la liste
     * {@link TestBusinessGetEvenementByLeadPoleID#expectedListeSize}.
     */
    private static RetourGetEvenementsByLeadPoleID expectedWrapListSize = new RetourGetEvenementsByLeadPoleID();

    /**
     * La taille de la liste d'évenement attendue lié à
     * {@link TestEvenementByRA#ID_LP_NOMINAL}.
     */
    private static final int TAILLE_LISTE = 2;

    /**
     * id du leadpole.
     */
    private static final int ID_LP_TEST_LISTE = 16;

    /**
     * L'id du lp dans le cas où il n'est affecté à aucun évenement.
     */
    private static final Integer ID_LP_ECHEC_LISTE_VIDE = 17;

    /**
     * Id type evenement dans Evenement.
     */
    private static final Integer ID_TYPE_EVENEMENT = 1;

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    /**
     * Initialisation du {@link TestBusinessGetEvenementByLeadPoleID#mock} et des
     * évenement de test.
     * @throws Exception e.
     */
    @BeforeClass
    public static void beforeAllTests() throws Exception {
        initEventNominal();
        initExpectedEvent();
        expectedListe.add(expectedEvent1);

        expectedListeSize.add(new EvenementDto());
        expectedListeSize.add(new EvenementDto());

        expectedWrapListSize.setEvenements(expectedListeSize);
        expectedWrapException.setLeadPoleException(new LeadPoleException("Aucune evenement n'a été affectée à ce LeadPole", null));

        mock = EasyMock.createMock(IDaoService.class);
        EasyMock.expect(mock.getEvenementsByLeadPoleID(ID_LP_NOMINAL)).andReturn(expectedListe);
        EasyMock.expect(mock.getEvenementsByLeadPoleID(ID_LP_TEST_LISTE)).andReturn(expectedWrapListSize.getEvenements());
        EasyMock.expect(mock.getEvenementsByLeadPoleID(ID_LP_ECHEC_LISTE_VIDE)).andThrow(expectedWrapException.getLeadPoleException());
        EasyMock.replay(mock);

        cpx = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        bu = cpx.getBean(IBusinessEvenement.class);

        Class<? extends IBusinessEvenement> clazz = bu.getClass();
        Field chp = clazz.getDeclaredField("proxy");
        chp.setAccessible(true);
        chp.set(bu, mock);
        chp.setAccessible(false);
    }

    /**
     * Test de la liste d'{@link EvenementDto}.
     * @throws LeadPoleException - Si la liste est vide.
     */
    @Test
    public void testNominal() throws LeadPoleException {
        RetourGetEvenementsByLeadPoleID wrap = new RetourGetEvenementsByLeadPoleID();
        wrap.setEvenements(mock.getEvenementsByLeadPoleID(ID_LP_NOMINAL));
        for (EvenementDto e : wrap.getEvenements()) {
            AssertionEntities.assertEntity(evenementNominal, e);
        }
    }

    /**
     * Test de la taille de la liste d'{@link EvenementDto}.
     * @throws LeadPoleException - Si la liste est vide.
     */
    @Test
    public void testTailleListe() throws LeadPoleException {
        Assert.assertEquals(expectedWrapListSize.getEvenements().size(), mock.getEvenementsByLeadPoleID(ID_LP_TEST_LISTE).size());
    }

    /**
     * Test d'échec dans le cas d'une liste vide.
     */
    @Test
    public void testEchecListeVide() {
        try {
            mock.getEvenementsByLeadPoleID(ID_LP_ECHEC_LISTE_VIDE);
            Assert.fail("Echec du test");

        } catch (LeadPoleException e) {
            Assert.assertEquals(expectedWrapException.getLeadPoleException().getMessage(), e.getMessage());
        }
    }

    /**
     * Après tests.
     */
    @AfterClass
    public static void afterAllTests() {
        EasyMock.verify(mock);
        cpx.close();
    }

    /**
     * Méthode pour set les valeurs de
     * {@link TestBusinessGetEvenementByLeadPoleID#evenementNominal}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initEventNominal() throws ParseException {
        evenementNominal.setId(ID_EVENEMENT_EXPECTED);
        evenementNominal.setDateDebut(SDF.parse("16-01-2020 20:00:00"));
        evenementNominal.setDateFin(SDF.parse("16-01-2020 23:30:00"));
        evenementNominal.setIntitule("Concert promotionnel Isika");
        evenementNominal.setDescription("Concert promotionnel d'artistes montants de la region de Malakoff");
        evenementNominal.setPrix(PRIX_EVENEMENT_EXPECTED);
        evenementNominal.setBudget(BUDGET_EVENEMENT_EXPECTED);

        evenementNominal.setAdresse(new AdresseDto());
        evenementNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        evenementNominal.getAdresse().setNomVoie("Thunder Bluff");
        evenementNominal.getAdresse().setNumeroVoie("13");

        evenementNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        evenementNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        evenementNominal.getAdresse().getTypeVoie().setLibelle("chemin");

        evenementNominal.getAdresse().setVille(new VilleDto());
        evenementNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        evenementNominal.getAdresse().getVille().setLibelle("Tana");
        evenementNominal.getAdresse().getVille().setPays(new PaysDto());
        evenementNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        evenementNominal.getAdresse().getVille().getPays().setLibelle("Madagascar");

        evenementNominal.setTypeEvenement(new TypeEvenementDto());
        evenementNominal.getTypeEvenement().setId(ID_TYPE_EVENEMENT);
        evenementNominal.getTypeEvenement().setLibelle("Majeur");
    }

    /**
     * Méthode pour set les valeurs de
     * {@link TestBusinessGetEvenementByLeadPoleID#expectedEvent1}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initExpectedEvent() throws ParseException {
        expectedEvent1.setId(ID_EVENEMENT_EXPECTED);
        expectedEvent1.setDateDebut(SDF.parse("16-01-2020 20:00:00"));
        expectedEvent1.setDateFin(SDF.parse("16-01-2020 23:30:00"));
        expectedEvent1.setIntitule("Concert promotionnel Isika");
        expectedEvent1.setDescription("Concert promotionnel d'artistes montants de la region de Malakoff");
        expectedEvent1.setPrix(PRIX_EVENEMENT_EXPECTED);
        expectedEvent1.setBudget(BUDGET_EVENEMENT_EXPECTED);

        expectedEvent1.setAdresse(new AdresseDto());
        expectedEvent1.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        expectedEvent1.getAdresse().setNomVoie("Thunder Bluff");
        expectedEvent1.getAdresse().setNumeroVoie("13");

        expectedEvent1.getAdresse().setTypeVoie(new TypeVoieDto());
        expectedEvent1.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        expectedEvent1.getAdresse().getTypeVoie().setLibelle("chemin");

        expectedEvent1.getAdresse().setVille(new VilleDto());
        expectedEvent1.getAdresse().getVille().setId(ID_VILLE_EVENT);
        expectedEvent1.getAdresse().getVille().setLibelle("Tana");
        expectedEvent1.getAdresse().getVille().setPays(new PaysDto());
        expectedEvent1.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        expectedEvent1.getAdresse().getVille().getPays().setLibelle("Madagascar");

        expectedEvent1.setTypeEvenement(new TypeEvenementDto());
        expectedEvent1.getTypeEvenement().setId(ID_TYPE_EVENEMENT);
        expectedEvent1.getTypeEvenement().setLibelle("Majeur");
    }
}
