package fr.isika.projet4.busines.back.impl.test.ressource;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessRessource;
import fr.isika.projet4.bu.impl.BusinessRessource;
import fr.isika.projet4.bu.rest.retour.ressource.RetourListeRessources;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe de test pour la méthode qui pemret de récupérer toutes les
 * ressources.
 * @author stagiaire
 *
 */
public class TestGetAllRessources {

    /**
     * logger.
     */
    private static Logger log = Logger.getLogger(TestGetAllRessources.class);

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessRessource business = new BusinessRessource();

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * Mock pour l'exception.
     */
    private static IDaoService mockExc = EasyMock.createMock(IDaoService.class);

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * nb ressources.
     *
     */
    private static final int NB_RESSOURCES = 1;

    private static final List<RessourceDto> EXPECTED_LIST = new ArrayList();

    /**
     * ben1.
     */
    private static final IntervenantExterneDto BEN_1 = new IntervenantExterneDto();

    /**
     * id ben1.
     */
    private static final int ID_BEN1 = 9;

    /**
     * Le wrap contenant l'exception du cas d'�chec et l'entit� du cas nominal.
     */
    private static RetourListeRessources expectedWrap = new RetourListeRessources();

    private static RessourceException exc = new RessourceException();

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     * @throws RessourceException i
     */
    @BeforeClass
    public static void beforeAllTests() throws RessourceException {

        BEN_1.setId(ID_BEN1);
        BEN_1.setMail("mail@yahoo.fr");
        BEN_1.setNumeroTelephone("0565323134");
        BEN_1.setRaisonSociale("Justine Duchamps");

        EXPECTED_LIST.add(BEN_1);
        exc = new RessourceException("Aucune �venement", null);
        expectedWrap.setRessources(EXPECTED_LIST);
        expectedWrap.setException(exc);
        EasyMock.expect(mock.getAllRessources()).andReturn(expectedWrap.getRessources());
        EasyMock.expect(mockExc.getAllRessources()).andThrow(expectedWrap.getException());
        EasyMock.replay(mock);
        bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessRessource.class);
        log.fatal(business);

        try {
            Class<? extends IBusinessRessource> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {

            e.printStackTrace();

        }
    }

    /**
     * test du nombre de bénévoeles dans la liste.
     * @throws RessourceException n
     */
    @Test
    public void testNbBenevoles() throws RessourceException {
        Assert.assertEquals(NB_RESSOURCES, mock.getAllRessources().size());
    }

    /**
     * Test exception.
     */
    @Test
    public void testEchec() {
        try {
            mockExc.getAllRessources();
        } catch (RessourceException e) {
            Assert.assertEquals(expectedWrap.getException().getMessage(), e.getMessage());
        }
    }

    /**
     * après toutes les méthodes.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        bf.close();

    }
}
