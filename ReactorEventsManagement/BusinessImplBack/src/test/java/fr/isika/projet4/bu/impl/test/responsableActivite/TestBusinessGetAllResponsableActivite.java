package fr.isika.projet4.bu.impl.test.responsableActivite;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessResponsableActivite;
import fr.isika.projet4.bu.impl.BusinessResponsableActivite;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetAllResponsableActivite;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * Test des services métier pour la méthode
 * {@link BusinessResponsableActivite#getAllResponsableActivite()}.
 * @author Qin
 *
 */
public class TestBusinessGetAllResponsableActivite {

    /**
     * log pour debuger.
     */
    private static Logger log = Logger.getLogger(TestBusinessGetAllResponsableActivite.class);

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessResponsableActivite bu;

    /**
     * ra.
     */
    private static ResponsableActiviteDto raNominal = new ResponsableActiviteDto();

    /**
     * ra2.
     */
    private static ResponsableActiviteDto ra2 = new ResponsableActiviteDto();

    /**
     * ra3.
     */
    private static ResponsableActiviteDto ra3 = new ResponsableActiviteDto();

    /**
     * La taille de la liste d'évenement attendue lié à {@link #ID_RA_NOMINAL}.
     */
    private static final int TAILLE_LISTE = 3;

    /**
     * Mock des dépendences du service.
     */
    private static IDaoService mock;

    /**
     * Mock des dépendences du service pour exception.
     */
    private static IDaoService mockExc;

    /**
     * Initialisation du {@link ClassPathXmlApplicationContext}.
     */
    private static ClassPathXmlApplicationContext cpx;

    /**
     * Le wrap {@link RetourGetAllResponsableActivite} contenant la
     * {@link RAException}.
     */
    private static RetourGetAllResponsableActivite expectedWrapException = new RetourGetAllResponsableActivite();

    /**
     * liste attendu.
     */
    private static List<ResponsableActiviteDto> expectedListe = new ArrayList<>();

    /**
     * Le wrap {@link RetourGetAllResponsableActivite} contenant la
     * {@link RAException}.
     */
    private static RetourGetAllResponsableActivite expectedWrapListSize = new RetourGetAllResponsableActivite();

    /**
     * Initialisation du mock et des responsable activite de test.
     * @throws Exception e.
     */
    @BeforeClass
    public static void beforeAllTests() throws Exception {
        expectedListe.add(raNominal);
        expectedListe.add(ra2);
        expectedListe.add(ra3);
        mock = EasyMock.createMock(IDaoService.class);
        mockExc = EasyMock.createMock(IDaoService.class);

        expectedWrapListSize.setResponsableActivites(expectedListe);
        expectedWrapException.setRaException(new RAException("Aucun responsable d'activit�", null));
        EasyMock.expect(mock.getAllResponsableActivite()).andReturn(expectedWrapListSize.getResponsableActivites());
        EasyMock.expect(mockExc.getAllResponsableActivite()).andThrow(expectedWrapException.getRaException());
        EasyMock.replay(mockExc);
        EasyMock.replay(mock);

        cpx = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        bu = cpx.getBean(IBusinessResponsableActivite.class);
        Class<? extends IBusinessResponsableActivite> clazz = bu.getClass();
        Field chp = clazz.getDeclaredField("proxy");
        chp.setAccessible(true);
        chp.set(bu, mock);
        chp.set(bu, mockExc);
        chp.setAccessible(false);
    }

    /**
     * methode de test.
     * @throws RAException - erreur.
     */
    @Test
    public void testTailleListe() throws RAException {
        Assert.assertEquals(TAILLE_LISTE, mock.getAllResponsableActivite().size());
    }

    /**
     * Methode de test exception.
     */
    @Test
    public void testEchec() {
        try {
            bu.getAllResponsableActivite();
        } catch (RAException e) {
            Assert.assertEquals(expectedWrapException.getRaException().getMessage(), e.getMessage());
        }
    }

    /**
     * Après tests.
     */
    @AfterClass
    public static void afterAllTests() {
        EasyMock.verify(mock);
        EasyMock.verify(mockExc);
        cpx.close();
    }
}