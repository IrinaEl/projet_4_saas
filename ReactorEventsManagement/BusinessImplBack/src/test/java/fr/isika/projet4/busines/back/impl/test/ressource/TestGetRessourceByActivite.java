package fr.isika.projet4.busines.back.impl.test.ressource;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessActivite;
import fr.isika.projet4.bu.api.IBusinessRessource;
import fr.isika.projet4.bu.impl.BusinessRessource;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;

public class TestGetRessourceByActivite {
    /**
     * logger.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessRessource business = new BusinessRessource();
    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;
    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * id activite;
     */
    private static final Integer ID_ACTIVITE_NOMINALE = 2;
   
    /**
     *  .
     */
    private static final RessourceDto EXPECTED_RESSOURCE = new RessourceDto();
    /**
     *  .
     */
    private static final Integer ID_EXPECTED_RESSOURCE = 2;
    /**
     *  .
     */
    private static final Integer ID_EXPECTED_PRIX = 350;
    /**
     *  .
     */
    private static final List<RessourceDto> EXPECTED_LIST = new ArrayList<>();
    /**
     * .
     */
    private static final Integer ID_EXPECTED_QUANTITE = 0;
    /**
     * avant tous les tests.
     * @throws RessourceException 
     */
    @BeforeClass
    public static void beforeAll() throws RessourceException {
        EXPECTED_RESSOURCE.setId(ID_EXPECTED_RESSOURCE);
        EXPECTED_RESSOURCE.setMail("prestataire@gmail.com");
        EXPECTED_RESSOURCE.setNumeroRCS("CRC563");
        EXPECTED_RESSOURCE.setNumeroTelephone("0177896352");
        EXPECTED_RESSOURCE.setPrix(ID_EXPECTED_PRIX);
        EXPECTED_RESSOURCE.setQuantite(ID_EXPECTED_QUANTITE);
        EXPECTED_RESSOURCE.setRaisonSociale("Prestataire & C0");
        EXPECTED_LIST.add(EXPECTED_RESSOURCE);
        
        EasyMock.expect(mock.getRessourcesByActivite(ID_ACTIVITE_NOMINALE)).andReturn(EXPECTED_LIST);
        EasyMock.replay(mock);
        bf = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessRessource.class);
        
        try {
            Class<? extends IBusinessRessource> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
  
    @Test
    public void testNominal() throws RessourceException {
        
        List<RessourceDto> liste = business.getRessourceByActivite(ID_ACTIVITE_NOMINALE);
        log.fatal(liste);
        
        for (RessourceDto r : liste) {
            AssertionEntities.assertEntity(EXPECTED_RESSOURCE, r);
        }
    }
        /**
         * après toutes les méthodes.
         */
        @AfterClass
        public static void finDesTests() {
            EasyMock.verify(mock);
            bf.close();

        }
            

}
