package fr.isika.projet4.business.back.impl.test.activite;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessActivite;
import fr.isika.projet4.bu.impl.BusinessActivite;
import fr.isika.projet4.bu.rest.args.activite.ArgAddResponsasbleActivity;
import fr.isika.projet4.bu.rest.retour.activite.RetourAddResponsableActivity;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.TypeActiviteDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * test métier de la méthode ajouter un responsable activite.
 * @author Stagiaire
 *
 */
public class TestBusinessAddResponsableActivite {

    /**
     * logger.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext cpx;

    /**
     * classe contenant les services à tester.
     */
    private static IBusinessActivite business = new BusinessActivite();

    /**
     * activite attendu.
     */
    private static ActiviteDto activiteNominal = new ActiviteDto();

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    /**
     * L'id de l'activite.
     */
    private static final Integer ID_ACTIVITE = 1;

    /**
     * nombre de place pour Activite.
     */
    private static final Integer NOMBRE_PLACE_ACTIVITE = 20;

    /**
     * le prix pour Activite.
     */
    private static final Integer PRIX_ACTIVITE = 0;

    /**
     * nombre de place pour Activite.
     */
    private static final Integer BUDGET_ACTIVITE = 200;

    /**
     * id de l'Evenement.
     */
    private static final Integer ID_EVENEMENT = 1;

    /**
     * Le prix de l'Evenement.
     */
    private static final double PRIX_EVENEMENT = 30;

    /**
     * Le budget de l'Evenement.
     */
    private static final double BUDGET_EVENEMENT = 50000;

    /**
     * L'id de l'adresse.
     */
    private static final Integer ID_ADRESSE_EVENT = 8;

    /**
     * L'id du type de voie lié à l'adresse.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE = 2;

    /**
     * L'id de la ville liée à l'adresse.
     */
    private static final Integer ID_VILLE = 6;

    /**
     * L'id du pays.
     */
    private static final Integer ID_PAYS = 9;

    /**
     * l'exception attendue en cas de retour d'une liste d'activité vide.
     */
    private static ActiviteException expectedException = new ActiviteException();

    /**
     * L'id du Type activite.
     */
    private static final Integer ID_TYPE_ACTIVITE = 3;

    /**
     * Id de Ra qu'il a choisi pour ajouter à Activite.
     */
    private static final Integer ID_RA_TEST = 12;

    /**
     * Id type evenement dans Evenement.
     */
    private static final Integer ID_TYPE_EVENEMENT = 1;

    /**
     * responsableDto choisi.
     */
    private static ResponsableActiviteDto raDto = new ResponsableActiviteDto();

    /**
     * activiteDto actuel.
     */
    private static ActiviteDto aDto = new ActiviteDto();

    /**
     * Le wrap pour l'exception.
     */
    private static RetourAddResponsableActivity expectedWrapException = new RetourAddResponsableActivity();

    /**
     * Le wrap pour le cas nominal.
     */
    private static RetourAddResponsableActivity expectedWrapActivite = new RetourAddResponsableActivity();

    /**
     * L'activit� pour l'exception.
     */
    private static ActiviteDto activitedExc = new ActiviteDto();

    /**
     * Le reponsable d'activit� pour l'exception.
     */

    private static ResponsableActiviteDto raExc = new ResponsableActiviteDto();

    private static ArgAddResponsasbleActivity arg = new ArgAddResponsasbleActivity();

    /**
     * Définition des valeurs de RA testée + Chargement du contexte spring.
     * @throws ParseException - en cas d'erreur de parsing par
     *                        {@link SimpleDateFormat}
     * @throws Exception      e.
     */
    @BeforeClass
    public static void beforeAllTests() throws Exception {
        aDto.setId(ID_ACTIVITE);
        aDto.setDateDebut(SDF.parse("25-11-2019 00:00:00"));
        aDto.setDateFin(SDF.parse("26-11-2019 00:00:00"));
        aDto.setDescription("activite de test");
        aDto.setIntitule("Prestation du groupe HQM");
        aDto.setNombrePlace(NOMBRE_PLACE_ACTIVITE);
        aDto.setPrix(PRIX_ACTIVITE);
        aDto.setBudget(BUDGET_ACTIVITE);

        aDto.setEvenement(new EvenementDto());
        aDto.getEvenement().setId(ID_EVENEMENT);
        aDto.getEvenement().setDateDebut(SDF.parse("16-01-2020 20:00:00"));
        aDto.getEvenement().setDateFin(SDF.parse("16-01-2020 23:30:00"));
        aDto.getEvenement().setIntitule("Concert promotionnel Isika");
        aDto.getEvenement().setDescription("Concert promotionnel d'artistes montants de la region de Malakoff");
        aDto.getEvenement().setPrix(PRIX_EVENEMENT);
        aDto.getEvenement().setBudget(BUDGET_EVENEMENT);
        aDto.getEvenement().setAdresse(new AdresseDto());
        aDto.getEvenement().getAdresse().setId(ID_ADRESSE_EVENT);
        aDto.getEvenement().getAdresse().setNomVoie("Thunder Bluff");
        aDto.getEvenement().getAdresse().setNumeroVoie("13");

        aDto.getEvenement().getAdresse().setTypeVoie(new TypeVoieDto());
        aDto.getEvenement().getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE);
        aDto.getEvenement().getAdresse().getTypeVoie().setLibelle("chemin");
        aDto.getEvenement().getAdresse().setVille(new VilleDto());
        aDto.getEvenement().getAdresse().getVille().setId(ID_VILLE);
        aDto.getEvenement().getAdresse().getVille().setLibelle("Tana");
        aDto.getEvenement().getAdresse().getVille().setPays(new PaysDto());
        aDto.getEvenement().getAdresse().getVille().getPays().setId(ID_PAYS);
        aDto.getEvenement().getAdresse().getVille().getPays().setLibelle("Madagascar");

        aDto.getEvenement().setTypeEvenement(new TypeEvenementDto());
        aDto.getEvenement().getTypeEvenement().setId(ID_TYPE_EVENEMENT);
        aDto.getEvenement().getTypeEvenement().setLibelle("Majeur");

        aDto.setTypeActivite(new TypeActiviteDto());
        aDto.getTypeActivite().setId(ID_TYPE_ACTIVITE);
        aDto.getTypeActivite().setDescription("une activite interieur");
        aDto.getTypeActivite().setIntitule("activite interieur");

        raDto.setId(ID_RA_TEST);
        raDto.setNom("Gnome");
        raDto.setPrenom("Michel");
        raDto.setMail("michel-gnome@wow.fr");

        activiteNominal = aDto;
        activiteNominal.setResponsableActivite(raDto);

        arg.setActiviteDto(activiteNominal);
        arg.setRaDto(activiteNominal.getResponsableActivite());

        expectedWrapActivite.setActiviteDto(activiteNominal);
        expectedWrapException.setActiviteException(new ActiviteException("Le serveur de données est hors service", null));
        EasyMock.expect(mock.addResponsableActivity(arg.getRaDto(), arg.getActiviteDto())).andReturn(expectedWrapActivite.getActiviteDto());
        EasyMock.expect(mock.addResponsableActivity(raExc, activitedExc)).andThrow(expectedWrapException.getActiviteException());
        EasyMock.replay(mock);
        cpx = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        business = cpx.getBean(IBusinessActivite.class);
        Class<? extends IBusinessActivite> clazz = business.getClass();
        Field chp = clazz.getDeclaredField("proxy");
        chp.setAccessible(true);
        chp.set(business, mock);
        chp.setAccessible(false);
    }

    /**
     * Test de ActiviteDto s'il est bien ajouté.
     * @throws ActiviteException e.
     */
    @Test
    public void testNominal() throws ActiviteException {

        ActiviteDto a = mock.addResponsableActivity(raDto, aDto);
        AssertionEntities.assertEntity(activiteNominal, a);
    }

    /**
     * Test du renvoi d'exception.
     */
    @Test
    public void testEchec() {
        try {
            mock.addResponsableActivity(raExc, activitedExc);
        } catch (ActiviteException e) {
            Assert.assertEquals(expectedWrapException.getActiviteException().getMessage(), e.getMessage());
        }
    }

    /**
     * after.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        cpx.close();

    }
}
