package fr.isika.projet4.business.back.impl.test.activite;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessActivite;
import fr.isika.projet4.bu.impl.BusinessActivite;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * classe de tests pour la méthode métier
 * {@link IBusinessActivite#getActivitesByRa(Integer)} récupérant une liste
 * des activités d'un {@link ResponsableActiviteDto}.
 * @author stagiaire
 *
 */
public class TestGetActivitesByRa {

    /**
     * classe contenant les services à tester.
     */
    private static IBusinessActivite business = new BusinessActivite();

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    // ---------------------------------------------------------------------------------------------//
    // DONNEES DE TEST
    // -----------------------------------------------------------------------------------------------//
    /**
     * la liste d'ativités attendues dans le cas nominal.
     */
    public static final List<ActiviteDto> EXPECTED_LIST = new ArrayList<>();

    /**
     * identifiant du RA dont les activités sont recherchées.
     */
    private static final Integer ID_RA_NOMINAL = 13;

    /**
     * identifiant du RA n'ayant aucune activité.
     */
    private static final Integer ID_RA_SANS_ACTIVITE = 14;

    /**
     * l'activité attendue en retour dans la liste testée dans le test nominal.
     */
    public static final ActiviteDto EXPECTED_ACTIVITE = new ActiviteDto();

    /**
     * identifiant de l'activité attendue dans le cas nominal.
     */
    private static final Integer ID_EXPECTED_ACTIVITE = 2;

    /**
     * le budget de l'activité attendue en retour du test nominal.
     */
    private static final double BUDGET_EXPECTED_ACTIVITE = 2000;

    /**
     * le prix de l'activité attendue en retour du test nominal.
     */
    private static final double PRIX_EXPECTED_ACTIVITE = 20;

    /**
     * le nombre de places de l'activité attendue en retour du test nominal.
     */
    private static final Integer NB_PLACES_ACTIVITE = 50;

    /**
     * nombre d'activités affectées à un RA testé.
     */
    private static final int NB_ACTIVITE_DU_RA = 1;

    /**
     * l'exception attendue en cas de retour d'une liste d'activité vide.
     */
    private static final ActiviteException EXPECTED_EXCEPTION = new ActiviteException(
            "Aucune activité n'a été affectée à ce responsable d'activité", null);

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    // -----------------------------------------------------------------------------------------------------------------//
    // METHODES DE TEST
    // --------------------------------------------------------------------------------------------------------------//
    /**
     * démarrage du spring et injection du mock.
     * @throws ActiviteException - en cas de liste vide (devrait pas arriver là) ou
     *                           échec de connexion au serveur de données
     * @throws ParseException    - en cas d'erreur de parsing par
     *                           {@link SimpleDateFormat}
     */
    @BeforeClass
    public static void beforeAllTests() throws ActiviteException, ParseException {
        EXPECTED_ACTIVITE.setId(ID_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setBudget(BUDGET_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setDateDebut(SDF.parse("06-11-2019"));
        EXPECTED_ACTIVITE.setDateFin(SDF.parse("29-11-2019"));
        EXPECTED_ACTIVITE.setIntitule("Activité test d'Irina");
        EXPECTED_ACTIVITE.setDescription("une activité super chouette");
        EXPECTED_ACTIVITE.setPrix(PRIX_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setNombrePlace(NB_PLACES_ACTIVITE);
        EXPECTED_LIST.add(EXPECTED_ACTIVITE);

        EasyMock.expect(mock.getActivitesByRa(ID_RA_NOMINAL)).andReturn(EXPECTED_LIST);
        EasyMock.expect(mock.getActivitesByRa(ID_RA_NOMINAL)).andReturn(EXPECTED_LIST);
        EasyMock.expect(mock.getActivitesByRa(ID_RA_SANS_ACTIVITE)).andThrow(EXPECTED_EXCEPTION);

        EasyMock.replay(mock);
        bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessActivite.class);

        try {
            Class<? extends IBusinessActivite> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    /**
     * test de la méthode de récupérations des activités du RA.
     * @throws ActiviteException - au cas où aucune activité n'est affectée au RA
     */
    @Test
    public void testNominal() throws ActiviteException {
        List<ActiviteDto> activites = business.getActivitesByRa(ID_RA_NOMINAL);
        for (ActiviteDto a : activites) {
            AssertionEntities.assertEntity(EXPECTED_ACTIVITE, a);
        }

    }

    /**
     * test du nombre d'activité affectées à un RA donné.
     * @throws ActiviteException - au cas où aucune activité n'est affectée au RA
     *
     */
    @Test
    public void testNbActivites() throws ActiviteException {
        Assert.assertEquals(NB_ACTIVITE_DU_RA, business.getActivitesByRa(ID_RA_NOMINAL).size());

    }

    /**
     * test du cas de retour d'une liste vide, pour le responsable auquel aucune
     * activité n'a été affectée.
     * @throws PersistenceException - Si l'unité de persistence est hors-service.
     */
    @Test
    public void testListesansActivites() {
        try {
            business.getActivitesByRa(ID_RA_SANS_ACTIVITE);
            Assert.fail("Test KO - Ce responsable a des activités");
        } catch (ActiviteException e) {

            Assert.assertEquals(EXPECTED_EXCEPTION.getMessage(), e.getMessage());
        }

    }

    /**
     * après toutes les méthodes.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        bf.close();

    }

}
