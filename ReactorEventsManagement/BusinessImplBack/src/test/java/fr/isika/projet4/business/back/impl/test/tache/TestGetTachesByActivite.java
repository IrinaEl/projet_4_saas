package fr.isika.projet4.business.back.impl.test.tache;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessTache;
import fr.isika.projet4.bu.impl.BusinessTache;
import fr.isika.projet4.bu.rst.retour.tache.RetourListeTaches;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.StatutTacheDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * récupérer une liste de tâches attachées à une activité.
 * @author stagiaire
 *
 */
public class TestGetTachesByActivite {

    /**
     * logger.
     */
    private static final Logger LOG = Logger.getLogger(TestGetTachesByActivite.class);

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final int ID_ACTIVITE_NOMINALE = 4;

    /**
     * la tâche attendue en retour dans la liste testée dans le test nominal.
     */
    private static final TacheDto EXPECTED_TACHE = new TacheDto();

    /**
     * statut de la tache attendue nominale.
     */
    private static final StatutTacheDto EXPECTED_STATUT_NOMINAL = new StatutTacheDto();

    /**
     * id du statut attendu.
     */
    private static final Integer EXPECTED_STATUT_ID = 1;

    /**
     * nb tache activite.
     */
    private static final int NB_TACHE_PAR_ACTIVITE = 1;

    /**
     * id activite sans tâche.
     */
    private static final int ID_ACTIVITE_SANS_TACHES = 6;

    /**
     * exception si pas de tâches affectées.
     */
    private static TacheException expectedException = new TacheException();

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer ID_TACHE_NOMINALE = 1;

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer DEADLINE_NOMINAL = 120;

    /**
     * la liste d'ativités attendues dans le cas nominal.
     */
    public static final List<TacheDto> EXPECTED_LIST = new ArrayList<>();

    /**
     * classe contenant les services à tester.
     */
    private static IBusinessTache business = new BusinessTache();

    /**
     * le wrap contenant la liste d'activit�s du cas nominal.
     */
    private static RetourListeTaches expectedWrapList = new RetourListeTaches();

    /**
     * le wrap contenant l'exception renvoy�e dans
     * {@link TestGetTachesByActivite#testActiviteSansTache()}.
     */
    private static RetourListeTaches expectedWrapException = new RetourListeTaches();

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * démarrage spring + création mock + set données test.
     * @throws TacheException n
     * @throws ParseException n
     */
    @BeforeClass
    public static void beforeAllTests() throws TacheException, ParseException {
        EXPECTED_STATUT_NOMINAL.setId(EXPECTED_STATUT_ID);
        EXPECTED_STATUT_NOMINAL.setIntitule("EN COURS");
        EXPECTED_TACHE.setId(ID_TACHE_NOMINALE);
        EXPECTED_TACHE.setDeadline(DEADLINE_NOMINAL);
        EXPECTED_TACHE.setStatut(EXPECTED_STATUT_NOMINAL);
        EXPECTED_LIST.add(EXPECTED_TACHE);
        expectedException = new TacheException("Aucune tâche n'a été créée pour cette activité", null);

        expectedWrapList.setTaches(EXPECTED_LIST);
        expectedWrapException.setException(expectedException);

        EasyMock.expect(mock.getTachesByActivite(ID_ACTIVITE_NOMINALE)).andReturn(expectedWrapList.getTaches());
        EasyMock.expect(mock.getTachesByActivite(ID_ACTIVITE_SANS_TACHES)).andThrow(expectedWrapException.getException());
        EasyMock.replay(mock);
        bf = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessTache.class);
        try {
            Class<? extends IBusinessTache> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {

            e.printStackTrace();
        }

    }

    /**
     * test du cas nominal.
     * @throws TacheException si echec
     */
    @Test
    public void testNominal() throws TacheException {
        List<TacheDto> taches = business.getTachesByActivite(ID_ACTIVITE_NOMINALE);
        for (TacheDto t : taches) {
            AssertionEntities.assertEntity(EXPECTED_TACHE, t);
        }

    }

    /**
     * test activité sans taches.
     * @throws PersistenceException si pas accès au serveur
     */
    @Test
    public void testActiviteSansTache() {
        try {
            business.getTachesByActivite(ID_ACTIVITE_SANS_TACHES);
            Assert.fail("Test KO - l'activité a des tâches");
        } catch (TacheException e) {
            Assert.assertEquals(expectedWrapException.getException().getMessage(), e.getMessage());
        }
    }

    /**
     * après toutes les méthodes.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        bf.close();

    }

}
