package fr.isika.projet4.busines.back.impl.test.ressource;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessRessource;
import fr.isika.projet4.bu.impl.BusinessRessource;
import fr.isika.projet4.bu.rest.retour.ressource.RetourListeBenevoles;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe de test pour la méthode qui pemret de récupérer tous les
 * intervenants externe de type id 10.
 * @author stagiaire
 *
 */
public class TestGetAllBenevoles {

    /**
     * id ben1.
     */
    private static final int ID_BEN1 = 9;

    /**
     * id ben2.
     */
    private static final int ID_BEN2 = 10;

    /**
     * id ben3.
     */
    private static final int ID_BEN3 = 11;

    /**
     * id ben4.
     */
    private static final int ID_BEN4 = 12;

    /**
     * ben1.
     */
    private static final IntervenantExterneDto BEN_1 = new IntervenantExterneDto();

    /**
     * ben2.
     */
    private static final IntervenantExterneDto BEN_2 = new IntervenantExterneDto();

    /**
     * ben3.
     */
    private static final IntervenantExterneDto BEN_3 = new IntervenantExterneDto();

    /**
     * ben4.
     */
    private static final IntervenantExterneDto BEN_4 = new IntervenantExterneDto();

    /**
     * index premier benevole.
     */
    private static final int INDEX_0 = 0;

    /**
     * index 2eme benevole.
     */
    private static final int INDEX_1 = 1;

    /**
     * index 3eme benevole.
     */
    private static final int INDEX_2 = 2;

    /**
     * index 4eme benevole.
     */
    private static final int INDEX_3 = 3;

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessRessource business = new BusinessRessource();

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * id du type intervenant externe.
     */
    private static final int ID_TYPE_INTERVENANT_EXTERNE = 10;

    /**
     * nb de bénévoles dans la base.
     */
    private static final int NB_BENEVOLES = 4;

    /**
     * liste de bénévoles retournés.
     */
    private static final List<IntervenantExterneDto> EXPECTED_LISTE = new ArrayList<>();

    /**
     * Le wrap contenant l'exception du cas d'�chec et l'entit� du cas nominal.
     */
    private static RetourListeBenevoles expectedWrap = new RetourListeBenevoles();

    /**
     * L'id � utiliser dans le cas d'�chec.
     */
    private static final int ID_EXCEPTION = 45;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     * @throws RessourceException i
     */
    @BeforeClass
    public static void beforeAllTests() throws RessourceException {

        setListeRetour();

        expectedWrap.setBenevoles(EXPECTED_LISTE);
        expectedWrap.setException(new RessourceException("Aucun b�n�vole", null));
        EasyMock.expect(mock.getAllBenevoles(ID_TYPE_INTERVENANT_EXTERNE)).andReturn(expectedWrap.getBenevoles());
        EasyMock.expect(mock.getAllBenevoles(ID_TYPE_INTERVENANT_EXTERNE)).andReturn(EXPECTED_LISTE);
        EasyMock.expect(mock.getAllBenevoles(ID_EXCEPTION)).andThrow(expectedWrap.getException());
        EasyMock.replay(mock);
        bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessRessource.class);

        try {
            Class<? extends IBusinessRessource> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {

            e.printStackTrace();

        }

    }

    /**
     * test du nombre de bénévoeles dans la liste.
     * @throws RessourceException n
     */
    @Test
    public void testNbBenevoles() throws RessourceException {
        Assert.assertEquals(NB_BENEVOLES, business.getAllBenevoles().size());

    }

    /**
     * test nominal.
     * @throws RessourceException n
     */
    @Test
    public void testNominal() throws RessourceException {
        List<IntervenantExterneDto> retour = business.getAllBenevoles();
        AssertionEntities.assertEntity(BEN_1, retour.get(INDEX_0));
        AssertionEntities.assertEntity(BEN_2, retour.get(INDEX_1));
        AssertionEntities.assertEntity(BEN_3, retour.get(INDEX_2));
        AssertionEntities.assertEntity(BEN_4, retour.get(INDEX_3));

    }

    /**
     * Test exception.
     */
    @Test
    public void testEchec() {
        try {
            mock.getAllBenevoles(ID_EXCEPTION);
        } catch (RessourceException e) {
            Assert.assertEquals(expectedWrap.getException().getMessage(), e.getMessage());
        }
    }

    /**
     * après toutes les méthodes.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        bf.close();

    }

    /**
     * méthode privée pour attribuée les valeurs aux bénévoes et les ajouter
     * dans la liste.
     */

    private static void setListeRetour() {

        BEN_1.setId(ID_BEN1);
        BEN_1.setMail("mail@yahoo.fr");
        BEN_1.setNumeroTelephone("0565323134");
        BEN_1.setRaisonSociale("Justine Duchamps");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        BEN_2.setId(ID_BEN2);
        BEN_2.setMail("mail@yahoo.fr");
        BEN_2.setNumeroTelephone("0565323134");
        BEN_2.setRaisonSociale("Youssef Bardi");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        BEN_3.setId(ID_BEN3);
        BEN_3.setMail("mail@yahoo.fr");
        BEN_3.setNumeroTelephone("0565323134");
        BEN_3.setRaisonSociale("Taystee Jefferson");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        BEN_4.setId(ID_BEN4);
        BEN_4.setMail("mail@yahoo.fr");
        BEN_4.setNumeroTelephone("0565323134");
        BEN_4.setRaisonSociale("Alexandra Le Du");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        EXPECTED_LISTE.add(BEN_1);
        EXPECTED_LISTE.add(BEN_2);
        EXPECTED_LISTE.add(BEN_3);
        EXPECTED_LISTE.add(BEN_4);

    }
}
