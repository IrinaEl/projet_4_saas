package fr.isika.projet4.business.back.impl.test.activite;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessActivite;
import fr.isika.projet4.bu.impl.BusinessActivite;
import fr.isika.projet4.bu.rest.retour.activite.RetourGetActiviteById;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * test de la méthode métier getAtiviteById.
 * @author stagiaire
 *
 */
public class TestGetActiviteById {

    /**
     * classe contenant les services à tester.
     */
    private static IBusinessActivite business = new BusinessActivite();

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * id de l'activité à tester.
     */
    private static final int ID_EXPECTED_ACTIVITE_NOMINALE = 2;

    /**
     * activité attendue.
     */
    private static final ActiviteDto EXPECTED_ACTIVITE = new ActiviteDto();

    /**
     * le budget de l'activité attendue en retour du test nominal.
     */
    private static final double BUDGET_EXPECTED_ACTIVITE = 150;

    /**
     * le prix de l'activité attendue en retour du test nominal.
     */
    private static final double PRIX_EXPECTED_ACTIVITE = 10;

    /**
     * le nombre de places de l'activité attendue en retour du test nominal.
     */
    private static final Integer NB_PLACES_ACTIVITE = 20;

    /**
     * id activité qui n'existe pas.
     */
    private static final Integer ID_ACTIVITE_INEXISTANTE = 100;

    /**
     * exception attendue en cas d'échec.
     */
    private static final ActiviteException EXPECTED_EXCEPTION = new ActiviteException();

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Le wrap contenant l'exception du cas d'�chec et l'activit� du cas nominal.
     */
    private static RetourGetActiviteById expectedWrap = new RetourGetActiviteById();

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * démarrage du spring et injection du mock.
     * @throws ActiviteException - en cas de liste vide (devrait pas arriver là) ou
     *                           échec de connexion au serveur de données
     * @throws ParseException    - en cas d'erreur de parsing par
     *                           {@link SimpleDateFormat}
     */
    @BeforeClass
    public static void beforeAllTest() throws ParseException, ActiviteException {

        EXPECTED_ACTIVITE.setId(ID_EXPECTED_ACTIVITE_NOMINALE);
        EXPECTED_ACTIVITE.setBudget(BUDGET_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setDateDebut(SDF.parse("25-11-2019"));
        EXPECTED_ACTIVITE.setDateFin(SDF.parse("26-11-2019"));
        EXPECTED_ACTIVITE.setIntitule("test activite 2");
        EXPECTED_ACTIVITE.setDescription("activite de test 2");
        EXPECTED_ACTIVITE.setPrix(PRIX_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setNombrePlace(NB_PLACES_ACTIVITE);

        expectedWrap.setActivite(EXPECTED_ACTIVITE);
        expectedWrap.setException(new ActiviteException("L'activité recherchée n'existe pas", null));

        EasyMock.expect(mock.getActiviteById(ID_EXPECTED_ACTIVITE_NOMINALE)).andReturn(expectedWrap.getActivite());
        EasyMock.expect(mock.getActiviteById(ID_ACTIVITE_INEXISTANTE)).andThrow(expectedWrap.getException());

        EasyMock.replay(mock);
        bf = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessActivite.class);

        try {
            Class<? extends IBusinessActivite> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);

        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {

            e.printStackTrace();

        }

    }

    /**
     * test nominal.
     * @throws ActiviteException e
     */
    @Test
    public void testNominal() throws ActiviteException {
        ActiviteDto retour = mock.getActiviteById(ID_EXPECTED_ACTIVITE_NOMINALE);
        AssertionEntities.assertEntity(EXPECTED_ACTIVITE, retour);
    }

    /**
     * test pour une activité recherchée par un ID qui n'existe pas.
     * @throws ActiviteException lancée si activité pas trouvée.
     */
    @Test
    public void testIDInexistant() throws ActiviteException {
        try {
            mock.getActiviteById(ID_ACTIVITE_INEXISTANTE);
            Assert.fail("Test KO - Activité avec cet identifiant existe");
        } catch (ActiviteException e) {
            Assert.assertEquals(expectedWrap.getException().getMessage(), e.getMessage());
        }

    }

    /**
     * après toutes les méthodes.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        bf.close();

    }
}
