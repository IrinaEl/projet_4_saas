package fr.isika.projet4.bu.impl.test.evenement;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessEvenement;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetByRA;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetEvenements;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.RAException;

/**
 * Test des services métier pour la méthode {@link BusinessEvenement#getAll()}.
 * @author stagiaire
 *
 */
public class TestGetAllEvenement {

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessEvenement bu;

    /**
     * L'évenement attendu.
     */
    private static EvenementDto eventNominal = new EvenementDto();

    /**
     * L'id de l'{@link #eventNominal}.
     */
    private static final Integer ID_EVENT_NOMINAL = 3;

    /**
     * Le prix de l'{@link #eventNominal}.
     */
    private static final double PRIX_EVENT_NOMINAL = 20;

    /**
     * Le budget de l'{@link #eventNominal}.
     */
    private static final double BUDGET_EVENT_NOMINAL = 15000;

    /**
     * L'id de l'adresse l'{@link #eventNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 7;

    /**
     * L'id du type de voie lié à l'adresse de l'{@link #eventNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 6;

    /**
     * L'id de la ville liée à l'adresse de l'{@link #eventNominal}.
     */
    private static final Integer ID_VILLE_EVENT = 10;

    /**
     * L'id du pays de l'{@link #eventNominal}.
     */
    private static final Integer ID_PAYS_EVENT = 7;

    /**
     * L'id du type de l'{@link #eventNominal}.
     */
    private static final int ID_TYPE_EVENT = 2;

    /**
     * Mock des dépendences du service.
     */
    private static IDaoService mock;

    /**
     * Mock des dépendences du service pour throw une exception.
     */
    private static IDaoService mockException;
    /**
     * Le wrap {@link RetourGetEvenements}.
     */
    private static RetourGetEvenements expectedWrapException = new RetourGetEvenements();

    /**
     * La liste attendue .
     */
    private static List<EvenementDto> expectedListe = new ArrayList<>();

    /**
     * L'évenement attendu .
     */
    private static EvenementDto expectedEvent1 = new EvenementDto();

    /**
     * Initialisation du {@link ClassPathXmlApplicationContext}.
     */
    private static ClassPathXmlApplicationContext cpx;
    /**
     * Initialisation du mock et des évenements de test.
     * @throws Exception -
     */
    @BeforeClass
    public static void beforeAllTests() throws Exception {
        initEventNominal();
        initExpectedEvent();
        expectedListe.add(expectedEvent1);

        expectedWrapException.setException(new EventException("il n'y a aucun évenement pour le moment", null));

        mock = EasyMock.createMock(IDaoService.class);

        mockException = EasyMock.createMock(IDaoService.class);

        EasyMock.expect(mock.getAllEvent()).andReturn(expectedListe);

        EasyMock.expect(mockException.getAllEvent()).andThrow(expectedWrapException.getException());

        EasyMock.replay(mock);

        EasyMock.replay(mockException);

        cpx = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");

        bu = cpx.getBean(IBusinessEvenement.class);
        Class<? extends IBusinessEvenement> clazz = bu.getClass();
        Field chp = clazz.getDeclaredField("proxy");
        chp.setAccessible(true);
        chp.set(bu, mock);
        chp.set(bu, mockException);
        chp.setAccessible(false);

    }
    /**
     * Test de la liste d'{@link EvenementDto}.
     * @throws EventException - Si la liste est vide.
     */
    @Test
    public void testNominal() throws EventException {
        List<EvenementDto> retour = mock.getAllEvent();
        for (EvenementDto e : retour) {
            AssertionEntities.assertEntity(eventNominal, e);
        }
    }
    /**
     * Test d'échec dans le cas d'une liste vide.
     */
    @Test
    public void testEchecListeVide() {
        try {
                mockException.getAllEvent();
                Assert.fail("Echec du test");
        } catch (EventException e) {
            Assert.assertEquals(expectedWrapException.getException().getMessage(), e.getMessage());
        }
    }

    /**
     * Méthode pour set les valeurs de {@link #eventNominal}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initEventNominal() throws ParseException {
        eventNominal.setId(ID_EVENT_NOMINAL);
        eventNominal.setBudget(BUDGET_EVENT_NOMINAL);
        eventNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("05-12-2019 19:00:00"));
        eventNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("05-12-2019 23:00:00"));
        eventNominal.setIntitule("Cocktail d'accueil de la promo CDI05");
        eventNominal.setDescription("Cocktail party d'accueil pour la promotion CDI05. "
                + "Champagne et foie gras, musicien de jazz et piste de danse. Tenue correcte exigée.");
        eventNominal.setPrix(PRIX_EVENT_NOMINAL);
        eventNominal.setAdresse(new AdresseDto());
        eventNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        eventNominal.getAdresse().setNomVoie("Mosolee");
        eventNominal.getAdresse().setNumeroVoie("3bis");
        eventNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        eventNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        eventNominal.getAdresse().getTypeVoie().setLibelle("intersection");
        eventNominal.getAdresse().setVille(new VilleDto());
        eventNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        eventNominal.getAdresse().getVille().setLibelle("Berne");
        eventNominal.getAdresse().getVille().setPays(new PaysDto());
        eventNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        eventNominal.getAdresse().getVille().getPays().setLibelle("Suisse");
        eventNominal.setTypeEvenement(new TypeEvenementDto());
        eventNominal.getTypeEvenement().setId(ID_TYPE_EVENT);
        eventNominal.getTypeEvenement().setLibelle("Mineur");
    }

    /**
     * Méthode pour set les valeurs de {@link #expectedEvent1}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initExpectedEvent() throws ParseException {
        expectedEvent1.setId(ID_EVENT_NOMINAL);
        expectedEvent1.setBudget(BUDGET_EVENT_NOMINAL);
        expectedEvent1.setDateDebut(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("05-12-2019 19:00:00"));
        expectedEvent1.setDateFin(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("05-12-2019 23:00:00"));
        expectedEvent1.setIntitule("Cocktail d'accueil de la promo CDI05");
        expectedEvent1.setDescription("Cocktail party d'accueil pour la promotion CDI05. "
                + "Champagne et foie gras, musicien de jazz et piste de danse. Tenue correcte exigée.");
        expectedEvent1.setPrix(PRIX_EVENT_NOMINAL);
        expectedEvent1.setAdresse(new AdresseDto());
        expectedEvent1.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        expectedEvent1.getAdresse().setNomVoie("Mosolee");
        expectedEvent1.getAdresse().setNumeroVoie("3bis");
        expectedEvent1.getAdresse().setTypeVoie(new TypeVoieDto());
        expectedEvent1.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        expectedEvent1.getAdresse().getTypeVoie().setLibelle("intersection");
        expectedEvent1.getAdresse().setVille(new VilleDto());
        expectedEvent1.getAdresse().getVille().setId(ID_VILLE_EVENT);
        expectedEvent1.getAdresse().getVille().setLibelle("Berne");
        expectedEvent1.getAdresse().getVille().setPays(new PaysDto());
        expectedEvent1.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        expectedEvent1.getAdresse().getVille().getPays().setLibelle("Suisse");
        expectedEvent1.setTypeEvenement(new TypeEvenementDto());
        expectedEvent1.getTypeEvenement().setId(ID_TYPE_EVENT);
        expectedEvent1.getTypeEvenement().setLibelle("Mineur");
    }

    /**
     * Après tests.
     */
    @AfterClass
    public static void afterAllTests() {
        EasyMock.verify(mock);
        EasyMock.verify(mockException);
        cpx.close();
    }
}
