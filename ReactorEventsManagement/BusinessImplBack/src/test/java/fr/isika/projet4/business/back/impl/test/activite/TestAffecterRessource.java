package fr.isika.projet4.business.back.impl.test.activite;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessActivite;
import fr.isika.projet4.bu.impl.BusinessActivite;
import fr.isika.projet4.bu.rest.args.tache.ArgAffecterRessource;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

public class TestAffecterRessource {

    /**
     * logger.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * classe contenant les services à tester.
     */
    private static IBusinessActivite business = new BusinessActivite();

    /**
     * id de la ressource à affecter.
     */
    private static final int ID_RESSOURCE_AFFECTEE = 9;

    /**
     * id de l'activité à laquelle la ressource est affectée.
     */
    private static final int ID_ACTIVITE = 2;

    /**
     * activité attendue en retour.
     */
    private static final ActiviteDto EXPECTED_ACTIVITE = new ActiviteDto();

    /**
     * le budget de l'activité attendue en retour du test nominal.
     */
    private static final double BUDGET_EXPECTED_ACTIVITE = 150;

    /**
     * le prix de l'activité attendue en retour du test nominal.
     */
    private static final double PRIX_EXPECTED_ACTIVITE = 10;

    /**
     * le nombre de places de l'activité attendue en retour du test nominal.
     */
    private static final Integer NB_PLACES_ACTIVITE = 20;

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * {@link ArgAffecterRessource}.
     */
    private static ArgAffecterRessource arg = new ArgAffecterRessource();

    /**
     * avant les tests.
     * @throws ParseException .
     * @throws ActiviteException .
     */
    @BeforeClass
    public static void beforeAllTests() throws ParseException, ActiviteException {
        EXPECTED_ACTIVITE.setId(ID_ACTIVITE);
        EXPECTED_ACTIVITE.setBudget(BUDGET_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setDateDebut(SDF.parse("25-11-2019"));
        EXPECTED_ACTIVITE.setDateFin(SDF.parse("26-11-2019"));
        EXPECTED_ACTIVITE.setIntitule("test activite 2");
        EXPECTED_ACTIVITE.setDescription("activite de test 2");
        EXPECTED_ACTIVITE.setPrix(PRIX_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setNombrePlace(NB_PLACES_ACTIVITE);

        arg.setIdActivite(ID_ACTIVITE);
        arg.setIdRessource(ID_RESSOURCE_AFFECTEE);

        EasyMock.expect(mock.affecterRessource(arg.getIdRessource(), arg.getIdActivite())).andReturn(EXPECTED_ACTIVITE);
        EasyMock.replay(mock);

        bf = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessActivite.class);
        try {
            Class<? extends IBusinessActivite> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {

            e.printStackTrace();
        }
    }

    /**
     * test nominal.
     * @throws ActiviteException b
     */
    @Test
    public void testNominal() throws ActiviteException {
        ActiviteDto retour = business.affecterRessource(ID_RESSOURCE_AFFECTEE, ID_ACTIVITE);
        AssertionEntities.assertEntity(EXPECTED_ACTIVITE, retour);

    }

    /**
     * Apr�s tests.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        bf.close();

    }

}
