package fr.isika.projet4.business.back.impl.test.activite;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessEvenement;
import fr.isika.projet4.bu.impl.test.evenement.TestBusinessGetByRA;
import fr.isika.projet4.bu.rest.retour.activite.RetourGetActivites;
import fr.isika.projet4.bu.rest.retour.evenement.RetourGetByRA;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.TypeActiviteDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.ActiviteException;
/**
 * Test des services métier pour la méthode getAllActiviteById.
 * @author stagiaire
 *
 */
public class TestGetAllActiviteByEvent {

    /**
     * Classe contenant le service à tester.
     */
    private static IBusinessEvenement bu;

    /**
     * L'id de l'evenement dans le cas nominal.
     */
    private static final Integer ID_EVENT_NOMINAL = 1;

    /**
     * L'activité attendu.
     */
    private static ActiviteDto activiteNominal = new ActiviteDto();

    /**
     * L'id de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer ID_ACTIVITE_NOMINAL = 1;

    /**
     * Le nombre de place de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer NOMBRE_PLACE_ACTIVITE_NOMINAL = 20;

    /**
     * Le prix de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final double PRIX_ACTIVITE_NOMINAL = 0;

    /**
     * Le budget de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final double BUDGET_ACTIVITE_NOMINAL = 200;

    /**
     * L'id de type d'activite de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer ID_TYPE_ACTIVITE_ACTIVITE_NOMINAL = 3;

    /**
     * L'id du responsable d'activite
     * {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer ID_RESPONSABLE_ACTIVITE_ACTIVITE_NOMINAL = 12;

    /**
     * Le prix de l'evenement de l'{@link #activiteNominal}.
     */
    private static final double PRIX_EVENT_ACTIVITE_NOMINAL = 30;

    /**
     * Le budget de l'evenement de l'{@link #activiteNominal}.
     */
    private static final double BUDGET_EVENT_ACTIVITE_NOMINAL = 50000;

    /**
     * L'id de l'adresse de l'evenement de l'{@link #activiteNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_ACTIVITE_NOMINAL = 8;

    /**
     * L'id du type de voie lié à l'adresse de l'evenement de
     * l'{@link #activiteNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT_ACTIVITE_NOMINAL = 2;

    /**
     * L'id de la ville liée à l'adresse de l'evenement de
     * l'{@link #activiteNominal}.
     */
    private static final Integer ID_VILLE_EVENT_ACTIVITE_NOMINAL = 6;

    /**
     * L'id du pays de l'evenement de l'{@link #activiteNominal}.
     */
    private static final Integer ID_PAYS_EVENT_ACTIVITE_NOMINAL = 9;

    /**
     * L'id du type de l'evenement de l'{@link #activiteNominal}.
     */
    private static final int ID_TYPE_EVENT_ACTIVITE_NOMINAL = 1;

    /**
     * L'id de l'evenement dans le cas alternatif (s'il n'y a aucune activité pour
     * cette évenement).
     */
    private static final Integer ID_EVENT_ECHEC_LISTE_VIDE = 3;

    /**
     * Exception attendue.
     */
    private static RetourGetActivites expectedWrapException = new RetourGetActivites();

    /**
     * Le wrap {@link RetourGetActivites} contenant la liste
     * {@link #expectedListeSize}.
     */
    private static RetourGetActivites expectedWrapListSize = new RetourGetActivites();

    /**
     * La liste attendue contenant les {@link ActiviteDto}.
     */
    private static List<ActiviteDto> expectedListeSize = new ArrayList<>();

    /**
     * L'id du {@link EvenementDto} pour le
     * {@link #testTailleListe()}.
     */
    private static final int ID_EVENT_TEST_TAILLE_LISTE = 5;
    /**
     * Mock des dépendences du service.
     */
    private static IDaoService mock;

    /**
     * La liste attendue .
     */
    private static List<ActiviteDto> expectedListe = new ArrayList<>();

    /**
     * L'évenement attendu .
     */
    private static ActiviteDto expectedAct1 = new ActiviteDto();

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext cpx;

    /**
     * Initialisation du mock et des évenements de test.
     * @throws Exception -
     */
    @BeforeClass
    public static void beforeAllTests() throws Exception {
        initActiviteNominal();
        initExpectedActivite();
        expectedListe.add(expectedAct1);

        expectedListeSize.add(new ActiviteDto());
        expectedListeSize.add(new ActiviteDto());
        expectedListeSize.add(new ActiviteDto());

        expectedWrapListSize.setActivites(expectedListeSize);
        expectedWrapException.setException(new ActiviteException("il n'y a pas encore d'activité pour cet évenement", null));

        mock = EasyMock.createMock(IDaoService.class);
        EasyMock.expect(mock.getAllActiviteByEvent(ID_EVENT_NOMINAL)).andReturn(expectedListe);

        EasyMock.expect(mock.getAllActiviteByEvent(ID_EVENT_ECHEC_LISTE_VIDE)).andThrow(expectedWrapException.getException());

        EasyMock.expect(mock.getAllActiviteByEvent(ID_EVENT_TEST_TAILLE_LISTE)).andReturn(expectedWrapListSize.getActivites());
        EasyMock.replay(mock);

        cpx = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");

        bu = cpx.getBean(IBusinessEvenement.class);
        Class<? extends IBusinessEvenement> clazz = bu.getClass();
        Field chp = clazz.getDeclaredField("proxy");
        chp.setAccessible(true);
        chp.set(bu, mock);
        chp.setAccessible(false);

    }
    /**
     * Test de la liste d'{@link ActiviteDto}.
     * @throws ActiviteException - Si la liste est vide.
     *                           - Si l'unité de persistence est hors-service.
     */
    @Test
    public void testNominal() throws ActiviteException {

        RetourGetActivites wrap = new RetourGetActivites();
        wrap.setActivites(mock.getAllActiviteByEvent(ID_EVENT_NOMINAL));

        for (ActiviteDto activiteDto : wrap.getActivites()) {
            AssertionEntities.assertEntity(activiteNominal, activiteDto);
        }
    }
    /**
     * Test d'échec dans le cas d'une liste vide (l'évenement n'a pas encore des
     * activités).
     */
    @Test
    public void testEchecListeVide() {
        try {
            mock.getAllActiviteByEvent(ID_EVENT_ECHEC_LISTE_VIDE);
            Assert.fail("Echec du test");
        } catch (ActiviteException e) {
            Assert.assertEquals(expectedWrapException.getException().getMessage(), e.getMessage());
        }
    }

    /**
     * Test de la taille de la liste.
     * @throws ActiviteException - Si la liste est vide. - Si l'unité de persistence
     *                           est hors-service.
     */
    @Test
    public void testTailleListe() throws ActiviteException {
        Assert.assertEquals(expectedWrapListSize.getActivites().size(), mock.getAllActiviteByEvent(ID_EVENT_TEST_TAILLE_LISTE).size());
    }
    /**
     * Méthode pour set les valeurs de {@link #activiteNominal}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initActiviteNominal() throws ParseException {

        activiteNominal.setId(ID_ACTIVITE_NOMINAL);
        activiteNominal.setBudget(BUDGET_ACTIVITE_NOMINAL);
        activiteNominal.setDateDebut(SDF.parse("25-11-2019 00:00:00"));
        activiteNominal.setDateFin(SDF.parse("26-11-2019 00:00:00"));
        activiteNominal.setDescription("Prestation du groupe HQM");

        activiteNominal.setEvenement(new EvenementDto());
        activiteNominal.getEvenement().setId(ID_EVENT_NOMINAL);
        activiteNominal.getEvenement().setBudget(BUDGET_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().setDateDebut(SDF.parse("16-01-2020 20:00:00"));
        activiteNominal.getEvenement().setDateFin(SDF.parse("16-01-2020 23:30:00"));
        activiteNominal.getEvenement().setDescription("Concert promotionnel d'artistes montants de la région de Malakoff");
        activiteNominal.getEvenement().setIntitule("Concert promotionnel Isika");
        activiteNominal.getEvenement().setPrix(PRIX_EVENT_ACTIVITE_NOMINAL);

        activiteNominal.getEvenement().setAdresse(new AdresseDto());
        activiteNominal.getEvenement().getAdresse().setId(ID_ADRESSE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().setNomVoie("Thunder Bluff");
        activiteNominal.getEvenement().getAdresse().setNumeroVoie("13");

        activiteNominal.getEvenement().getAdresse().setTypeVoie(new TypeVoieDto());
        activiteNominal.getEvenement().getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().getTypeVoie().setLibelle("chemin");

        activiteNominal.getEvenement().getAdresse().setVille(new VilleDto());
        activiteNominal.getEvenement().getAdresse().getVille().setId(ID_VILLE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().getVille().setLibelle("Tana");

        activiteNominal.getEvenement().getAdresse().getVille().setPays(new PaysDto());
        activiteNominal.getEvenement().getAdresse().getVille().getPays().setId(ID_PAYS_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().getVille().getPays().setLibelle("Madagascar");

        activiteNominal.getEvenement().setTypeEvenement(new TypeEvenementDto());
        activiteNominal.getEvenement().getTypeEvenement().setId(ID_TYPE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getTypeEvenement().setLibelle("Majeur");

        activiteNominal.setIntitule("Prestation du groupe HQM");
        activiteNominal.setNombrePlace(NOMBRE_PLACE_ACTIVITE_NOMINAL);
        activiteNominal.setPrix(PRIX_ACTIVITE_NOMINAL);

        activiteNominal.setResponsableActivite(new ResponsableActiviteDto());
        activiteNominal.getResponsableActivite().setId(ID_RESPONSABLE_ACTIVITE_ACTIVITE_NOMINAL);
        activiteNominal.getResponsableActivite().setMail("michel-gnome@wow.fr");
        activiteNominal.getResponsableActivite().setNom("Gnome");
        activiteNominal.getResponsableActivite().setPrenom("Michel");

        activiteNominal.setTypeActivite(new TypeActiviteDto());
        activiteNominal.getTypeActivite().setId(ID_TYPE_ACTIVITE_ACTIVITE_NOMINAL);
        activiteNominal.getTypeActivite().setIntitule("Concert");
        activiteNominal.getTypeActivite().setDescription("");
    }
    /**
     * Méthode pour set les valeurs de {@link #expectedAct1}.
     * @throws ParseException - Erreur du {@link SimpleDateFormat}
     */
    private static void initExpectedActivite() throws ParseException {

        expectedAct1.setId(ID_ACTIVITE_NOMINAL);
        expectedAct1.setBudget(BUDGET_ACTIVITE_NOMINAL);
        expectedAct1.setDateDebut(SDF.parse("25-11-2019 00:00:00"));
        expectedAct1.setDateFin(SDF.parse("26-11-2019 00:00:00"));
        expectedAct1.setDescription("Prestation du groupe HQM");

        expectedAct1.setEvenement(new EvenementDto());
        expectedAct1.getEvenement().setId(ID_EVENT_NOMINAL);
        expectedAct1.getEvenement().setBudget(BUDGET_EVENT_ACTIVITE_NOMINAL);
        expectedAct1.getEvenement().setDateDebut(SDF.parse("16-01-2020 20:00:00"));
        expectedAct1.getEvenement().setDateFin(SDF.parse("16-01-2020 23:30:00"));
        expectedAct1.getEvenement().setDescription("Concert promotionnel d'artistes montants de la région de Malakoff");
        expectedAct1.getEvenement().setIntitule("Concert promotionnel Isika");
        expectedAct1.getEvenement().setPrix(PRIX_EVENT_ACTIVITE_NOMINAL);

        expectedAct1.getEvenement().setAdresse(new AdresseDto());
        expectedAct1.getEvenement().getAdresse().setId(ID_ADRESSE_EVENT_ACTIVITE_NOMINAL);
        expectedAct1.getEvenement().getAdresse().setNomVoie("Thunder Bluff");
        expectedAct1.getEvenement().getAdresse().setNumeroVoie("13");

        expectedAct1.getEvenement().getAdresse().setTypeVoie(new TypeVoieDto());
        expectedAct1.getEvenement().getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT_ACTIVITE_NOMINAL);
        expectedAct1.getEvenement().getAdresse().getTypeVoie().setLibelle("chemin");

        expectedAct1.getEvenement().getAdresse().setVille(new VilleDto());
        expectedAct1.getEvenement().getAdresse().getVille().setId(ID_VILLE_EVENT_ACTIVITE_NOMINAL);
        expectedAct1.getEvenement().getAdresse().getVille().setLibelle("Tana");

        expectedAct1.getEvenement().getAdresse().getVille().setPays(new PaysDto());
        expectedAct1.getEvenement().getAdresse().getVille().getPays().setId(ID_PAYS_EVENT_ACTIVITE_NOMINAL);
        expectedAct1.getEvenement().getAdresse().getVille().getPays().setLibelle("Madagascar");

        expectedAct1.getEvenement().setTypeEvenement(new TypeEvenementDto());
        expectedAct1.getEvenement().getTypeEvenement().setId(ID_TYPE_EVENT_ACTIVITE_NOMINAL);
        expectedAct1.getEvenement().getTypeEvenement().setLibelle("Majeur");

        expectedAct1.setIntitule("Prestation du groupe HQM");
        expectedAct1.setNombrePlace(NOMBRE_PLACE_ACTIVITE_NOMINAL);
        expectedAct1.setPrix(PRIX_ACTIVITE_NOMINAL);

        expectedAct1.setResponsableActivite(new ResponsableActiviteDto());
        expectedAct1.getResponsableActivite().setId(ID_RESPONSABLE_ACTIVITE_ACTIVITE_NOMINAL);
        expectedAct1.getResponsableActivite().setMail("michel-gnome@wow.fr");
        expectedAct1.getResponsableActivite().setNom("Gnome");
        expectedAct1.getResponsableActivite().setPrenom("Michel");

        expectedAct1.setTypeActivite(new TypeActiviteDto());
        expectedAct1.getTypeActivite().setId(ID_TYPE_ACTIVITE_ACTIVITE_NOMINAL);
        expectedAct1.getTypeActivite().setIntitule("Concert");
        expectedAct1.getTypeActivite().setDescription("");
    }
    /**
     * Après tests.
     */
    @AfterClass
    public static void afterAllTests() {
        EasyMock.verify(mock);
        cpx.close();
    }
}
