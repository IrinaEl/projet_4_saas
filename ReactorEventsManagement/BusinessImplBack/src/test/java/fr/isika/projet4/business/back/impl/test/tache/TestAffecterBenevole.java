package fr.isika.projet4.business.back.impl.test.tache;

import java.lang.reflect.Field;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessTache;
import fr.isika.projet4.bu.impl.BusinessTache;
import fr.isika.projet4.bu.rest.args.tache.ArgAffecterBenevole;
import fr.isika.projet4.bu.rst.retour.tache.RetourUneTache;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.StatutTacheDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * test métier de la méthode affectation bénévole.
 * @author stagiaire
 *
 */
public class TestAffecterBenevole {

    /**
     * logger.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * mock faisant office de la classe contenant les services DAO appellés par les
     * services business testés (leurs dépendences).
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * classe contenant les services à tester.
     */
    private static IBusinessTache business = new BusinessTache();

    /**
     * la tâche attendue en retour dans la liste testée dans le test nominal.
     */
    private static final TacheDto EXPECTED_TACHE = new TacheDto();

    /**
     * statut de la tache attendue nominale.
     */
    private static final StatutTacheDto EXPECTED_STATUT_NOMINAL = new StatutTacheDto();

    /**
     * id du statut attendu.
     */
    private static final Integer EXPECTED_STATUT_ID = 1;

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer ID_TACHE_NOMINALE = 1;

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer DEADLINE_NOMINAL = 120;

    /**
     * bénévole à affecter.
     */
    private static final IntervenantExterneDto BENEVOLE_AFFECTE = new IntervenantExterneDto();

    /**
     * id du bénévole à affecter.
     */
    private static final int ID_BENEVOLE_AFFECTE = 11;

    /**
     * id du bénévole attendu.
     */
    private static final Integer ID_BENEVOLE_ATTENDU = 11;

    /**
     * tache qui doit être retournée par la méthode.
     */
    private static TacheDto TACHE_APRES_TRAITEMENT = new TacheDto();

    /**
     * La classe d'arg contenant nos entit�s de tests.
     */
    private static ArgAffecterBenevole arg = new ArgAffecterBenevole();

    /**
     * Le wrap contenant la t�che de test.
     */
    private static RetourUneTache expectedWrapEntity = new RetourUneTache();

    /**
     * Le wrap contenant l'exception de test.
     */
    private static RetourUneTache expectedWrapException = new RetourUneTache();

    /**
     * T�che pour tester l'exception.
     */
    private static TacheDto tacheException = new TacheDto();

    /**
     * Intervenant externe pour tester l'exception.
     */
    private static IntervenantExterneDto benevoleException = new IntervenantExterneDto();

    /**
     * démarrage spring + création mock + set données test.
     * @throws TacheException n
     * @throws ParseException n
     */
    @BeforeClass
    public static void beforeAllTests() throws TacheException, ParseException {
        EXPECTED_STATUT_NOMINAL.setId(EXPECTED_STATUT_ID);
        EXPECTED_STATUT_NOMINAL.setIntitule("EN COURS");
        EXPECTED_TACHE.setId(ID_TACHE_NOMINALE);
        EXPECTED_TACHE.setDeadline(DEADLINE_NOMINAL);
        EXPECTED_TACHE.setStatut(EXPECTED_STATUT_NOMINAL);

        BENEVOLE_AFFECTE.setId(ID_BENEVOLE_AFFECTE);
        BENEVOLE_AFFECTE.setMail("mail@yahoo.fr");
        BENEVOLE_AFFECTE.setNumeroTelephone("0565323134");
        BENEVOLE_AFFECTE.setRaisonSociale("Taystee Jefferson");

        arg.setBenevole(BENEVOLE_AFFECTE);
        arg.setTache(EXPECTED_TACHE);
        expectedWrapEntity.setTache(arg.getTache());
        expectedWrapException.setException(new TacheException("Vous ne pouvez pas affecter ce b�n�vole � cette t�che", null));

        TACHE_APRES_TRAITEMENT = EXPECTED_TACHE;
        TACHE_APRES_TRAITEMENT.setBenevole(BENEVOLE_AFFECTE);

        EasyMock.expect(mock.affecterBenevole(expectedWrapEntity.getTache(), arg.getBenevole())).andReturn(TACHE_APRES_TRAITEMENT);
        EasyMock.expect(mock.affecterBenevole(tacheException, benevoleException)).andThrow(expectedWrapException.getException());
        EasyMock.replay(mock);
        bf = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        business = bf.getBean(IBusinessTache.class);
        try {
            Class<? extends IBusinessTache> clazz = business.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(business, mock);
            proxyField.setAccessible(false);

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {

            e.printStackTrace();
        }

    }

    /**
     * test nominal.
     * @throws TacheException b
     */
    @Test
    public void testNominal() throws TacheException {

        TacheDto retour = business.affecterBenevole(EXPECTED_TACHE, BENEVOLE_AFFECTE);

        AssertionEntities.assertEntity(TACHE_APRES_TRAITEMENT, retour);
        log.fatal(TACHE_APRES_TRAITEMENT.getBenevole().getId());
        log.fatal(business);
    }

    /**
     * test exception.
     */
    @Test
    public void testException() {
        try {
            mock.affecterBenevole(tacheException, benevoleException);
        } catch (TacheException e) {
            Assert.assertEquals(expectedWrapException.getException().getMessage(), e.getMessage());
            ;
        }
    }

    /**
     * after.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
        bf.close();

    }

}
