package fr.isika.projet4.bu.impl.test.evenement;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.bu.api.IBusinessEvenement;
import fr.isika.projet4.business.back.impl.test.assertions.AssertionEntities;
import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * Classe de test de la méthode {@link IDaoEvenement#getAllDoneEvents()}.
 * * @author stagiaire
 *
 */
public class TestGetAllDoneEvents {

    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(TestGetAllDoneEvents.class);

    /**
     * La classe à mocker.
     */
    private static IDaoService mock;

    /**
     * La business dans laquelle se trouve le service à tester.
     */
    private static IBusinessEvenement bu;

    /**
     * L'événement attendu dans l'enchaînement nominal.
     */
    private static EvenementDto evenementNominal = new EvenementDto();

    /**
     * L'identifiant de l'{@link #evenementNominal}.
     */
    private static final int ID_EVENT = 5;

    /**
     * Le prix de l'{@link #evenementNominal}.
     */
    private static final double PRIX_EVENT_NOMINAL = 5;

    /**
     * Le budget de l'{@link #evenementNominal}.
     */
    private static final double BUDGET_EVENT_NOMINAL = 500;

    /**
     * L'id de l'adresse l'{@link #evenementNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 6;

    /**
     * L'id du type de voie lié à l'adresse de l'{@link #evenementNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 2;

    /**
     * L'id de la ville liée à l'adresse de l'{@link #evenementNominal}.
     */
    private static final Integer ID_VILLE_EVENT = 3;

    /**
     * L'id du pays de l'{@link #evenementNominal}.
     */
    private static final Integer ID_PAYS_EVENT = 3;

    /**
     * L'id du type de l'{@link #evenementNominal}.
     */
    private static final int ID_TYPE_EVENT = 1;

    private static List<EvenementDto> expectedList = new ArrayList<>();

    /**
     * Pour initialiser spring.
     */
    private static ClassPathXmlApplicationContext spring;

    /**
     * Initialisation de spring et remplissage de l'evenement.
     * @throws EventException .
     * @throws ParseException .
     */
    @BeforeClass
    public static void initTest() throws ParseException, EventException {
        evenementNominal.setId(ID_EVENT);
        evenementNominal.setBudget(BUDGET_EVENT_NOMINAL);
        evenementNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("14-02-2019 12:30:00"));
        evenementNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse("14-02-2019 17:00:00"));
        evenementNominal.setIntitule("Célébration de la St-Valentin");
        evenementNominal.setDescription(
                "Venez nombreux cet après-midi de célébration des amoureux ! "
                        + "Au programme : divers petits jeux en coopération qui vous permettront de vous rapprocher de votre moitié ! "
                        + "Pas en couple ? Aucun problème, venez avec vos amis car, comme on dit, plus on est de fous plus on rit !");
        evenementNominal.setPrix(PRIX_EVENT_NOMINAL);
        evenementNominal.setAdresse(new AdresseDto());
        evenementNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        evenementNominal.getAdresse().setNomVoie("Tarrides");
        evenementNominal.getAdresse().setNumeroVoie("87");
        evenementNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        evenementNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        evenementNominal.getAdresse().getTypeVoie().setLibelle("chemin");
        evenementNominal.getAdresse().setVille(new VilleDto());
        evenementNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        evenementNominal.getAdresse().getVille().setLibelle("Marrakech");
        evenementNominal.getAdresse().getVille().setPays(new PaysDto());
        evenementNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        evenementNominal.getAdresse().getVille().getPays().setLibelle("Maroc");
        evenementNominal.setTypeEvenement(new TypeEvenementDto());
        evenementNominal.getTypeEvenement().setId(ID_TYPE_EVENT);
        evenementNominal.getTypeEvenement().setLibelle("Majeur");

        expectedList.add(evenementNominal);

        mock = EasyMock.createMock(IDaoService.class);
        EasyMock.expect(mock.getAllDoneEvents()).andReturn(expectedList);
        EasyMock.replay(mock);

        spring = new ClassPathXmlApplicationContext("classpath:spring-bu-back.xml");
        bu = spring.getBean(IBusinessEvenement.class);

        try {
            Class<? extends IBusinessEvenement> clazz = bu.getClass();
            Field chp = clazz.getDeclaredField("proxy");
            chp.setAccessible(true);
            chp.set(bu, mock);
            chp.setAccessible(false);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            log.fatal(e);
        }
    }

    /**
     * Test nominal de la méthode {@link IDaoEvenement#getAllDoneEvents()}.
     * @throws EventException S'il n'y a aucun événement correspondant.
     */
    @Test
    public void testNominal() throws EventException {
        List<EvenementDto> events = bu.getAllDoneEvents();
        Assert.assertNotNull(events);
        for (EvenementDto evenementDto : events) {
            log.debug(evenementDto);
            log.debug(evenementDto.getId());
            log.debug(evenementDto.getBudget());
            if (evenementDto.getId() == ID_EVENT) {
                AssertionEntities.assertEntity(evenementNominal, evenementDto);
            }
        }
    }

    /**
     * Pour refermer la ressource qui initialise spring.
     */
    @AfterClass
    public static void afterTest() {
        spring.close();
    }

}
