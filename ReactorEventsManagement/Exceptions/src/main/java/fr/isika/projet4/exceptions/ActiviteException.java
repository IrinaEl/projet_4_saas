package fr.isika.projet4.exceptions;

/**
 * Classe de gestion des exceptions liées aux {@link Activite}.
 * @author stagiaire
 *
 */
public class ActiviteException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * constructeur vide.
     */
    public ActiviteException() {

    }

    /**
     *
     * @param paramMessage - message affiché si l'exception est lancée.
     * @param paramCause   - cause de l'exception.
     */
    public ActiviteException(String paramMessage, Throwable paramCause) {
        super(paramMessage, paramCause);

    }

    /**
     * constructeur avec un paramère.
     * @param paramMessage g
     */
    public ActiviteException(String paramMessage) {
        super(paramMessage);
    }

}
