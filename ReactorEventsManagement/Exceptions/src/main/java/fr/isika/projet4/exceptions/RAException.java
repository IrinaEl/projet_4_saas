package fr.isika.projet4.exceptions;

/**
 * Classe de gestion des exceptions liées à un reponsable d'activité.
 * @author ludwig
 *
 */
public class RAException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public RAException() {

    }

    /**
     * @param paramMessage - Message to set.
     * @param paramCause   - Throwable to set.
     */
    public RAException(String paramMessage, Throwable paramCause) {
        super(paramMessage, paramCause);
    }

    /**
     * constructeur avec une String en param pour la sérialisation.
     * @param paramMessage - message
     */
    public RAException(String paramMessage) {
        super(paramMessage);
    }

}
