package fr.isika.projet4.exceptions;

/**
 * Classe de gestion des exceptions liées aux {@link TacheDto}.
 * @author stagiaire
 *
 */
public class TacheException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * constructeur vide.
     */
    public TacheException() {

    }

    /**
     *
     * @param paramMessage - message affiché si l'exception est lancée.
     * @param paramCause   - cause de l'exception.
     */
    public TacheException(String paramMessage, Throwable paramCause) {

    }

    /**
     * constructeur avec un paramère.
     * @param paramMessage g
     */
    public TacheException(String paramMessage) {

    }

}
