package fr.isika.projet4.exceptions;

/**
 * Classe de gestion des exceptions liées à un Lead Pole.
 * @author stagiaire
 *
 */
public class LeadPoleException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructeur.
     */
    public LeadPoleException() {
        super();
    }

    /**
     * @param paramMessage - message to set.
     * @param paramCause   - Throwable to set.
     */
    public LeadPoleException(String paramMessage, Throwable paramCause) {
        super(paramMessage, paramCause);
    }

}
