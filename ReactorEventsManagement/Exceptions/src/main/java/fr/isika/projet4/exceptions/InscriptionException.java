package fr.isika.projet4.exceptions;


/**
 * Classe d'exceptions d'une inscription a un evenement ou une activité.
 * @author stagiaire
 *
 */
public class InscriptionException extends Exception {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public InscriptionException() {
        super();
    }
    /**
     * Constructeur surchargé exception.
     * @param paramMessage - message to set.
     * @param paramCause - throwable to set.
     */
    public InscriptionException(String paramMessage, Throwable paramCause) {
        super(paramMessage, paramCause);
    }

}
