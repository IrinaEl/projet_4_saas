package fr.isika.projet4.exceptions;

/**
 * class exception connexion.
 * @author stagiaire
 */
public class ConnexionException extends Exception {

    /**
     * serial Version .
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default Constructor.
     */
    public ConnexionException() {
        super();
    }

    /**
     * @param paramMessage .
     * @param paramCause .
     */
    public ConnexionException(String paramMessage, Throwable paramCause) {
        super(paramMessage, paramCause);
    }

    /**
     * @param paramMessage .
     */
    public ConnexionException(String paramMessage) {
        super(paramMessage);
    }

}
