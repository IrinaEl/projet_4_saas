package fr.isika.projet4.exceptions;

/**
 * exception levée en cas d'échec des méthodes liées aux ressources.
 * @author stagiaire
 *
 */
public class RessourceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public RessourceException() {
    }

    /**
     * @param paramMessage message affiché
     * @param paramCause   cause exception
     */
    public RessourceException(String paramMessage, Throwable paramCause) {

    }

    /**
     * @param paramMessage param
     */
    public RessourceException(String paramMessage) {

    }

}
