package fr.isika.projet4.exceptions;

/**
 * Classe de gestion des exceptions liées à un {@link Evenement}.
 * @author stagiaire
 *
 */
public class EventException extends Exception {

    /**
     * pour la serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default Constructor.
     */
    public EventException() {
        super();
    }

    /**
     * @param paramMessage the message to set
     * @param paramCause   the throwable to set
     */
    public EventException(String paramMessage, Throwable paramCause) {
        super(paramMessage, paramCause);
    }

    /**
     * @param paramMessage the message to set
     */
    public EventException(String paramMessage) {
        super(paramMessage);
    }

}
