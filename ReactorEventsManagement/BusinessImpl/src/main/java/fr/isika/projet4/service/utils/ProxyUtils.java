package fr.isika.projet4.service.utils;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import fr.isika.projet4.data.api.service.IDaoService;

/**
 * Classe utilitaire pour la generation des proxys des WS_SOAP du DAO.
 * @author ashkel
 */
public final class ProxyUtils {

    /**
     * namespace pour les services dao.
     */
    private static final String NAMESPACE = "http://isika.projet4.com";

    /**
     * chemin wsdl.
     */
    private static String serviceUtilisateurWsdl = "http://localhost:8181/DataImpl/al03-services/service-app?wsdl";

    /**
     * Default private constructor.
     */
    private ProxyUtils() {
    }

    /**
     * Create a webservice proxy.
     * @param serviceEndpoint end point.
     * @param                 <T> This is the type parameter
     * @param wsdl            url to wsdl.
     * @return the proxy.
     * @throws MalformedURLException .
     */
    public static IDaoService getProxy() throws MalformedURLException {
        QName qname = new QName(NAMESPACE, "serviceApp");
        QName portQname = new QName(NAMESPACE, "projet4");
        Service serviceWeb = Service.create(new URL(serviceUtilisateurWsdl), qname);
        return serviceWeb.getPort(portQname, IDaoService.class);
    }
}
