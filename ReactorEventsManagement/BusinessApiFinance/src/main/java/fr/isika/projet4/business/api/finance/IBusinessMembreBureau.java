package fr.isika.projet4.business.api.finance;

/**
 * Interface définissant les différentes méthodes permettant d'afficher les
 * statistiques concernant les finances.
 * @author stagiaire
 *
 */
public interface IBusinessMembreBureau {

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.RecetteDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param idEvenement l'identifiant de
     *                    l'{@link fr.isika.projet4.dto.EvenementDto} en question.
     * @return la somme de toutes les recettes.
     */
    double sommeRecetteParEvenement(int idEvenement);

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.DepenseDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param idEvenement l'identifiant de
     *                    l'{@link fr.isika.projet4.dto.EvenementDto} en question.
     * @return la somme de toutes les depenses.
     */
    double sommeDepenseParEvenement(int idEvenement);

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusActiviteDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @param idEvent L'identtifiant de l'{@link fr.isika.projet4.dto.EvenementDto}
     *                concerné.
     * @return Le nombre de revenus d'activité en fonction de l'événement.
     */
    Long nbRevenuActByIdEvent(int idEvent);

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusSponsoringDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @param idEvent L'identifiant de l'{@link fr.isika.projet4.dto.EvenementDto}
     *                concerné.
     * @return Le nombre de revenus de sponsoring en fonction de l'événement.
     */
    long nbRevSponsoParEvenement(int idEvent);
}
