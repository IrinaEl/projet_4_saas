/**
 * Package contenant les interfaces concernant l'affichage des statistiques
 * liées aux finances.
 * @author stagiaire
 *
 */
package fr.isika.projet4.business.api.finance;