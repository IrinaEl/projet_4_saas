/**
 * Package contenant les classes de test concernant les méthodes de
 * {@link fr.isika.projet4.business.api.finance.IBusinessMembreBureau}.
 * @author stagiaire
 *
 */
package fr.isika.projet4.bu.impl.finance.test.membre.bureau;