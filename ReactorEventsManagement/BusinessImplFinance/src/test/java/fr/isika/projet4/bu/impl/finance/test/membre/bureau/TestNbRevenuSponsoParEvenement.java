package fr.isika.projet4.bu.impl.finance.test.membre.bureau;

import java.lang.reflect.Field;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.business.api.finance.IBusinessMembreBureau;
import fr.isika.projet4.data.api.service.IDaoService;

/**
 * Classe de test de la méthode
 * {@link IBusinessMembreBureau#nbRevenuActByIdEvent(int)}.
 * @author stagiaire
 *
 */
public class TestNbRevenuSponsoParEvenement {

    /**
     * L'interface contenant le service à tester.
     */
    private static IBusinessMembreBureau bu;

    /**
     * Le dao à mocker.
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);;

    /**
     * L'id de l'{@link fr.isika.projet4.entity.Evenement} concerné.
     */
    private static final int EVENT_ID = 5;

    /**
     * Le nombre de {@link fr.isika.projet4.entity.RevenusActivite} attendu.
     */
    private static final long EXPECTED_NB = 1;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext ac;

    /**
     * Méthode d'initiation pour les tests et qui lance Spring.
     */
    @BeforeClass
    public static void initTest() {
        ac = new ClassPathXmlApplicationContext("classpath:spring-bu-finance.xml");
        bu = ac.getBean(IBusinessMembreBureau.class);

        try {
            Class<? extends IBusinessMembreBureau> clazz = bu.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(bu, mock);
            proxyField.setAccessible(false);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
            e.printStackTrace();
        }
        EasyMock.expect(mock.nbRevenuActByIdEvent(EVENT_ID)).andReturn(EXPECTED_NB);

        EasyMock.replay(mock);
    }

    /**
     * Test du cas nominal de la méthode
     * {@link IBusinessMembreBureau#nbRevenuActByIdEvent(int)}. Elle doit retourner
     * {@link #EXPECTED_NB}.
     */
    @Test
    public void testNominal() {
        long retour = bu.nbRevenuActByIdEvent(EVENT_ID);
        Assert.assertNotNull(retour);
        Assert.assertEquals(EXPECTED_NB, retour);
    }

    /**
     * Fermeture de spring et vérificztion du nombre de passage dans le mock.
     */
    @AfterClass
    public static void finTests() {
        EasyMock.verify(mock);
        ac.close();
    }
}
