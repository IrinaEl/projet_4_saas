package fr.isika.projet4.bu.impl.finance.test.membre.bureau;

import java.lang.reflect.Field;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.business.api.finance.IBusinessMembreBureau;
import fr.isika.projet4.data.api.service.IDaoService;

/**
 * Classe de test pour
 * {@link IBusinessMembreBureau#sommeDepenseParEvenement(int)}.
 * @author stagiaire
 *
 */
public class TestSommeDepenseParEvenement {

    /**
     * Le service la classe du service à tester.
     */
    private static IBusinessMembreBureau bu;

    /**
     * Mock du service injecté dans {@link IBusinessMembreBureau}.
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * Le résultat attendu après l'addition de toutes les recettes.
     */
    private static final double EXPECTED_SOME = 2500.0;

    /**
     * Le réusltat si l'événement n'a pas de dépenses qui lui sont liées.
     */
    private static final double RESULTAT_SI_EXCEPTION = 0.0;

    /**
     * L'identifiant de l'événement que l'on cherche.
     */
    private static final int ID_EVENEMENT = 1;

    /**
     * L'identifiant d'un événement sans dépense.
     */
    private static final int MAUVAIS_ID_EVENEMENT = 100;

    /**
     * Delta maximum entre la valeur attendu et la valeur test.
     */
    private static final double DELTA_EQUALS = 0.01;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext ac;

    /**
     * Méthode d'initiation pour les tests et qui lance Spring.
     */
    @BeforeClass
    public static void initTest() {
        ac = new ClassPathXmlApplicationContext("classpath:spring-bu-finance.xml");
        bu = ac.getBean(IBusinessMembreBureau.class);

        try {
            Class<? extends IBusinessMembreBureau> clazz = bu.getClass();
            Field proxyField = clazz.getDeclaredField("proxy");
            proxyField.setAccessible(true);
            proxyField.set(bu, mock);
            proxyField.setAccessible(false);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
            e.printStackTrace();
        }

        EasyMock.expect(mock.sommeDepenseParEvenement(ID_EVENEMENT)).andReturn(EXPECTED_SOME);
        EasyMock.expect(mock.sommeDepenseParEvenement(MAUVAIS_ID_EVENEMENT)).andReturn(RESULTAT_SI_EXCEPTION);

        EasyMock.replay(mock);
    }

    /**
     * Test nominal de la méthode
     * {@link IBusinessMembreBureau#sommeDepenseParEvenement(int)}. Elle doit
     * retourner {@link #EXPECTED_SOME}.
     */
    @Test
    public void testNominalSommeDepenseParEvenement() {
        double retour = bu.sommeDepenseParEvenement(ID_EVENEMENT);
        Assert.assertNotNull(retour);
        Assert.assertEquals(EXPECTED_SOME, retour, DELTA_EQUALS);
    }

    /**
     * Test alternatif de la méthode
     * {@link IBusinessMembreBureau#sommeDepenseParEvenement(int)}. Elle doit
     * retourner {@link #RESULTAT_SI_EXCEPTION}.
     */
    @Test
    public void testAlternatif() {
        double retour = bu.sommeDepenseParEvenement(MAUVAIS_ID_EVENEMENT);
        Assert.assertNotNull(retour);
        Assert.assertEquals(RESULTAT_SI_EXCEPTION, retour, DELTA_EQUALS);
    }

    /**
     * Fermeture de la ressource qui permet de démarrer Spring.
     */
    @AfterClass
    public static void close() {
        ac.close();
        EasyMock.verify(mock);
    }
}
