/**
 * Package contenant le service REST à exposer.
 * @author stagiaire
 *
 */
package fr.isika.projet4.bu.impl.finance.rest;