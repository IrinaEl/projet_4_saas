package fr.isika.projet4.bu.impl.finance.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.projet4.bu.impl.finance.wrap.retour.RetourCount;
import fr.isika.projet4.bu.impl.finance.wrap.retour.RetourSomme;
import fr.isika.projet4.business.api.finance.IBusinessMembreBureau;

/**
 * Classe exposant les services rest du projet.
 * @author stagiaire
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/finance-rest")
public class FinanceRest {

    /**
     * Instanciation du service concernant les
     * {@link fr.isika.projet4.dto.MembreAdministrationDto}.
     */
    @Autowired
    private IBusinessMembreBureau buMB;

    /**
     * Pour débugger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.RecetteDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param paramIdEvenement l'identifiant de
     *                         l'{@link fr.isika.projet4.dto.EvenementDto} en
     *                         question.
     * @return la somme de toutes les recettes.
     */
    @GetMapping(path = "/somme-recette/{idEvent}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RetourSomme sommeRecetteParEvenement(
                                                @PathVariable(name = "idEvent") int paramIdEvenement) {
        RetourSomme retour = new RetourSomme();
        retour.setSomme(buMB.sommeRecetteParEvenement(paramIdEvenement));
        log.debug(retour);
        log.debug(retour.getSomme());
        return retour;
    }

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.DepenseDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param idEvenement l'identifiant de
     *                    l'{@link fr.isika.projet4.dto.EvenementDto} en question.
     * @return la somme de toutes les depenses.
     */
    @GetMapping(path = "/somme-depense/{idEvent}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RetourSomme sommeDepenseParEvenement(@PathVariable(name = "idEvent") int idEvenement) {
        RetourSomme retour = new RetourSomme();
        retour.setSomme(buMB.sommeDepenseParEvenement(idEvenement));
        return retour;
    }

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusActiviteDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @param idEvent L'identtifiant de l'{@link fr.isika.projet4.dto.EvenementDto}
     *                concerné.
     * @return Le nombre de revenus d'activité en fonction de l'événement.
     */
    @GetMapping(path = "/nb-rev-act/{idEvent}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RetourCount nbRevenuActByIdEvent(@PathVariable(name = "idEvent") int idEvent) {
        RetourCount retour = new RetourCount();
        retour.setNombreCompte(buMB.nbRevenuActByIdEvent(idEvent));
        return retour;
    }

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusSponsoringDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @param idEvent L'identifiant de l'{@link fr.isika.projet4.dto.EvenementDto}
     *                concerné.
     * @return Le nombre de revenus de sponsoring en fonction de l'événement.
     */
    @GetMapping(path = "/nb-rev-sponso/{idEvent}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RetourCount nbRevSponsoParEvenement(@PathVariable(name = "idEvent") int idEvent) {
        RetourCount retour = new RetourCount();
        retour.setNombreCompte(buMB.nbRevSponsoParEvenement(idEvent));
        return retour;
    }
}
