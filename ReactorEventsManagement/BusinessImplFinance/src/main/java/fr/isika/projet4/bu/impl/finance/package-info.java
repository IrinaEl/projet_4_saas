/**
 * Package contenant les classes concernant l'affichage des statistiques liées
 * aux finances.
 * @author stagiaire
 *
 */
package fr.isika.projet4.bu.impl.finance;