package fr.isika.projet4.bu.impl.finance;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.business.api.finance.IBusinessMembreBureau;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * Classe implémentant les méthodes de {@link IBusinessMembreBureau} permettant
 * au {@link fr.isika.projet4.dto.MembreAdministrationDto} de voir les
 * statistiques liées aux finances.
 * @author stagiaire
 *
 */
@Service
public class BusinessMembreBureau implements IBusinessMembreBureau {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * Proxy pour utiliser les méthodes du DAO.
     */
    private IDaoService proxy;

    /**
     * Constructeur par défaut instanciant le proxy grâce à
     * {@link ProxyUtils#getProxy()}.
     */
    public BusinessMembreBureau() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    @Override
    public double sommeRecetteParEvenement(int paramIdEvenement) {
        return proxy.sommeRecetteParEvenement(paramIdEvenement);
    }

    @Override
    public double sommeDepenseParEvenement(int paramIdEvenement) {
        return proxy.sommeDepenseParEvenement(paramIdEvenement);
    }

    @Override
    public Long nbRevenuActByIdEvent(int newIdEvent) {
        return proxy.nbRevenuActByIdEvent(newIdEvent);
    }

    @Override
    public long nbRevSponsoParEvenement(int paramIdEvent) {
        return proxy.nbRevSponsoParEvenement(paramIdEvent);
    }

}
