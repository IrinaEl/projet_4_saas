package fr.isika.projet4.bu.impl.finance.wrap.retour;

/**
 * Retour pour toutes les méthodes qui utilise la méthode SQL d'aggrégation
 * SUM().
 * @author stagiaire
 *
 */
public class RetourSomme {

    /**
     * La somme que retournera la méthode en question.
     */
    private Double somme;

    /**
     * @return the somme
     */
    public Double getSomme() {
        return somme;
    }

    /**
     * @param paramSomme the somme to set
     */
    public void setSomme(Double paramSomme) {
        somme = paramSomme;
    }

    /**
     * @param paramSomme la somme.
     */
    public RetourSomme(Double paramSomme) {
        super();
        somme = paramSomme;
    }

    /**
     * Constructeur par défaut.
     */
    public RetourSomme() {
        super();
    }

}
