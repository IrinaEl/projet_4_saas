package fr.isika.projet4.bu.impl.finance.wrap.retour;

/**
 * Classe englobante pour les retours REST ne contenant que des {@link Long}.
 * @author daphnemerck
 *
 */
public class RetourCount {

    /**
     * Le nombre retourné par une requête COUNT().
     */
    private long nombreCompte;

    /**
     * @return the nombreCompte
     */
    public long getNombreCompte() {
        return nombreCompte;
    }

    /**
     * @param newNombreCompte the nombreCompte to set
     */
    public void setNombreCompte(long newNombreCompte) {
        nombreCompte = newNombreCompte;
    }

}
