package fr.isika.projet4.data.api;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Interface dao inscription.
 * @author stagiaire
 *
 */
public interface IDaoInscription {

    /**
     *
     * @param paramActivite l'activité sur laquelle on s'inscrit.
     * @param paramInscription L'{@link InscriptionDto} générée.
     * @return une {@link InscriptionDto}
     * @throws InscriptionException .
     */
    InscriptionDto inscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) throws InscriptionException;
    /**
     * Interface du service pour l'inscription à un
     *  {@link EvenementDto}.
     * @param paramEvent l'{@link EvenementDto} sur lequel on veut
     * s'inscrire.
     * @param paramInscription l'{@link InscriptionDto} générée.
     * @return une {@link InscriptionDto}.
     * @throws InscriptionException .
     */
    InscriptionDto inscriptionEvenement(EvenementDto paramEvent, InscriptionDto paramInscription) throws InscriptionException;

    /**
     * Interface du service qui vérifie si une meme personne est deja inscrite sur une activité.
     * @param paramActivite l'activité à verifier.
     * @param paramInscription l'inscription à verifier.
     * @return true si déjà inscrit, false si non.
     */
    Boolean verifInscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription);

    /**
     * Interface du service qui vérifie si une meme personne est deja inscrite sur une activité.
     * @param paramEvenement l'evenement à verifier.
     * @param paramInscription l'inscription à verifier.
     * @return true si déjà inscrit, false si non.
     */
    Boolean verifInscriptionEvenement(EvenementDto paramEvenement, InscriptionDto paramInscription);
}
