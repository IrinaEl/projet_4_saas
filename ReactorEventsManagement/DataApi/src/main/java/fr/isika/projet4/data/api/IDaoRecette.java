package fr.isika.projet4.data.api;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.FluxDto;
import fr.isika.projet4.dto.InscriptionDto;

/**
 * Interface définissant les méthodes liées à la récupération des
 * {@link fr.isika.projet4.dto.RecetteDto} dans l'unité de persistance.
 * @author stagiaire
 *
 */
public interface IDaoRecette {

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.RecetteDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param idEvenement l'identifiant de
     *                    l'{@link fr.isika.projet4.dto.EvenementDto} en question.
     * @return la somme de toutes les recettes.
     */
    double sommeRecetteParEvenement(int idEvenement);


    /**
     * Récup de la recette d'une inscription.
     * @param paramInscription l'inscription dont on veut la recette.
     * @param paramActivite .
     * @param paramEvenement .
     * @return {@link FluxDto}
     */
    FluxDto recetteParInscription(InscriptionDto paramInscription, ActiviteDto paramActivite, EvenementDto paramEvenement);
}
