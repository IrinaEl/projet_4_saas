package fr.isika.projet4.data.api;

/**
 * Interface définissant les méthodes liées aux
 * {@link fr.isika.projet4.dto.RevenusActiviteDto}.
 * @author stagiaire
 *
 */
public interface IDaoRevenusActivite {

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusActiviteDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @param idEvent L'identifiant de l'{@link fr.isika.projet4.dto.EvenementDto}
     *                concerné.
     * @return Le nombre de revenus d'activité en fonction de l'événement.
     */
    Long nbRevenuActByIdEvent(int idEvent);
}
