package fr.isika.projet4.data.api;

import java.util.List;

import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * Classe déclarant le sméthodes de pesistence relative aux ressources.
 * @author stagiaire
 *
 */
public interface IDaoRessource {

    /**
     * méthode retournant une liste de bénévoles.
     * @param idTypeIntervenant idTypeIntervenant correspondant au libellé bénévole
     * @return une lsite de ressource de type intervenant externe de type bénévole
     * @throws RessourceException en cas de liste vide
     */
    List<IntervenantExterneDto> getAllBenevoles(Integer idTypeIntervenant) throws RessourceException;
    /**
     * délcaration de la méthode permettant de voir les ressources affectées à une activité.
     * @param idActivite
     * @return liste de ressource affectées
     * @throws RessourceException r
     */
    List<RessourceDto> getRessourceByActivite(Integer idActivite) throws RessourceException;
    
    List<RessourceDto> getAllRessources() throws RessourceException;

}
