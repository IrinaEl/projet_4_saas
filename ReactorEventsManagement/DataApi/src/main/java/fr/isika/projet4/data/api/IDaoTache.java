package fr.isika.projet4.data.api;

import java.util.List;

import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * définition des méthodes permettants de manipuler les {@link TacheDto}.
 * @author stagiaire
 **/
public interface IDaoTache {

    /**
     * déclaration de la méthode permettant de récupérer une liste de tâches
     * attachées à une activité.
     * @param idActivite - identifiant de l'activité
     * @return {@link TacheDto}
     * @throws TacheException en cas de liste vide.
     */
    List<TacheDto> getTachesByActivite(Integer idActivite) throws TacheException;

    /**
     * déclaration de la méthode permettant d'affecter un bénévole à une tâche.
     * @param tache    - de la tâche à laquelle le bénévoe sera affecté.
     * @param benevole - bénévole qui sera affecté à la tâche.
     * @return {@link TacheDto}
     * @throws TacheException - exception retourné si échec
     */
    TacheDto affecterBenevole(TacheDto tache, RessourceDto benevole) throws TacheException;

}
