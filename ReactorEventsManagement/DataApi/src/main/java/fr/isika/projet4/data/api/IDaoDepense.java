package fr.isika.projet4.data.api;

/**
 * Interface définissant les méthodes liées à la récupération des
 * {@link fr.isika.projet4.dto.DepenseDto} dans l'unité de persistance.
 * @author stagiaire
 *
 */
public interface IDaoDepense {

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.DepenseDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param idEvenement l'identifiant de
     *                    l'{@link fr.isika.projet4.dto.EvenementDto} en question.
     * @return la somme de toutes les depenses.
     */
    double sommeDepenseParEvenement(int idEvenement);
}
