package fr.isika.projet4.data.api;

import java.util.List;

import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * classe déclarant les méthodes de persistence relatives au dto.
 * {@link ResponsableActiviteDto}
 * @author Irina El
 *
 */
public interface IDaoResponsableActivite {

    /**
     * méthode permettant de récupérer le dto {@link ResponsableActiviteDto} à
     * partir de son identifiant.
     * @param paramIdRa - identifiant du RA recherché
     * @return un dto {@link ResponsableActiviteDto}
     * @throws RAException en cas de retour null.
     */
    ResponsableActiviteDto getRaById(Integer paramIdRa) throws RAException;

    /**
     * Méthode permet de récupérer une liste de {@link ResponsableActiviteDto}.
     * @return une liste de ResponsableActiviteDto
     * @throws RAException en cas de retour null
     */
    List<ResponsableActiviteDto> getAllResponsableActivite() throws RAException;
}
