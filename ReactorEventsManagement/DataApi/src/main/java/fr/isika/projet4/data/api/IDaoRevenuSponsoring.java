package fr.isika.projet4.data.api;

/**
 * Interface définissant les méthodes liées aux
 * {@link fr.isika.projet4.dto.RevenusSponsoringDto}.
 * @author statgiaire
 *
 */
public interface IDaoRevenuSponsoring {

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusSponsoringDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @param idEvent L'identifiant de l'{@link fr.isika.projet4.dto.EvenementDto}
     *                concerné.
     * @return Le nombre de revenus de sponsoring en fonction de l'événement.
     */
    long nbRevSponsoParEvenement(int idEvent);
}
