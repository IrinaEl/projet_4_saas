package fr.isika.projet4.data.api;

import java.util.List;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.LeadPoleDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.LeadPoleException;
import fr.isika.projet4.exceptions.RAException;

/**
 * Classe définissant les services de persistence pour l'entité
 * {@link EvenementDto}.
 * @author ludwig.
 */
public interface IDaoEvenement {

    /**
     * Méthode permettant de récupérer la liste des {@link EvenementDto} en cours
     * de création ou créés, pour un responsable d'activité.
     * @param idRA - L'id du responsable d'activité concerné.
     * @return
     *         <ul>
     *         <li>La liste des {@link EvenementDto}</li>
     *         </ul>
     * @throws RAException
     *                     <ul>
     *                     <li>Si le responsable d'activité n'est affecté à aucun
     *                     {@link EvenementDto}</li>
     *                     <li>Si l'unité de persistence est hors-service
     *                     </ul>
     */
    List<EvenementDto> getByRA(int idRA) throws RAException;

    /**
     * Méthode permettant de récupérer la liste de tout les {@link EvenementDto}
     * en cours pour le {@link fr.isika.projet4.dto.MembreAdministrationDto}.
     * @return
     *         <ul>
     *         <li>La liste des {@link EvenementDto}</li>
     *         </ul>
     * @throws EventException
     *                        <ul>
     *                        <li>S'il n'y a aucun évenement pour le moment</li>
     *                        <li>Si l'unité de persistence est hors-service</li>
     *                        </ul>
     */
    List<EvenementDto> getAll() throws EventException;

    /**
     * Méthode permettant de récupérer la liste des {@link EvenementDto} en cours
     * de création ou créés, pour un LeadPole.
     * @param leadPoleID - L'id du LeadPole concerné.
     * @return
     *         <ul>
     *         <li>La liste des {@link EvenementDto}</li>
     *         </ul>
     * @throws LeadPoleException
     *                           <ul>
     *                           <li>Si le LeadPole n'est affecté à aucun
     *                           {@link EvenementDto}</li>
     *                           <li>Si l'unité de persistence est hors-service
     *                           </ul>
     */
    List<EvenementDto> getEvenementsByLeadPoleID(int leadPoleID) throws LeadPoleException;

    /**
     * Méthode qui permet de créer un {@link EvenementDto} par un Membre de
     * l'administration.
     * @param evenement           - l'objet évènement à créer.
     * @param activite            - objet activite créé.
     * @param typeEvenement       - l'objet typeévènement à créer.
     * @param adresse             - objet activite créé.
     * @param leadPole            - l'objet évènement à créer.
     * @param sponsor             - objet activite créé.
     * @param responsableActivite - ra � creer.
     *
     * @throws EventException
     *                        <ul>
     *                        <li>Si {@link EvenementDto} existe déjà dans la
     *                        base</li>
     *                        <li>Si l'unité de persistence est hors-service</li>
     *                        </ul>
     * @return {@link EvenementDto}.
     */
    EvenementDto ajouter(EvenementDto evenement, ActiviteDto activite, TypeEvenementDto typeEvenement, AdresseDto adresse,
                         LeadPoleDto leadPole, SponsorDto sponsor, ResponsableActiviteDto responsableActivite) throws EventException;

    /**
     * Interface de la méthode permettant de récupérer les evenements par le Id.
     * @param paramId concerne l'id de l'événement.
     * @return un evenement.
     * @throws EventException si l'evenement n'existe pas.
     */
    EvenementDto getEventById(int paramId) throws EventException;

    /**
     * Méthode récupérant les {@link EvenementDto} dont la date de fin est
     * supérieure à la date du jour.
     * @return Une liste d'{@link EvenementDto}.
     * @throws EventException
     *                        <ul>
     *                        <li>S'il y'a aucun évenement pour le moment</li>
     *                        <li>Si l'unité de persistence est hors-service</li>
     *                        </ul>
     */
    List<EvenementDto> getAllDoneEvents() throws EventException;

}
