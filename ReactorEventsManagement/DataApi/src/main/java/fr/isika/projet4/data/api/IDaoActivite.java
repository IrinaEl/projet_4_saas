package fr.isika.projet4.data.api;

import java.util.List;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * définition des méthodes permettants de manipuler les {@link Activite}.
 * @author stagiaire
 **/
public interface IDaoActivite {

    /**
     * déclaration de la méthode permettant de récupérer une liste d'activités.
     * affectées à un {@link ResponsableActivite}
     * @param idRa l'identifiant du {@ResponsableActivite} dont les activités sont
     *             recherchées
     * @return une {@link List} des {@link Activite} du {@link ResponsableActivite}
     * @throws ActiviteException si le RA n'a été affecté à aucun évènement
     */
    List<ActiviteDto> getActivitesByRa(Integer idRa) throws ActiviteException;

    /**
     * Méthode permettant de récupérer la liste de tout les {@link ActviteDto} pour
     * un {@link EvenementDto}.
     * @param idEvent - L'id de l'évenmenet concerné.
     * @return
     *         <ul>
     *         <li>La liste des {@link ActiviteDto}</li>
     *         </ul>
     * @throws ActiviteException
     *                           <ul>
     *                           <li>Si l'évenement n'a pas encore une
     *                           {@link ActiviteDto}</li>
     *                           <li>Si l'unité de persistence est hors-service</li>
     *                           </ul>
     */
    List<ActiviteDto> getAllActiviteByEvent(Integer idEvent) throws ActiviteException;

    /**
     * déclaration de la méthode permettant de récupérer le dto {@link ActiviteDto}
     * par son identifiant.
     * @param idActivite - l'identifiat du dto {@link ActiviteDto} à récupérer
     * @return - un dto {@link ActiviteDto}
     * @throws ActiviteException - lorsque l'activité n'existe pas
     */
    ActiviteDto getActiviteById(Integer idActivite) throws ActiviteException;

    /**
     * Méthode permet d'ajouter un Responsable Activite à une Activite.
     * @param responsableActiviteDto responsable Dto concerné
     * @param activiteDto            une activitéDto
     * @return une nouvelle activiteDto
     * @throws ActiviteException
     *                           <ul>
     *                           <li>si ResponsableActivite existe déjà
     *                           <li>
     *                           <li>si ResponsableActiviteDto est null</li>
     *                           <li>Si l'unité de persistence est hors-service</li>
     *                           </ul>
     */
    ActiviteDto addResponsableActivity(ResponsableActiviteDto responsableActiviteDto, ActiviteDto activiteDto) throws ActiviteException;
    /**
     * déclaration de la méthode permettant d'affecter une ressource à une activité.
     * @param idRessource r
     * @param idActivite a
     * @return une activite
     * @throws ActiviteException b
     */
    ActiviteDto affecterRessource(Integer idRessource, Integer idActivite) throws ActiviteException;
}
