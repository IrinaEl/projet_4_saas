package fr.isika.projet4.business.api.front;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Classe de business des services d'inscription.
 * @author stagiaire
 *
 */
public interface IBusinessInscription {

   /**
    * Interface de service pour l'inscription à une {@link ActiviteDto}.
    * @param paramActivite l'activité sur laquelle on s'inscrit.
    * @param paramInscription l'inscription générée.
    * @return une {@link InscriptionDto}.
    * @throws InscriptionException .
    */
    InscriptionDto inscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) throws InscriptionException;
    /**
     * Interface du service pour l'inscription à un
     *  {@link EvenementDto}.
     * @param paramEvent l'{@link EvenementDto} sur lequel on veut
     * s'inscrire.
     * @param paramInscription l'{@link InscriptionDto} générée.
     * @return une {@link InscriptionDto}.
     * @throws InscriptionException .
     */
    InscriptionDto inscriptionEvenement(EvenementDto paramEvent, InscriptionDto paramInscription) throws InscriptionException;
}
