package fr.isika.projet4.business.api.front;

import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.exceptions.EventException;

public interface IBusinessConsultation {

    /**
     * @param paramId identifiant de l'événement.
     * @return un évenement.
     * @throws EventException .
     */
    public EvenementDto getEventById(int paramId) throws EventException;
}
