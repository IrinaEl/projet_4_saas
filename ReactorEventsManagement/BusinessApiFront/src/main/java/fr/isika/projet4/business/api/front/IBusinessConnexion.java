package fr.isika.projet4.business.api.front;

import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.exceptions.ConnexionException;
/**
 * Classe de Business des services de connexion.
 * @author stagiaire
 *
 */
public interface IBusinessConnexion {

    /**
     * interface methode se connecter pour le LP, MB et RA.
     * @param mail : email de l'utilisateur.
     * @param mdp  : mot de passe utilisateur.
     * @return un UtilisateurDto.
     * @throws ConnexionException .
     */
    UtilisateurDto seConnecterBack(String mail, String mdp) throws ConnexionException;
    /**
     * interface methode se connecter pour le sponsor.
     * @param mail : email sponsor.
     * @param mdp  : mot de passe du sponsor.
     * @return un SponsorDto.
     * @throws ConnexionException .
     */
    SponsorDto seConnecterFront(String mail, String mdp) throws ConnexionException;
}
