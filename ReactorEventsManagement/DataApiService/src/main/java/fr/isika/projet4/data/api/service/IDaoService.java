package fr.isika.projet4.data.api.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.FluxDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.LeadPoleDto;
import fr.isika.projet4.dto.MembreAdministrationDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.exceptions.ActiviteException;
import fr.isika.projet4.exceptions.ConnexionException;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.InscriptionException;
import fr.isika.projet4.exceptions.LeadPoleException;
import fr.isika.projet4.exceptions.RAException;
import fr.isika.projet4.exceptions.RessourceException;
import fr.isika.projet4.exceptions.TacheException;

/**
 * interface de service.
 * @author stagiaire
 */
@WebService(targetNamespace = "http://isika.projet4.com", name = "DaoService")
public interface IDaoService {

    /*******************
     * PARTIE DU FRONT *
     *******************/

    /**
     * interface methode se connecter pour le LP, MB et RA.
     * @param mail : email utilisateur.
     * @param mdp  : mot de passe utilisateur.
     * @return un UtilisateurDto.
     */
    @WebMethod(operationName = "seConnecterBack")
    @WebResult(name = "Utilisateur")
    UtilisateurDto seConnecterBack(@WebParam(name = "mailUtilisateur") String mail,
                                   @WebParam(name = "mdpUtilisateur") String mdp) throws ConnexionException;

    /**
     * interface methode se connecter pour le sponsor.
     * @param mail : email sponsor.
     * @param mdp  : mot de passe du sponsor.
     * @return un SponsorDto.
     */
    @WebMethod(operationName = "seConnecterFront")
    @WebResult(name = "Sponsor")
    SponsorDto seConnecterFront(@WebParam(name = "mailSponsor") String mail,
                                @WebParam(name = "mdpSponsor") String mdp) throws ConnexionException;

    /******************
     * PARTIE DU BACK *
     ******************/

    /**
     * Interface du service pour la méthode getByRA pour le responsable
     * d'activité.
     * @param idRa - L'{@link ResponsableActiviteDto#getId()} du
     *             {@link ResponsableActiviteDto}
     * @return - Une liste d'{@link EvenementDto}
     * @throws RAException - Exception levée si :
     *                     <ul>
     *                     <li>La liste est vide, quand le
     *                     {@link ResponsableActiviteDto} n'est affecté à aucun
     *                     {@link EvenementDto}.</li>
     *                     <li>Le serveur de données est hors-service</li>
     *                     </ul>
     */
    @WebMethod(operationName = "getByRA")
    @WebResult(name = "Evenement")
    List<EvenementDto> getByRA(@WebParam(name = "idResponsableActivite") int idRa) throws RAException;

    /**
     * Interface du service permettant de récupérer une liste d'activités
     * affectées à un RA.
     * @param idRa - l'identifiant du RA dont les activités sont recherchées
     * @return - une liste d'{@link ActiviteDto} affectées à un
     *         {@link ResponsableActiviteDto}
     * @throws ActiviteException lorsque :
     *                           <ul>
     *                           <li>le {@link ResponsableActiviteDto} n'a aucune
     *                           activités qui lui est affectée</li>
     *                           <li>le serveur des données n'est pas
     *                           accessible</li>
     *                           </ul>
     */
    @WebMethod(operationName = "getActiviteByRa")
    @WebResult(name = "Activites")
    List<ActiviteDto> getActivitesByRa(@WebParam(name = "idResponsableActivite") Integer idRa) throws ActiviteException;

    /**
     * Interface du service pour la méthode getAllEvent pour le
     * {@link MembreAdministrationDto}.
     * @return
     *         <ul>
     *         <li>La liste des {@link EvenementDto}</li>
     *         </ul>
     * @throws EventException
     *                        <ul>
     *                        <li>S'il y'a aucun évenement pour le moment</li>
     *                        <li>Si l'unité de persistence est hors-service</li>
     *                        </ul>
     */
    @WebMethod(operationName = "getAllEvent")
    @WebResult(name = "Evenements")
    List<EvenementDto> getAllEvent() throws EventException;

    /**
     * Interface du service pour la méthode getActiviteByEvent pour un
     * {@link EvenementDto}.
     * @param idEvent - L'id de l'évenmenet concerné.
     * @return
     *         <ul>
     *         <li>La liste des {@link ActiviteDto}</li>
     *         </ul>
     * @throws ActiviteException
     *                           <ul>
     *                           <li>Si l'évenement n'a pas encore une
     *                           {@link ActiviteDto}</li>
     *                           <li>Si l'unité de persistence est
     *                           hors-service</li>
     *                           </ul>
     */
    @WebMethod(operationName = "getAllActiviteByEvent")
    @WebResult(name = "ActivitesByEvent")
    List<ActiviteDto> getAllActiviteByEvent(@WebParam(name = "idEvenement") Integer idEvent) throws ActiviteException;

    /**
     * déclaration du service permettant de récupérer le dto
     * {@link ResponsableActiviteDto} par son identifiant.
     * @param idRa - l'identifiant du {@link ResponsableActiviteDto} recherché
     * @return le dto {@link ResponsableActiviteDto} recherché
     * @throws ResponsableActiviteException exception
     */
    @WebMethod(operationName = "getRaById")
    @WebResult(name = "ResponsableActivite")
    ResponsableActiviteDto getRaById(@WebParam(name = "idRa") Integer idRa) throws RAException;

    /**
     * exposition du service permettant de récupérer l'activité par son
     * identifiant.
     * @param idActivite - l'identifiant de l'activité à récupérer
     * @return le dto {@link ActiviteDto}
     * @throws ActiviteException - si l'activité recherchée n'existe pas
     */
    @WebMethod(operationName = "getActiviteById")
    @WebResult(name = "Activite")
    ActiviteDto getActiviteById(@WebParam(name = "idActivite") Integer idActivite) throws ActiviteException;

    @WebMethod(operationName = "getTachesByActivite")
    @WebResult(name = "Taches")
    List<TacheDto> getTachesByActivite(@WebParam(name = "idActivite") Integer idActivite) throws TacheException;

    @WebMethod(operationName = "affecterBenevole")
    @WebResult(name = "Tache")
    TacheDto affecterBenevole(TacheDto tache, RessourceDto benevole) throws TacheException;

    @WebMethod(operationName = "getEvenementsByLeadPoleID")
    @WebResult(name = "EvenementsByLeadPole")
    List<EvenementDto> getEvenementsByLeadPoleID(@WebParam(name = "leadPoleID") int leadPoleID) throws LeadPoleException;

    @WebMethod(operationName = "getAllResponsableActivite")
    @WebResult(name = "ResponsableActivites")
    List<ResponsableActiviteDto> getAllResponsableActivite() throws RAException;

    @WebMethod(operationName = "ajouter")
    @WebResult(name = "Evenement")
    EvenementDto
            ajouter(
                    @WebParam(name = "paramEvenement") EvenementDto paramEvenement,
                    @WebParam(name = "paramActivite") ActiviteDto paramActivite,
                    @WebParam(name = "paramTypeEvenement") TypeEvenementDto paramTypeEvenement,
                    @WebParam(name = "paramAdresse") AdresseDto paramAdresse,
                    @WebParam(name = "paramLeadPole") LeadPoleDto paramLeadPole,
                    @WebParam(name = "paramSponsor") SponsorDto paramSponsor) throws EventException;

    @WebMethod(operationName = "getAllBenevoles")
    @WebResult(name = "Benevoles")
    List<IntervenantExterneDto> getAllBenevoles(Integer idTypeIntervenant) throws RessourceException;

    @WebMethod(operationName = "addResponsableActivity")
    @WebResult(name = "activiteAvecRa")
    ActiviteDto addResponsableActivity(ResponsableActiviteDto responsableActiviteDto, ActiviteDto activiteDto) throws ActiviteException;

    /**
     * Interface du service pour l'inscription a une {@link ActiviteDto}.
     * @param paramActivite    l'{@link ActiviteDto} sur laquelle on souhaite
     *                         s'inscrire.
     * @param paramInscription l'{@link InscriptionDto} générée.
     * @return une {@link InscriptionDto}.
     * @throws InscriptionException .
     */
    @WebMethod(operationName = "inscriptionActivite")
    @WebResult(name = "InscriptionActivite")
    InscriptionDto inscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) throws InscriptionException;

    /**
     * Interface du service pour l'inscription à un {@link EvenementDto}.
     * @param paramEvent       l'{@link EvenementDto} sur lequel on veut s'inscrire.
     * @param paramInscription l'{@link InscriptionDto} générée.
     * @return une {@link InscriptionDto}.
     * @throws InscriptionException .
     */
    @WebMethod(operationName = "inscriptionEvenement")
    @WebResult(name = "InscriptionEvenement")
    InscriptionDto inscriptionEvenement(EvenementDto paramEvent, InscriptionDto paramInscription) throws InscriptionException;

    /**
     * Interface du service qui vérifie si une meme personne est deja inscrite sur
     * une {@link ActiviteDto}.
     * @param paramActivite    l'activité à verifier.
     * @param paramInscription l'inscription à verifier.
     * @return true si déjà inscrit, false si non.
     */
    @WebMethod(operationName = "verifInscriptionActivite")
    @WebResult(name = "RetourVerificationAct")
    Boolean verifInscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription);

    /**
     * Interface du service qui vérifie si une meme personne est deja inscrite sur
     * une activité.
     * @param paramEvenement   l'evenement à verifier.
     * @param paramInscription l'inscription à verifier.
     * @return true si déjà inscrit, false si non.
     */
    @WebMethod(operationName = "verifInscriptionEvenement")
    @WebResult(name = "RetourVerificationEvent")
    Boolean verifInscriptionEvenement(EvenementDto paramEvenement, InscriptionDto paramInscription);

    @WebMethod(operationName = "getEventById")
    @WebResult(name = "Evenement")
    public EvenementDto getEventById(@WebParam(name = "idEvent") int paramId) throws EventException;

    /**
     * Récup de la recette d'une inscription.
     * @param paramInscription l'inscription dont on veut la recette.
     * @param paramActivite    .
     * @param paramEvenement   .
     * @return {@link FluxDto}
     */
    @WebMethod(operationName = "recetteParInscription")
    @WebResult(name = "RecetteInscription")
    FluxDto recetteParInscription(InscriptionDto paramInscription, ActiviteDto paramActivite, EvenementDto paramEvenement);

    /***********************
     * PARTIE DES FINANCES *
     ***********************/

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.RecetteDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param idEvenement l'identifiant de
     *                    l'{@link fr.isika.projet4.dto.EvenementDto} en question.
     * @return la somme de toutes les recettes.
     */
    @WebMethod(operationName = "sommeRecetteParEvenement")
    @WebResult(name = "SommeRecettesParEvenement")
    double sommeRecetteParEvenement(@WebParam(name = "idEvenement") int idEvenement);

    /**
     * Récupération et addition de toutes les
     * {@link fr.isika.projet4.dto.DepenseDto} liées à un
     * {@link fr.isika.projet4.dto.EvenementDto}.
     * @param idEvenement l'identifiant de
     *                    l'{@link fr.isika.projet4.dto.EvenementDto} en question.
     * @return la somme de toutes les depenses.
     */
    @WebMethod(operationName = "sommeDepenseParEvenement")
    @WebResult(name = "SommeDepensesParEvenement")
    double sommeDepenseParEvenement(@WebParam(name = "idEvenement") int idEvenement);

    /**
     * Méthode récupérant les {@link EvenementDto} dont la date de fin est
     * supérieure à la date du jour.
     * @return Une liste d'{@link EvenementDto}.
     * @throws EventException
     *                        <ul>
     *                        <li>S'il y'a aucun évenement pour le moment</li>
     *                        <li>Si l'unité de persistence est hors-service</li>
     *                        </ul>
     */
    List<EvenementDto> getAllDoneEvents() throws EventException;

    /**
     * Exposition de la méthode affectant une ressource à un activité.
     * @param idRessource g
     * @param idActivite  g
     * @return g
     * @throws ActiviteException g
     */
    @WebMethod(operationName = "affecterRessource")
    @WebResult(name = "Activite")
    ActiviteDto affecterRessource(Integer idRessource, Integer idActivite) throws ActiviteException;

    /**
     * Exposition de la méthode permettat de récupérer les ressources d'une
     * activité.
     * @param idActivite h
     * @return j
     * @throws RessourceException h
     */
    @WebMethod(operationName = "getRessourcesById")
    @WebResult(name = "Ressources")
    List<RessourceDto> getRessourcesByActivite(@WebParam(name = "idActivite") Integer idActivite) throws RessourceException;

    @WebMethod(operationName = "getAllRessources")
    @WebResult(name = "Ressources")
    List<RessourceDto> getAllRessources() throws RessourceException;

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusActiviteDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @return Le nombre de revenus d'activité en fonction de l'événement.
     */
    Long nbRevenuActByIdEvent(int idEvent);

    /**
     * Méthodes qui compte le nombre de
     * {@link fr.isika.projet4.dto.RevenusSponsoringDto} en fonction d'un
     * {@link fr.isika.projet4.dto.EvenementDto} donné.
     * @param idEvent L'identifiant de l'{@link fr.isika.projet4.dto.EvenementDto}
     *                concerné.
     * @return Le nombre de revenus de sponsoring en fonction de l'événement.
     */
    long nbRevSponsoParEvenement(int idEvent);

}
