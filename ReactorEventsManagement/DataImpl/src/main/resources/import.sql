-- -------------------------------------------
-- PARTIE ADRESSE ET SES D�PENDENCES
-- -------------------------------------------
INSERT INTO `pays` (`pays_id`, `pays_libelle`) VALUES (NULL, 'France'), (NULL, 'Espagne'), (NULL, 'Maroc'), (NULL, 'Tunisie'), (NULL, 'Etats-Unis'), (NULL, 'Belgique'), (NULL, 'Suisse'), (NULL, 'Portugal'), (NULL, 'Madagascar'), (NULL, 'Chine');
INSERT INTO `ville` (`ville_id`, `ville_libelle`, `ville_pays_id`) VALUES (NULL, 'Paris', '1'), (NULL, 'Madrid', '2'), (NULL, 'Marrakech', '3'), (NULL, 'New york', '5'), (NULL, 'Bruxelles', '6'), (NULL, 'Tana', '9'), (NULL, 'Lisbonne', '8'), (NULL, 'Pekin', '10'), (NULL, 'Shanghai', '10'), (NULL, 'Berne', '7');
INSERT INTO `code_postal` (`code_postal_id`, `code_postal_numero`) VALUES (NULL, '75'), (NULL, '36CX'), (NULL, '2B'), (NULL, '236'), (NULL, '456'), (NULL, '98'), (NULL, 'A63'), (NULL, '10N'), (NULL, '16'), (NULL, 'AX3');
INSERT INTO `type_voie` (`type_voie_id`, `type_voie_libelle`) VALUES(NULL, 'rue'), (NULL, 'chemin'), (NULL, 'impasse'), (NULL, 'avenue'), (NULL, 'boulevard'), (NULL, 'intersection'), (NULL, 'hameau'), (NULL, 'sentier');
INSERT INTO `adresse` (`adresse_id`, `adresse_nom_voie`, `adresse_numero_voie`, `adresse_type_voie_id`, `adresse_ville_id`) VALUES (NULL, 'Marechal ferrand', '6', '5', '1'), (NULL, '5th', '5', '1', '4'), (NULL, 'Gringo mas', '36', '3', '2'), (NULL, 'General leclerc', '12', '4', '1'), (NULL, 'Serre-rocheuse', '2', '7', '5'), (NULL, 'Tarrides', '87', '2', '3'), (NULL, 'Mosolee', '3bis', '6', '10'), (NULL, 'Thunder Bluff', '13', '2', '6');

-- --------------------------------------------
-- PARTIE �V�NEMENT ET SES D�PENDENCES
-- --------------------------------------------
INSERT INTO type_evenement (type_evenement_libelle) VALUES ('Majeur'), ('Mineur');
INSERT INTO `evenement` (`evenement_budget`, `evenement_date_debut`, `evenement_date_fin`, `evenement_description`, `evenement_intitule`, `evenement_prix`, `evenement_adresse_id`, `evenement_type_evenment_id`) VALUES (50000, '2020-01-16 20:00:00', '2020-01-16 23:30:00', 'Concert promotionnel d\'artistes montants de la region de Malakoff', 'Concert promotionnel Isika', 30, 8, 1), (1000, '2019-12-31 00:00:00', '2020-01-01 04:00:00', 'Soir�e du jour de l\'an � Isika ! Boissons, snacks, dancefloor dans la grande salle, blind-tests musicaux ! Alcool � volont� selon les limites du stock disponible ...', 'Soir�e jour de l\'an Isika', 10, 4, 1), (15000, '2019-12-02 19:00:00', '2019-12-02 23:00:00', 'Cocktail party d\'accueil pour la promotion CDI05. Champagne et foie gras, musicien de jazz et piste de danse. Tenue correcte exig�e.', 'Cocktail d\'accueil de la promo CDI05', 20, 7, 2), (100000, '2020-02-14 00:00:00', '2020-02-15 00:00:00', 'Soir�e pour fêter la fin de promo AL3 ! Venez nombreux !', 'Pot de fin AL03 Isika', 10, 6, 2), (500, '2020-02-14 12:30:00', '2020-02-14 17:00:00', 'Venez nombreux cet apr�s-midi de c�l�bration des amoureux ! ', 'C�l�bration de la St-Valentin', 0, 6, 1);

-- --------------------------------------------
-- PARTIE UTILISATEUR ET SES ENFANTS
-- --------------------------------------------
INSERT INTO `utilisateur` (`utilisateur_mail`, `utilisateur_mdp`, `utilisateur_nom`, `utilisateur_prenom`) VALUES ('jamespat@gmail.com', 'james', 'StPatrick', 'James'), ('sarahbasquez@yahoo.fr', 'arah', 'Basquez', 'Sarah'), ('von-damien@gmail.com', 'von', 'Von', 'Damien'), ('kastel-stephane@wanadoo.fr', 'kastel', 'Kastel', 'Stephane'), ('joris-boutier@gmail.com', 'joris', 'Boutier', 'Joris'), ('bellir-gaultier@mail.fr', 'bellir', 'Bellir', 'Gaultier'), ('paul-paul@paul.com', 'paul', 'Paul', 'Paul'), ('Jeanlahorde@wow.fr', 'horde', 'LaHorde', 'Jean'), ('luc-tirisfal@wow.fr', 'tirisfal', 'Tirisfal', 'Luc'), ('pierre-orgrimmar@wow.fr', 'orgrimmar', 'Orgrimmar', 'Pierre'), ('louis-leglas@start.fr', 'leglas', 'LeGlas', 'Louis'), ('michel-gnome@wow.fr', 'gnome', 'Gnome', 'Michel'), ('valentin-mage@wow.fr', 'mage', 'Mage', 'Valentin'), ('vanessa-novagivre@hotmail.fr', 'vanessa', 'Nova', 'Vanessa'), ('ludwig-llofler@hotmail.fr', 'ludwig', 'LLofler', 'Ludwig'), ('daphne-lemoine@hotmail.fr', 'daphne', 'lemoine', 'Daphne'), ('seb-chop@hotmail.fr', 'sebastien', 'chopin', 'Sebastien'), ('soumaya-hammami@hotmail.fr', 'soumaya', 'Hammami', 'Soumaya'), ('thomas.gibson@hotmail.fr', 'thomas', 'gibson', 'Thomas');
INSERT INTO `sponsor` (`sponsor_logo`, `sponsor_raison_sociale`, `utilisateur_id`) VALUES ('', NULL, '1'), ('', NULL, '2'), ('', NULL, '3'), ('', NULL, '4'), ('', NULL, '5');
INSERT INTO `membre_administration` (`utilisateur_id`) VALUES ('6'), ('7'), ('8'), ('9'), ('10');
INSERT INTO `responsable_activite` (`utilisateur_id`) VALUES ('11'), ('12'), ('13'), ('14');
INSERT INTO `lead_pole` (`utilisateur_id`) VALUES ('15'), ('16'), ('17'), ('18'), ('19');

-- --------------------------------------------
-- ACTIVIT� ET SES D�PENDENCES
-- --------------------------------------------
INSERT INTO type_activite (type_activite_description, type_activite_intitule) VALUES ('', 'Conf�rence'), ('', 'Buvette'), ('', 'Concert'), ('', 'Pot'), ('', 'Jeu'), ('', 'Billetterie');
INSERT INTO `activite` (`activite_budget`, `activite_date_debut`, `activite_date_fin`, `activite_description`, `activite_intitule`, `activite_nombre_place`, `activite_prix`, `id_evenement_activite`, `id_responsable_activite_activite`, `id_type_activite_activite`) VALUES ('200', '2019-11-25 00:00:00', '2019-11-26 00:00:00', 'activite de test', 'Prestation du groupe HQM', '20', '0.0', '1', '12', 3), ('150', '2019-11-25 00:00:00', '2019-11-26 00:00:00', 'activite de test 2', 'Atelier peinture', '20', '10.0', '2', '13', 2), ('2000', '2019-12-31 00:00:00', '2020-01-01 04:00:00', 'Profitez d\'un ap�ritif � volont� comprenant boissons et plats de toutes sortes', 'Buvette', '20', '10.0', '2', '11', 2), ('2000', '2019-12-31 00:00:00', '2019-11-26 00:00:00', 'Review de l\'ann�e 2019 en compagnie d\'un orchestre ind�pendant', 'Conf�rence', '20', '10.0', '2', '11', 1), ('50', '2020-02-14 13:00:00', '2020-02-14 17:00:00', 'On vous bande les yeux, et on vous l�che dans un labyrinthe, mais n\'ayez crainte, votre partenaire est l� pour vous guider et vous amener � la sortie !', 'Le sombre labyrinthe', '10', '0.0', '5', '11', 5), ('0', '2020-02-14 13:00:00', '2020-02-14 17:00:00', 'Pour g�rer les entr�es � l\'�v�nement', 'Billetterie d\'entr�e', '50', '0.0', '5', '11', 6), ('200', '2019-02-14 13:00:00', '2019-02-14 17:00:00', 'Venez jouez aux devinettes ! Tabou, mime et autres charades vous seront propos�s. Venez � bout de ces d�fis en �quipe et gagnez d\'adorables peluches !', 'Devinette', '30', '0.0', '5', '11', 5);

-- ---------------------------------------------
-- RESSOURCE, SES ENFANTS ET LEURS D�PENDENCES
-- ---------------------------------------------
INSERT INTO `ressource` (`ressource_mail`, `ressource_numero_rcs`, `ressource_numero_telephone`, `ressource_prix`, `ressource_quantite`, `ressource_raison_social`) VALUES ('abg-socle@gmail.com', 'CRC563', '0177896352', '350', '40', 'ABG-industries'),('prestataire@gmail.com', 'CRC563', '0177896352', '350', '0', 'Prestataire & C0'), ('nouveau_monde@gmail.com', 'CRC563', '0177896352', '120', '0', 'Nouveau Monde Compagnie'), ('j.bernand@gmail.com', 'CRC563', '0177896352', '70', '0', 'J�r�me Bernard Micro-entreprise'), ('sarl@gmail.com', 'CRC563', '0177896352', '200', '0', 'SARL Trois �toiles'), ('vonglix-studios@wanadoo.fr', 'CRC2563', '0756321456', '28', '5', 'VONBLIX-studiosParis'),('Michel.b@gmail.com', NULL, '0632569878', '30.00', '1', 'Michel-bezier.org'), ('secure-postol@hotmail.fr', 'CRC5789', '0456879865', '20', '30', 'SECURE-postol Industries'),  ('mail@yahoo.fr', null, '0565323134', 0, 0, 'Justine Duchamps'), ('mail@yahoo.fr', null, '0565323134', 0, 0, 'Youssef Bardi'), ('mail@yahoo.fr', null, '0565323134', 0, 0, 'Taystee Jefferson'), ('mail@yahoo.fr', 0, '0565323134', 0, 0, 'Alexandra Le Du'), ('marie-bidaz@yahoo.fr', NULL, '0632252145', '35', '3', 'photArtDexios'), ('dexios-consulting@gmail.com', 'CRC5321', '0458253625', '150', '200', 'Dexios-Consulting'),('alefabarea@gmail.com', 'CDM2022', '0752312546', '85', '130', 'BareaIndustries'), ('pinet-d@yahoo.fr', 'CTR2632', '0565323134', '450', '80', 'M.Pinet entreprise'), ('monmail@yahoo.fr', 'CTR2632', '0565323134', '450', '80', 'Mat�riel Event'), ('mail@yahoo.fr', 'CTR2632', '0565323134', '45', '6', 'Sono Ev�nement'), ('mail@yahoo.fr', 'CTR2632', '0565323134', '60', '20', 'Mat�riel & CO'), ('bus@yahoo.fr', 'CTR2632', '0565323134', '450', '80', 'Transport Event');
INSERT INTO `type_intervenant` (`type_intervenant_description`, `type_intervenant_intitule`) VALUES (NULL, 'Conferencier'), (NULL, 'Prestataire buvette'), (NULL, 'Paneliste'), (NULL, 'Formateur'), (NULL, 'Prestataire securite'), (NULL, 'Prestataire transport'), (NULL, 'Prestataire logistique'), (NULL, 'Prestataire restauration'), (NULL, 'Assistant de galerie'), (NULL, 'B�n�vole');
INSERT INTO `intervenant_externe` (`ressource_id`, `intervenant_externe_type_intervenant_id`) VALUES ('1', '2'),('2', '3'),('3', '1'),('4', '4'), ('9', '10'), ('10', '10'), ('11', '10'), ('12', '10');
INSERT INTO `materiel` (`materiel_intitule`, `ressource_id`) VALUES ('video projecteur', '13'), ('panneau de projection', '14'),('pc projection', '15'), ('Bus 50 places', '16'),('barrieres securite', '17'), ('kit buvette', '18'),('cadres photo', '19'), ('Kit sono', '20');

-- --------------------------------------------
-- T�CHE ET SES D�PENDENCES
-- --------------------------------------------
INSERT INTO statut_tache (statut_tache_intitule) VALUES ('EN COURS'),('TERMIN�E');
INSERT INTO type_tache (type_tache_description, type_tache_intitule) VALUES ('', 'R�server une salle'), ('', 'Commander de l\'eau'), ('','Commander des softs'), ('', 'Commander des alcools'), ('', 'Trouver un traiteur'), ('', 'Trouver un conf�rencier'), ('', 'Solliciter des sponsors'), ('', 'Louer des si�ges');
INSERT INTO tache (tache_deadline, id_benevole, id_statut_tache, id_type_tache_tache) VALUES (120, null, 1, 1), (240, null, 1, 1), (7, null, 1, 2), (3, null, 1, 2), (14, null, 1, 3), (21, null, 1, 3), (30, null, 1, 4), (60, null, 1, 4), (120, null, 1, 5), (180, null, 1, 5), (240, null, 1, 6), (145, null, 1, 6), (30, null, 1, 7), (240, null, 1, 8), (4, null, 1, 8), (200, 8, 1, 1), (6, null, 1, 2), (15, 7, 2, 3), (12, null, 1, 1), (5, 9, 1, 2), (3, null, 1, 3), (20, 10, 2, 4), (8, null, 1, 5), (10, 11, 2, 7);



-- --------------------------------------------
-- FLUX, SES ENFANTS ET LEURS D�PENDENCES
-- --------------------------------------------
INSERT INTO facture (facture_date_creation, facture_delai_paiment, facture_numero) VALUES ('2019-11-25 00:00:00', NULL, 123456789), ('2019-11-25 00:00:00', 30, 0987654321), ('2019-11-25 00:00:00', 45, 789654562123), ('2019-11-25 00:00:00', NULL, 321988523), ('2019-11-25 09:55:41', NULL, 87643865198);
INSERT INTO flux (flux_montant, flux_activite_id, flux_evenement_id, flux_facture_id) VALUES (30, NULL, 1, 1), (30, NULL, 1, 4), (2500, NULL, 1, 2), (534.75, 2, 2, 3), (25000, NULL, 5, 5), (5, NULL, 5, NULL), (5, NULL, 5, NULL), (5, NULL, 5, NULL), (150, 7, 5, NULL), (1350, NULL, 2, NULL), (3650, NULL, 2, NULL), (1250, NULL, 2, NULL), (7550, NULL, 1, NULL), (7550, NULL, 4, NULL), (550, NULL, 4, NULL), (7550, NULL, 4, NULL), (7520, NULL, 4, NULL), (200, NULL, 3, NULL), (2560, NULL, 3, NULL), (1230, NULL, 3, NULL);
INSERT INTO recette (recette_date_entree, flux_id) VALUES ('2019-11-25 09:55:41', 1), ('2019-11-25 18:35:26', 2), ('2019-08-25 18:35:26', 5), ('2019-01-14 18:35:26', 6), ('2019-02-02 19:57:23', 7), ('2019-01-18 15:26:35', 8), ('2019-01-18 15:26:35', 9), ('2019-02-18 15:26:35', 10), ('2019-01-18 15:26:35', 11), ('2019-06-18 15:26:35', 12), ('2019-06-18 15:26:35', 13), ('2019-06-18 15:26:35', 14), ('2019-06-18 15:26:35', 15), ('2019-06-18 15:26:35', 16), ('2019-06-18 15:26:35', 17), ('2019-06-18 15:26:35', 18), ('2019-06-18 15:26:35', 19), ('2019-06-18 15:26:35', 20);
INSERT INTO depense (depense_date_sortie, flux_id, depense_ressource_id) VALUES ('2019-01-25 09:55:41', 3, 4), ('2019-11-29 09:55:41', 4, 5), ('2019-02-01 11:35:26', 9, 2);
INSERT INTO revenus_activite (flux_id) VALUES (1), (2), (6), (7), (8), (9), (12), (16), (17), (18), (20);
INSERT INTO revenus_sponsoring (flux_id, revenus_sponsoring_sponsor) VALUES (5, 1), (10, 1), (11,1), (13,1), (14,1), (15,1), (19, 1);


-- Manque les dons, � voir plus tard.

-- ---------------------------------------------
-- INSCRIPTION
-- ---------------------------------------------
INSERT INTO inscription (inscription_date_naissance, inscription_mail, inscription_montant_regle, inscription_nom, inscription_prenom, inscription_evenement_id) VALUES ('1993-11-25 00:00:00', 'samantha.north@gmail.com', 30, 'North', 'Samantha', 1), ('2005-11-25 00:00:00', 'ryan.bertho@gmail.com', 30, 'Bertho', 'Ryan', 1), ('2019-01-14 18:35:26', 'ryan.bertho@gmail.com', 5, 'Bertho', 'Ryan', 5), ('2019-02-02 19:57:23', 'valerie.balard@gmail.com', 5, 'Balard', 'Val�rie', 5), ('2019-01-18 15:26:35', 'corinne.petit@gmail.com', 5, 'Petit', 'Corinne', 5);

-- ---------------------------------------------
-- TABLES D'ASSOCIATION
-- ---------------------------------------------
-- ADRESSE
INSERT INTO `adresses_ressources` (`adresses_ressources_ressources_id`, `adresses_ressources_adresse_id`) VALUES ('1', '2'), ('7', '8'), ('2', '4'), ('3', '1'), ('4', '3'), ('8', '6'), ('6', '7'), ('5', '5');
INSERT INTO adresse_evenement (adresse_adresse_id, evenements_evenement_id) VALUES (1, 1), (2, 2), (3, 3);

-- �V�NEMENT
INSERT INTO evenement_leadpole (evenement_leadpole_id, leadpole_evenement_id) VALUES (1, 15), (2, 15), (3, 15), (4, 15), (5, 15);
INSERT INTO evenement_ressource (evenement_ressource_ressource_id, evenement_ressource_evenement_id) VALUES (1, 4);
INSERT INTO evenement_sponsor (evenement_sponsor_sponsor_id, evenement_sponsor_evenement_id) VALUES (1, 1);

-- ACTIVIT�
INSERT INTO activite_inscription (activite_inscription_inscription_id, activite_inscription_activite_id) VALUES (1, 1), (2, 1);
INSERT INTO activite_ressource (activite_ressource_ressource_id, activite_ressource_activite_id) VALUES (2, 2), (13, 5),(14, 5), (15, 5), (16, 5);

-- TYPE ACTIVIT�
INSERT INTO type_activite_tache (id_tache, id_type_activite) VALUES (2, 3), (13, 3), (16, 2), (17, 2), (18, 2), (1,1), (19, 5), (20, 5), (21, 5), (22, 5), (23, 5), (24, 5);