package fr.isika.projet4.data.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.projet4.data.api.IDaoInscription;
import fr.isika.projet4.data.assembler.DtoToEntity;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.entity.Activite;
import fr.isika.projet4.entity.Evenement;
import fr.isika.projet4.entity.Inscription;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Classe d'implementation de l'api {@link IDaoInscription}.
 * @author stagiaire
 *
 */
@Repository
@Transactional(rollbackFor = InscriptionException.class)
public class DaoInscription extends DAO implements IDaoInscription {

    /**
     * Pour les log.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * implementation methode inscription evenement.
     * @throws InscriptionException .
     */
    @Override
    public InscriptionDto inscriptionEvenement(EvenementDto paramEvent, InscriptionDto paramInscription) throws InscriptionException {
        Inscription inscription = DtoToEntity.transformDtoToEntity(paramInscription);
        try {
            Evenement event = getEntityManager().find(Evenement.class, paramEvent.getId());
            inscription.setEvenement(event);
            getEntityManager().persist(inscription);

        } catch (Exception e) {
            log.fatal(e.getMessage(), e);
            throw new InscriptionException("contraintes", null);
        }
        return EntityToDto.transformEntityToDto(inscription);
    }

    /**
     * Implementation methode inscriptionActivite.
     * @throws InscriptionException .
     */
    @Override
    public InscriptionDto inscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) throws InscriptionException {
        Inscription inscription = DtoToEntity.transformDtoToEntity(paramInscription);
        try {
            Activite act = getEntityManager().find(Activite.class, paramActivite.getId());
            inscription.setActivites(new ArrayList<>());
            inscription.setEvenement(act.getEvenement());
            getEntityManager().persist(inscription);
            act.getInscriptions().add(inscription);
            getEntityManager().merge(act);
        } catch (Exception e) {
            log.fatal(e.getMessage(), e);
            throw new InscriptionException("contraintes", null);
        }
        return EntityToDto.transformEntityToDto(inscription);
    }

    /**
     * Methode qui vérifie si une meme personne est deja inscrite sur une
     * activité.
     * @param paramActivite    l'activité à verifier.
     * @param paramInscription l'inscription à verifier.
     * @return true si déjà inscrit, false si non.
     */
    @Override
    public Boolean verifInscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) {
        Activite entity = getEntityManager().find(Activite.class, paramActivite.getId());
        List<Inscription> listeInscriptions = entity.getInscriptions();
        for (int i = 0; i < listeInscriptions.size(); i++) {
            if (listeInscriptions.get(i).getMail().equals(paramInscription.getMail())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Methode qui verifie si une meme personne est deja inscrite sur un evenement.
     * @param paramEvenement   l'evenement a verifier.
     * @param paramInscription l'incription a verifier.
     * @return true si déjà inscrit, false si non.
     */
    @Override
    public Boolean verifInscriptionEvenement(EvenementDto paramEvenement, InscriptionDto paramInscription) {
        Evenement entity = getEntityManager().find(Evenement.class, paramEvenement.getId());
        List<Inscription> listeInscriptions = entity.getInscriptions();
        for (int i = 0; i < listeInscriptions.size(); i++) {
            if (listeInscriptions.get(i).getMail().equals(paramInscription.getMail())) {
                return true;
            }
        }
        return false;
    }
}
