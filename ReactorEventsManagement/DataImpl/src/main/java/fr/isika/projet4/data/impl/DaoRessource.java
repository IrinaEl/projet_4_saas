package fr.isika.projet4.data.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.isika.projet4.data.api.IDaoRessource;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.entity.IntervenantExterne;
import fr.isika.projet4.entity.Ressource;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe impléméntant les méthodes de persistence relative aux ressources.
 * @author stagiaire
 *
 */
@Service
public class DaoRessource extends DAO implements IDaoRessource {

    @Override
    public List<IntervenantExterneDto> getAllBenevoles(Integer paramIdTypeIntervenant) throws RessourceException {

        List<IntervenantExterneDto> listeDto = new ArrayList<>();

        List<IntervenantExterne> liste = getEntityManager()
                .createQuery("SELECT i FROM IntervenantExterne i WHERE i.typeIntervenant.id = :pidTypeIntervenant",
                        IntervenantExterne.class)
                .setParameter("pidTypeIntervenant", paramIdTypeIntervenant).getResultList();
        for (IntervenantExterne i : liste) {
            listeDto.add(EntityToDto.transformEntityToDto(i));
        }

        return listeDto;
    }

    @Override
    public List<RessourceDto> getRessourceByActivite(Integer paramIdActivite) throws RessourceException {
        List<RessourceDto> retour = new ArrayList<>();
        @SuppressWarnings("unchecked")
        List<Ressource> liste = getEntityManager().createQuery("SELECT a.ressources FROM Activite a WHERE a.id = :pidActivite").setParameter("pidActivite", paramIdActivite).getResultList();
        
        
        for (Ressource r : liste) {
          
            retour.add(EntityToDto.transformEntityToDto(r));
        }
        if (retour.isEmpty()) {
            throw new RessourceException("Aucun prestataire n'a été affecté à cette activité", null);
        }
        return retour;
    }

    @Override
    public List<RessourceDto> getAllRessources() throws RessourceException {
        @SuppressWarnings("unchecked")
        List<Ressource> liste = getEntityManager().createQuery("SELECT r FROM Ressource r").getResultList();
        List<RessourceDto> retour = new ArrayList<>();
        for (Ressource r : liste) {
            retour.add(EntityToDto.transformEntityToDto(r));
        }
        return retour;
    }

}
