package fr.isika.projet4.data.impl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.projet4.data.api.IDaoRevenuSponsoring;

/**
 * Classe implémentant les méthodes de {@link IDaoRevenuSponsoring}.
 * @author stagiaire
 *
 */
@Repository
@Transactional
public class DaoRevenuSponsoring extends DAO implements IDaoRevenuSponsoring {

    private Logger log = Logger.getLogger(getClass());

    /**
     * Requête JPQL pour compter le nombre de
     * {@link fr.isika.projet4.entity.RevenusSponsoring} par
     * {@link fr.isika.projet4.entity.Evenement}.
     */
    private static final String REQ_COUNT = "SELECT COUNT(*) FROM RevenusSponsoring rs WHERE rs.evenement.id = ?1";

    /**
     * Position du paramètre de l'id pour la requête {@link #REQ_COUNT}.
     */
    private static final int PARAM_ID_REQ_COUNT = 1;

    @Override
    public long nbRevSponsoParEvenement(int paramIdEvent) {
        log.debug(getEntityManager().createQuery(REQ_COUNT, Long.class)
                .setParameter(PARAM_ID_REQ_COUNT, paramIdEvent).getSingleResult());
        return getEntityManager().createQuery(REQ_COUNT, Long.class)
                .setParameter(PARAM_ID_REQ_COUNT, paramIdEvent).getSingleResult();
    }

}
