package fr.isika.projet4.data.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.projet4.data.api.IDaoRecette;
import fr.isika.projet4.data.assembler.DtoToEntity;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.FluxDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.entity.Activite;
import fr.isika.projet4.entity.Flux;
import fr.isika.projet4.entity.Inscription;
import fr.isika.projet4.entity.Recette;
import fr.isika.projet4.entity.RevenusActivite;

/**
 * Classe implémentant les méthodes de l'interface {@link IDaoRecette}.
 * @author stagiaire
 *
 */
@Repository
@Transactional
public class DaoRecette extends DAO implements IDaoRecette {

    /**
     * Requête jpql pour additionner les recettes d'un
     * {@link fr.isika.projet4.entity.Evenement}.
     */
    private static final String REQ_SOMME_PAR_EVENEMENT = "SELECT SUM(r.montant) FROM Recette r WHERE r.evenement.id = ?1";

    /**
     * Position du paramètre de l'id de l'{@link fr.isika.projet4.entity.Evenement}
     * dans {@value #REQ_SOMME_PAR_EVENEMENT}.
     */
    private static final int PARAM_EVENT_REQ_SOMME_PAR_EVENEMENT = 1;

    /**
     * Le double à retourner si jamais il n'y a aucune données dans l'unité de
     * persistence.
     */
    private static final double RESULTAT_SI_EXCEPTION = 0.0;

    /**
     * Pour le catch.
     */
    private Logger log = Logger.getLogger(getClass());

    @Override
    public double sommeRecetteParEvenement(int paramIdEvenement) {
        try {
            return super.getEntityManager().createQuery(REQ_SOMME_PAR_EVENEMENT, Double.class)
                    .setParameter(PARAM_EVENT_REQ_SOMME_PAR_EVENEMENT, paramIdEvenement).getSingleResult();
        } catch (Exception e) {
            log.fatal(e);
            return RESULTAT_SI_EXCEPTION;
        }
    }

    /**
     * methode recette d'une inscription.
     * @param paramInscription .
     * @param paramActivite    .
     * @param paramEvenement   .
     * @return .
     */
    @Override
    public FluxDto recetteParInscription(InscriptionDto paramInscription, ActiviteDto paramActivite,
                                         EvenementDto paramEvenement) {
        log.debug(paramInscription.getId());
        Inscription entity = getEntityManager().find(Inscription.class, paramInscription.getId());
        for (Activite a : entity.getActivites()) {
            log.debug(a);
        }
        Flux flux = null;
        if (entity.getActivites().isEmpty()) {
            flux = new Recette();
            flux.setEvenment(DtoToEntity.transformDtoToEntity(paramEvenement));
            ((Recette) flux).setDateEntree(new Date(System.currentTimeMillis()));
            flux.setMontant(paramEvenement.getPrix());
        } else {
            flux = new RevenusActivite();
            flux.setActivite(DtoToEntity.transformDtoToEntity(paramActivite));
            ((RevenusActivite) flux).setDateEntree(new Date(System.currentTimeMillis()));
            flux.setEvenment(DtoToEntity.transformDtoToEntity(paramEvenement));
            flux.setMontant(paramActivite.getPrix() + paramEvenement.getPrix());
        }
        return EntityToDto.transformEntityToDto(flux);
    }
}
