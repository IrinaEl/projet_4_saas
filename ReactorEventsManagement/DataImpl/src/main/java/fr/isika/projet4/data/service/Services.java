package fr.isika.projet4.data.service;

import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isika.projet4.data.api.IDaoActivite;
import fr.isika.projet4.data.api.IDaoDepense;
import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.api.IDaoInscription;
import fr.isika.projet4.data.api.IDaoRecette;
import fr.isika.projet4.data.api.IDaoResponsableActivite;
import fr.isika.projet4.data.api.IDaoRessource;
import fr.isika.projet4.data.api.IDaoRevenuSponsoring;
import fr.isika.projet4.data.api.IDaoRevenusActivite;
import fr.isika.projet4.data.api.IDaoTache;
import fr.isika.projet4.data.api.IDaoUtilisateur;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.FluxDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.LeadPoleDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.exceptions.ActiviteException;
import fr.isika.projet4.exceptions.ConnexionException;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.InscriptionException;
import fr.isika.projet4.exceptions.LeadPoleException;
import fr.isika.projet4.exceptions.RAException;
import fr.isika.projet4.exceptions.RessourceException;
import fr.isika.projet4.exceptions.TacheException;

/**
 * class exposant l'ensemble des services de l'application.
 * @author stagiaire
 *
 */
@WebService(endpointInterface = "fr.isika.projet4.data.api.service.IDaoService",
            targetNamespace = "http://isika.projet4.com",
            serviceName = "serviceApp",
            portName = "projet4")
@Service
public class Services implements IDaoService {

    /**
     * injection et "instanciation" du dao de maniere à utiliser les méthode de
     * cette classe.
     */
    @Autowired
    private IDaoUtilisateur dao;

    /**
     * Injection du {@link fr.isika.projet4.data.impl.DaoEvenement} pour
     * l'utilisation de ses méthodes.
     */
    @Autowired
    private IDaoEvenement daoEvent;

    /**
     * Injection de la classe contenant les méthodes de persistence liées à
     * l'entité{@link fr.isika.projet4.entity.Activite} dans le contexte de Spring.
     */
    @Autowired
    private IDaoActivite daoActivite;

    /**
     * Injection de {@link fr.isika.projet4.data.impl.DaoRecette}.
     */
    @Autowired
    private IDaoRecette daoRecette;

    /**
     * Injection de {@link fr.isika.projet4.data.impl.DaoDepense}.
     */
    @Autowired
    private IDaoDepense daoDepense;

    /**
     * Injection de {@link fr.isika.projet4.data.impl.DaoInscription}.
     */
    @Autowired
    private IDaoInscription daoInscription;

    /**
     * Injection de {@link fr.isika.projet4.data.impl.DaoRevenuActivite}.
     */
    @Autowired
    private IDaoRevenusActivite daoRevAct;

    /**
     * Injection de {@link fr.isika.projet4.data.impl.DaoRevenuSponsoring}.
     */
    @Autowired
    private IDaoRevenuSponsoring daoRevSponso;

    // ---------------------------------
    // FRONT
    // ---------------------------------

    /**
     * injection de la classe contenant les méthodes de persistence liées à
     * l'entité {@link fr.isika.projet4.entity.ResponsableActivite} dans le
     * contente de Spring.
     */
    @Autowired
    private IDaoResponsableActivite daoRa;

    /**
     * injection de la classe contenant les méthodes de persistence liées à
     * l'entité {@link fr.isika.projet4.entity.Tache} dans le contente de Spring.
     */
    @Autowired
    private IDaoTache daoTache;

    /**
     * injection de la classe contenant les méthodes de persistence liées à
     * l'entité {@link fr.isika.projet4.entity.Ressource} dans le contente de
     * Spring.
     */
    @Autowired
    private IDaoRessource daoRessource;

    /**
     * Méthode de connexion appeller dans le service.
     * @param paramMail d'un utilisateur.
     * @param paramMdp  d'un utilisateur.
     * @return un utilisateur dto.
     * @throws ConnexionException dans le cas d'un problème de connexion lieu.
     */
    @Override
    public UtilisateurDto seConnecterBack(String paramMail, String paramMdp) throws ConnexionException {
        return dao.seConnecterBack(paramMail, paramMdp);

    }

    /**
     * @param paramMail du sponsor.
     * @param paramMdp  du sponsor.
     * @return un sponsor dto.
     * @throws ConnexionException dans le cas d'un problème de connexion lieu.
     */
    @Override
    public SponsorDto seConnecterFront(String paramMail, String paramMdp) throws ConnexionException {
        return dao.seConnecterFront(paramMail, paramMdp);
    }

    @Override
    public InscriptionDto inscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) throws InscriptionException {
        return daoInscription.inscriptionActivite(paramActivite, paramInscription);
    }

    @Override
    public InscriptionDto inscriptionEvenement(EvenementDto paramEvent, InscriptionDto paramInscription) throws InscriptionException {
        return daoInscription.inscriptionEvenement(paramEvent, paramInscription);
    }

    @Override
    public Boolean verifInscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) {
        return daoInscription.verifInscriptionActivite(paramActivite, paramInscription);
    }

    @Override
    public Boolean verifInscriptionEvenement(EvenementDto paramEvenement, InscriptionDto paramInscription) {
        return daoInscription.verifInscriptionEvenement(paramEvenement, paramInscription);
    }

    @Override
    public FluxDto recetteParInscription(InscriptionDto paramInscription, ActiviteDto paramActivite, EvenementDto paramEvenement) {
        return daoRecette.recetteParInscription(paramInscription, paramActivite, paramEvenement);
    }

    // ---------------------------------
    // BACK
    // ---------------------------------

    @Override
    public List<EvenementDto> getByRA(int paramIdRa) throws RAException {
        return daoEvent.getByRA(paramIdRa);
    }

    @Override
    public List<EvenementDto> getAllEvent() throws EventException {
        return daoEvent.getAll();
    }

    @Override
    public List<ActiviteDto> getAllActiviteByEvent(Integer paramIdEvent) throws ActiviteException {
        return daoActivite.getAllActiviteByEvent(paramIdEvent);
    }

    @Override
    public List<ActiviteDto> getActivitesByRa(Integer paramIdRa) throws ActiviteException {
        return daoActivite.getActivitesByRa(paramIdRa);
    }

    // ---------------------------------
    // FINANCES
    // ---------------------------------

    @Override
    public double sommeRecetteParEvenement(int paramIdEvenement) {
        return daoRecette.sommeRecetteParEvenement(paramIdEvenement);
    }

    @Override
    public double sommeDepenseParEvenement(int paramIdEvenement) {
        return daoDepense.sommeDepenseParEvenement(paramIdEvenement);
    }

    /**
     * Méthode pour récuperer un evenement par son Id exposer dans le service.
     * @param paramId : id de l'événement.
     * @return un évenement.
     * @throws EventException : si l'id ne correspond à aucun événement.
     */
    @Override
    public EvenementDto getEventById(int paramId) throws EventException {
        return daoEvent.getEventById(paramId);
    }

    @Override
    public List<EvenementDto> getAllDoneEvents() throws EventException {
        return daoEvent.getAllDoneEvents();
    }

    @Override
    public ResponsableActiviteDto getRaById(Integer paramIdRa) throws RAException {
        return daoRa.getRaById(paramIdRa);

    }

    @Override
    public ActiviteDto getActiviteById(Integer paramIdActivite) throws ActiviteException {

        return daoActivite.getActiviteById(paramIdActivite);
    }

    @Override
    public List<TacheDto> getTachesByActivite(Integer paramIdActivite) throws TacheException {

        return daoTache.getTachesByActivite(paramIdActivite);
    }

    @Override
    public TacheDto affecterBenevole(TacheDto paramTache, RessourceDto paramBenevole) throws TacheException {

        return daoTache.affecterBenevole(paramTache, paramBenevole);
    }

    @Override
    public List<EvenementDto> getEvenementsByLeadPoleID(int leadPoleId) throws LeadPoleException {

        return daoEvent.getEvenementsByLeadPoleID(leadPoleId);
    }

    @Override
    public List<ResponsableActiviteDto> getAllResponsableActivite() throws RAException {

        return daoRa.getAllResponsableActivite();
    }

    @Override
    public ActiviteDto addResponsableActivity(ResponsableActiviteDto paramResponsableActiviteDto,
                                              ActiviteDto paramActiviteDto) throws ActiviteException {

        return daoActivite.addResponsableActivity(paramResponsableActiviteDto, paramActiviteDto);
    }

    @Override
    public List<IntervenantExterneDto> getAllBenevoles(Integer paramIdTypeIntervenant) throws RessourceException {

        return daoRessource.getAllBenevoles(paramIdTypeIntervenant);
    }

    @Override
    public EvenementDto ajouter(EvenementDto paramEvenement, ActiviteDto paramActivite, TypeEvenementDto paramTypeEvenement,
                                AdresseDto paramAdresse, LeadPoleDto paramLeadPole, SponsorDto paramSponsor) throws EventException {
        return null;
    }

    @Override
    public ActiviteDto affecterRessource(Integer paramIdRessource, Integer paramIdActivite) throws ActiviteException {

        return daoActivite.affecterRessource(paramIdRessource, paramIdActivite);
    }

    @Override
    public List<RessourceDto> getRessourcesByActivite(Integer paramIdActivite) throws RessourceException {

        return daoRessource.getRessourceByActivite(paramIdActivite);
    }

    @Override
    public List<RessourceDto> getAllRessources() throws RessourceException {

        return daoRessource.getAllRessources();
    }

    @Override
    public Long nbRevenuActByIdEvent(int newIdEvent) {
        return daoRevAct.nbRevenuActByIdEvent(newIdEvent);
    }

    @Override
    public long nbRevSponsoParEvenement(int paramIdEvent) {
        return daoRevSponso.nbRevSponsoParEvenement(paramIdEvent);
    }

}
