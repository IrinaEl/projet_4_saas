/**
 *
 */
package fr.isika.projet4.data.assembler;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.TypeActiviteDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.entity.Activite;
import fr.isika.projet4.entity.Adresse;
import fr.isika.projet4.entity.Evenement;
import fr.isika.projet4.entity.Inscription;
import fr.isika.projet4.entity.Pays;
import fr.isika.projet4.entity.ResponsableActivite;
import fr.isika.projet4.entity.TypeActivite;
import fr.isika.projet4.entity.TypeEvenement;
import fr.isika.projet4.entity.TypeVoie;
import fr.isika.projet4.entity.Ville;

/**
 * Classe assembler pour transformer dto en entity.
 * @author stagiaire
 *
 */
public final class DtoToEntity {

    /**
     * Constructeur privé pour les classes utilitaires.
     */
    private DtoToEntity() {
    }

    /**
     * transforme un dto evenement en entity evenement.
     * @param paramEvenement .
     * @return un evenement
     */
    public static Evenement transformDtoToEntity(EvenementDto paramEvenement) {
        if (paramEvenement != null) {
            Evenement evenement = new Evenement();
            evenement.setAdresse(transformDtoToEntity(paramEvenement.getAdresse()));
            evenement.setBudget(paramEvenement.getBudget());
            evenement.setDateDebut(paramEvenement.getDateDebut());
            evenement.setDateFin(paramEvenement.getDateFin());
            evenement.setDescription(paramEvenement.getDescription());
            evenement.setIntitule(paramEvenement.getIntitule());
            evenement.setPrix(paramEvenement.getPrix());
            evenement.setTypeEvenement(transformDtoToEntity(paramEvenement.getTypeEvenement()));
            return evenement;
        }
        return null;
    }

    /**
     * transforme type evenement dto en type evenement entity.
     * @param paramTypeEvenement .
     * @return type evenement .
     */
    public static TypeEvenement transformDtoToEntity(TypeEvenementDto paramTypeEvenement) {
        if (paramTypeEvenement != null) {
            TypeEvenement typeEvenementDto = new TypeEvenement();
            typeEvenementDto.setId(paramTypeEvenement.getId());
            typeEvenementDto.setLibelle(paramTypeEvenement.getLibelle());
            return typeEvenementDto;
        }
        return null;
    }

    /**
     * transforme une dto adresse en entity adresse.
     * @param paramAdresse .
     * @return une adresse
     */
    public static Adresse transformDtoToEntity(AdresseDto paramAdresse) {
        if (paramAdresse != null) {
            Adresse adresse = new Adresse();
            adresse.setId(paramAdresse.getId());
            adresse.setNomVoie(paramAdresse.getNomVoie());
            adresse.setNumeroVoie(paramAdresse.getNumeroVoie());
            adresse.setTypeVoie(transformDtoToEntity(paramAdresse.getTypeVoie()));
            adresse.setVille(transformDtoToEntity(paramAdresse.getVille()));
            return adresse;
        }
        return null;
    }

    public static TypeVoie transformDtoToEntity(TypeVoieDto paramTypeVoie) {
        if (paramTypeVoie != null) {
            TypeVoie typeVoie = new TypeVoie();
            typeVoie.setId(paramTypeVoie.getId());
            typeVoie.setLibelle(paramTypeVoie.getLibelle());
            return typeVoie;
        }
        return null;
    }

    /**
     * transforme un dto ville en ville entity.
     * @param paramVille .
     * @return une villeDto
     */
    public static Ville transformDtoToEntity(VilleDto paramVille) {
        Ville ville = new Ville();
        ville.setId(paramVille.getId());
        ville.setLibelle(paramVille.getLibelle());
        ville.setPays(transformDtoToEntity(paramVille.getPays()));
        return ville;
    }

    /**
     * transforme un dto pays en pays entity.
     * @param paramPays .
     * @return pays entity
     */
    private static Pays transformDtoToEntity(PaysDto paramPays) {
        Pays pays = new Pays();
        pays.setId(paramPays.getId());
        pays.setLibelle(paramPays.getLibelle());
        return pays;
    }

    /**
     * transforme un incription dto en inscription entity.
     * @param paramInscriptionDto .
     * @return une inscription entity.
     */
    public static Inscription transformDtoToEntity(InscriptionDto paramInscriptionDto) {

        Inscription ins = new Inscription();
        if (paramInscriptionDto != null) {
            ins.setId(paramInscriptionDto.getId());
            ins.setNom(paramInscriptionDto.getNom());
            ins.setPrenom(paramInscriptionDto.getPrenom());
            ins.setDateNaissance(paramInscriptionDto.getDateNaissance());
            ins.setMail(paramInscriptionDto.getMail());
            ins.setMontantRegle(paramInscriptionDto.getMontantRegle());
            ins.setEvenement(transformDtoToEntity(paramInscriptionDto.getEvenement()));
            return ins;

        }
        return null;
    }

    /**
     * transforme un ResponsableActiviteDto en ResponsableActivite entity.
     * @param responsableActviteDto dto.
     * @return un ResponsableActivite entity
     */
    public static ResponsableActivite transformDtoToEntity(ResponsableActiviteDto responsableActviteDto) {
        ResponsableActivite ra = new ResponsableActivite();
        ra.setIdUtil(responsableActviteDto.getId());
        ra.setNomUtil(responsableActviteDto.getNom());
        ra.setPrenomUtil(responsableActviteDto.getPrenom());
        ra.setMailUtil(responsableActviteDto.getMail());
        ra.setMdpUtil(responsableActviteDto.getMdp());
        return ra;
    }

    /**
     * transforme un ActiviteDto en Activite entity.
     * @param activiteDto.
     * @return un Activite entity
     */
    public static Activite transformDtoToEntity(ActiviteDto activiteDto) {
        Activite activite = new Activite();
        activite.setId(activiteDto.getId());
        activite.setBudget(activiteDto.getBudget());
        activite.setDateDebut(activiteDto.getDateDebut());
        activite.setDateFin(activiteDto.getDateFin());
        activite.setDescription(activiteDto.getDescription());
        activite.setIntitule(activiteDto.getIntitule());
        activite.setNombrePlace(activiteDto.getNombrePlace());
        activite.setPrix(activiteDto.getPrix());
        if (activiteDto.getEvenement() != null) {
            activite.setEvenement(transformDtoToEntity(activiteDto.getEvenement()));
        }
        if (activiteDto.getResponsableActivite() != null) {
            activite.setResponsableActivite(transformDtoToEntity(activiteDto.getResponsableActivite()));
        }
        if (activiteDto.getTypeActivite() != null) {
            activite.setTypeActivite(transformDtoToEntity(activiteDto.getTypeActivite()));
        }

        return activite;
    }

    /**
     * transforme un TypeActiviteDto en Typeactivite entity.
     * @param typeActiviteDto
     * @return un TypeActivite entity
     */
    public static TypeActivite transformDtoToEntity(TypeActiviteDto typeActiviteDto) {
        TypeActivite typeActivite = new TypeActivite();
        if (typeActiviteDto != null) {
            typeActivite.setId(typeActiviteDto.getId());
            typeActivite.setDescription(typeActiviteDto.getDescription());
            typeActivite.setIntitule(typeActiviteDto.getIntitule());
            return typeActivite;
        }
        return null;
    }

}
