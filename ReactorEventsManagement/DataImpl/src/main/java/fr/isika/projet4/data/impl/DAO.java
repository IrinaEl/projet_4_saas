package fr.isika.projet4.data.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Class utilitaire pour entityManager.
 * @author stagiaire
 */
@Transactional
public class DAO {

    /**
     * Déclaration de l'entityManager.
     */
    @PersistenceContext(unitName = "projet_4_saas_persistence_unit")
    private EntityManager entityManager;

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
