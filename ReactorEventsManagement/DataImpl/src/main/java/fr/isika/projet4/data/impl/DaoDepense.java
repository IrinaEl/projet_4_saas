package fr.isika.projet4.data.impl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.projet4.data.api.IDaoDepense;

/**
 * Classe implémentant les méthodes de l'interface {@link IDaoDepense}.
 * @author stagiaire
 *
 */
@Repository
@Transactional
public class DaoDepense extends DAO implements IDaoDepense {

    /**
     * Requête jpql pour additionner les recettes d'un
     * {@link fr.isika.projet4.entity.Evenement}.
     */
    private static final String REQ_DEPENSE_PAR_EVENEMENT = "SELECT SUM(d.montant) FROM Depense d WHERE d.evenement.id = ?1";

    /**
     * Position du paramètre de l'id de l'{@link fr.isika.projet4.entity.Evenement}
     * dans {@value #REQ_SOMME_PAR_EVENEMENT}.
     */
    private static final int PARAM_EVENT_REQ_DEPENSE_PAR_EVENEMENT = 1;

    /**
     * Le double à retourner si jamais il n'y a aucune données dans l'unité de
     * persistence.
     */
    private static final double RESULTAT_SI_EXCEPTION = 0.0;

    /**
     * Pour le catch.
     */
    private Logger log = Logger.getLogger(getClass());

    @Override
    public double sommeDepenseParEvenement(int paramIdEvenement) {
        try {
            return super.getEntityManager().createQuery(REQ_DEPENSE_PAR_EVENEMENT, Double.class)
                    .setParameter(PARAM_EVENT_REQ_DEPENSE_PAR_EVENEMENT, paramIdEvenement).getSingleResult();
        } catch (Exception e) {
            log.fatal(e);
            return RESULTAT_SI_EXCEPTION;
        }
    }

}
