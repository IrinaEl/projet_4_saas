package fr.isika.projet4.data.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.springframework.stereotype.Service;

import fr.isika.projet4.data.api.IDaoTache;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.entity.Ressource;
import fr.isika.projet4.entity.Tache;
import fr.isika.projet4.exceptions.TacheException;

/**
 * classe implémentant les méthodes de persistence liées au dto
 * {@link TacheDto}.
 * @author stagiaire
 *
 */
@Service
public class DaoTache extends DAO implements IDaoTache {

    /**
     * requete permettant de récupérer une liste de tâches à partir d'un
     * identifiant de l'activité.
     */
    private static final String REQ_TACHES_PAR_ACTIVITE = "SELECT a.typeActivite.taches FROM Activite a WHERE a.id = :pidActivite";

    @Override
    public List<TacheDto> getTachesByActivite(Integer paramIdActivite) throws TacheException {
        try {
            List<TacheDto> liste = new ArrayList<>();

            @SuppressWarnings("unchecked")
            List<Tache> listeEnt = getEntityManager().createQuery(REQ_TACHES_PAR_ACTIVITE)
                    .setParameter("pidActivite", paramIdActivite).getResultList();

            for (Tache t : listeEnt) {
                liste.add(EntityToDto.transformEntityToDto(t));
            }
            if (liste.isEmpty()) {
                throw new TacheException("Aucune tâche n'a été créée pour cette activité");
            }
            return liste;
        } catch (PersistenceException e) {
            throw new TacheException("Le serveur de données est hors service", null);
        }
    }

    @Override
    public TacheDto affecterBenevole(TacheDto tache, RessourceDto benevole) throws TacheException {

        Tache ta = getEntityManager().find(Tache.class, tache.getId());
        Ressource re = getEntityManager().find(Ressource.class, benevole.getId());
        ta.setBenevole(re);
        getEntityManager().merge(ta);
        return EntityToDto.transformEntityToDto(ta);
    }

}
