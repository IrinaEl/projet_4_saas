package fr.isika.projet4.data.impl;

import java.util.List;

import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;

import fr.isika.projet4.data.api.IDaoResponsableActivite;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.entity.ResponsableActivite;
import fr.isika.projet4.exceptions.RAException;

/**
 * classe impléménetnat les méthodes de persistence relative au RA.
 * @author stagiaire
 *
 */
@Repository
public class DaoResponsableActivite extends DAO implements IDaoResponsableActivite {

    /**
     * requête pour récupérer le RA à partir de son identifiant.
     */
    private static final String REQ_GET_BY_ID = "SELECT ra FROM ResponsableActivite ra WHERE ra.idUtil = :pidUtil";

    /**
     * la requête pour rechercher une liste de Responsable Activite.
     * {@link ResponsableActiviteDto}.
     */
    private static final String REQ_GET_ALL_RA = "SELECT r FROM ResponsableActivite r";

    @Override
    public ResponsableActiviteDto getRaById(Integer paramIdRa) throws RAException {

        return EntityToDto.transformEntityToDto(getEntityManager().createQuery(REQ_GET_BY_ID, ResponsableActivite.class)
                .setParameter("pidUtil", paramIdRa).getSingleResult());
    }

    @Override
    public List<ResponsableActiviteDto> getAllResponsableActivite() throws RAException {
        try {
            List<ResponsableActiviteDto> ras = EntityToDto
                    .transformEntityToDtoRas(getEntityManager().createQuery(REQ_GET_ALL_RA, ResponsableActivite.class).getResultList());
            if (ras.isEmpty()) {
                throw new RAException("la liste est vide");
            }
            for (ResponsableActiviteDto responsableActiviteDto : ras) {
                responsableActiviteDto.setMdp(null);
            }
            return ras;
        } catch (PersistenceException e) {
            throw new RAException("Le serveur de données est hors service");
        }
    }
}
