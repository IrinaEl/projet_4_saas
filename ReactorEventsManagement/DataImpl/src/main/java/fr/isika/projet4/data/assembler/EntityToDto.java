package fr.isika.projet4.data.assembler;

import java.util.ArrayList;
import java.util.List;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.FluxDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.LeadPoleDto;
import fr.isika.projet4.dto.MaterielDto;
import fr.isika.projet4.dto.MembreAdministrationDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.StatutTacheDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.dto.TypeActiviteDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeIntervenantDto;
import fr.isika.projet4.dto.TypeTacheDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.entity.Activite;
import fr.isika.projet4.entity.Adresse;
import fr.isika.projet4.entity.Evenement;
import fr.isika.projet4.entity.Flux;
import fr.isika.projet4.entity.Inscription;
import fr.isika.projet4.entity.IntervenantExterne;
import fr.isika.projet4.entity.LeadPole;
import fr.isika.projet4.entity.Materiel;
import fr.isika.projet4.entity.MembreAdministration;
import fr.isika.projet4.entity.Pays;
import fr.isika.projet4.entity.ResponsableActivite;
import fr.isika.projet4.entity.Ressource;
import fr.isika.projet4.entity.Sponsor;
import fr.isika.projet4.entity.StatutTache;
import fr.isika.projet4.entity.Tache;
import fr.isika.projet4.entity.TypeActivite;
import fr.isika.projet4.entity.TypeEvenement;
import fr.isika.projet4.entity.TypeIntervenant;
import fr.isika.projet4.entity.TypeTache;
import fr.isika.projet4.entity.TypeVoie;
import fr.isika.projet4.entity.Utilisateur;
import fr.isika.projet4.entity.Ville;

/**
 * Class transform EntityToDto.
 * @author stagiaire.
 */
public final class EntityToDto {

    /**
     * Constructeur privé.
     */
    private EntityToDto() {
    }

    /**
     * Transform utilisateur en DTO selon type.
     * @param user lié à l'entity.
     * @return un userDto.
     */
    public static UtilisateurDto transformEntityToDto(Utilisateur user) {

        UtilisateurDto userDto;

        if (user.getClass() == ResponsableActivite.class) {
            userDto = transformEntityToDto((ResponsableActivite) user);
            userDto.setType(userDto.getClass().getSimpleName());
            return userDto;
        }
        if (user.getClass() == LeadPole.class) {
            userDto = transformEntityToDto((LeadPole) user);
            userDto.setType(userDto.getClass().getSimpleName());
            return userDto;
        }
        if (user.getClass() == MembreAdministration.class) {

            userDto = transformEntityToDto((MembreAdministration) user);
            userDto.setType(userDto.getClass().getSimpleName());
            return userDto;
        }
        return null;
    }

    /**
     * Transform entity en DTO.
     * @param sponsor lié à l'entity.
     * @return un userDto.
     */
    public static SponsorDto transformEntityToDto(Sponsor sponsor) {
        SponsorDto sponsorDto = new SponsorDto();
        sponsorDto.setId(sponsor.getIdUtil());
        sponsorDto.setNom(sponsor.getNomUtil());
        sponsorDto.setPrenom(sponsor.getPrenomUtil());
        sponsorDto.setMail(sponsor.getMailUtil());
        sponsorDto.setMdp(sponsor.getMdpUtil());
        sponsorDto.setLogo(sponsor.getLogo());
        sponsorDto.setRaisonSociale(sponsor.getRaisonSociale());
        return sponsorDto;
    }

    /**
     * tranformation Evenement entity en dto.
     * @param event entity à transformer.
     * @return le DTO Evenement
     */
    public static EvenementDto transformEntityToDto(Evenement event) {
        if (event != null) {
            EvenementDto eventDto = new EvenementDto();
            eventDto.setId(event.getId());
            eventDto.setBudget(event.getBudget());
            eventDto.setDateDebut(event.getDateDebut());
            eventDto.setDateFin(event.getDateFin());
            eventDto.setDescription(event.getDescription());
            eventDto.setIntitule(event.getIntitule());
            eventDto.setPrix(event.getPrix());
            eventDto.setTypeEvenement(transformEntityToDto(event.getTypeEvenement()));
            eventDto.setAdresse(transformEntityToDto(event.getAdresse()));
            return eventDto;
        }
        return null;

    }

    /**
     * tranformation Type Evenement entity en dto.
     * @param typeEvenement entity à transformer.
     * @return le DTO Type Evenement
     */
    public static TypeEvenementDto transformEntityToDto(TypeEvenement typeEvenement) {
        TypeEvenementDto typeEvenementDto = new TypeEvenementDto();
        if (typeEvenement != null) {
            typeEvenementDto.setId(typeEvenement.getId());
            typeEvenementDto.setLibelle(typeEvenement.getLibelle());
        }
        return typeEvenementDto;
    }

    /**
     * transform responsable activité.
     * @param respEnt le RA à transformer.
     * @return le DTO RA.
     */
    public static ResponsableActiviteDto transformEntityToDto(ResponsableActivite respEnt) {
        if (respEnt != null) {
            ResponsableActiviteDto respDto = new ResponsableActiviteDto();
            respDto.setId(respEnt.getIdUtil());
            respDto.setNom(respEnt.getNomUtil());
            respDto.setPrenom(respEnt.getPrenomUtil());
            respDto.setMail(respEnt.getMailUtil());
            respDto.setMdp(respEnt.getMdpUtil());
            return respDto;
        }
        return null;
    }

    /**
     * transform lead pole.
     * @param leadEnt le LP à transformer
     * @return le DTO LP
     */
    public static LeadPoleDto transformEntityToDto(LeadPole leadEnt) {
        LeadPoleDto leadDto = new LeadPoleDto();
        leadDto.setId(leadEnt.getIdUtil());
        leadDto.setNom(leadEnt.getNomUtil());
        leadDto.setPrenom(leadEnt.getPrenomUtil());
        leadDto.setMail(leadEnt.getMailUtil());
        leadDto.setMdp(leadEnt.getMdpUtil());
        return leadDto;
    }

    /**
     * transofrm membre de l'administrtion.
     * @param membreEnt le MB à transformer
     * @return le DTO MB
     */
    public static MembreAdministrationDto transformEntityToDto(MembreAdministration membreEnt) {
        MembreAdministrationDto membreDto = new MembreAdministrationDto();
        membreDto.setId(membreEnt.getIdUtil());
        membreDto.setNom(membreEnt.getNomUtil());
        membreDto.setPrenom(membreEnt.getPrenomUtil());
        membreDto.setMail(membreEnt.getMailUtil());
        membreDto.setMdp(membreEnt.getMdpUtil());
        return membreDto;
    }

    /**
     * Transformation de l'entity {@link Activite} en dto {@link ActiviteDto}.
     * @param activite l'entity à transformer
     * @return activiteDto
     */
    public static ActiviteDto transformEntityToDto(Activite activite) {
        ActiviteDto activiteDto = new ActiviteDto();
        activiteDto.setId(activite.getId());
        activiteDto.setBudget(activite.getBudget());
        activiteDto.setDateDebut(activite.getDateDebut());
        activiteDto.setDateFin(activite.getDateFin());
        activiteDto.setDescription(activite.getDescription());
        activiteDto.setIntitule(activite.getIntitule());
        activiteDto.setNombrePlace(activite.getNombrePlace());
        activiteDto.setPrix(activite.getPrix());
        if (activite.getEvenement() != null) {
            activiteDto.setEvenement(transformEntityToDto(activite.getEvenement()));
        }
        if (activite.getResponsableActivite() != null) {
            activiteDto.setResponsableActivite(transformEntityToDto(activite.getResponsableActivite()));
        }
        if (activite.getTypeActivite() != null) {
            activiteDto.setTypeActivite(transformEntityToDto(activite.getTypeActivite()));
        }
        return activiteDto;
    }

    /**
     * tranformation Adresse entity en dto.
     * @param adresseEnt entity à transformer.
     * @return le DTO Adresse
     */
    public static AdresseDto transformEntityToDto(Adresse adresseEnt) {
        AdresseDto adresseDto = new AdresseDto();
        adresseDto.setId(adresseEnt.getId());
        adresseDto.setNomVoie(adresseEnt.getNomVoie());
        adresseDto.setNumeroVoie(adresseEnt.getNumeroVoie());
        adresseDto.setTypeVoie(transformEntityToDto(adresseEnt.getTypeVoie()));
        adresseDto.setVille(transformEntityToDto(adresseEnt.getVille()));
        return adresseDto;
    }

    /**
     * transformation TypeVoie entity en dto.
     * @param typeEntity entity type de voie à transformer
     * @return le dto type de voie
     */
    public static TypeVoieDto transformEntityToDto(TypeVoie typeEntity) {
        TypeVoieDto typeDto = new TypeVoieDto();
        typeDto.setId(typeEntity.getId());
        typeDto.setLibelle(typeEntity.getLibelle());
        return typeDto;
    }

    /**
     * transformation Ville entity en dto.
     * @param villeEnt à transformer
     * @return le dto à retourner
     */
    public static VilleDto transformEntityToDto(Ville villeEnt) {
        VilleDto villeDto = new VilleDto();
        villeDto.setId(villeEnt.getId());
        villeDto.setLibelle(villeEnt.getLibelle());
        villeDto.setPays(transformEntityToDto(villeEnt.getPays()));
        return villeDto;
    }

    /**
     * transformation Pays entity en dto.
     * @param paysEnt pays entity à transformer
     * @return le dto pays
     */
    public static PaysDto transformEntityToDto(Pays paysEnt) {
        PaysDto paysDto = new PaysDto();
        paysDto.setId(paysEnt.getId());
        paysDto.setLibelle(paysEnt.getLibelle());
        return paysDto;
    }

    /**
     * transformation Type activité entity en dto.
     * @param typeActivite entity à transformer.
     * @return le dto Type activité
     */
    public static TypeActiviteDto transformEntityToDto(TypeActivite typeActivite) {
        TypeActiviteDto typeDto = new TypeActiviteDto();
        if (typeActivite != null) {
            typeDto.setId(typeActivite.getId());
            typeDto.setDescription(typeActivite.getDescription());
            typeDto.setIntitule(typeActivite.getIntitule());
            return typeDto;
        }
        return null;
    }

    /**
     * Transforme une liste d'evenements entity to dto.
     * @param evenements liste {@link Evenement}
     * @return evenementsDto.
     */
    public static List<EvenementDto> transformEntityToDto(List<Evenement> evenements) {
        List<EvenementDto> evenementsDto = new ArrayList<>();
        for (Evenement e : evenements) {
            evenementsDto.add(transformEntityToDto(e));
        }
        return evenementsDto;
    }

    /**
     * Transforme une liste d'activites entity to dto.
     * @param activites liste {@link Activite}
     * @return activitesDto.
     */
    public static List<ActiviteDto> transformEntityToDtoActivites(List<Activite> activites) {
        List<ActiviteDto> activitesDto = new ArrayList<>();
        for (Activite a : activites) {
            activitesDto.add(transformEntityToDto(a));
        }
        return activitesDto;
    }

    /**
     * Transforme une inscription en inscription dto.
     * @param paramInscription l'inscription a transformer.
     * @return une {@link InscriptionDto}.
     */
    public static InscriptionDto transformEntityToDto(Inscription paramInscription) {
        if (paramInscription != null) {
            InscriptionDto inscriptionDto = new InscriptionDto();
            inscriptionDto.setId(paramInscription.getId());
            inscriptionDto.setNom(paramInscription.getNom());
            inscriptionDto.setPrenom(paramInscription.getPrenom());
            inscriptionDto.setDateNaissance(paramInscription.getDateNaissance());
            inscriptionDto.setMail(paramInscription.getMail());
            inscriptionDto.setMontantRegle(paramInscription.getMontantRegle());
            inscriptionDto.setEvenement(transformEntityToDto(paramInscription.getEvenement()));
            return inscriptionDto;
        }
        return null;
    }

    /**
     * transformEntityToDto(Flux flux).
     * @param flux l'entity a transfo
     * @return .
     */
    public static FluxDto transformEntityToDto(Flux flux) {
        FluxDto fluxDto = new FluxDto();
        fluxDto.setId(flux.getId());
        if (flux.getActivite() != null) {
            fluxDto.setActivite(transformEntityToDto(flux.getActivite()));
        }
        if (flux.getEvenment() != null) {
            fluxDto.setEvenment(transformEntityToDto(flux.getEvenment()));
        }
        fluxDto.setMontant(flux.getMontant());
        return fluxDto;
    }

    /**
     * Conversion d'une entity {@link Tache} en dto {@link TacheDto}.
     * @param tacheEnt entity à transformer
     * @return {@link TacheDto}
     */
    public static TacheDto transformEntityToDto(Tache tacheEnt) {
        TacheDto tacheDto = new TacheDto();
        tacheDto.setId(tacheEnt.getId());
        tacheDto.setDeadline(tacheEnt.getDeadline());
        tacheDto.setStatut(transformEntityToDto(tacheEnt.getStatut()));
        if (tacheEnt.getTypeTaches() != null) {
            tacheDto.setTypeTaches(transformEntityToDto(tacheEnt.getTypeTaches()));
        }
        if (tacheEnt.getBenevole() != null) {
            tacheDto.setBenevole(transformEntityToDto(tacheEnt.getBenevole()));
        }
        return tacheDto;
    }

    /**
     * conversion entity {@link Ressource} en dto {@link RessourceDto}, selon le
     * type.
     * @param resEnt entity à convertir
     * @return un dto Ressource //
     */

    /**
     * conversion entity en dto de l'intervenant externe.
     * @param resEnt à transformer
     * @return dto intervenant
     */
    public static IntervenantExterneDto transformEntityToDto(IntervenantExterne resEnt) {
        IntervenantExterneDto resDto = new IntervenantExterneDto();
        resDto.setId(resEnt.getId());
        resDto.setMail(resEnt.getMail());
        resDto.setNumeroTelephone(resEnt.getNumeroTelephone());
        resDto.setRaisonSociale(resEnt.getRaisonSociale());
        resDto.setTypeIntervenant(transformEntityToDto(resEnt.getTypeIntervenant()));

        if (resEnt.getNumeroRCS() != null) {
            resDto.setNumeroRCS(resEnt.getNumeroRCS());
        }
        if (resEnt.getPrix() != 0) {
            resDto.setPrix(resEnt.getPrix());
        }
        if (resEnt.getQuantite() != 0) {
            resDto.setQuantite(resEnt.getQuantite());
        }

        return resDto;
    }

    /**
     * conversion type intervenant.
     * @param matEnt à convertir
     * @return dto à retourner
     */
    public static TypeIntervenantDto transformEntityToDto(TypeIntervenant matEnt) {
        TypeIntervenantDto typeDto = new TypeIntervenantDto();
        typeDto.setId(matEnt.getId());
        typeDto.setIntitule(matEnt.getIntitule());
        typeDto.setDescription(matEnt.getDescription());
        return null;
    }

    /**
     * conversion materiel.
     * @param resEnt r
     * @return r
     */
    public static MaterielDto transformEntityToDto(Materiel resEnt) {
        MaterielDto resDto = new MaterielDto();
        resDto.setId(resEnt.getId());
        resDto.setMail(resEnt.getMail());
        resDto.setNumeroTelephone(resEnt.getNumeroTelephone());
        resDto.setRaisonSociale(resEnt.getRaisonSociale());
        resDto.setIntitule(resEnt.getIntitule());

        if (resEnt.getNumeroRCS() != null) {
            resDto.setNumeroRCS(resEnt.getNumeroRCS());
        }
        if (resEnt.getPrix() != 0) {
            resDto.setPrix(resEnt.getPrix());
        }
        if (resEnt.getQuantite() != 0) {
            resDto.setQuantite(resEnt.getQuantite());
        }

        return resDto;
    }

    /**
     * conversion pour statut de la tâche.
     * @param statutEnt - entity à convertir
     * @return dto {@link StatutTacheDto}
     */
    public static StatutTacheDto transformEntityToDto(StatutTache statutEnt) {
        StatutTacheDto statutDto = new StatutTacheDto();
        statutDto.setId(statutEnt.getId());
        statutDto.setIntitule(statutEnt.getIntitule());
        return statutDto;
    }

    /**
     * conversion entity {@link Ressource} en dto {@link RessourceDto}.
     * @param resEnt entity à convertir
     * @return un dto Ressource
     */
    public static RessourceDto transformEntityToDto(Ressource resEnt) {
        if (resEnt.getClass() == IntervenantExterne.class) {
            RessourceDto resDto = transformEntityToDto((IntervenantExterne) resEnt);
            resDto.setType(resDto.getClass().getSimpleName());
            return resDto;
        }

        if (resEnt.getClass() == Materiel.class) {
            RessourceDto resDto = transformEntityToDto((Materiel) resEnt);
            resDto.setType(resDto.getClass().getSimpleName());
            return resDto;
        }

        return null;
    }

    /**
     * conversion pour statut de la tâche.
     * @param statutEnt - entity à convertir
     * @return dto {@link StatutTacheDto}
     */

    /**
     * conversion d'une entity {@link TypeTache} en dto {@link TypeTacheDto}.
     * @param typeEnt - entity à transformer
     * @return {@link TypeTacheDto}
     */
    public static TypeTacheDto transformEntityToDto(TypeTache typeEnt) {
        TypeTacheDto typeDto = new TypeTacheDto();
        typeDto.setId(typeEnt.getId());
        typeDto.setDescription(typeEnt.getDescription());
        typeDto.setIntitule(typeEnt.getIntitule());
        return typeDto;
    }

    /**
     * conversion d'une liste entity {@link ResponsableActivite} en dto
     * {@link ResponsableActiviteDto}.
     * @param ras entity à transformer
     * @return {@link ResponsableActiviteDto}
     */
    public static List<ResponsableActiviteDto> transformEntityToDtoRas(List<ResponsableActivite> ras) {
        List<ResponsableActiviteDto> raDtos = new ArrayList<>();
        for (ResponsableActivite ra : ras) {
            raDtos.add(transformEntityToDto(ra));
        }
        return raDtos;
    }
}
