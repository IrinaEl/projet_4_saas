package fr.isika.projet4.data.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import fr.isika.projet4.data.api.IDaoActivite;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.entity.Activite;
import fr.isika.projet4.entity.ResponsableActivite;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * classe implémentant les méthodes relatives aux {@link Activite}.
 * @author stagiaire
 *
 */

@Repository
public class DaoActivite extends DAO implements IDaoActivite {

    /**
     * logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * la requête pour rechercher une liste d'activité d'un
     * {@link fr.isika.projet4.entity.ResponsableActivite.ResponsableActivite}.
     */
    private static final String REQ_ACTIVITE_BY_RA = "SELECT a FROM Activite a WHERE a.responsableActivite.idUtil = :pidUtil";

    /**
     * Requête pour récupérer une liste {@link ActiviteDto} en fonction de l'id
     * d'un {@link EvenementDto}.
     */
    private static final String REQ_FIND_ACTIVITE_BY_EVENT = "SELECT a FROM Activite a WHERE a.evenement.id = ? ORDER BY a.dateDebut ASC ";

    /**
     * Index du paramètre id pour la requête FIND_ACTIVITE_BY_EVENT.
     */
    private static final int INDEX_REQ_FIND_ACTIVITE_BY_EVENT = 1;

    /**
     * la requête pour rechercher une activité à partir de son identifiant.
     */
    private static final String REQ_GET_ACTIVITE_BY_ID = "SELECT a FROM Activite a WHERE a.id = :pidActivite";

    /**
     * Le message d'exception à throw si le serveur de données est hors-service.
     */
    private static final String EXC_SERVEUR_HS = "Le serveur de données est hors service";

    @Override
    public List<ActiviteDto> getActivitesByRa(Integer paramIdRa) throws ActiviteException {
        try {
            List<ActiviteDto> liste = new ArrayList<>();
            List<Activite> listeEnt = getEntityManager().createQuery(REQ_ACTIVITE_BY_RA, Activite.class).setParameter("pidUtil", paramIdRa)
                    .getResultList();

            for (Activite a : listeEnt) {
                liste.add(EntityToDto.transformEntityToDto(a));

            }

            if (liste.isEmpty()) {
                throw new ActiviteException("Aucune activité n'a été affectée à ce responsable d'activité", null);
            }

            return liste;
        } catch (PersistenceException e) {
            throw new ActiviteException(EXC_SERVEUR_HS, null);
        }
    }

    @Override
    public List<ActiviteDto> getAllActiviteByEvent(Integer paramIdEvent) throws ActiviteException {
        try {
            List<ActiviteDto> retourDto = EntityToDto
                    .transformEntityToDtoActivites(getEntityManager().createQuery(REQ_FIND_ACTIVITE_BY_EVENT, Activite.class)
                            .setParameter(INDEX_REQ_FIND_ACTIVITE_BY_EVENT, paramIdEvent)
                            .getResultList());
            if (retourDto.isEmpty()) {
                throw new ActiviteException("il n'y a pas encore d'activité pour cet évenement", null);
            }
            for (ActiviteDto activiteDto : retourDto) {
                if (activiteDto.getResponsableActivite() != null) {
                    activiteDto.getResponsableActivite().setMdp(null);
                }
            }
            return retourDto;
        } catch (PersistenceException e) {
            throw new ActiviteException(EXC_SERVEUR_HS, null);
        }
    }

    @Override
    public ActiviteDto getActiviteById(Integer paramIdActivite) throws ActiviteException {
        try {
            return EntityToDto.transformEntityToDto(getEntityManager().createQuery(REQ_GET_ACTIVITE_BY_ID, Activite.class)
                    .setParameter("pidActivite", paramIdActivite).getSingleResult());
        } catch (NoResultException e) {
            throw new ActiviteException("L'activité recherchée n'existe pas", null);
        }

    }

    @Override
    public ActiviteDto addResponsableActivity(ResponsableActiviteDto paramResponsableActiviteDto,
                                              ActiviteDto paramActiviteDto) throws ActiviteException {
        try {
            Activite activite = getEntityManager().find(Activite.class,
                    paramActiviteDto.getId());
            ResponsableActivite responsableActivite = getEntityManager().find(ResponsableActivite.class,
                    paramResponsableActiviteDto.getId());
            activite.setResponsableActivite(responsableActivite);
            ActiviteDto activiteDto = EntityToDto.transformEntityToDto(getEntityManager().merge(activite));
            activiteDto.getResponsableActivite().setMdp(null);
            return activiteDto;

        } catch (PersistenceException e) {
            log.fatal(e);
            throw new ActiviteException(EXC_SERVEUR_HS, null);
        }
    }

    @Override
    public ActiviteDto affecterRessource(Integer paramIdRessource, Integer paramIdActivite) throws ActiviteException {

        getEntityManager()
                .createNativeQuery(
                        "INSERT INTO activite_ressource (activite_ressource_ressource_id, activite_ressource_activite_id) VALUES (?, ?)")
                .setParameter(1, paramIdRessource).setParameter(2, paramIdActivite).executeUpdate();
        return EntityToDto.transformEntityToDto(getEntityManager().find(Activite.class, paramIdActivite));
    }

}