package fr.isika.projet4.data.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import fr.isika.projet4.data.api.IDaoRevenusActivite;

/**
 * Classe implémentant les méthodes de l'interface {@link IDaoRevenusActivite}.
 * @author daphnemerck
 *
 */
@Repository
@Transactional
public class DaoRevenuActivite extends DAO implements IDaoRevenusActivite {

    /**
     * Requête JPQL pour compter le nombre de
     * {@link fr.isika.projet4.entity.RevenusActivite} par
     * {@link fr.isika.projet4.entity.Evenement}.
     */
    private static final String REQ_COUNT = "SELECT COUNT(*) FROM RevenusActivite ra WHERE ra.evenement.id = ?1";

    /**
     * Position du paramètre de l'id pour la requête {@link #REQ_COUNT}.
     */
    private static final int PARAM_ID_REQ_COUNT = 1;

    @Override
    public Long nbRevenuActByIdEvent(int idEvent) {
        return getEntityManager().createQuery(REQ_COUNT, Long.class)
                .setParameter(PARAM_ID_REQ_COUNT, idEvent).getSingleResult();
    }

}
