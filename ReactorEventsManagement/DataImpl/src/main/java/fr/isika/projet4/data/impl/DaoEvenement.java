
package fr.isika.projet4.data.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.assembler.DtoToEntity;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.LeadPoleDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.entity.Evenement;
import fr.isika.projet4.entity.LeadPole;
import fr.isika.projet4.entity.Sponsor;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.LeadPoleException;
import fr.isika.projet4.exceptions.RAException;

/**
 * Implémentation service de persistence pour l'entité
 * {@link fr.isika.projet4.entity.Evenement}.
 * @author ludwig
 */
@Repository
@Transactional
public class DaoEvenement extends DAO implements IDaoEvenement {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * Requête pour récupérer une liste d'évenements en fonction de l'id d'un
     * {@link fr.isika.projet4.entity.ResponsableActivite}.
     */
    private static final String REQ_SELECT_EVENT_BY_ID = "SELECT DISTINCT a.evenement FROM Activite a "
            + " WHERE a.responsableActivite.id = ?1";

    /**
     * Requête pour récupérer la liste de tout les évenements.
     */
    private static final String REQ_FIND_ALL = "SELECT e FROM Evenement e WHERE e.dateFin >= sysDate() ORDER BY e.dateDebut ASC ";

    /**
     * Requête pour récupérer la liste de tous les événements finis.
     */
    private static final String REQ_FIND_ALL_FINISHED = "SELECT  e FROM Evenement e ORDER BY e.dateDebut ASC";

    /**
     * Index du paramètre id pour la requête select.
     */
    private static final int INDEX_REQ_SELECT_ID = 1;

    /**
     * Requete pour récupere un événement en fonction de son ID.
     */
    private static final String REQ_GET_EVENT_BY_ID = "Select e FROM Evenement e WHERE e.id = ?1";

    /**
     * Requête pour récupérer la liste des évènements par LP id.
     */
    private static final String REQ_SELECT_EVENT_BY_LEADPOLE_ID = "SELECT l.listeEvenements FROM LeadPole l WHERE l.id= :paramId";

    @Override
    public List<EvenementDto> getByRA(int paramIdRA) throws RAException {
        List<EvenementDto> retour = EntityToDto
                .transformEntityToDto(getEntityManager().createQuery(REQ_SELECT_EVENT_BY_ID, Evenement.class)
                        .setParameter(INDEX_REQ_SELECT_ID, paramIdRA)
                        .getResultList());
        if (retour.isEmpty()) {
            log.debug("Liste vide");
            throw new RAException("Vous n'êtes affecté à aucun évenement", null);
        }
        return retour;
    }

    @Override
    public List<EvenementDto> getAll() throws EventException {
        List<EvenementDto> retourDto = EntityToDto.transformEntityToDto(getEntityManager().createQuery(REQ_FIND_ALL, Evenement.class)
                .getResultList());
        if (retourDto.isEmpty()) {
            throw new EventException("il n'y a aucun évenement pour le moment", null);
        }
        return retourDto;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EvenementDto> getEvenementsByLeadPoleID(int leadPoleID) throws LeadPoleException {
        List<Evenement> evenements = getEntityManager().createQuery(REQ_SELECT_EVENT_BY_LEADPOLE_ID)
                .setParameter("paramId", leadPoleID)
                .getResultList();
        List<EvenementDto> retour = EntityToDto
                .transformEntityToDto(evenements);
        if (retour.isEmpty()) {
            throw new LeadPoleException("Aucune evenement n'a été affectée à ce LeadPole", null);
        }
        return retour;
    }

    @Override
    public EvenementDto getEventById(int paramId) throws EventException {
        try {
            return EntityToDto
                    .transformEntityToDto(getEntityManager().createQuery(REQ_GET_EVENT_BY_ID, Evenement.class).setParameter(1, paramId)
                            .getSingleResult());
        } catch (NoResultException e) {
            log.fatal(e);
            throw new EventException("Cet événement n'existe pas", null);
        }
    }

    @Override
    public List<EvenementDto> getAllDoneEvents() throws EventException {
        List<EvenementDto> retourDto = EntityToDto
                .transformEntityToDto(getEntityManager().createQuery(REQ_FIND_ALL_FINISHED, Evenement.class)
                        .getResultList());
        if (retourDto.isEmpty()) {
            throw new EventException("Il n'y a aucun évenement pour le moment", null);
        }
        return retourDto;
    }

    @Override
    public EvenementDto ajouter(EvenementDto paramEvenement, ActiviteDto paramActivite, TypeEvenementDto paramTypeEvenement,
                                AdresseDto paramAdresse, LeadPoleDto paramLeadPole, SponsorDto paramSponsor,
                                ResponsableActiviteDto paramResponsableActivite) throws EventException {
        Evenement evenement = DtoToEntity.transformDtoToEntity(paramEvenement);

        // Dans la table je trouve le leadpole et je récupère son id
        LeadPole lp = getEntityManager().find(LeadPole.class, paramLeadPole.getId());
        // A l'évènement j'ajoute le leadpole que je viens de récupérer dans la base
        // à
        // la liste de leadpole
        // La table évènement contient une liste de leadPole (ceci grace à
        // l'annotation
        // @JoinTable(name = "evenement_leadpole", joinColumns = @JoinColumn(name =
        // "evenement_leadpole_id"), private List<LeadPole> leadPoles;)
        // c'est pour ça qu'on associe (ajoute) le leadPOLE a un évènement
        evenement.setLeadPoles(new ArrayList<>());
        evenement.getLeadPoles().add(lp);
        // met à jour la table lead pole dans la base de donnée en fonction du lead
        // pole
        // ajouté
        evenement.setActivites(new ArrayList<>());
        evenement.getActivites().add(DtoToEntity.transformDtoToEntity(paramActivite));
        getEntityManager().persist(evenement.getAdresse().getVille().getPays());
        getEntityManager().persist(evenement.getAdresse().getVille());
        getEntityManager().persist(evenement.getAdresse().getTypeVoie());
        getEntityManager().persist(evenement.getAdresse());
        getEntityManager().persist(evenement);

        // Dans la table je trouve le le sponsor et je récupère son id
        Sponsor sp = getEntityManager().find(Sponsor.class, paramSponsor.getId());
        /*
         * Au sponsor j'ajoute un évènement à sa liste d'évènement Dans ce cas
         * c'est le sponsor qui contient une liste d'évènement car dans la classe
         * sponsor contient les annotations @JoinTable(name = "evenement_sponsor",
         * joinColumns = @JoinColumn(name = "evenement_sponsor_sponsor_id"), foreignKey
         * = @ForeignKey(name = "FK_SPONSOR_EVENT"),inverseJoinColumns
         * = @JoinColumn(name = "evenement_sponsor_evenement_id"), inverseForeignKey
         * = @ForeignKey(name = "FK_EVENT_SPONSOR") c'est pour ça qu'on ajoute les
         * évènements au sponsor.
         */
        sp.getEvenements().add(evenement);
        // met à jour le sponsor dans la base de donnée en fonction de l'évènement
        // ajouté
        getEntityManager().merge(sp);
        return EntityToDto.transformEntityToDto(evenement);

    }

}
