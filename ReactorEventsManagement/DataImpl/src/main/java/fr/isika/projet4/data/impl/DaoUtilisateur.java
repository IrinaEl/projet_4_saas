package fr.isika.projet4.data.impl;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import fr.isika.projet4.data.api.IDaoUtilisateur;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.entity.Sponsor;
import fr.isika.projet4.entity.Utilisateur;
import fr.isika.projet4.exceptions.ConnexionException;

/**
 * ² Class d'implementation des méthodes liées aux utilisateurs.
 * @author stagiaire
 */
@Repository
@Transactional
public class DaoUtilisateur extends DAO implements IDaoUtilisateur {

    /**
     * Constante pour la requete se connecter.
     */
    private static final String REQ_CONNEXIONBACK = "SELECT u FROM Utilisateur u WHERE u.mailUtil = ?1 AND u.mdpUtil = ?2 ";

    /**
     * Constante pour la requete se connecter.
     */
    private static final String REQ_CONNEXIONFRONT = "SELECT s FROM Sponsor s WHERE s.mailUtil = ?1 AND s.mdpUtil = ?2";

    /**
     * Méthode se connecter pour le RA, le MB et le LP.
     * @throws ConnexionException .
     */
    @Override
    public UtilisateurDto seConnecterBack(String paramMail, String paramMdp) throws ConnexionException {

        UtilisateurDto retour = new UtilisateurDto();
        try {

            retour = EntityToDto.transformEntityToDto(getEntityManager().createQuery(REQ_CONNEXIONBACK, Utilisateur.class)
                    .setParameter(1, paramMail).setParameter(2, paramMdp).getSingleResult());
            if (retour == null) {
                throw new ConnexionException("Vous n'êtes pas autorisé à vous connecter sur cet espace", null);
            }
            retour.setMdp(null);
        } catch (NoResultException e) {

            throw new ConnexionException("Login/Mot de passe éronné", null);
        }

        return retour;
    }

    /**
     * Méthode se connecter pour le sponsor.
     * @throws ConnexionException
     */
    @Override
    public SponsorDto seConnecterFront(String paramMail, String paramMdp) throws ConnexionException {
        SponsorDto retourSponsor = new SponsorDto();
        try {

            retourSponsor = EntityToDto.transformEntityToDto(getEntityManager().createQuery(REQ_CONNEXIONFRONT, Sponsor.class)
                    .setParameter(1, paramMail).setParameter(2, paramMdp).getSingleResult());
            retourSponsor.setMdp(null);
        } catch (NoResultException e) {

            throw new ConnexionException("Login/Mot de passe éronné", null);
        }
        return retourSponsor;
    }

}
