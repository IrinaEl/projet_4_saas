package fr.isika.projet4.data.impl.test.ressource;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoRessource;
import fr.isika.projet4.data.impl.DaoRessource;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;

public class TestGetAllRessources {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoRessource dao = new DaoRessource();

    /**
     * nb ressources.
     *
     */
    private static final int NB_RESSOURCES = 20;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     */
    @BeforeClass
    public static void beforeAllTests() {

        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoRessource.class);
    }

    /**
     * nominale.
     * @throws RessourceException b
     */
    @Test
    public void testNbBenevoles() throws RessourceException {

        List<RessourceDto> retour = dao.getAllRessources();
        Assert.assertEquals(NB_RESSOURCES, retour.size());

    }

}
