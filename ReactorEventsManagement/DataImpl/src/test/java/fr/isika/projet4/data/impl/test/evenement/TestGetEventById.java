package fr.isika.projet4.data.impl.test.evenement;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.impl.DaoEvenement;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * Classe de test permettant de tester la méthode getEventById.
 * @author stagiaire
 *
 */
public class TestGetEventById {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoEvenement dao = new DaoEvenement();

    /**
     * L'évenement attendu.
     */
    private static EvenementDto eventNominal = new EvenementDto();

    /**
     * L'id de l'événement.
     */
    private static final Integer ID_EVENT_NOMINAL = 1;

    /**
     * Le prix de l'événement.
     */
    private static final double PRIX_EVENT_NOMINAL = 30;

    /**
     * Le budget de l'événement.
     */
    private static final double BUDGET_EVENT_NOMINAL = 50000;

    /**
     * L'id de l'adresse l'événement.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 8;

    /**
     * L'id du type de voie lié à l'adresse de l'événement.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 2;

    /**
     * date de debut de l'evenement 1.
     */
    private static final String DATE_DEBUT = "16-01-2020 20:00:00";

    /**
     * date de fin de l'evenement 1.
     */
    private static final String DATE_FIN = "16-01-2020 23:30:00";

    /**
     * L'id de la ville liée à l'adresse de l'événement.
     */
    private static final Integer ID_VILLE_EVENT = 6;

    /**
     * L'id du pays de l'événement.
     */
    private static final Integer ID_PAYS_EVENT = 9;

    /**
     * L'id de la voie de l'événement.
     */
    private static final String NUMERO_VOIE = "13";

    /**
     * exception event get by id.
     */
    private static EventException expectedException = new EventException();

    /**
     * Chargement contexte Spring avant tests et variables de tests.
     * @throws ParseException - Si erreur de parse sur le parse de la date.
     */
    @BeforeClass
    public static void beforeAllTests() throws ParseException {
        eventNominal.setId(ID_EVENT_NOMINAL);
        eventNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse(DATE_DEBUT));
        eventNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse(DATE_FIN));
        eventNominal.setIntitule("Concert promotionnel Isika");
        eventNominal.setDescription("Concert promotionnel d'artistes montants de la region de Malakoff");
        eventNominal.setPrix(PRIX_EVENT_NOMINAL);
        eventNominal.setBudget(BUDGET_EVENT_NOMINAL);
        eventNominal.setAdresse(new AdresseDto());
        eventNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        eventNominal.getAdresse().setNomVoie("Thunder Bluff");
        eventNominal.getAdresse().setNumeroVoie(NUMERO_VOIE);
        eventNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        eventNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        eventNominal.getAdresse().getTypeVoie().setLibelle("chemin");
        eventNominal.getAdresse().setVille(new VilleDto());
        eventNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        eventNominal.getAdresse().getVille().setLibelle("Tana");
        eventNominal.getAdresse().getVille().setPays(new PaysDto());
        eventNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        eventNominal.getAdresse().getVille().getPays().setLibelle("Madagascar");
        expectedException = new EventException("cet événement n'existe pas", null);
        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoEvenement.class);
    }

    /**
     * test nominal pour la methode get event by id.
     * @throws EventException pour relever une exception lié à l'événement.
     * @throws                SQLException .
     */
    @Test
    public void testNominal() throws EventException, SQLException {
        EvenementDto retour = dao.getEventById(ID_EVENT_NOMINAL);
        AssertionEntities.assertEntity(eventNominal, retour);
    }

}
