package fr.isika.projet4.data.impl.test.evenement;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.impl.DaoEvenement;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.LeadPoleException;

/**
 * Test de la méthode pour récupérer la liste{@link EvenementDto} par
 * LeadPole id.
 * @author stagiaire
 *
 */
public class TestEventByLeadPoleId {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoEvenement dao = new DaoEvenement();

    /**
     * L'évenement attendu.
     */
    private static EvenementDto evenementNominal = new EvenementDto();

    /**
     * identificat du LP dans le test nominal.
     */
    private static final Integer ID_LP_NOMINAL = 15;

    /**
     * L'id de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_EVENEMENT_EXPECTED = 1;

    /**
     * Le budget de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final double BUDGET_EVENEMENT_EXPECTED = 50000;

    /**
     * Le prix de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final double PRIX_EVENEMENT_EXPECTED = 30;

    /**
     * l'exception attendue en cas de retour d'une liste d'activité vide.
     */
    private static LeadPoleException expectedException = new LeadPoleException();

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    /**
     * L'id de l'adresse l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 8;

    /**
     * L'id du type de voie lié à l'adresse de
     * l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 2;

    /**
     * L'id de la ville liée à l'adresse de
     * l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_VILLE_EVENT = 6;

    /**
     * L'id du pays de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_PAYS_EVENT = 9;

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     * @throws ParseException - Si erreur de parse du {@link SimpleDateFormat}
     */
    /**
     * La taille de la liste d'évenement attendue lié à
     * {@link TestEvenementByRA#ID_LP_NOMINAL}.
     */
    private static final int TAILLE_LISTE = 5;

    /**
     * L'id du lp dans le cas où il n'est affecté à aucun évenement.
     */
    private static final Integer ID_LP_ECHEC_LISTE_VIDE = 17;

    /**
     * Id type evenement dans Evenement.
     */
    private static final Integer ID_TYPE_EVENEMENT = 1;

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     * @throws ParseException - Si erreur de parse du {@link SimpleDateFormat}
     */
    @BeforeClass
    public static void beforeAllTests() throws ParseException {
        evenementNominal.setId(ID_EVENEMENT_EXPECTED);
        evenementNominal.setDateDebut(SDF.parse("16-01-2020 20:00:00"));
        evenementNominal.setDateFin(SDF.parse("16-01-2020 23:30:00"));
        evenementNominal.setIntitule("Concert promotionnel Isika");
        evenementNominal.setDescription("Concert promotionnel d'artistes montants de la region de Malakoff");
        evenementNominal.setPrix(PRIX_EVENEMENT_EXPECTED);
        evenementNominal.setBudget(BUDGET_EVENEMENT_EXPECTED);

        evenementNominal.setAdresse(new AdresseDto());
        evenementNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        evenementNominal.getAdresse().setNomVoie("Thunder Bluff");
        evenementNominal.getAdresse().setNumeroVoie("13");

        evenementNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        evenementNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        evenementNominal.getAdresse().getTypeVoie().setLibelle("chemin");

        evenementNominal.getAdresse().setVille(new VilleDto());
        evenementNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        evenementNominal.getAdresse().getVille().setLibelle("Tana");
        evenementNominal.getAdresse().getVille().setPays(new PaysDto());
        evenementNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        evenementNominal.getAdresse().getVille().getPays().setLibelle("Madagascar");

        evenementNominal.setTypeEvenement(new TypeEvenementDto());
        evenementNominal.getTypeEvenement().setId(ID_TYPE_EVENEMENT);
        evenementNominal.getTypeEvenement().setLibelle("Majeur");

        expectedException = new LeadPoleException("Aucune evenement n'a été affectée à ce LeadPole", null);
        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoEvenement.class);
    }

    /**
     * Test de la liste d'{@link EvenementDto}.
     * @throws LeadPoleException - Si la liste est vide.
     * @throws SQLException      - Si l'unité de persistence est hors-service.
     */
    @Test
    public void testNominal() throws LeadPoleException, SQLException {
        EvenementDto retour = dao.getEvenementsByLeadPoleID(ID_LP_NOMINAL).get(0);
        AssertionEntities.assertEntity(evenementNominal, retour);
    }

    /**
     * Test de la taille de la liste d'{@link EvenementDto}.
     * @throws LeadPoleException - Si la liste est vide.
     * @throws SQLException      - Si l'unié de persistence est hors-service.
     */
    @Test
    public void testTailleListe() throws LeadPoleException, SQLException {
        Assert.assertEquals(TAILLE_LISTE, dao.getEvenementsByLeadPoleID(ID_LP_NOMINAL).size());
    }

    /**
     * Test d'échec dans le cas d'une liste vide.
     * @throws SQLException      - Si l'unité de persistence est hors-service.
     * @throws LeadPoleException - Si la liste est vide.
     */
    @Test
    public void testEchecListeVide() throws SQLException, LeadPoleException {
        try {
            dao.getEvenementsByLeadPoleID(ID_LP_ECHEC_LISTE_VIDE);
            Assert.fail("Echec du test");
        } catch (LeadPoleException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }
}
