package fr.isika.projet4.data.impl.test.evenement;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.impl.DaoEvenement;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.LeadPoleDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.TypeActiviteDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * Classe pour tester la méthode ajouter de {@link IDaoEvenement}.
 * @author stagiaire
 *
 */
public class TestAjouter {

    /**
     * constante évènement.
     */
    private static final double PRIX_EVENEMENT = 50;

    /**
     * constante évènement.
     */
    private static final double BUDGET_EVENEMENT = 5000;

    /**
     * constante id de l'évènement.
     */
    private static final int ID_EVENEMENT = 8;

    /**
     * constante id de l'adresse.
     */
    private static final Integer ID_ADRESSE = 9;

    /**
     * constante du type de voie.
     */
    private static final Integer ID_TYPE_VOIE = 9;

    /**
     * constante de ville.
     */
    private static final Integer ID_VILLE = 11;

    /**
     * constante de pays.
     */
    private static final Integer ID_PAYS = 11;

    /**
     * constante prix.
     */
    private static final double PRIX_ACTIVITE = 0;

    /**
     * constante de id type evenement.
     */
    private static final int ID_TYPE_EVENEMENT = 1;

    /**
     * constante budget activité.
     */
    private static final double BUDGET_ACTIVITE = 0;

    /**
     * nombre de place activite.
     */
    private static final Integer NBRE_PLACE_ACTIVITE = 25;

    /**
     * id de l'activité.
     */
    private static final Integer ID_ACTIVITE = null;

    /**
     * id du leadpole associé à l'évènement.
     */
    private static final Integer ID_LEAD = 1;

    /**
     * id du sponsor de l'évènement.
     */
    private static final Integer ID_SPONSOR = 2;

    /**
     * dernier id attendu inséré dans la table évènement.
     */
    private static final Integer DERNIER_EVENEMENT = 6;

    /**
     * Classe qui contient le service à tester.
     */
    private static IDaoEvenement dao = new DaoEvenement();

    /**
     * Evènement nominal.
     */
    private static EvenementDto evenementNominal = new EvenementDto();

    /**
     * Evènement attendu.
     */
    private static EvenementDto evenementAttendu = new EvenementDto();

    /**
     * Activité attendu.
     */
    private static ActiviteDto activiteAttendu = new ActiviteDto();

    /**
     * Intitulé erreur attendu géré par l'xception.
     */
    private static EventException exceptionAttendu = new EventException();

    /**
     * activité nominal.
     */
    private static ActiviteDto activiteNominal = new ActiviteDto();

    /**
     * adresse nominal paramètre.
     */
    private static AdresseDto adresseNominal = new AdresseDto();

    /**
     * type evénement nominal.
     */
    private static TypeEvenementDto typeEvenementNominal = new TypeEvenementDto();

    /**
     * leadpole nominal paramètre.
     */
    private static LeadPoleDto leadPoleNominal = new LeadPoleDto();

    /**
     * sponsor nominal.
     */
    private static SponsorDto sponsorNominal = new SponsorDto();

    /**
     * settage des valeurs dans la classe évènement et chargement du contexte de
     * spring avant les tests.
     * @throws ParseException gggggggggggggggg.
     */
    @BeforeClass
    public static void beforeAllTests() throws ParseException {
        evenementNominal.setId(null);
        evenementNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy").parse("15-01-2019"));
        evenementNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy").parse("12-02-2019"));
        evenementNominal.setIntitule("evenement de noel evry");
        evenementNominal.setDescription("noel à evry est un moment de joie, où tous les habitants vivent ensemble");
        evenementNominal.setPrix(PRIX_EVENEMENT);
        evenementNominal.setBudget(BUDGET_EVENEMENT);
        evenementNominal.setAdresse(new AdresseDto());
        evenementNominal.getAdresse().setNomVoie("rue de lourde");
        evenementNominal.getAdresse().setNumeroVoie("39");
        evenementNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        evenementNominal.getAdresse().getTypeVoie().setLibelle("rue");
        evenementNominal.getAdresse().setVille(new VilleDto());
        evenementNominal.getAdresse().getVille().setLibelle("paris");
        evenementNominal.getAdresse().getVille().setPays(new PaysDto());
        evenementNominal.getAdresse().getVille().getPays().setLibelle("france");
        evenementNominal.getAdresse().getVille().getPays().setLibelle("france");
        evenementNominal.setTypeEvenement(new TypeEvenementDto());
        evenementNominal.getTypeEvenement().setId(ID_TYPE_EVENEMENT);
        evenementNominal.getTypeEvenement().setLibelle("majeur");
        activiteNominal.setEvenement(evenementNominal);
        activiteNominal.setId(ID_ACTIVITE);
        activiteNominal.setPrix(PRIX_ACTIVITE);
        activiteNominal.setBudget(BUDGET_ACTIVITE);
        activiteNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy").parse("16-01-2019"));
        activiteNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy").parse("18-01-2019"));
        activiteNominal.setDescription("activité eest une activité et une activité vitale d'activité");
        activiteNominal.setIntitule("Arc en ciel");
        activiteNominal.setNombrePlace(NBRE_PLACE_ACTIVITE);
        activiteNominal.setTypeActivite(new TypeActiviteDto());
        activiteNominal.getTypeActivite().setIntitule("Arc en ciel");
        activiteNominal.setResponsableActivite(new ResponsableActiviteDto());
        activiteNominal.getResponsableActivite().setMail("test@mail");
        activiteNominal.getResponsableActivite().setMdp("testMdp");
        activiteNominal.getResponsableActivite().setNom("testNom");
        activiteNominal.getResponsableActivite().setPrenom("testPrenom");

        evenementAttendu.setId(DERNIER_EVENEMENT);
        evenementAttendu.setDateDebut(new SimpleDateFormat("dd-MM-yyyy").parse("15-01-2019"));
        evenementAttendu.setDateFin(new SimpleDateFormat("dd-MM-yyyy").parse("12-02-2019"));
        evenementAttendu.setIntitule("evenement de noel evry");
        evenementAttendu.setDescription("noel à evry est un moment de joie, où tous les habitants vivent ensemble");
        evenementAttendu.setPrix(PRIX_EVENEMENT);
        evenementAttendu.setBudget(BUDGET_EVENEMENT);
        evenementAttendu.setAdresse(new AdresseDto());
        evenementAttendu.getAdresse().setId(ID_ADRESSE);
        evenementAttendu.getAdresse().setNomVoie("rue de lourde");
        evenementAttendu.getAdresse().setNumeroVoie("39");
        evenementAttendu.getAdresse().setTypeVoie(new TypeVoieDto());
        evenementAttendu.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE);
        evenementAttendu.getAdresse().getTypeVoie().setLibelle("rue");
        evenementAttendu.getAdresse().setVille(new VilleDto());
        evenementAttendu.getAdresse().getVille().setId(ID_VILLE);
        evenementAttendu.getAdresse().getVille().setLibelle("paris");
        evenementAttendu.getAdresse().getVille().setPays(new PaysDto());
        evenementAttendu.getAdresse().getVille().getPays().setId(ID_PAYS);
        evenementAttendu.getAdresse().getVille().getPays().setLibelle("france");
        evenementAttendu.setTypeEvenement(new TypeEvenementDto());
        evenementAttendu.getTypeEvenement().setId(ID_TYPE_EVENEMENT);
        evenementAttendu.getTypeEvenement().setLibelle("majeur");
        activiteAttendu.setEvenement(evenementAttendu);
        activiteAttendu.setId(ID_ACTIVITE);
        activiteAttendu.setPrix(PRIX_ACTIVITE);
        activiteAttendu.setBudget(BUDGET_ACTIVITE);
        activiteAttendu.setDateDebut(new SimpleDateFormat("dd-MM-yyyy").parse("16-01-2019"));
        activiteAttendu.setDateFin(new SimpleDateFormat("dd-MM-yyyy").parse("18-01-2019"));
        activiteAttendu.setDescription("activité eest une activité et une activité vitale d'activité");
        activiteAttendu.setIntitule("Arc en ciel");
        activiteAttendu.setNombrePlace(NBRE_PLACE_ACTIVITE);
        activiteAttendu.setTypeActivite(new TypeActiviteDto());
        ;
        activiteAttendu.getTypeActivite().setIntitule("Arc en ciel");

        leadPoleNominal.setId(ID_LEAD);
        sponsorNominal.setId(ID_SPONSOR);

        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoEvenement.class);

    }

    /**
     * Test ajout nominal.
     * @throws EventException du cas.
     */
    @Test
    public void testAjoutNominal() throws EventException {
        EvenementDto retour = dao.ajouter(evenementNominal, activiteNominal, typeEvenementNominal, adresseNominal, leadPoleNominal,
                sponsorNominal, activiteNominal.getResponsableActivite());
        AssertionEntities.assertEntity(evenementAttendu, retour);

    }

}
