package fr.isika.projet4.data.impl.test.tache;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoTache;
import fr.isika.projet4.data.impl.DaoTache;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.StatutTacheDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * classe de test pour méthode de persistence affecterBenevole.
 * @author stagiaire
 *
 */
public class TestAffecterBenevole {

    /**
     * logger.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * classe contenant la méthode testée.
     */
    private static IDaoTache dao = new DaoTache();

    /**
     * la tâche attendue en retour dans la liste testée dans le test nominal.
     */
    private static final TacheDto EXPECTED_TACHE = new TacheDto();

    /**
     * statut de la tache attendue nominale.
     */
    private static final StatutTacheDto EXPECTED_STATUT_NOMINAL = new StatutTacheDto();

    /**
     * id du statut attendu.
     */
    private static final Integer EXPECTED_STATUT_ID = 1;

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer ID_TACHE_NOMINALE = 1;

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer DEADLINE_NOMINAL = 120;

    /**
     * bénévole à affecter.
     */
    private static final IntervenantExterneDto BENEVOLE_AFFECTE = new IntervenantExterneDto();

    /**
     * id du bénévole à affecter.
     */
    private static final int ID_BENEVOLE_AFFECTE = 11;

    /**
     * id du bénévole attendu.
     */
    private static final Integer ID_BENEVOLE_ATTENDU = 11;

    /**
     * before.
     */
    @BeforeClass
    public static void beforeAllTests() {
        EXPECTED_STATUT_NOMINAL.setId(EXPECTED_STATUT_ID);
        EXPECTED_STATUT_NOMINAL.setIntitule("EN COURS");
        EXPECTED_TACHE.setId(ID_TACHE_NOMINALE);
        EXPECTED_TACHE.setDeadline(DEADLINE_NOMINAL);
        EXPECTED_TACHE.setStatut(EXPECTED_STATUT_NOMINAL);

        BENEVOLE_AFFECTE.setId(ID_BENEVOLE_AFFECTE);
        BENEVOLE_AFFECTE.setMail("mail@yahoo.fr");
        BENEVOLE_AFFECTE.setNumeroTelephone("0565323134");
        BENEVOLE_AFFECTE.setRaisonSociale("Taystee Jefferson");

        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoTache.class);
    }

    /**
     * test nominal.
     * @throws TacheException
     */
    @Test
    public void testNominal() throws TacheException {

        TacheDto retour = dao.affecterBenevole(EXPECTED_TACHE, BENEVOLE_AFFECTE);
        log.fatal(retour);
        Assert.assertEquals(ID_BENEVOLE_ATTENDU, retour.getBenevole().getId());

    }

}
