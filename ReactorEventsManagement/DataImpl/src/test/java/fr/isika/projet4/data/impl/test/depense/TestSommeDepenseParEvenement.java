package fr.isika.projet4.data.impl.test.depense;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoDepense;

/**
 * Classe de test pour la méthode
 * {@link IDaoDepense#sommeDepenseParEvenement(int)}.
 * @author stagiaire
 *
 */
public class TestSommeDepenseParEvenement {

    /**
     * Calsse contenant le service à tester.
     */
    private static IDaoDepense dao;

    /**
     * Le résultat attendu après l'addition de toutes les recettes.
     */
    private static final double EXPECTED_SOMME = 2500.0;

    /**
     * Le résultat retourné si l'événement ne contient aucune dépense.
     */
    private static final double RESULTAT_SI_EXCEPTION = 0.0;

    /**
     * L'identifiant de l'événement que l'on cherche.
     */
    private static final int ID_EVENEMENT = 1;

    /**
     * L'identifiant d'un événement qui n'a pas de dépense.
     */
    private static final int MAUVAIS_ID_EVENEMENT = 100;

    /**
     * Delta maximum entre la valeur attendu et la valeur test.
     */
    private static final double DELTA_EQUALS = 0.01;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext ac;

    /**
     * Méthode d'initiation pour les tests et qui lance Spring.
     */
    @BeforeClass
    public static void initTest() {
        ac = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = ac.getBean(IDaoDepense.class);
    }

    /**
     * Test nominal de la méthode {@link IDaoDepense#sommeDepenseParEvenement(int)}.
     * Elle doit retourner {@link #EXPECTED_SOMME}.
     */
    @Test
    public void testNominalSommeDepenseParEvenement() {
        double retour = dao.sommeDepenseParEvenement(ID_EVENEMENT);
        Assert.assertNotNull(retour);
        Assert.assertEquals(EXPECTED_SOMME, retour, DELTA_EQUALS);
    }

    /**
     * Test sénario alternatif de la méthode
     * {@link IDaoDepense#sommeDepenseParEvenement(int)}. Elle doit retourner
     * {@link #RESULTAT_SI_EXCEPTION}.
     */
    @Test
    public void testAlternatif() {
        double retour = dao.sommeDepenseParEvenement(MAUVAIS_ID_EVENEMENT);
        Assert.assertNotNull(retour);
        Assert.assertEquals(RESULTAT_SI_EXCEPTION, retour, DELTA_EQUALS);
    }

    /**
     * Fermeture de la ressource qui permet de démarrer Spring.
     */
    @AfterClass
    public static void close() {
        ac.close();
    }
}
