/**
 * Package comprenant les classes de tests des méthode de
 * {@link fr.isika.projet4.data.api.IDaoRevenusActivite}.
 * @author daphnemerck
 *
 */
package fr.isika.projet4.data.impl.test.revenu.activite;