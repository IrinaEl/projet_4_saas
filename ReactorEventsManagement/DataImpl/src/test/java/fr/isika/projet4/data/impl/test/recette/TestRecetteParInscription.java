package fr.isika.projet4.data.impl.test.recette;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoRecette;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.FluxDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;

/**
 * Classe de test methode recette par inscription.
 * @author stagiaire
 *
 */
public class TestRecetteParInscription {

    /**
     * Le service la classe du service à tester.
     */
    private static IDaoRecette dao;

    /**
     * flux attendu.
     */
    private static FluxDto expectedFlux = new FluxDto();

    /**
     * flux id.
     */
    private static final int FLUX_ID = 10;

    /**
     * L'inscription en entr�e.
     */
    private static InscriptionDto inscriptionNominal = new InscriptionDto();

    /**
     * Id de l'inscription.
     */
    private static final int ID_INS = 3;

    /**
     * Un evenement.
     */
    private static EvenementDto event = new EvenementDto();

    /**
     * Id de l'event.
     */
    private static final int ID_EVENT = 5;

    /**
     * Budget de l'evenement.
     */
    private static final double BUDGET_EVENT = 500.0;

    /**
     * Prix de l'evenement.
     */
    private static final double PRIX_EVENT = 0.0;

    /**
     * Adresse de l'evenement.
     */
    private static AdresseDto adresseEvent = new AdresseDto();

    /**
     * Id de l'adresse.
     */
    private static final int ID_ADRESSE = 8;

    /**
     * type de voie.
     */
    private static TypeVoieDto typeVoie = new TypeVoieDto();

    /**
     * Id type voie.
     */
    private static final int ID_TYPE_VOIE = 2;

    /**
     * ville.
     */
    private static VilleDto ville = new VilleDto();

    /**
     * id ville.
     */
    private static final int ID_VILLE = 3;

    /**
     * pays.
     */
    private static PaysDto pays = new PaysDto();

    /**
     * id pays.
     */
    private static final int ID_PAYS = 3;

    /**
     * le type d'evenement.
     */
    private static TypeEvenementDto type = new TypeEvenementDto();

    /**
     * chiffre un.
     */
    private static final int ID_UN = 1;

    /**
     * L'activité sur laquelle on veut s'inscrire.
     */
    private static ActiviteDto act = new ActiviteDto();

    /**
     * Ressource pour Spring.
     */
    private static ClassPathXmlApplicationContext spring;

    /**
     * Test.
     * @throws ParseException erreur.
     */
    @BeforeClass
    public static void initTest() throws ParseException {
        spring = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = spring.getBean(IDaoRecette.class);

        typeVoie.setId(ID_TYPE_VOIE);
        typeVoie.setLibelle("chemin");

        pays.setId(ID_PAYS);
        pays.setLibelle("Maroc");

        ville.setId(ID_VILLE);
        ville.setLibelle("Marrakech");
        ville.setPays(pays);

        adresseEvent.setId(ID_ADRESSE);
        adresseEvent.setNomVoie("Tarrides");
        adresseEvent.setNumeroVoie("87");
        adresseEvent.setTypeVoie(typeVoie);
        adresseEvent.setVille(ville);

        type.setId(ID_UN);
        type.setLibelle("Majeur");

        event.setId(ID_EVENT);
        event.setBudget(BUDGET_EVENT);
        event.setDateDebut(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-02-14 12:30:00"));
        event.setDateFin(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-02-14 17:00:00"));
        event.setDescription("Venez nombreux cet apr�s-midi de c�l�bration des amoureux ! ");
        event.setIntitule("C�l�bration de la St-Valentin");
        event.setPrix(PRIX_EVENT);
        event.setAdresse(adresseEvent);
        event.setTypeEvenement(type);

        inscriptionNominal.setId(ID_INS);
        inscriptionNominal.setNom("Bertho");
        inscriptionNominal.setPrenom("Ryan");
        inscriptionNominal.setMail("ryan.bertho@gmail.com");
        inscriptionNominal.setDateNaissance(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-01-14 18:35:26"));
        inscriptionNominal.setEvenement(event);
        inscriptionNominal.setMontantRegle(PRIX_EVENT);

        expectedFlux.setId(FLUX_ID);
        expectedFlux.setMontant(event.getPrix());
    }

    /**
     * test.
     */
    @Test
    public void testInscriptionEvent() {
        FluxDto retour = dao.recetteParInscription(inscriptionNominal, null, event);
        AssertionEntities.assertEntity(expectedFlux, retour);
    }

    /**
     * Test.
     */
    @AfterClass
    public static void finTests() {
        spring.close();
    }
}
