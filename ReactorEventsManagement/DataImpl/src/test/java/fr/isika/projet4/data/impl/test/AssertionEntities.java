package fr.isika.projet4.data.impl.test;

import org.junit.Assert;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.FluxDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.StatutTacheDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.dto.TypeActiviteDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeTacheDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.dto.VilleDto;

/**
 * Classe d'assertion.
 * @author ludwig
 *
 */
public final class AssertionEntities {

    /**
     * Delta maximum entre la valeur attendu et la valeur test.
     */
    private static final double DELTA_EQUALS = 0.01;

    /**
     * Default constructor.
     */
    private AssertionEntities() {
    }

    /**
     * methode d'assertion test entity utilisateur dto.
     * @param expected : utilisateur attendu.
     * @param test     : utilisateur testé.
     */
    public static void assertEntity(UtilisateurDto expected, UtilisateurDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getMail());
        Assert.assertNotNull(test.getNom());
        Assert.assertNotNull(test.getPrenom());
        Assert.assertNull(test.getMdp());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getMail(), test.getMail());
        Assert.assertEquals(expected.getPrenom(), test.getPrenom());
        Assert.assertEquals(expected.getNom(), test.getNom());

    }

    /**
     * methode d'assertion test entity sponsor dto.
     * @param expected : sponsor attendu.
     * @param test     : sponsor testé.
     */
    public static void assertEntity(SponsorDto expected, SponsorDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getMail());
        Assert.assertNotNull(test.getNom());
        Assert.assertNotNull(test.getPrenom());
        Assert.assertNull(test.getMdp());
        if (test.getRaisonSociale() != null) {
            Assert.assertNotNull(test.getRaisonSociale());
        }
        Assert.assertNotNull(test.getLogo());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getMail(), test.getMail());
        Assert.assertEquals(expected.getPrenom(), test.getPrenom());
        Assert.assertEquals(expected.getNom(), test.getNom());
        Assert.assertEquals(expected.getRaisonSociale(), test.getRaisonSociale());
        Assert.assertEquals(expected.getLogo(), test.getLogo());

    }

    /**
     * Assertion pour un {@link EvenementDto}.
     * @param expected - L'évenement attendu
     * @param test     - L'évenement de test
     */
    public static void assertEntity(EvenementDto expected, EvenementDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getDateDebut());
        Assert.assertNotNull(test.getDateFin());
        Assert.assertNotNull(test.getIntitule());
        Assert.assertNotNull(test.getDescription());
        Assert.assertNotNull(test.getPrix());
        Assert.assertNotNull(test.getBudget());

        if (test.getAdresse() != null && expected.getAdresse() != null) {
            assertEntity(expected.getAdresse(), test.getAdresse());
        }
        if (test.getTypeEvenement() != null && expected.getTypeEvenement() != null) {
            assertEntity(expected.getTypeEvenement(), test.getTypeEvenement());
        }
        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getDateDebut(), test.getDateDebut());
        Assert.assertEquals(expected.getDateFin(), test.getDateFin());
        Assert.assertEquals(expected.getIntitule(), test.getIntitule());
        Assert.assertEquals(expected.getDescription(), test.getDescription());
        Assert.assertEquals(expected.getPrix(), test.getPrix(), DELTA_EQUALS);
        Assert.assertEquals(expected.getBudget(), test.getBudget(), DELTA_EQUALS);
    }

    /**
     * Assertion pour un {@link TypeEvenementDto}.
     * @param expected - Le type d'évenement attendue
     * @param test     - Le type d'évenement de test
     */
    public static void assertEntity(TypeEvenementDto expected, TypeEvenementDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getLibelle());
        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getLibelle(), test.getLibelle());
    }

    /**
     * Assertion pour un {@link AdresseDto}.
     * @param expected - L'adresse attendue
     * @param test     - L'adresse de test
     */
    public static void assertEntity(AdresseDto expected, AdresseDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getNomVoie());
        Assert.assertNotNull(test.getNumeroVoie());
        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getNomVoie(), test.getNomVoie());
        Assert.assertEquals(expected.getNumeroVoie(), test.getNumeroVoie());

        if (test.getTypeVoie() != null && expected.getTypeVoie() != null) {
            assertEntity(expected.getTypeVoie(), test.getTypeVoie());
        }
        if (test.getVille() != null && expected.getVille() != null) {
            assertEntity(expected.getVille(), test.getVille());
        }
    }

    /**
     * Assertion pour un {@link TypeVoieDto}.
     * @param expected - Le type de voie attendu
     * @param test     - Le type de voie de test
     */
    public static void assertEntity(TypeVoieDto expected, TypeVoieDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getLibelle());
        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getLibelle(), test.getLibelle());
    }

    /**
     * Assertion pour un {@link VilleDto}.
     * @param expected - La ville attendue
     * @param test     - La ville de test
     */
    public static void assertEntity(VilleDto expected, VilleDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getLibelle());
        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getLibelle(), test.getLibelle());

        if (test.getPays() != null && expected.getPays() != null) {
            assertEntity(expected.getPays(), test.getPays());
        }
    }

    /**
     * Assertion pour un {@link PaysDto}.
     * @param expected - Le pays attendu
     * @param test     - Le pays de test
     */
    public static void assertEntity(PaysDto expected, PaysDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getLibelle());
        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getLibelle(), test.getLibelle());
    }

    /**
     * Assertion pour {@link ActiviteDto}.
     * @param expected - l'activité attendue
     * @param test     - l'activité testée
     */
    public static void assertEntity(ActiviteDto expected, ActiviteDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getBudget());
        Assert.assertNotNull(test.getDateDebut());
        Assert.assertNotNull(test.getDateFin());
        Assert.assertNotNull(test.getDescription());
        Assert.assertNotNull(test.getIntitule());
        Assert.assertNotNull(test.getNombrePlace());
        Assert.assertNotNull(test.getPrix());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getBudget(), test.getBudget(), DELTA_EQUALS);
        Assert.assertEquals(expected.getDateDebut(), test.getDateDebut());
        Assert.assertEquals(expected.getDateFin(), test.getDateFin());
        Assert.assertEquals(expected.getDescription(), test.getDescription());
        Assert.assertEquals(expected.getIntitule(), test.getIntitule());
        Assert.assertEquals(expected.getNombrePlace(), test.getNombrePlace());
        Assert.assertEquals(expected.getPrix(), test.getPrix(), DELTA_EQUALS);

        if (test.getResponsableActivite() != null && expected.getResponsableActivite() != null) {
            assertEntity(expected.getResponsableActivite(), test.getResponsableActivite());
        }

        if (test.getEvenement() != null && expected.getEvenement() != null) {
            assertEntity(expected.getEvenement(), test.getEvenement());
        }

        if (test.getTypeActivite() != null && expected.getTypeActivite() != null) {
            assertEntity(expected.getTypeActivite(), test.getTypeActivite());
        }
    }

    /**
     * Assertion pour {@link TypeActiviteDto}.
     * @param expected - le type d'activité attendu
     * @param test     - le type d'activité testé
     */
    public static void assertEntity(TypeActiviteDto expected, TypeActiviteDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getDescription());
        Assert.assertNotNull(test.getIntitule());
    }

    /**
     *
     * @param expected l'inscription attendue.
     * @param test     l'inscription que l'on teste.
     */
    public static void assertEntity(InscriptionDto expected, InscriptionDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getNom());
        Assert.assertNotNull(test.getPrenom());
        Assert.assertNotNull(test.getMail());
        Assert.assertNotNull(test.getDateNaissance());
        Assert.assertNotNull(test.getMontantRegle());
        Assert.assertNotNull(test.getEvenement());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getNom(), test.getNom());
        Assert.assertEquals(expected.getMail(), test.getMail());
        Assert.assertEquals(expected.getDateNaissance(), test.getDateNaissance());
        Assert.assertEquals(expected.getPrenom(), test.getPrenom());
        Assert.assertEquals(expected.getMontantRegle(), test.getMontantRegle(), DELTA_EQUALS);

    }

    /**
     *
     * @param expected la tache attendue.
     * @param test     la tache testée.
     */
    public static void assertEntity(TacheDto expected, TacheDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getDeadline());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getDeadline(), test.getDeadline());

        assertEquals(expected.getStatut(), test.getStatut());

        if (expected.getTypeTaches() != null && test.getTypeTaches() != null) {
            assertEntity(expected.getTypeTaches(), test.getTypeTaches());
        }
        if (expected.getBenevole() != null && test.getBenevole() != null) {
            assertEntity(expected.getBenevole(), test.getBenevole());
        }

    }

    /**
     *
     * @param expected statut tache attendu.
     * @param test     statut tache testée.
     */
    public static void assertEquals(StatutTacheDto expected, StatutTacheDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getIntitule());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getIntitule(), test.getIntitule());

    }

    /**
     *
     * @param expected ressource attendue.
     * @param test     ressource testée.
     */
    public static void assertEntity(RessourceDto expected, RessourceDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getMail());
        Assert.assertNotNull(test.getNumeroTelephone());
        Assert.assertNotNull(test.getRaisonSociale());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getMail(), test.getMail());
        Assert.assertEquals(expected.getNumeroTelephone(), test.getNumeroTelephone());
        Assert.assertEquals(expected.getRaisonSociale(), test.getRaisonSociale());

        if (test.getNumeroRCS() != null && expected.getNumeroRCS() != null) {
            Assert.assertEquals(expected.getNumeroRCS(), test.getNumeroRCS());
        }
        if (test.getPrix() != 0 && expected.getPrix() != 0) {
            Assert.assertEquals(expected.getPrix(), test.getPrix(), DELTA_EQUALS);
        }
        if (test.getQuantite() != 0 && expected.getQuantite() != 0) {
            Assert.assertEquals(expected.getQuantite(), test.getQuantite(), DELTA_EQUALS);
        }

    }

    /**
     *
     * @param expected type tache attendu.
     * @param test     type de la tache testé.
     */
    public static void assertEntity(TypeTacheDto expected, TypeTacheDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getId());
        Assert.assertNotNull(test.getDescription());
        Assert.assertNotNull(test.getIntitule());

        Assert.assertEquals(expected.getId(), test.getId());
        Assert.assertEquals(expected.getDescription(), test.getDescription());
        Assert.assertEquals(expected.getIntitule(), test.getIntitule());

    }

    /**
     * .
     * @param expected .
     * @param test     .
     */
    public static void assertEntity(FluxDto expected, FluxDto test) {
        Assert.assertNotNull(test);
        Assert.assertNotNull(test.getMontant());

        Assert.assertEquals(expected.getMontant(), test.getMontant(), DELTA_EQUALS);
    }
}
