package fr.isika.projet4.data.impl.test.evenement;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.impl.DaoEvenement;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * Test de la méthode de récupération d'une liste d'{@link EvenementDto} par id.
 * @author ludwig
 *
 */
public class TestEvenementByRA {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoEvenement dao = new DaoEvenement();

    /**
     * L'id du ra dans le cas nominal.
     */
    private static final Integer ID_RA_NOMINAL = 12;

    /**
     * L'évenement attendu.
     */
    private static EvenementDto eventNominal = new EvenementDto();

    /**
     * L'id de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_EVENT_NOMINAL = 1;

    /**
     * Le prix de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final double PRIX_EVENT_NOMINAL = 30;

    /**
     * Le budget de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final double BUDGET_EVENT_NOMINAL = 50000;

    /**
     * L'id de l'adresse l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 8;

    /**
     * L'id du type de voie lié à l'adresse de
     * l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 2;

    /**
     * L'id de la ville liée à l'adresse de
     * l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_VILLE_EVENT = 6;

    /**
     * L'id du pays de l'{@link TestEvenementByRA#eventNominal}.
     */
    private static final Integer ID_PAYS_EVENT = 9;

    /**
     * L'id du ra affecté à plusieurs évenement.
     */
    private static final Integer ID_RA_ALTERNATIF = 11;

    /**
     * La taille de la liste d'évenement attendue lié à
     * {@link TestEvenementByRA#ID_RA_NOMINAL}.
     */
    private static final int TAILLE_LISTE = 2;

    /**
     * L'id du ra dans le cas où il n'est affecté à aucun évenement.
     */
    private static final Integer ID_RA_ECHEC_LISTE_VIDE = 4;

    /**
     * Exception attendue.
     */
    private static RAException expectedException = new RAException();

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     * @throws ParseException - Si erreur de parse du {@link SimpleDateFormat}
     */
    @BeforeClass
    public static void beforeAllTests() throws ParseException {
        eventNominal.setId(ID_EVENT_NOMINAL);
        eventNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse("16-01-2020 20:00:00"));
        eventNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse("16-01-2020 23:30:00"));
        eventNominal.setIntitule("Concert promotionnel Isika");
        eventNominal.setDescription("Concert promotionnel d'artistes montants de la region de Malakoff");
        eventNominal.setPrix(PRIX_EVENT_NOMINAL);
        eventNominal.setBudget(BUDGET_EVENT_NOMINAL);
        eventNominal.setAdresse(new AdresseDto());
        eventNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        eventNominal.getAdresse().setNomVoie("Thunder Bluff");
        eventNominal.getAdresse().setNumeroVoie("13");
        eventNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        eventNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        eventNominal.getAdresse().getTypeVoie().setLibelle("chemin");
        eventNominal.getAdresse().setVille(new VilleDto());
        eventNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        eventNominal.getAdresse().getVille().setLibelle("Tana");
        eventNominal.getAdresse().getVille().setPays(new PaysDto());
        eventNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        eventNominal.getAdresse().getVille().getPays().setLibelle("Madagascar");
        expectedException = new RAException("Vous n'êtes affecté à aucun évenement", null);
        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoEvenement.class);
    }

    /**
     * Test de la liste d'{@link EvenementDto}.
     * @throws RAException  - Si la liste est vide.
     * @throws SQLException - Si l'unité de persistence est hors-service.
     */
    @Test
    public void testNominal() throws RAException, SQLException {
        List<EvenementDto> retour = dao.getByRA(ID_RA_NOMINAL);
        for (EvenementDto e : retour) {
            AssertionEntities.assertEntity(eventNominal, e);
        }
    }

    /**
     * Test de la taille de la liste d'{@link EvenementDto}.
     * @throws RAException  - Si la liste est vide.
     * @throws SQLException - Si l'unié de persistence est hors-service.
     */
    @Test
    public void testTailleListe() throws RAException, SQLException {
        Assert.assertEquals(TAILLE_LISTE, dao.getByRA(ID_RA_ALTERNATIF).size());
    }

    /**
     * Test d'échec dans le cas d'une liste vide.
     * @throws SQLException - Si l'unité de persistence est hors-service.
     */
    @Test
    public void testEchecListeVide() throws SQLException {
        try {
            List<EvenementDto> retour = dao.getByRA(ID_RA_ECHEC_LISTE_VIDE);
            Assert.fail("Echec du test");
        } catch (RAException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }
}
