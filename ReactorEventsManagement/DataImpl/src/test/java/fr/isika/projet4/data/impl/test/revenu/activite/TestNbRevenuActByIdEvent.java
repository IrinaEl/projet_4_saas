package fr.isika.projet4.data.impl.test.revenu.activite;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoRevenusActivite;

/**
 * Classe de test de la méthode
 * {@link IDaoRevenusActivite#nbRevenuActByIdEvent(int)}.
 * @author daphnemerck
 *
 */
public class TestNbRevenuActByIdEvent {

    /**
     * L'interface contenant le service à tester.
     */
    private static IDaoRevenusActivite dao;

    /**
     * L'id de l'{@link fr.isika.projet4.entity.Evenement} concerné.
     */
    private static final int EVENT_ID = 5;

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * Le nombre de {@link fr.isika.projet4.entity.RevenusActivite} attendu.
     */
    private static final long EXPECTED_NB = 4;

    /**
     * La ressource pour démarrer Spring.
     */
    private static ClassPathXmlApplicationContext spring;

    /**
     * Démarrage de Spring et injection du dao.
     */
    @BeforeClass
    public static void initTest() {
        spring = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = spring.getBean(IDaoRevenusActivite.class);
    }

    /**
     * Test du cas nominal de la méthode
     * {@link IDaoRevenusActivite#nbRevenuActByIdEvent(int)}. Elle doit retourner
     * {@link #EXPECTED_NB}.
     */
    @Test
    public void testNominal() {
        long retour = dao.nbRevenuActByIdEvent(EVENT_ID);
        log.debug("RETOUR : " + retour);
        Assert.assertNotNull(retour);
        Assert.assertEquals(EXPECTED_NB, retour);
    }

    /**
     * Fermeture de la ressource qui démarre Spring.
     */
    @AfterClass
    public static void finTests() {
        spring.close();
    }
}
