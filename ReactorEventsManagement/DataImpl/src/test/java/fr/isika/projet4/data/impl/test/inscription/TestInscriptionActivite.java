package fr.isika.projet4.data.impl.test.inscription;

import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoInscription;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.entity.Activite;
import fr.isika.projet4.entity.Inscription;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * classe de test d'une inscription a une {@link Activite}.
 * @author stagiaire
 *
 */
public class TestInscriptionActivite {

    /**
     * Pour tester a l'aide des log.
     */
    private static Logger log = Logger.getLogger(TestInscriptionActivite.class);

    /**
     * Classe contenant le service a tester.
     */
    private static IDaoInscription dao;

    /**
     * L'inscription a inserer.
     */
    private static InscriptionDto inscriptionNominal = new InscriptionDto();

    /**
     * L'activité sur laquelle on veut s'inscrire.
     */
    private static ActiviteDto act = new ActiviteDto();

    /**
     * Le dernier id inseré.
     */
    private static final Integer LAST_INSERT_ID = 6;

    /**
     * montant reglé du nominal.
     */
    private static final Double MONTANT_REGLE = 100.2;

    /**
     * L'inscription attendue.
     */
    private static Inscription expectedInscription = new Inscription();

    /**
     * L'inscription pour un echec pour mail non renseigné.
     */
    private static InscriptionDto inscriptionEchecMailNull = new InscriptionDto();

    /**
     * Erreur attendue.
     */
    private static InscriptionException expectedException = new InscriptionException("contraintes", null);

    /**
     * inscription deja présente sur une activité.
     */
    private static InscriptionDto inscriptionDejaInscrit = new InscriptionDto();

    /**
     * methode before tests.
     */
    @BeforeClass
    public static void beforeAllTests() {
        inscriptionNominal.setId(null);
        inscriptionNominal.setNom("Borius");
        inscriptionNominal.setPrenom("totor");
        inscriptionNominal.setMail("mail@mail.com");
        inscriptionNominal.setMontantRegle(MONTANT_REGLE);
        inscriptionNominal.setDateNaissance(new Date());

        expectedInscription.setId(LAST_INSERT_ID);
        expectedInscription.setNom(inscriptionNominal.getNom());
        expectedInscription.setPrenom(inscriptionNominal.getPrenom());
        expectedInscription.setMail(inscriptionNominal.getMail());
        expectedInscription.setMontantRegle(inscriptionNominal.getMontantRegle());
        expectedInscription.setDateNaissance(inscriptionNominal.getDateNaissance());

        inscriptionEchecMailNull.setId(null);
        inscriptionEchecMailNull.setNom("Bastien");
        inscriptionEchecMailNull.setPrenom("Boris");
        inscriptionEchecMailNull.setMail(null);
        inscriptionEchecMailNull.setMontantRegle(MONTANT_REGLE);
        inscriptionEchecMailNull.setDateNaissance(new Date());

        inscriptionDejaInscrit.setId(null);
        inscriptionDejaInscrit.setDateNaissance(new Date());
        inscriptionDejaInscrit.setMail("samantha.north@gmail.com");
        inscriptionDejaInscrit.setMontantRegle(MONTANT_REGLE);
        inscriptionDejaInscrit.setNom("Samantha");
        inscriptionDejaInscrit.setPrenom("test");

        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoInscription.class);
    }

    /**
     * Test du nominal d'inscription a une activité.
     * @throws InscriptionException pas ici car nominal.
     */
    @Test
    public void testInscriptionNominal() throws InscriptionException {
        act.setId(2);
        InscriptionDto retour = dao.inscriptionActivite(act, inscriptionNominal);
        AssertionEntities.assertEntity(EntityToDto.transformEntityToDto(expectedInscription), retour);
    }

    /**
     * methode test echec de l'inscription a une activité.
     */
    @Test
    public void testEchecInscriptionMailNull() {
        act.setId(1);
        try {
            dao.inscriptionActivite(act, inscriptionEchecMailNull);
        } catch (InscriptionException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }

    /**
     * methode de test echec car déjà inscrit.
     */
    @Test
    public void testEchecDejaInscrit() {
        act.setId(1);
        Boolean retour = dao.verifInscriptionActivite(act, inscriptionDejaInscrit);
        Assert.assertEquals(true, retour);
    }

}
