package fr.isika.projet4.data.impl.test.evenement;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.impl.DaoEvenement;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * Test de la méthode de récupération d'une liste de tout les
 * {@link EvenementDto} en cours .
 * @author stagiaire
 *
 */
public class TestAllEvenement {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoEvenement dao = new DaoEvenement();

    /**
     * L'évenement attendu.
     */
    private static EvenementDto eventNominal = new EvenementDto();

    /**
     * L'id de l'{@link #eventNominal}.
     */
    private static final Integer ID_EVENT_NOMINAL = 2;

    /**
     * Le prix de l'{@link #eventNominal}.
     */
    private static final double PRIX_EVENT_NOMINAL = 10;

    /**
     * Le budget de l'{@link #eventNominal}.
     */
    private static final double BUDGET_EVENT_NOMINAL = 1000;

    /**
     * L'id de l'adresse l'{@link #eventNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 4;

    /**
     * L'id du type de voie lié à l'adresse de l'{@link #eventNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 4;

    /**
     * L'id de la ville liée à l'adresse de l'{@link #eventNominal}.
     */
    private static final Integer ID_VILLE_EVENT = 1;

    /**
     * L'id du pays de l'{@link #eventNominal}.
     */
    private static final Integer ID_PAYS_EVENT = 1;

    /**
     * L'id du type de l'{@link #eventNominal}.
     */
    private static final int ID_TYPE_EVENT = 1;

    /**
     * La taille de la liste d'évenement attendue lié à {@link #ID_RA_NOMINAL}.
     */
    private static final int TAILLE_LISTE = 4;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext ac;

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     * @throws ParseException - Si erreur de parse du {@link SimpleDateFormat}
     */
    @BeforeClass
    public static void beforeAllTests() throws ParseException {
        eventNominal.setId(ID_EVENT_NOMINAL);
        eventNominal.setBudget(BUDGET_EVENT_NOMINAL);
        eventNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("31-12-2019 00:00:00"));
        eventNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("01-01-2020 04:00:00"));
        eventNominal.setIntitule("Soir�e jour de l'an Isika");
        eventNominal
                .setDescription(
                        "Soir�e du jour de l'an � Isika ! Boissons, snacks, dancefloor dans la grande salle, blind-tests musicaux ! Alcool � volont� selon les limites du stock disponible ...");
        eventNominal.setPrix(PRIX_EVENT_NOMINAL);
        eventNominal.setAdresse(new AdresseDto());
        eventNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        eventNominal.getAdresse().setNomVoie("General leclerc");
        eventNominal.getAdresse().setNumeroVoie("12");
        eventNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        eventNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        eventNominal.getAdresse().getTypeVoie().setLibelle("avenue");
        eventNominal.getAdresse().setVille(new VilleDto());
        eventNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        eventNominal.getAdresse().getVille().setLibelle("Paris");
        eventNominal.getAdresse().getVille().setPays(new PaysDto());
        eventNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        eventNominal.getAdresse().getVille().getPays().setLibelle("France");
        eventNominal.setTypeEvenement(new TypeEvenementDto());
        eventNominal.getTypeEvenement().setId(ID_TYPE_EVENT);
        eventNominal.getTypeEvenement().setLibelle("Majeur");
        ac = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = ac.getBean(IDaoEvenement.class);
    }

    /**
     * Test de la liste d'{@link EvenementDto}.
     * @throws EventException - S'il y'a aucun évenement pour le moment. - Si
     *                        l'unité de persistence est hors-service.
     */
    @Test
    public void testNominal() throws EventException {
        EvenementDto retour = dao.getAll().get(0);
        AssertionEntities.assertEntity(eventNominal, retour);
    }

    /**
     * Test de la taille de la liste.
     * @throws EventException - S'il y'a aucun évenement pour le moment. - Si
     *                        l'unité de persistence est hors-service.
     */
    @Test
    public void testTailleListe() throws EventException {
        Assert.assertEquals(TAILLE_LISTE, dao.getAll().size());
    }

    /**
     * Fermeture de la ressource qui permet de démarrer Spring.
     */
    @AfterClass
    public static void close() {
        ac.close();
    }
}
