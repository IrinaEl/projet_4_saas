/**
 * Package contenant les tests pour la DAO concernant l'entité
 * {@link fr.isika.projet4.entity.Depense}.
 * @author stagiaire
 *
 */
package fr.isika.projet4.data.impl.test.depense;