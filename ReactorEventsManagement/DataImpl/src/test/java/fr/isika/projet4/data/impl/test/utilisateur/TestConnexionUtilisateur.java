package fr.isika.projet4.data.impl.test.utilisateur;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoUtilisateur;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.exceptions.ConnexionException;

/**
 * class de test pour la connexion d'un Utlisateur.
 * @author stagiaire
 */
public class TestConnexionUtilisateur {

    /**
     * Interface utilisateur dao dans l'optique d'effectué les tests sur les
     * méthodes de cette derniere.
     */
    private static IDaoUtilisateur dao;

    /**
     * attribut mail sponsor.
     */
    private String mailSponsor = "jamespat@gmail.com";

    /**
     * attribut mdp sponsor.
     */
    private String mdpSponsor = "james";

    /**
     * attribut mail existant.
     */
    private String mailExist = "pierre-orgrimmar@wow.fr";

    /**
     * Attribut mot de passe existant.
     */
    private String mdpExist = "orgrimmar";

    /**
     * Attribut mail n'existe pas .
     */
    private String mailExistPas = "existepas@gmail.com";

    /**
     * Attribut mdp n'existe pas.
     */
    private String mdpExistPas = "mdpexistepas";

    /**
     * constante id expected User.
     */
    private static final Integer ID_EXPECTED_USER = 10;

    /**
     * Utilisateur attendu.
     */
    private UtilisateurDto expectedUser = new UtilisateurDto(ID_EXPECTED_USER, mailExist, mdpExist, "Orgrimmar", "Pierre");

    /**
     * LP attendu.
     */
    private static UtilisateurDto expectedLP = new UtilisateurDto(15, "ludwig-llofler@hotmail.fr", "ludwig", "LLofler", "Ludwig");

    /**
     * RA attendu.
     */
    private UtilisateurDto expectedRA = new UtilisateurDto(11, "louis-leglas@start.fr", "leglas", "LeGlas", "Louis");

    /**
     * Instanciation Connexion Exception.
     */
    private ConnexionException expectedException = new ConnexionException("Login/Mot de passe éronné", null);

    /**
     * Instanciation Connexion Exception sponsor.
     */
    private ConnexionException expectedExceptionSponsor = new ConnexionException(
            "Vous n'êtes pas autorisé à vous connecter sur cet espace",
            null);

    /**
     * Chargement contexte spring avant les tests.
     */
    @BeforeClass
    public static void beforeAllTest() {
        expectedLP.setType("");
        expectedLP.getType();
        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoUtilisateur.class);
    }

    /**
     * Méthode de test nominal de la méthode connexion utilisateur.
     * @throws ConnexionException .
     */
    @Test
    public void testNominal() throws ConnexionException {
        UtilisateurDto retour = dao.seConnecterBack(mailExist, mdpExist);
        AssertionEntities.assertEntity(expectedUser, retour);
    }

    /**
     * Test pour augmenter le code coverage.
     * @throws ConnexionException
     */
    @Test
    public void testNominalLP() throws ConnexionException {
        UtilisateurDto retour = dao.seConnecterBack(expectedLP.getMail(), expectedLP.getMdp());
        AssertionEntities.assertEntity(expectedLP, retour);
    }

    /**
     * Test pour augmenter le code coverage.
     * @throws ConnexionException
     */
    @Test
    public void testNominalRA() throws ConnexionException {
        UtilisateurDto retour = dao.seConnecterBack(expectedRA.getMail(), expectedRA.getMdp());
        AssertionEntities.assertEntity(expectedRA, retour);
    }

    /**
     * test pour echec connexion mail.
     */
    @Test
    public void testEchec() {
        try {
            dao.seConnecterBack(mailExistPas, mdpExist);
            Assert.fail("ca ne doit pas marcher");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }

    /**
     * test pour echec connexion mot de passe.
     */
    @Test
    public void testEchecMdp() {
        try {
            dao.seConnecterBack(mailExist, mdpExistPas);
            Assert.fail("ca ne doit pas marcher");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }

    /**
     * test si sponsor tente de se connecter sur le back.
     */
    @Test
    public void testEchecSponsor() {
        try {
            dao.seConnecterBack(mailSponsor, mdpSponsor);
            Assert.fail("ca ne doit pas marcher!");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedExceptionSponsor.getMessage(), e.getMessage());
        }

    }
}