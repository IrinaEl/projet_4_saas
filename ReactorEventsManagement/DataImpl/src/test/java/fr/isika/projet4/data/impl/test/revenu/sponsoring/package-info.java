/**
 * Package comprenant les classes de tests des méthodes de
 * {@link fr.isika.projet4.data.api.IDaoRevenuSponsoring}.
 * @author daphnemerck
 *
 */
package fr.isika.projet4.data.impl.test.revenu.sponsoring;