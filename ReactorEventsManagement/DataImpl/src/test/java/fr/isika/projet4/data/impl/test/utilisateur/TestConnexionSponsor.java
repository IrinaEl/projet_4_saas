package fr.isika.projet4.data.impl.test.utilisateur;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoUtilisateur;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.exceptions.ConnexionException;

/**
 * class de test pour la connexion d'un Sponsor.
 * @author stagiaire
 */
public class TestConnexionSponsor {

    /**
     * Interface utilisateur dao dans l'optique d'effectué les tests sur les
     * méthodes de cette derniere.
     */
    private static IDaoUtilisateur dao;

    /**
     * attribut mail existant.
     */
    private String mailExist = "jamespat@gmail.com";

    /**
     * Attribut mot de passe existant.
     */
    private String mdpExist = "james";

    /**
     * Attribut mail n'existe pas .
     */
    private String mailExistPas = "boin@gmail.com";

    /**
     * Attribut mdp n'existe pas.
     */
    private String mdpExistPas = "mdpexistepas";

    /**
     * Sponsor attendu.
     */
    private SponsorDto expectedSponsor = new SponsorDto(1, mailExist, mdpExist, "StPatrick", "James", "", null);

    /**
     * Instanciation Connexion Exception.
     */
    private static ConnexionException expectedException = new ConnexionException("Login/Mot de passe éronné", null);

    /**
     * Chargement contexte spring avant les tests.
     */
    @BeforeClass
    public static void beforeAllTest() {
        expectedException = new ConnexionException("Login/Mot de passe éronné", null);
        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoUtilisateur.class);
    }

    /**
     * Méthode de test nominal de la méthode connexion utilisateur.
     * @throws ConnexionException .
     */
    @Test
    public void testNominal() throws ConnexionException {
        SponsorDto retour = dao.seConnecterFront(mailExist, mdpExist);
        AssertionEntities.assertEntity(expectedSponsor, retour);
    }

    /**
     * test pour echec connexion mail.
     */
    @Test
    public void testEchec() {
        try {
            dao.seConnecterFront(mailExistPas, mdpExist);
            Assert.fail("ca ne doit pas marcher");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }

    /**
     * test pour echec connexion mot de passe.
     */
    @Test
    public void testEchecMdp() {
        try {
            dao.seConnecterFront(mailExist, mdpExistPas);
            Assert.fail("ca ne doit pas marcher");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }
}