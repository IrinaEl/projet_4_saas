package fr.isika.projet4.data.impl.test.activite;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoActivite;
import fr.isika.projet4.data.impl.DaoActivite;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * classe de test pour la méthode get Activite by id.
 * @author stagiaire
 *
 */
public class TestGetActiviteById {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoActivite dao = new DaoActivite();

    /**
     * id de l'activité à tester.
     */
    private static final int ID_EXPECTED_ACTIVITE_NOMINALE = 2;

    /**
     * activité attendue.
     */
    private static final ActiviteDto EXPECTED_ACTIVITE = new ActiviteDto();

    /**
     * le budget de l'activité attendue en retour du test nominal.
     */
    private static final double BUDGET_EXPECTED_ACTIVITE = 150;

    /**
     * le prix de l'activité attendue en retour du test nominal.
     */
    private static final double PRIX_EXPECTED_ACTIVITE = 10;

    /**
     * le nombre de places de l'activité attendue en retour du test nominal.
     */
    private static final Integer NB_PLACES_ACTIVITE = 20;

    /**
     * id activité qui n'existe pas.
     */
    private static final Integer ID_ACTIVITE_INEXISTANTE = 100;

    /**
     * exception attendue en cas d'échec.
     */
    private static final ActiviteException EXPECTED_EXCEPTION = new ActiviteException("L'activité recherchée n'existe pas", null);

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * Chargement contexte Spring avant tests et variables de tests.
     * @throws ParseException - Si erreur de parse sur le parse de la date.
     */
    @BeforeClass
    public static void beforeAllTests() throws ParseException {
        EXPECTED_ACTIVITE.setId(ID_EXPECTED_ACTIVITE_NOMINALE);
        EXPECTED_ACTIVITE.setBudget(BUDGET_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setDateDebut(SDF.parse("25-11-2019"));
        EXPECTED_ACTIVITE.setDateFin(SDF.parse("26-11-2019"));
        EXPECTED_ACTIVITE.setIntitule("Atelier peinture");
        EXPECTED_ACTIVITE.setDescription("activite de test 2");
        EXPECTED_ACTIVITE.setPrix(PRIX_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setNombrePlace(NB_PLACES_ACTIVITE);

        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoActivite.class);

    }

    /**
     * test nominal.
     * @throws ActiviteException c
     */
    @Test
    public void testNominal() throws ActiviteException {
        ActiviteDto retour = dao.getActiviteById(ID_EXPECTED_ACTIVITE_NOMINALE);
        AssertionEntities.assertEntity(EXPECTED_ACTIVITE, retour);

    }

    /**
     * test pour une activité recherchée par un ID qui n'existe pas.
     * @throws ActiviteException lancée si activité pas trouvée.
     */
    @Test
    public void testIDInexistant() throws ActiviteException {
        try {
            dao.getActiviteById(ID_ACTIVITE_INEXISTANTE);
            Assert.fail("Test KO - Activité avec cet identifiant existe");
        } catch (ActiviteException e) {
            Assert.assertEquals(EXPECTED_EXCEPTION.getMessage(), e.getMessage());
        }

    }

}
