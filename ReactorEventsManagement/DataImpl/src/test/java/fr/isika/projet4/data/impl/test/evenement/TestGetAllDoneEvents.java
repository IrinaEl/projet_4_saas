package fr.isika.projet4.data.impl.test.evenement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoEvenement;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * Classe de test de la méthode {@link IDaoEvenement#getAllDoneEvents()}.
 * * @author stagiaire
 *
 */
public class TestGetAllDoneEvents {

    /**
     * La classe dans laquelle se trouve le service à tester.
     */
    private static IDaoEvenement dao;

    /**
     * Le nombre d'éléments que la liste doit contenir.
     */
    private static final int EXPECTED_LIST_SIZE = 5;

    /**
     * L'événement attendu dans l'enchaînement nominal.
     */
    private static EvenementDto evenementNominal = new EvenementDto();

    /**
     * L'identifiant de l'{@link #evenementNominal}.
     */
    private static final int ID_EVENT = 3;

    /**
     * Le prix de l'{@link #evenementNominal}.
     */
    private static final double PRIX_EVENT_NOMINAL = 20;

    /**
     * Le budget de l'{@link #evenementNominal}.
     */
    private static final double BUDGET_EVENT_NOMINAL = 15000;

    /**
     * L'id de l'adresse l'{@link #evenementNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 7;

    /**
     * L'id du type de voie lié à l'adresse de l'{@link #evenementNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 6;

    /**
     * L'id de la ville liée à l'adresse de l'{@link #evenementNominal}.
     */
    private static final Integer ID_VILLE_EVENT = 10;

    /**
     * L'id du pays de l'{@link #evenementNominal}.
     */
    private static final Integer ID_PAYS_EVENT = 7;

    /**
     * L'id du type de l'{@link #evenementNominal}.
     */
    private static final int ID_TYPE_EVENT = 2;

    /**
     * Pour initialiser spring.
     */
    private static ClassPathXmlApplicationContext spring;

    /**
     * Initialisation de spring et remplissage de l'evenement.
     * @throws ParseException .
     */
    @BeforeClass
    public static void initTest() throws ParseException {
        evenementNominal.setId(ID_EVENT);
        evenementNominal.setBudget(BUDGET_EVENT_NOMINAL);
        evenementNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse("02-12-2019 19:00:00"));
        evenementNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse("02-12-2019 23:00:00"));
        evenementNominal.setIntitule("Cocktail d'accueil de la promo CDI05");
        evenementNominal.setDescription(
                "Cocktail party d'accueil pour la promotion CDI05. Champagne et foie gras, musicien de jazz et piste de danse. Tenue correcte exig�e.");
        evenementNominal.setPrix(PRIX_EVENT_NOMINAL);
        evenementNominal.setAdresse(new AdresseDto());
        evenementNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        evenementNominal.getAdresse().setNomVoie("Mosolee");
        evenementNominal.getAdresse().setNumeroVoie("3bis");
        evenementNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        evenementNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        evenementNominal.getAdresse().getTypeVoie().setLibelle("intersection");
        evenementNominal.getAdresse().setVille(new VilleDto());
        evenementNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        evenementNominal.getAdresse().getVille().setLibelle("Berne");
        evenementNominal.getAdresse().getVille().setPays(new PaysDto());
        evenementNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        evenementNominal.getAdresse().getVille().getPays().setLibelle("Suisse");
        evenementNominal.setTypeEvenement(new TypeEvenementDto());
        evenementNominal.getTypeEvenement().setId(ID_TYPE_EVENT);
        evenementNominal.getTypeEvenement().setLibelle("Mineur");

        spring = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = spring.getBean(IDaoEvenement.class);
    }

    /**
     * Test nominal de la méthode {@link IDaoEvenement#getAllDoneEvents()}.
     * @throws EventException S'il n'y a aucun événement correspondant.
     */
    @Test
    public void testNominal() throws EventException {
        List<EvenementDto> events = dao.getAllDoneEvents();
        Assert.assertNotNull(events);
        Assert.assertEquals(EXPECTED_LIST_SIZE, events.size());
        for (EvenementDto evenementDto : events) {
            if (evenementDto.getId() == ID_EVENT) {
                AssertionEntities.assertEntity(evenementNominal, evenementDto);
            }
        }
    }

    /**
     * Pour refermer la ressource qui initialise spring.
     */
    @AfterClass
    public static void afterTest() {
        spring.close();
    }

}
