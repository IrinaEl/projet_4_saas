package fr.isika.projet4.data.impl.test.recette;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoRecette;

/**
 * Classe de test pour {@link IDaoRecette#sommeRecetteParEvenement(int)}.
 * @author stagiaire
 *
 */
public class TestSommeRecetteParEvenement {

    /**
     * Le service la classe du service à tester.
     */
    private static IDaoRecette dao;

    /**
     * Le résultat attendu après l'addition de toutes les recettes.
     */
    private static final double EXPECTED_SOMME = 7610.0;

    /**
     * L'identifiant de l'événement que l'on cherche.
     */
    private static final int ID_EVENEMENT = 1;

    /**
     * L'identifiant d'un événement qui n'a aucune recette.
     */
    private static final int MAUVAIS_ID_EVENEMENT = 100;

    /**
     * La valeur que doit retourner la méthode s'il n'y a aucune recette dans
     * l'unité de persistence.
     */
    private static final double RESULTAT_SI_EXCEPTION = 0.0;

    /**
     * Delta maximum entre la valeur attendu et la valeur test.
     */
    private static final double DELTA_EQUALS = 0.01;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext ac;

    /**
     * Méthode d'initiation pour les tests et qui lance Spring.
     */
    @BeforeClass
    public static void initTest() {
        ac = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = ac.getBean(IDaoRecette.class);
    }

    /**
     * Test nominal de la méthode
     * {@link IDaoRecette#sommeRecetteParEvenement(int)}. Elle doit retourner
     * {@link #EXPECTED_SOMME}.
     */
    @Test
    public void testNominalSommeRecetteParEvenement() {
        double retour = dao.sommeRecetteParEvenement(ID_EVENEMENT);
        Assert.assertNotNull(retour);
        Assert.assertEquals(EXPECTED_SOMME, retour, DELTA_EQUALS);
    }

    /**
     * Tes d'échec de la méthode
     * {@link IDaoRecette#sommeRecetteParEvenement(int)}. Elle doit retourner
     * {@link #RESULTAT_SI_EXCEPTION}
     */
    @Test
    public void testEchec() {
        double retour = dao.sommeRecetteParEvenement(MAUVAIS_ID_EVENEMENT);
        Assert.assertNotNull(retour);
        Assert.assertEquals(RESULTAT_SI_EXCEPTION, retour, DELTA_EQUALS);
    }

    /**
     * Fermeture de la ressource qui permet de démarrer Spring.
     */
    @AfterClass
    public static void close() {
        ac.close();
    }
}
