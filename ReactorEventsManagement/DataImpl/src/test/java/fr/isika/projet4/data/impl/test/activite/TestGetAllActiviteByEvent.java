package fr.isika.projet4.data.impl.test.activite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoActivite;
import fr.isika.projet4.data.impl.DaoActivite;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.dto.TypeActiviteDto;
import fr.isika.projet4.dto.TypeEvenementDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * Test de la méthode de récupération d'une liste d'{@link ActiviteDto} par
 * {@link EvenementDto}.
 * @author stagiaire
 *
 */
public class TestGetAllActiviteByEvent {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoActivite dao = new DaoActivite();

    /**
     * L'id de l'evenement dans le cas nominal.
     */
    private static final Integer ID_EVENT_NOMINAL = 1;

    /**
     * L'activité attendu.
     */
    private static ActiviteDto activiteNominal = new ActiviteDto();

    /**
     * L'id de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer ID_ACTIVITE_NOMINAL = 1;

    /**
     * Le nombre de place de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer NOMBRE_PLACE_ACTIVITE_NOMINAL = 20;

    /**
     * Le prix de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final double PRIX_ACTIVITE_NOMINAL = 0;

    /**
     * Le budget de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final double BUDGET_ACTIVITE_NOMINAL = 200;

    /**
     * L'id de type d'activite de {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer ID_TYPE_ACTIVITE_ACTIVITE_NOMINAL = 3;

    /**
     * L'id du responsable d'activite
     * {@link TestGetAllActiviteByEvent#activiteNominal}.
     */
    private static final Integer ID_RESPONSABLE_ACTIVITE_ACTIVITE_NOMINAL = 12;

    /**
     * Le prix de l'evenement de l'{@link #activiteNominal}.
     */
    private static final double PRIX_EVENT_ACTIVITE_NOMINAL = 30;

    /**
     * Le budget de l'evenement de l'{@link #activiteNominal}.
     */
    private static final double BUDGET_EVENT_ACTIVITE_NOMINAL = 50000;

    /**
     * L'id de l'adresse de l'evenement de l'{@link #activiteNominal}.
     */
    private static final Integer ID_ADRESSE_EVENT_ACTIVITE_NOMINAL = 8;

    /**
     * L'id du type de voie lié à l'adresse de l'evenement de
     * l'{@link #activiteNominal}.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT_ACTIVITE_NOMINAL = 2;

    /**
     * L'id de la ville liée à l'adresse de l'evenement de
     * l'{@link #activiteNominal}.
     */
    private static final Integer ID_VILLE_EVENT_ACTIVITE_NOMINAL = 6;

    /**
     * L'id du pays de l'evenement de l'{@link #activiteNominal}.
     */
    private static final Integer ID_PAYS_EVENT_ACTIVITE_NOMINAL = 9;

    /**
     * L'id du type de l'evenement de l'{@link #activiteNominal}.
     */
    private static final int ID_TYPE_EVENT_ACTIVITE_NOMINAL = 1;

    /**
     * La taille de la liste de l'évenement attendue.
     */
    private static final int TAILLE_LIST = 1;

    /**
     * L'id de l'evenement dans le cas alternatif (s'il n'y a aucune activité pour
     * cette évenement).
     */
    private static final Integer ID_EVENT_ECHEC_LISTE_VIDE = 3;

    /**
     * Exception attendue.
     */
    private static ActiviteException expectedException = new ActiviteException();

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext ac;

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     * @throws ParseException - Si erreur de parse du {@link SimpleDateFormat}
     */
    @BeforeClass
    public static void beforAllTests() throws ParseException {
        activiteNominal.setId(ID_ACTIVITE_NOMINAL);
        activiteNominal.setBudget(BUDGET_ACTIVITE_NOMINAL);
        activiteNominal.setDateDebut(SDF.parse("25-11-2019 00:00:00"));
        activiteNominal.setDateFin(SDF.parse("26-11-2019 00:00:00"));
        activiteNominal.setDescription("activite de test");

        activiteNominal.setEvenement(new EvenementDto());
        activiteNominal.getEvenement().setId(ID_EVENT_NOMINAL);
        activiteNominal.getEvenement().setBudget(BUDGET_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().setDateDebut(SDF.parse("16-01-2020 20:00:00"));
        activiteNominal.getEvenement().setDateFin(SDF.parse("16-01-2020 23:30:00"));
        activiteNominal.getEvenement().setDescription("Concert promotionnel d'artistes montants de la region de Malakoff");
        activiteNominal.getEvenement().setIntitule("Concert promotionnel Isika");
        activiteNominal.getEvenement().setPrix(PRIX_EVENT_ACTIVITE_NOMINAL);

        activiteNominal.getEvenement().setAdresse(new AdresseDto());
        activiteNominal.getEvenement().getAdresse().setId(ID_ADRESSE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().setNomVoie("Thunder Bluff");
        activiteNominal.getEvenement().getAdresse().setNumeroVoie("13");

        activiteNominal.getEvenement().getAdresse().setTypeVoie(new TypeVoieDto());
        activiteNominal.getEvenement().getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().getTypeVoie().setLibelle("chemin");

        activiteNominal.getEvenement().getAdresse().setVille(new VilleDto());
        activiteNominal.getEvenement().getAdresse().getVille().setId(ID_VILLE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().getVille().setLibelle("Tana");

        activiteNominal.getEvenement().getAdresse().getVille().setPays(new PaysDto());
        activiteNominal.getEvenement().getAdresse().getVille().getPays().setId(ID_PAYS_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getAdresse().getVille().getPays().setLibelle("Madagascar");

        activiteNominal.getEvenement().setTypeEvenement(new TypeEvenementDto());
        activiteNominal.getEvenement().getTypeEvenement().setId(ID_TYPE_EVENT_ACTIVITE_NOMINAL);
        activiteNominal.getEvenement().getTypeEvenement().setLibelle("Majeur");

        activiteNominal.setIntitule("Prestation du groupe HQM");
        activiteNominal.setNombrePlace(NOMBRE_PLACE_ACTIVITE_NOMINAL);
        activiteNominal.setPrix(PRIX_ACTIVITE_NOMINAL);

        activiteNominal.setResponsableActivite(new ResponsableActiviteDto());
        activiteNominal.getResponsableActivite().setId(ID_RESPONSABLE_ACTIVITE_ACTIVITE_NOMINAL);
        activiteNominal.getResponsableActivite().setMail("michel-gnome@wow.fr");
        activiteNominal.getResponsableActivite().setNom("Gnome");
        activiteNominal.getResponsableActivite().setPrenom("Michel");

        activiteNominal.setTypeActivite(new TypeActiviteDto());
        activiteNominal.getTypeActivite().setId(ID_TYPE_ACTIVITE_ACTIVITE_NOMINAL);
        activiteNominal.getTypeActivite().setIntitule("Concert");
        activiteNominal.getTypeActivite().setDescription("");

        expectedException = new ActiviteException("il n'y a pas encore d'activité pour cet évenement", null);

        ac = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = ac.getBean(IDaoActivite.class);

    }

    /**
     * Test de la liste d'{@link ActiviteDto}.
     * @throws ActiviteException - Si la liste est vide. - Si l'unité de persistence
     *                           est hors-service.
     */
    @Test
    public void testNominal() throws ActiviteException {

        List<ActiviteDto> activites = dao.getAllActiviteByEvent(ID_EVENT_NOMINAL);

        for (ActiviteDto activiteDto : activites) {
            AssertionEntities.assertEntity(activiteNominal, activiteDto);
        }
    }

    /**
     * Test de la taille de la liste.
     * @throws ActiviteException - Si la liste est vide. - Si l'unité de persistence
     *                           est hors-service.
     */
    @Test
    public void testTailleListe() throws ActiviteException {
        Assert.assertEquals(TAILLE_LIST, dao.getAllActiviteByEvent(ID_EVENT_NOMINAL).size());
    }

    /**
     * Test d'échec dans le cas d'une liste vide (l'évenement n'a pas encore des
     * activités).
     */
    @Test
    public void testEchecListeVide() {
        try {
            dao.getAllActiviteByEvent(ID_EVENT_ECHEC_LISTE_VIDE);
            Assert.fail("Echec du test");
        } catch (ActiviteException e) {
            Assert.assertEquals(expectedException.getMessage(), e.getMessage());
        }
    }

    /**
     * Fermeture de la ressource qui permet de démarrer Spring.
     */
    @AfterClass
    public static void close() {
        ac.close();
    }
}
