package fr.isika.projet4.data.impl.test.ressource;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoActivite;
import fr.isika.projet4.data.impl.DaoActivite;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * test.
 * @author stagiaire
 *
 */
public class TestAffecterRessource {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoActivite dao = new DaoActivite();

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * id de la ressource à affecter.
     */
    private static final int ID_RESSOURCE_AFFECTEE = 9;

    /**
     * id de l'activité à laquelle affecter.
     */
    private static final int ID_ACTIVITE = 2;

    /**
     * id de l'activité à tester.
     */
    private static final int ID_EXPECTED_ACTIVITE_NOMINALE = 2;

    /**
     * activité attendue en retour.
     */
    private static final ActiviteDto EXPECTED_ACTIVITE = new ActiviteDto();

    /**
     * le budget de l'activité attendue en retour du test nominal.
     */
    private static final double BUDGET_EXPECTED_ACTIVITE = 150;

    /**
     * le prix de l'activité attendue en retour du test nominal.
     */
    private static final double PRIX_EXPECTED_ACTIVITE = 10;

    /**
     * le nombre de places de l'activité attendue en retour du test nominal.
     */
    private static final Integer NB_PLACES_ACTIVITE = 20;

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * avant tous les tests.
     * @throws ParseException b
     */
    @BeforeClass
    public static void beforeAlltests() throws ParseException {
        EXPECTED_ACTIVITE.setId(ID_EXPECTED_ACTIVITE_NOMINALE);
        EXPECTED_ACTIVITE.setBudget(BUDGET_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setDateDebut(SDF.parse("25-11-2019"));
        EXPECTED_ACTIVITE.setDateFin(SDF.parse("26-11-2019"));
        EXPECTED_ACTIVITE.setIntitule("Atelier peinture");
        EXPECTED_ACTIVITE.setDescription("activite de test 2");
        EXPECTED_ACTIVITE.setPrix(PRIX_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setNombrePlace(NB_PLACES_ACTIVITE);

        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoActivite.class);
    }

    /**
     * nominal.
     * @throws ActiviteException  b
     * @throws RessourceException
     */
    @Test
    public void testNominal() throws ActiviteException, RessourceException {

        ActiviteDto retour = dao.affecterRessource(ID_RESSOURCE_AFFECTEE, ID_ACTIVITE);
        AssertionEntities.assertEntity(EXPECTED_ACTIVITE, retour);

    }

}
