package fr.isika.projet4.data.impl.test.tache;

import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoTache;
import fr.isika.projet4.data.impl.DaoTache;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.StatutTacheDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * test de la méthode de recherche de tâches à remplir pour une activité
 * d'activité.
 * @author stagiaire
 *
 */

public class TestGetTachesByActivite {

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * classe contenant la méthode testée.
     */
    private static IDaoTache dao = new DaoTache();

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer ID_ACTIVITE_NOMINALE = 4;

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer ID_TACHE_NOMINALE = 1;

    /**
     * identifiant de l'activité dont les tâches sont recherchées.
     */
    private static final Integer DEADLINE_NOMINAL = 120;

    /**
     * la tâche attendue en retour dans la liste testée dans le test nominal.
     */
    private static final TacheDto EXPECTED_TACHE = new TacheDto();

    /**
     * statut de la tache attendue nominale.
     */
    private static final StatutTacheDto EXPECTED_STATUT_NOMINAL = new StatutTacheDto();

    /**
     * id du statut attendu.
     */
    private static final Integer EXPECTED_STATUT_ID = 1;

    /**
     * nb tache activite.
     */
    private static final int NB_TACHE_PAR_ACTIVITE = 1;

    /**
     * id activite sans tâche.
     */
    private static final int ID_ACTIVITE_SANS_TACHES = 6;

    /**
     * exception si pas de tâches affectées.
     */
    private static final TacheException EXPECTED_EXCEPTION = new TacheException("Aucune tâche n'a été créée pour cette activité", null);

    /**
     * Définition des valeurs de la tâche testée + Chargement du contexte spring.
     *
     */
    @BeforeClass
    public static void beforeAllTests() {

        EXPECTED_STATUT_NOMINAL.setId(EXPECTED_STATUT_ID);
        EXPECTED_STATUT_NOMINAL.setIntitule("EN COURS");
        EXPECTED_TACHE.setId(ID_TACHE_NOMINALE);
        EXPECTED_TACHE.setDeadline(DEADLINE_NOMINAL);
        EXPECTED_TACHE.setStatut(EXPECTED_STATUT_NOMINAL);
        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoTache.class);

    }

    /**
     * test du cas nominal.
     * @throws TacheException       - au cas où aucune tâche n'est affectée au RA
     * @throws PersistenceException - au cas où la base de données n'est pas
     *                              accessible
     */
    @Test
    public void testNominal() throws TacheException, PersistenceException {
        List<TacheDto> taches = dao.getTachesByActivite(ID_ACTIVITE_NOMINALE);
        for (TacheDto t : taches) {
            AssertionEntities.assertEntity(EXPECTED_TACHE, t);
        }

    }

    /**
     * test nb tache par activite.
     * @throws TacheException       exception si liste vide
     * @throws PersistenceException exception si pas accès u serveur
     */
    @Test
    public void testNombretache() throws TacheException, PersistenceException {
        Assert.assertEquals(NB_TACHE_PAR_ACTIVITE, dao.getTachesByActivite(ID_ACTIVITE_NOMINALE).size());
    }

    /**
     * test activité sans taches.
     * @throws PersistenceException si pas accès au serveur
     */
    @Test
    public void testActiviteSansTache() throws PersistenceException {
        try {
            dao.getTachesByActivite(ID_ACTIVITE_SANS_TACHES);
            Assert.fail("Test KO - l'activité a des tâches");
        } catch (TacheException e) {
            Assert.assertEquals(EXPECTED_EXCEPTION.getMessage(), e.getMessage());
        }
    }

    /**
     * Fermeture de la ressource qui permet de démarrer Spring.
     */
    @AfterClass
    public static void close() {
        bf.close();
    }
}