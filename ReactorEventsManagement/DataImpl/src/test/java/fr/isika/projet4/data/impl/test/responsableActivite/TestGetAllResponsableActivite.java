package fr.isika.projet4.data.impl.test.responsableActivite;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoResponsableActivite;
import fr.isika.projet4.data.impl.DaoResponsableActivite;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * Test de la méthode de récupération d'une liste de tous les
 * {@linkResponsableActiviteDto}.
 * @author stagiaire
 *
 */
public class TestGetAllResponsableActivite {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoResponsableActivite dao = new DaoResponsableActivite();

    /**
     * La taille de la liste d'évenement attendue lié à {@link #ID_RA_NOMINAL}.
     */
    private static final int TAILLE_LISTE = 4;

    /**
     * Chargement contexte Spring avant tests + variable de tests.
     */
    @BeforeClass
    public static void beforeAllTests() {
        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoResponsableActivite.class);
    }

    /**
     * Test de la liste d'{@link ResponsableActiviteDto}.
     * @throws RAException - S'il y'a aucun responsable activite pour le moment. -
     *                     Si l'unité de persistence est hors-service.
     */
    @Test
    public void testTailleListe() throws RAException {
        Assert.assertEquals(TAILLE_LISTE, dao.getAllResponsableActivite().size());
    }
}
