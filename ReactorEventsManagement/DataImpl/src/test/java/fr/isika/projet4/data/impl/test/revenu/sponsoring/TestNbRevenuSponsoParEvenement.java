package fr.isika.projet4.data.impl.test.revenu.sponsoring;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoRevenuSponsoring;
import fr.isika.projet4.data.api.IDaoRevenusActivite;

/**
 * Classe de test de la méthode
 * {@link IDaoRevenusActivite#nbRevenuActByIdEvent(int)}.
 * @author daphnemerck
 *
 */
public class TestNbRevenuSponsoParEvenement {

    /**
     * L'interface contenant le service à tester.
     */
    private static IDaoRevenuSponsoring dao;

    /**
     * L'id de l'{@link fr.isika.projet4.entity.Evenement} concerné.
     */
    private static final int EVENT_ID = 5;

    /**
     * Le nombre de {@link fr.isika.projet4.entity.RevenusActivite} attendu.
     */
    private static final long EXPECTED_NB = 1;

    /**
     * La ressource pour démarrer Spring.
     */
    private static ClassPathXmlApplicationContext spring;

    /**
     * Démarrage de Spring et injection du dao.
     */
    @BeforeClass
    public static void initTest() {
        spring = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = spring.getBean(IDaoRevenuSponsoring.class);
    }

    /**
     * Test du cas nominal de la méthode
     * {@link IDaoRevenusActivite#nbRevenuActByIdEvent(int)}. Elle doit retourner
     * {@link #EXPECTED_NB}.
     */
    @Test
    public void testNominal() {
        long retour = dao.nbRevSponsoParEvenement(EVENT_ID);
        Assert.assertNotNull(retour);
        Assert.assertEquals(EXPECTED_NB, retour);
    }

    /**
     * Fermeture de la ressource qui démarre Spring.
     */
    @AfterClass
    public static void finTests() {
        spring.close();
    }
}
