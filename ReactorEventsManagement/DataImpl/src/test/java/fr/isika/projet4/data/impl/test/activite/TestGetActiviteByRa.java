package fr.isika.projet4.data.impl.test.activite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoActivite;
import fr.isika.projet4.data.impl.DaoActivite;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * test de la méthode de recherche d'activités affectées à un responsable
 * d'activité.
 * @author stagiaire
 *
 */
public class TestGetActiviteByRa {

    /**
     * logger.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * classe contenenant la méthode testée.
     */
    private static IDaoActivite dao = new DaoActivite();

    /**
     * identificat du RA dans le test nominal.
     */
    private static final Integer ID_RA_NOMINAL = 13;

    /**
     * identifiant du RA n'ayant aucune activité.
     */
    private static final Integer ID_RA_SANS_ACTIVITE = 14;

    /**
     * l'activité attendue en retour dans la liste testée dans le test nominal.
     */
    private static final ActiviteDto EXPECTED_ACTIVITE = new ActiviteDto();

    /**
     * identifiant de l'activité attendue dans le cas nominal.
     */
    private static final Integer ID_EXPECTED_ACTIVITE = 2;

    /**
     * le budget de l'activité attendue en retour du test nominal.
     */

    private static final double BUDGET_EXPECTED_ACTIVITE = 150;

    /**
     * le nombre de places de l'activité attendue en retour du test nominal.
     */
    private static final Integer NB_PLACES_ACTIVITE = 20;

    /**
     * le prix de l'activité attendue en retour du test nominal.
     */
    private static final double PRIX_EXPECTED_ACTIVITE = 10;

    /**
     * nombre d'activités affectées à un RA testé.
     */
    private static final int NB_ACTIVITE_DU_RA = 1;

    /**
     * l'exception attendue en cas de retour d'une liste d'activité vide.
     */
    private static final ActiviteException EXPECTED_EXCEPTION = new ActiviteException(
            "Aucune activité n'a été affectée à ce responsable d'activité", null);

    /***
     * objet pour formater et parser les dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Définition des valeurs de l'activité testée + Chargement du contexte
     * spring.
     * @throws ParseException - en cas d'erreur de parsing par
     *                        {@link SimpleDateFormat}
     */

    @BeforeClass
    public static void beforeAllTests() throws ParseException {

        EXPECTED_ACTIVITE.setId(ID_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setBudget(BUDGET_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setDateDebut(SDF.parse("25-11-2019"));
        EXPECTED_ACTIVITE.setDateFin(SDF.parse("26-11-2019"));
        EXPECTED_ACTIVITE.setIntitule("Atelier peinture");
        EXPECTED_ACTIVITE.setDescription("activite de test 2");
        EXPECTED_ACTIVITE.setPrix(PRIX_EXPECTED_ACTIVITE);
        EXPECTED_ACTIVITE.setNombrePlace(NB_PLACES_ACTIVITE);

        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoActivite.class);
    }

    /**
     * test du cas nominal.
     * @throws ActiviteException    - au cas où aucune activité n'est affectée au
     *                              RA
     * @throws PersistenceException - au cas où la base de données n'est pas
     *                              accessible
     */
    @Test
    public void testNominal() throws ActiviteException, PersistenceException {

        List<ActiviteDto> activites = dao.getActivitesByRa(ID_RA_NOMINAL);

        for (ActiviteDto a : activites) {
            AssertionEntities.assertEntity(EXPECTED_ACTIVITE, a);
        }

    }

    /**
     * test du nombre d'activité affectées à un RA donné.
     * @throws ActiviteException    - au cas où aucune activité n'est affectée au
     *                              RA
     * @throws PersistenceException - au cas où la base de données n'est pas
     *                              accessible
     */
    @Test
    public void testNombreActivite() throws ActiviteException, PersistenceException {
        Assert.assertEquals(NB_ACTIVITE_DU_RA, dao.getActivitesByRa(ID_RA_NOMINAL).size());

    }

    /**
     * test du cas de retour d'une liste vide, pour le responsable auquel aucune
     * activité n'a été affectée.
     * @throws PersistenceException - Si l'unité de persistence est hors-service.
     */
    @Test
    public void testListeSansActivites() throws PersistenceException {
        try {
            dao.getActivitesByRa(ID_RA_SANS_ACTIVITE);

            Assert.fail("Test KO - Ce responsable a des activités");
        } catch (ActiviteException e) {

            Assert.assertEquals(EXPECTED_EXCEPTION.getMessage(), e.getMessage());
        }

    }

    /**
     * Fermeture de la ressource qui permet de démarrer Spring.
     */
    @AfterClass
    public static void close() {
        bf.close();
    }

}
