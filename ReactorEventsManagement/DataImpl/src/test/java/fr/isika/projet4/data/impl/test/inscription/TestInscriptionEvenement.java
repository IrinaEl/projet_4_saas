package fr.isika.projet4.data.impl.test.inscription;

import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoInscription;
import fr.isika.projet4.data.assembler.EntityToDto;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.entity.Inscription;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Classe de test inscription evenement.
 * @author stagiaire
 *
 */
public class TestInscriptionEvenement {

    /**
     * Pour tester a l'aide des log.
     */
    private static Logger log = Logger.getLogger(TestInscriptionEvenement.class);

    /**
     * Classe contenant le service a tester.
     */
    private static IDaoInscription dao;

    /**
     * L'evenement nominal.
     */
    private static EvenementDto evenementNominal = new EvenementDto();

    /**
     * L'inscription a inserer.
     */
    private static InscriptionDto inscriptionNominal = new InscriptionDto();

    /**
     * Le dernier id inseré.
     */
    private static final Integer LAST_INSERT_ID = 6;

    /**
     * montant reglé du nominal.
     */
    private static final Double MONTANT_REGLE = 100.2;

    /**
     * L'inscription attendue.
     */
    private static Inscription expectedInscription = new Inscription();

    /**
     * inscription deja présente sur une activité.
     */
    private static InscriptionDto inscriptionDejaInscrit = new InscriptionDto();

    /**
     * methode before tests.
     */
    @BeforeClass
    public static void beforeAllTests() {

        evenementNominal.setId(1);
        inscriptionNominal.setId(null);
        inscriptionNominal.setNom("Borius");
        inscriptionNominal.setPrenom("totor");
        inscriptionNominal.setMail("mail@mail.com");
        inscriptionNominal.setMontantRegle(MONTANT_REGLE);
        inscriptionNominal.setDateNaissance(new Date());

        expectedInscription.setId(LAST_INSERT_ID);
        expectedInscription.setNom(inscriptionNominal.getNom());
        expectedInscription.setPrenom(inscriptionNominal.getPrenom());
        expectedInscription.setMail(inscriptionNominal.getMail());
        expectedInscription.setMontantRegle(inscriptionNominal.getMontantRegle());
        expectedInscription.setDateNaissance(inscriptionNominal.getDateNaissance());

        inscriptionDejaInscrit.setId(null);
        inscriptionDejaInscrit.setDateNaissance(new Date());
        inscriptionDejaInscrit.setMail("samantha.north@gmail.com");
        inscriptionDejaInscrit.setMontantRegle(MONTANT_REGLE);
        inscriptionDejaInscrit.setNom("Samantha");
        inscriptionDejaInscrit.setPrenom("test");

        BeanFactory bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoInscription.class);
    }

    /**
     * Test du nominal d'inscription a une activité.
     * @throws InscriptionException pas ici car nominal.
     */
    @Test
    public void testInscriptionNominal() throws InscriptionException {
        InscriptionDto retour = dao.inscriptionEvenement(evenementNominal, inscriptionNominal);
        AssertionEntities.assertEntity(EntityToDto.transformEntityToDto(expectedInscription), retour);
    }

    /**
     * Test de l'echec a inscription evenement car déjà inscrit.
     * @throws InscriptionException car deja inscrit.
     */
    @Test
    public void testEchecDejaInscritEvenement() throws InscriptionException {
        Boolean retour = dao.verifInscriptionEvenement(evenementNominal, inscriptionDejaInscrit);
        Assert.assertEquals(true, retour);
    }
}
