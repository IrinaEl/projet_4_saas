package fr.isika.projet4.data.impl.test.ressource;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoRessource;
import fr.isika.projet4.data.impl.DaoRessource;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe de test pour getAllBenevoles.
 * @author stagiaire
 *
 */
public class TestGetRessourcesByActivite {

    /**
     * logger.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoRessource dao = new DaoRessource();

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * id activite;
     */
    private static final Integer ID_ACTIVITE_NOMINALE = 2;

    /**
     * .
     */
    private static final RessourceDto EXPECTED_RESSOURCE = new RessourceDto();

    /**
     * .
     */
    private static final Integer ID_EXPECTED_RESSOURCE = 2;

    /**
     * .
     */
    private static final Integer ID_EXPECTED_PRIX = 350;

    /**
     * .
     */
    private static final Integer ID_EXPECTED_QUANTITE = 0;

    // private static TypeIntervenantDto EXPECTED_TYPE = new TypeIntervenantDto();

    @BeforeClass
    public static void beforeAll() {
        EXPECTED_RESSOURCE.setId(ID_EXPECTED_RESSOURCE);
        EXPECTED_RESSOURCE.setMail("prestataire@gmail.com");
        EXPECTED_RESSOURCE.setNumeroRCS("CRC563");
        EXPECTED_RESSOURCE.setNumeroTelephone("0177896352");
        EXPECTED_RESSOURCE.setPrix(ID_EXPECTED_PRIX);
        EXPECTED_RESSOURCE.setQuantite(ID_EXPECTED_QUANTITE);
        EXPECTED_RESSOURCE.setRaisonSociale("Prestataire & C0");

        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoRessource.class);

    }

    @Test
    public void testNominal() throws RessourceException {

        List<RessourceDto> liste = dao.getRessourceByActivite(ID_ACTIVITE_NOMINALE);
        log.fatal(liste);
        for (RessourceDto r : liste) {
            AssertionEntities.assertEntity(EXPECTED_RESSOURCE, r);
        }

    }

}
