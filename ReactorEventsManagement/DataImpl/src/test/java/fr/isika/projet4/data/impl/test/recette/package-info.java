/**
 * Package de tests pour la DAO concernant l'entite
 * {@link fr.isika.projet4.entity.Recette}.
 * @author stagiaire
 *
 */
package fr.isika.projet4.data.impl.test.recette;