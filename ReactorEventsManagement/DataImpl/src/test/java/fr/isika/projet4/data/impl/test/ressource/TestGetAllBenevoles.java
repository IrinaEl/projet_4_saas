package fr.isika.projet4.data.impl.test.ressource;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.data.api.IDaoRessource;
import fr.isika.projet4.data.impl.DaoRessource;
import fr.isika.projet4.data.impl.test.AssertionEntities;
import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe de test pour getAllBenevoles.
 * @author stagiaire
 *
 */
public class TestGetAllBenevoles {

    /**
     * Classe contenant le service à tester.
     */
    private static IDaoRessource dao = new DaoRessource();

    /**
     * id ben1.
     */
    private static final int ID_BEN1 = 9;

    /**
     * id ben2.
     */
    private static final int ID_BEN2 = 10;

    /**
     * id ben3.
     */
    private static final int ID_BEN3 = 11;

    /**
     * id ben4.
     */
    private static final int ID_BEN4 = 12;

    /**
     * ben1.
     */
    private static final IntervenantExterneDto BEN_1 = new IntervenantExterneDto();

    /**
     * ben2.
     */
    private static final IntervenantExterneDto BEN_2 = new IntervenantExterneDto();

    /**
     * ben3.
     */
    private static final IntervenantExterneDto BEN_3 = new IntervenantExterneDto();

    /**
     * ben4.
     */
    private static final IntervenantExterneDto BEN_4 = new IntervenantExterneDto();

    /**
     * id du type intervenant externe.
     */
    private static final int ID_TYPE_INTERVENANT_EXTERNE = 10;

    /**
     * nb de bénévoles dans la base.
     */
    private static final int NB_BENEVOLES = 4;

    /**
     * Service qui permet de démarrer Spring.
     */
    private static ClassPathXmlApplicationContext bf;

    /**
     * liste de bénévoles retournés.
     */
    private static final List<IntervenantExterneDto> EXPECTED_LISTE = new ArrayList<>();

    /**
     * index premier benevole.
     */
    private static final int INDEX_0 = 0;

    /**
     * index 2eme benevole.
     */
    private static final int INDEX_1 = 1;

    /**
     * index 3eme benevole.
     */
    private static final int INDEX_2 = 2;

    /**
     * index 4eme benevole.
     */
    private static final int INDEX_3 = 3;

    /**
     * Chargement contexte Spring avant tests + variables de tests.
     */
    @BeforeClass
    public static void beforeAllTests() {

        setListeRetour();

        bf = new ClassPathXmlApplicationContext("classpath:spring-jpa.xml");
        dao = bf.getBean(IDaoRessource.class);
    }

    /**
     * test nominal.
     * @throws RessourceException si problème
     */
    @Test
    public void testNbBenevoles() throws RessourceException {

        List<IntervenantExterneDto> retour = dao.getAllBenevoles(ID_TYPE_INTERVENANT_EXTERNE);
        Assert.assertEquals(NB_BENEVOLES, retour.size());

    }

    /**
     * test nominal.
     * @throws RessourceException n
     */
    @Test
    public void testNominal() throws RessourceException {
        List<IntervenantExterneDto> retour = dao.getAllBenevoles(ID_TYPE_INTERVENANT_EXTERNE);
        AssertionEntities.assertEntity(BEN_1, retour.get(INDEX_0));
        AssertionEntities.assertEntity(BEN_2, retour.get(INDEX_1));
        AssertionEntities.assertEntity(BEN_3, retour.get(INDEX_2));
        AssertionEntities.assertEntity(BEN_4, retour.get(INDEX_3));

    }

    /**
     * méthode privée pour attribuée les valeurs aux bénévoes et les ajouter dans la
     * liste.
     */

    private static void setListeRetour() {

        BEN_1.setId(ID_BEN1);
        BEN_1.setMail("mail@yahoo.fr");
        BEN_1.setNumeroTelephone("0565323134");
        BEN_1.setRaisonSociale("Justine Duchamps");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        BEN_2.setId(ID_BEN2);
        BEN_2.setMail("mail@yahoo.fr");
        BEN_2.setNumeroTelephone("0565323134");
        BEN_2.setRaisonSociale("Youssef Bardi");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        BEN_3.setId(ID_BEN3);
        BEN_3.setMail("mail@yahoo.fr");
        BEN_3.setNumeroTelephone("0565323134");
        BEN_3.setRaisonSociale("Taystee Jefferson");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        BEN_4.setId(ID_BEN4);
        BEN_4.setMail("mail@yahoo.fr");
        BEN_4.setNumeroTelephone("0565323134");
        BEN_4.setRaisonSociale("Alexandra Le Du");
//        ben1.setQuantite(0);
//        ben1.setPrix(0);

        EXPECTED_LISTE.add(BEN_1);
        EXPECTED_LISTE.add(BEN_2);
        EXPECTED_LISTE.add(BEN_3);
        EXPECTED_LISTE.add(BEN_4);

    }

}
