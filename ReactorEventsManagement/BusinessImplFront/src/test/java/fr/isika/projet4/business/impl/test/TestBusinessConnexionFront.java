package fr.isika.projet4.business.impl.test;

import java.lang.reflect.Field;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.business.api.front.IBusinessConnexion;
import fr.isika.projet4.business.impl.front.BusinessConnexion;
import fr.isika.projet4.business.impl.wrap.args.ArgumentsConnexion;
import fr.isika.projet4.business.impl.wrap.retour.RetourConnexionFront;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.exceptions.ConnexionException;

/**
 * classe de test Connexion business.
 * @author stagiaire
 */
public class TestBusinessConnexionFront {

    /**
     * Creation du mock.
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * classe contenent le service à tester.
     */
    private static IBusinessConnexion bu = new BusinessConnexion();

    /**
     * attribut mail existant.
     */
    private static String mailExist = "jamespat@gmail.com";

    /**
     * attribut mail existant mais pas un sponsor.
     */
    private static String mailExistButNotSponsor = "paul-paul@paul.com";

    /**
     * attribut mdp existant mais pas un sponsor.
     */
    private static String mdpExistButNotSponsor = "paul";

    /**
     * mail n'existe pas.
     */
    private static String mailExistPas = "nbjjo@nkhjj.fr";

    /**
     * Attribut mot de passe existant.
     */
    private static String mdpExist = "james";

    /**
     * Sponsor pour ajout nominal.
     */
    private static SponsorDto expectedSponsor = new SponsorDto(1, mailExist, null, "StPatrick", "James", "", null);

    /**
     * Sponsor avec echec de mail.
     */
    private static SponsorDto sponsorEchecMail = new SponsorDto(1, mailExistPas, mdpExist, "StPatrick", "James", "", null);

    /**
     * Utilisateur autre tentant de se connecter sur l'espace sponsor.
     */
    private static SponsorDto notSponsor = new SponsorDto(1, mailExistButNotSponsor, mdpExistButNotSponsor, "Paul", "Paul", "", null);

    /**
     * Exception mail attendu.
     */
    private static ConnexionException expectedExeptionMail = new ConnexionException(
            "Login/Mot de passe erroné(s)", null);

    /**
     * Exception attendu lorsque qu'un utilisateur tente de se connecter en tant que
     * sponsor.
     */
    private static ConnexionException expectedExeptionNotSponsorButUserQuandMeme = new ConnexionException(
            "Vous n'êtes pas autorisé à vous connecter sur cet espace",
            null);

    /**
     * Pour améliorer le code coverage.
     */
    private static ArgumentsConnexion args = new ArgumentsConnexion();

    /**
     * Pour améliorer le code coverage.
     */
    private static RetourConnexionFront retour = new RetourConnexionFront();

    /**
     * Apres tous les tests.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
    }

    /**
     * Injection de Mock.
     * @throws ConnexionException .
     */
    @BeforeClass
    public static void initTests() throws ConnexionException {
        args.setMail(mailExist);
        args.setMdp(mdpExist);
        retour.setUser(expectedSponsor);
        retour.setError(expectedExeptionMail);
        EasyMock.expect(mock.seConnecterFront(args.getMail(), args.getMdp())).andReturn(retour.getUser());
        EasyMock.expect(mock.seConnecterFront(mailExistButNotSponsor, mdpExistButNotSponsor))
                .andThrow(expectedExeptionNotSponsorButUserQuandMeme);
        EasyMock.expect(mock.seConnecterFront(mailExistPas, mdpExist)).andThrow(retour.getError());
        EasyMock.replay(mock);

        BeanFactory bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-front.xml");
        bu = bf.getBean(IBusinessConnexion.class);

        try {
            Class<? extends IBusinessConnexion> clazz = bu.getClass();
            Field daoField = clazz.getDeclaredField("proxy");
            daoField.setAccessible(true);
            daoField.set(bu, mock);
            daoField.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * test nominal.
     * @throws ConnexionException .
     */
    @Test
    public void testNominal() throws ConnexionException {
        SponsorDto retour = bu.seConnecterFront(mailExist, mdpExist);
        AssertionEntities.assertEntity(expectedSponsor, retour);
    }

    /**
     * Test echec mail.
     */
    @Test
    public void testEchecMail() {
        try {
            bu.seConnecterFront(mailExistPas, mdpExist);
            Assert.fail("ca ne doit pas fonctionner");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedExeptionMail.getMessage(), e.getMessage());
        }
    }

    /**
     * Test pour exception si utilisateur mais pas sponsor.
     */
    @Test
    public void testUserButNotSponsor() {
        try {
            bu.seConnecterFront(mailExistButNotSponsor, mdpExistButNotSponsor);
            Assert.fail("ca ne doit pas fonctionner");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedExeptionNotSponsorButUserQuandMeme.getMessage(), e.getMessage());
        }
    }

}
