package fr.isika.projet4.business.impl.test;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.business.api.front.IBusinessConsultation;
import fr.isika.projet4.business.impl.front.BusinessConsultation;
import fr.isika.projet4.business.impl.wrap.retour.RetourConsultation;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.AdresseDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.PaysDto;
import fr.isika.projet4.dto.TypeVoieDto;
import fr.isika.projet4.dto.VilleDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * classe de test business Consultation Evenement.
 * @author stagiaire
 */
public class TestBusinessConsultationEvenement {

    /**
     * Creation du mock.
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * classe contenent le service à tester.
     */
    private static IBusinessConsultation bu = new BusinessConsultation();

    /**
     * L'évenement attendu.
     */
    private static EvenementDto eventNominal = new EvenementDto();

    /**
     * L'id de l'événement.
     */
    private static final Integer ID_EVENT_NOMINAL = 1;

    /**
     * Le prix de l'événement.
     */
    private static final double PRIX_EVENT_NOMINAL = 30;

    /**
     * Le budget de l'événement.
     */
    private static final double BUDGET_EVENT_NOMINAL = 50000;

    /**
     * L'id de l'adresse l'événement.
     */
    private static final Integer ID_ADRESSE_EVENT_NOMINAL = 8;

    /**
     * L'id du type de voie lié à l'adresse de l'événement.
     */
    private static final Integer ID_TYPE_VOIE_ADRESSE_EVENT = 2;

    /**
     * date de debut de l'evenement 1.
     */
    private static final String DATE_DEBUT = "16-01-2020 20:00:00";

    /**
     * date de fin de l'evenement 1.
     */
    private static final String DATE_FIN = "16-01-2020 23:30:00";

    /**
     * L'id de la ville liée à l'adresse de l'événement.
     */
    private static final Integer ID_VILLE_EVENT = 6;

    /**
     * L'id du pays de l'événement.
     */
    private static final Integer ID_PAYS_EVENT = 9;

    /**
     * L'id de la voie de l'événement.
     */
    private static final String NUMERO_VOIE = "13";

    /**
     * Pour améliorer le code coverage.
     */
    private static RetourConsultation retour = new RetourConsultation();

    /**
     * Apres tous les tests.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
    }

    /**
     * Injection de Mock.
     * @throws EventException lorsqu'aucun evenement n'ai trouvé avec l'id.
     * @throws ParseException
     */
    @BeforeClass
    public static void initTests() throws EventException, ParseException {

        eventNominal.setId(ID_EVENT_NOMINAL);
        eventNominal.setDateDebut(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse(DATE_DEBUT));
        eventNominal.setDateFin(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse(DATE_FIN));
        eventNominal.setIntitule("Concert promotionnel Isika");
        eventNominal.setDescription("Concert promotionnel d'artistes montants de la région de Malakoff");
        eventNominal.setPrix(PRIX_EVENT_NOMINAL);
        eventNominal.setBudget(BUDGET_EVENT_NOMINAL);
        eventNominal.setAdresse(new AdresseDto());
        eventNominal.getAdresse().setId(ID_ADRESSE_EVENT_NOMINAL);
        eventNominal.getAdresse().setNomVoie("Thunder Bluff");
        eventNominal.getAdresse().setNumeroVoie(NUMERO_VOIE);
        eventNominal.getAdresse().setTypeVoie(new TypeVoieDto());
        eventNominal.getAdresse().getTypeVoie().setId(ID_TYPE_VOIE_ADRESSE_EVENT);
        eventNominal.getAdresse().getTypeVoie().setLibelle("chemin");
        eventNominal.getAdresse().setVille(new VilleDto());
        eventNominal.getAdresse().getVille().setId(ID_VILLE_EVENT);
        eventNominal.getAdresse().getVille().setLibelle("Tana");
        eventNominal.getAdresse().getVille().setPays(new PaysDto());
        eventNominal.getAdresse().getVille().getPays().setId(ID_PAYS_EVENT);
        eventNominal.getAdresse().getVille().getPays().setLibelle("Madagascar");

        retour.setEvent(eventNominal);

        EasyMock.expect(mock.getEventById(ID_EVENT_NOMINAL)).andReturn(retour.getEvent());
        EasyMock.replay(mock);

        BeanFactory bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-front.xml");
        bu = bf.getBean(IBusinessConsultation.class);

        try {
            Class<? extends IBusinessConsultation> clazz = bu.getClass();
            Field daoField = clazz.getDeclaredField("proxy");
            daoField.setAccessible(true);
            daoField.set(bu, mock);
            daoField.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /**
     * test business .
     * @throws EventException : exception
     */
    @Test
    public void testNominal() throws EventException {
        EvenementDto retour = bu.getEventById(ID_EVENT_NOMINAL);
        AssertionEntities.assertEntity(eventNominal, retour);
    }
}
