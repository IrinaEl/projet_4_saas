package fr.isika.projet4.business.impl.test;

import java.lang.reflect.Field;
import java.util.Date;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.business.api.front.IBusinessInscription;
import fr.isika.projet4.business.impl.front.BusinessInscription;
import fr.isika.projet4.business.impl.wrap.args.ArgumentsInscriptionActivite;
import fr.isika.projet4.business.impl.wrap.retour.RetourInscription;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Classe de test du business {@link BusinessInscription} pour l'inscription a
 * une activité.
 * @author stagiaire
 *
 */
public class TestBusinessInscriptionActivite {

    /**
     * Creation du mock.
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * classe contenent le service à tester.
     */
    private static IBusinessInscription bu = new BusinessInscription();

    /**
     * Inscription pour ajout nominal.
     */
    private static InscriptionDto inscriptionNominal = new InscriptionDto();

    /**
     * l'inscription qu'on attends.
     */
    private static InscriptionDto expectedInscription = new InscriptionDto();

    /**
     * Inscription pour echec deja inscrit sur activité.
     */
    private static InscriptionDto inscriptionEchecDejaInscrit = new InscriptionDto();

    /**
     * Activite sur lquelle on veut s'inscrire.
     */
    private static ActiviteDto act = new ActiviteDto();

    /**
     * Evenement pour l'activité.
     */
    private static EvenementDto event = new EvenementDto();

    /**
     * Montant reglé pour inscription.
     */
    private static final double MONTANT_REGLE = 200.35;

    /**
     * Identifiant attendu pour le cas nominal.
     */
    private static final int LAST_INSERT_ID = 3;

    /**
     * Exception attendue pour deja inscrit a l'activité.
     */
    private static InscriptionException expectedExceptionDejaInscrit = new InscriptionException(
            "Vous êtes déjà enregistré(e) sur cette activité.", null);

    /**
     * Pour améliorer le code coverage.
     */
    private static RetourInscription retour = new RetourInscription();

    /**
     * Pour améliorer le code coverage.
     */
    private static ArgumentsInscriptionActivite args = new ArgumentsInscriptionActivite();

    /**
     * Apres tous les tests.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
    }

    /**
     * Avant les tests.
     * @throws InscriptionException .
     */
    @BeforeClass
    public static void initTests() throws InscriptionException {
        act.setId(2);
        event.setId(2);
        inscriptionNominal.setId(null);
        inscriptionNominal.setNom("testBusiness");
        inscriptionNominal.setPrenom("testBusiness");
        inscriptionNominal.setMail("testBusiness@mail.com");
        inscriptionNominal.setMontantRegle(MONTANT_REGLE);
        inscriptionNominal.setDateNaissance(new Date());
        inscriptionNominal.setEvenement(event);

        expectedInscription.setId(LAST_INSERT_ID);
        expectedInscription.setNom(inscriptionNominal.getNom());
        expectedInscription.setPrenom(inscriptionNominal.getPrenom());
        expectedInscription.setMail(inscriptionNominal.getMail());
        expectedInscription.setMontantRegle(inscriptionNominal.getMontantRegle());
        expectedInscription.setDateNaissance(inscriptionNominal.getDateNaissance());
        expectedInscription.setEvenement(inscriptionNominal.getEvenement());

        inscriptionEchecDejaInscrit.setId(null);
        inscriptionEchecDejaInscrit.setNom("Samantha");
        inscriptionEchecDejaInscrit.setMail("samantha.north@gmail.com");
        inscriptionEchecDejaInscrit.setPrenom("Samantha");
        inscriptionEchecDejaInscrit.setEvenement(event);
        inscriptionEchecDejaInscrit.setMontantRegle(MONTANT_REGLE);
        inscriptionEchecDejaInscrit.setDateNaissance(new Date());

        retour.setInscription(expectedInscription);
        retour.setError(expectedExceptionDejaInscrit);

        args.setParamActivite(act);
        args.setParamInscriptionAct(inscriptionNominal);
        // expects mock nominal
        EasyMock.expect(mock.inscriptionActivite(args.getParamActivite(), args.getParamInscriptionAct()))
                .andReturn(retour.getInscription());
        // expects mock echec
        EasyMock.expect(mock.verifInscriptionActivite(act, inscriptionEchecDejaInscrit)).andReturn(true);
        EasyMock.expect(mock.verifInscriptionActivite(act, inscriptionNominal)).andReturn(false);

        // démarrage du mock
        EasyMock.replay(mock);

        BeanFactory bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-front.xml");
        bu = bf.getBean(IBusinessInscription.class);

        try {
            Class<? extends IBusinessInscription> clazz = bu.getClass();
            Field daoField = clazz.getDeclaredField("proxy");
            daoField.setAccessible(true);
            daoField.set(bu, mock);
            daoField.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode de test d'une inscription activité nominale.
     * @throws InscriptionException pas ici car nominal.
     */
    @Test
    public void testInscriptionNominal() throws InscriptionException {
        InscriptionDto retour = bu.inscriptionActivite(act, inscriptionNominal);
        AssertionEntities.assertEntity(expectedInscription, retour);
    }

    /**
     * Methode de test d'une inscription activité avec echec car déjà inscrit.
     * @throws InscriptionException exception a retourner car déjà inscrit.
     */
    @Test
    public void testEchecInscriptionDejaInscrit() throws InscriptionException {
        try {
            bu.inscriptionActivite(act, inscriptionEchecDejaInscrit);
            Assert.fail("ça doit pas marcher");
        } catch (InscriptionException e) {
            Assert.assertEquals(retour.getError().getMessage(), e.getMessage());
        }
    }
}
