package fr.isika.projet4.business.impl.test;

import java.lang.reflect.Field;
import java.util.Date;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.business.api.front.IBusinessInscription;
import fr.isika.projet4.business.impl.front.BusinessInscription;
import fr.isika.projet4.business.impl.wrap.args.ArgumentsInscriptionEvenement;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Classe de test du business {@link BusinessInscription} pour l'inscription a
 * un evenement.
 * @author stagiaire
 */
public class TestBusinessInscriptionEvenement {

    /**
     * Creation du mock.
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * classe contenent le service à tester.
     */
    private static IBusinessInscription bu = new BusinessInscription();

    /**
     * Inscription pour ajout nominal.
     */
    private static InscriptionDto inscriptionNominal = new InscriptionDto();

    /**
     * l'inscription qu'on attends.
     */
    private static InscriptionDto expectedInscription = new InscriptionDto();

    /**
     * Inscription pour echec deja inscrit sur activité.
     */
    private static InscriptionDto inscriptionEchecDejaInscrit = new InscriptionDto();

    /**
     * Evenement ou l'on souhaite s'inscrire.
     */
    private static EvenementDto event = new EvenementDto();

    /**
     * Montant reglé pour inscription.
     */
    private static final double MONTANT_REGLE = 200.35;

    /**
     * Identifiant attendu pour le cas nominal.
     */
    private static final int LAST_INSERT_ID = 3;

    /**
     * Identifiant evenement nominal.
     */
    private static final int ID_EVENEMENT = 3;

    /**
     * Exception attendue pour deja inscrit a l'activité.
     */
    private static InscriptionException expectedExceptionDejaInscrit = new InscriptionException(
            "Vous êtes déjà enregistré(e) sur cet évènement.", null);

    /**
     * Pour le code coverage.
     */
    private static ArgumentsInscriptionEvenement args = new ArgumentsInscriptionEvenement();

    /**
     * Apres tous les tests.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
    }

    /**
     * Avant tout les tests.
     */
    @BeforeClass
    public static void initTests() {
        inscriptionNominal.setId(null);
        inscriptionNominal.setNom("testBusiness");
        inscriptionNominal.setPrenom("testBusiness");
        inscriptionNominal.setMail("testBusiness@mail.com");
        inscriptionNominal.setMontantRegle(MONTANT_REGLE);
        inscriptionNominal.setDateNaissance(new Date());
        inscriptionNominal.setEvenement(event);

        expectedInscription.setId(LAST_INSERT_ID);
        expectedInscription.setNom(inscriptionNominal.getNom());
        expectedInscription.setPrenom(inscriptionNominal.getPrenom());
        expectedInscription.setMail(inscriptionNominal.getMail());
        expectedInscription.setMontantRegle(inscriptionNominal.getMontantRegle());
        expectedInscription.setDateNaissance(inscriptionNominal.getDateNaissance());
        expectedInscription.setEvenement(inscriptionNominal.getEvenement());

        inscriptionEchecDejaInscrit.setId(null);
        inscriptionEchecDejaInscrit.setNom("Samantha");
        inscriptionEchecDejaInscrit.setMail("samantha.north@gmail.com");
        inscriptionEchecDejaInscrit.setPrenom("Samantha");
        inscriptionEchecDejaInscrit.setEvenement(event);
        inscriptionEchecDejaInscrit.setMontantRegle(MONTANT_REGLE);
        inscriptionEchecDejaInscrit.setDateNaissance(new Date());

        args.setParamEvenement(event);
        args.setParamInscriptionEvent(inscriptionNominal);
        // expects mock nominal
        try {
            EasyMock.expect(mock.inscriptionEvenement(args.getParamEvenement(), args.getParamInscriptionEvent()))
                    .andReturn(expectedInscription);
        } catch (InscriptionException e1) {
            e1.printStackTrace();
        }
        // expects mock echec
        EasyMock.expect(mock.verifInscriptionEvenement(event, inscriptionEchecDejaInscrit)).andReturn(true);
        EasyMock.expect(mock.verifInscriptionEvenement(event, inscriptionNominal)).andReturn(false);

        // démarrage du mock
        EasyMock.replay(mock);

        BeanFactory bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-front.xml");
        bu = bf.getBean(IBusinessInscription.class);

        try {
            Class<? extends IBusinessInscription> clazz = bu.getClass();
            Field daoField = clazz.getDeclaredField("proxy");
            daoField.setAccessible(true);
            daoField.set(bu, mock);
            daoField.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * methode de test d'une inscription evenement nominal.
     * @throws InscriptionException .
     */
    @Test
    public void testInscriptionEvenementNominal() throws InscriptionException {
        event.setId(ID_EVENEMENT);
        InscriptionDto retour = bu.inscriptionEvenement(event, inscriptionNominal);
        AssertionEntities.assertEntity(expectedInscription, retour);
    }

    /**
     * methode de test d'un echec inscription car déjà inscrit.
     * @throws InscriptionException car deja inscrit.
     */
    @Test
    public void testEchecDejaInscritEvenement() throws InscriptionException {
        try {
            bu.inscriptionEvenement(event, inscriptionEchecDejaInscrit);
            Assert.fail("ça doit pas marcher");
        } catch (InscriptionException e) {
            Assert.assertEquals(expectedExceptionDejaInscrit.getMessage(), e.getMessage());
        }
    }

}
