package fr.isika.projet4.business.impl.test;

import java.lang.reflect.Field;

import org.easymock.EasyMock;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.isika.projet4.business.api.front.IBusinessConnexion;
import fr.isika.projet4.business.impl.front.BusinessConnexion;
import fr.isika.projet4.business.impl.wrap.retour.RetourConnexionBack;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.exceptions.ConnexionException;

/**
 * classe de test Connexion business.
 * @author stagiaire
 */
public class TestBusinessConnexionBack {

    /**
     * Creation du mock.
     */
    private static IDaoService mock = EasyMock.createMock(IDaoService.class);

    /**
     * classe contenent le service à tester.
     */
    private static IBusinessConnexion bu = new BusinessConnexion();

    /**
     * attribut mail LP.
     */
    private static String mailLp = "ludwig-llofler@hotmail.fr";

    /**
     * attribut mail MB.
     */
    private static String mailMb = "paul-paul@paul.com";

    /**
     * attribut mail RA.
     */
    private static String mailRa = "louis-leglas@start.fr";

    /**
     * attribut mdp LP.
     */
    private static String mdpLp = "ludwig";

    /**
     * attribut mdp MB.
     */
    private static String mdpMb = "paul";

    /**
     * attribut mdp RA.
     */
    private static String mdpRa = "leglas";

    /**
     * mail n'existe pas.
     */
    private static String mailExistPas = "nbjjo@nkhjj.fr";

    /**
     * Attribut mail sponsor.
     */
    private static String mailSponsor = "sarahbasquez@yahoo.fr";

    /**
     * Attribut mdp sponsor.
     */
    private static String mdpSponsor = "arah";

    /**
     * RA pour ajout nominal.
     */
    private static UtilisateurDto expectedRA = new UtilisateurDto(11, mailRa, null, "LeGlas", "Louis");

    /**
     * MB pour ajout nominal.
     */
    private static UtilisateurDto expectedMb = new UtilisateurDto(7, mailMb, null, "Paul", "Paul");

    /**
     * LP pour ajout nominal.
     */
    private static UtilisateurDto expectedLp = new UtilisateurDto(15, mailLp, null, "LLofler", "Ludwig");

    /**
     * Exception mail attendu.
     */
    private static ConnexionException expectedExeptionMail = new ConnexionException();

    /**
     * Exception attendu lorsque qu'un utilisateur tente de se connecter en tant que
     * sponsor.
     */
    private static ConnexionException expectedExeptionNotUserButSponsor = new ConnexionException(
            "Vous n'êtes pas autorisé à vous connecter sur cet espace",
            null);

    /**
     * Pour améliorer le code coverage.
     */
    private static RetourConnexionBack retour = new RetourConnexionBack();

    /**
     * Apres tous les tests.
     */
    @AfterClass
    public static void finDesTests() {
        EasyMock.verify(mock);
    }

    /**
     * Injection de Mock.
     * @throws ConnexionException .
     */
    @BeforeClass
    public static void initTests() throws ConnexionException {
        expectedExeptionMail = new ConnexionException("Login/Mot de passe erroné(s)", null);
        retour.setUser(expectedRA);
        retour.setError(expectedExeptionNotUserButSponsor);
        EasyMock.expect(mock.seConnecterBack(mailLp, mdpLp)).andReturn(expectedLp);
        EasyMock.expect(mock.seConnecterBack(mailMb, mdpMb)).andReturn(expectedMb);
        EasyMock.expect(mock.seConnecterBack(mailRa, mdpRa)).andReturn(retour.getUser());

        EasyMock.expect(mock.seConnecterBack(mailSponsor, mdpSponsor))
                .andThrow(retour.getError());
        EasyMock.expect(mock.seConnecterBack(mailExistPas, mdpLp)).andThrow(expectedExeptionMail);
        EasyMock.expect(mock.seConnecterBack(mailExistPas, mdpMb)).andThrow(expectedExeptionMail);
        EasyMock.expect(mock.seConnecterBack(mailExistPas, mdpRa)).andThrow(expectedExeptionMail);
        EasyMock.replay(mock);

        BeanFactory bf = new ClassPathXmlApplicationContext(
                "classpath:spring-bu-front.xml");
        bu = bf.getBean(IBusinessConnexion.class);

        try {
            Class<? extends IBusinessConnexion> clazz = bu.getClass();
            Field daoField = clazz.getDeclaredField("proxy");
            daoField.setAccessible(true);
            daoField.set(bu, mock);
            daoField.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * test nominal LP.
     * @throws ConnexionException .
     */
    @Test
    public void testNominalLp() throws ConnexionException {
        UtilisateurDto retour = bu.seConnecterBack(mailLp, mdpLp);
        AssertionEntities.assertEntity(expectedLp, retour);
    }

    /**
     * test nominal Mb.
     * @throws ConnexionException .
     */
    @Test
    public void testNominalMb() throws ConnexionException {
        UtilisateurDto retour = bu.seConnecterBack(mailMb, mdpMb);
        AssertionEntities.assertEntity(expectedMb, retour);
    }

    /**
     * test nominal Ra.
     * @throws ConnexionException .
     */
    @Test
    public void testNominalRa() throws ConnexionException {
        UtilisateurDto retour = bu.seConnecterBack(mailRa, mdpRa);
        AssertionEntities.assertEntity(expectedRA, retour);
    }

    /**
     * Test echec mail Lp.
     */
    @Test
    public void testEchecMailLp() {
        try {
            bu.seConnecterBack(mailExistPas, mdpLp);
            Assert.fail("ca ne doit pas fonctionner");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedExeptionMail.getMessage(), e.getMessage());
        }
    }

    /**
     * Test echec mail Mb.
     */
    @Test
    public void testEchecMailMb() {
        try {
            bu.seConnecterBack(mailExistPas, mdpMb);
            Assert.fail("ca ne doit pas fonctionner");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedExeptionMail.getMessage(), e.getMessage());
        }
    }

    /**
     * Test echec mail RA.
     */
    @Test
    public void testEchecMailRa() {
        try {
            bu.seConnecterBack(mailExistPas, mdpRa);
            Assert.fail("ca ne doit pas fonctionner");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedExeptionMail.getMessage(), e.getMessage());
        }
    }

    /**
     * Test pour exception si sponsor mais pas Utilisateur.
     */
    @Test
    public void testSponsorButNotUser() {
        try {
            bu.seConnecterBack(mailSponsor, mdpSponsor);
            Assert.fail("ca ne doit pas fonctionner");
        } catch (ConnexionException e) {
            Assert.assertEquals(expectedExeptionNotUserButSponsor.getMessage(), e.getMessage());
        }
    }
}
