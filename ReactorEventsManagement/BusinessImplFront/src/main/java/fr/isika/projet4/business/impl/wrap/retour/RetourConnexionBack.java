package fr.isika.projet4.business.impl.wrap.retour;

import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.exceptions.ConnexionException;

/**
 * Classe pour le wrapping de la connexion Back.
 * @author stagiaire
 *
 */
public class RetourConnexionBack {

    /**
     * L'utilisateur a connecter.
     */
    private UtilisateurDto user;

    /**
     * L'erreur a retourner.
     */
    private ConnexionException error;

    /**
     * @return the user
     */
    public UtilisateurDto getUser() {
        return user;
    }

    /**
     * @param paramUser the user to set
     */
    public void setUser(UtilisateurDto paramUser) {
        this.user = paramUser;
    }

    /**
     * @return the error
     */
    public ConnexionException getError() {
        return error;
    }

    /**
     * @param paramError the error to set
     */
    public void setError(ConnexionException paramError) {
        this.error = paramError;
    }

}
