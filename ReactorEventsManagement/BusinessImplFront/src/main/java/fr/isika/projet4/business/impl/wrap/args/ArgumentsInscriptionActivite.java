package fr.isika.projet4.business.impl.wrap.args;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.InscriptionDto;

/**
 * Classe décrivant les arguments de la méthode d'inscription a une activté.
 * @author stagiaire
 *
 */
public class ArgumentsInscriptionActivite {

    /**
     * argument inscription.
     */
    private InscriptionDto paramInscriptionAct;
    /**
     * argument activité.
     */
    private ActiviteDto activiteParam;

    /**
     * @return the paramInscriptionAct
     */
    public InscriptionDto getParamInscriptionAct() {
        return paramInscriptionAct;
    }

    /**
     * @param paramInscriptionActivite the paramInscriptionAct to set
     */
    public void setParamInscriptionAct(InscriptionDto paramInscriptionActivite) {
        this.paramInscriptionAct = paramInscriptionActivite;
    }

    /**
     * @return the paramActivite
     */
    public ActiviteDto getParamActivite() {
        return activiteParam;
    }
    /**
     * @param paramActivite the paramActivite to set
     */
    public void setParamActivite(ActiviteDto paramActivite) {
        this.activiteParam = paramActivite;
    }
}
