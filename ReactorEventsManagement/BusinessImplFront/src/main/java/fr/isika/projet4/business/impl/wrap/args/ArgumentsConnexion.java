package fr.isika.projet4.business.impl.wrap.args;

/**
 * Classe decrivant les arguments de la methode de connexion.
 * @author stagiaire
 *
 */
public class ArgumentsConnexion {

    /**
     * Attribut mail.
     */
    private String mail;

    /**
     * Attribut mot de passe.
     */
    private String mdp;

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param paramMail the mail to set
     */
    public void setMail(String paramMail) {
        mail = paramMail;
    }

    /**
     * @return the mdp
     */
    public String getMdp() {
        return mdp;
    }

    /**
     * @param paramMdp the mdp to set
     */
    public void setMdp(String paramMdp) {
        mdp = paramMdp;
    }
}
