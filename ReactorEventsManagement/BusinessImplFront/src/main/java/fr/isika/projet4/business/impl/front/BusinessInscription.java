package fr.isika.projet4.business.impl.front;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.business.api.front.IBusinessInscription;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.exceptions.InscriptionException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * Classe de business front implementant la classe d'api de
 * {@link IBusinessInscription}.
 * @author stagiaire
 */
@Service
public class BusinessInscription implements IBusinessInscription {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * proxy.
     */
    private IDaoService proxy;

    /**
     * constructeur défaut.
     */
    public BusinessInscription() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    /**
     * Methode d'inscription a une activité.
     */
    @Override
    public InscriptionDto inscriptionActivite(ActiviteDto paramActivite, InscriptionDto paramInscription) throws InscriptionException {
        if (proxy.verifInscriptionActivite(paramActivite, paramInscription)) {
            throw new InscriptionException("Vous êtes déjà enregistré(e) sur cette activité.", null);
        }
        return proxy.inscriptionActivite(paramActivite, paramInscription);
    }

    /**
     * Methode d'inscription a un evenement.
     */
    @Override
    public InscriptionDto inscriptionEvenement(EvenementDto paramEvent, InscriptionDto paramInscription) throws InscriptionException {
        if (proxy.verifInscriptionEvenement(paramEvent, paramInscription)) {
            throw new InscriptionException("Vous êtes déjà enregistré(e) sur cet évènement.", null);
        }

        return proxy.inscriptionEvenement(paramEvent, paramInscription);
    }

}
