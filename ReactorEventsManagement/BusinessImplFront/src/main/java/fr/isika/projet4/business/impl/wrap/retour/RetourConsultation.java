package fr.isika.projet4.business.impl.wrap.retour;

import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.exceptions.EventException;

/**
 * Classe de retour de la méthode de consultation getEventById.
 * @author stagiaire
 */
public class RetourConsultation {

    /**
     * Retour evenement.
     */
    EvenementDto event;

    /**
     * Retour erreur.
     */
    EventException error;

    /**
     * @return the event
     */
    public EvenementDto getEvent() {
        return event;
    }

    /**
     * @param paramEvent the event to set
     */
    public void setEvent(EvenementDto paramEvent) {
        event = paramEvent;
    }

    /**
     * @return the error
     */
    public EventException getError() {
        return error;
    }

    /**
     * @param paramError the error to set
     */
    public void setError(EventException paramError) {
        error = paramError;
    }

}
