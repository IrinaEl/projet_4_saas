package fr.isika.projet4.business.impl.wrap.args;

import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.InscriptionDto;

/**
 * Classe décrivant les arguments de la méthode d'inscription a un evenement.
 * @author stagiaire
 *
 */
public class ArgumentsInscriptionEvenement {

    /**
     * argument inscription.
     */
    private InscriptionDto paramInscriptionEvenement;

    /**
     * argument evenement.
     */
    private EvenementDto paramEvent;

    /**
     * @return the paramInscriptionEvent
     */
    public InscriptionDto getParamInscriptionEvent() {
        return paramInscriptionEvenement;
    }

    /**
     * @param paramInscriptionEvent the paramInscriptionEvent to set
     */
    public void setParamInscriptionEvent(InscriptionDto paramInscriptionEvent) {
        this.paramInscriptionEvenement = paramInscriptionEvent;
    }

    /**
     * @return the paramEvenement
     */
    public EvenementDto getParamEvenement() {
        return paramEvent;
    }

    /**
     * @param paramEvenement the paramEvenement to set
     */
    public void setParamEvenement(EvenementDto paramEvenement) {
        this.paramEvent = paramEvenement;
    }

}
