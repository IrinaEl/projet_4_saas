/**
 * Package contenant les services rest de l'implémentation des couches métier.
 * @author stagiaire
 *
 */
package fr.isika.projet4.business.impl.rest;