package fr.isika.projet4.business.impl.wrap.retour;

import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.exceptions.ConnexionException;

/**
 * Classe pour le wrapping de la connexion Front.
 * @author stagiaire
 *
 */
public class RetourConnexionFront {

    /**
     * utilisateur a connecter.
     */
    private SponsorDto user;

    /**
     * error connection.
     */
    private ConnexionException error;

    /**
     * @return the user
     */
    public SponsorDto getUser() {
        return user;
    }

    /**
     * @param paramUser the user to set
     */
    public void setUser(SponsorDto paramUser) {
        this.user = paramUser;
    }

    /**
     * @return the error
     */
    public ConnexionException getError() {
        return error;
    }

    /**
     * @param paramError the error to set
     */
    public void setError(ConnexionException paramError) {
        this.error = paramError;
    }

}
