package fr.isika.projet4.business.impl.wrap.retour;

import fr.isika.projet4.dto.InscriptionDto;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Classe pour le wrapping de l'inscription a une activité.
 * @author stagiaire
 *
 */
public class RetourInscription {

    /**
     * retour inscription.
     */
    private InscriptionDto inscription;

    /**
     * retour error inscription.
     */
    private InscriptionException error;

    /**
     * @return the inscription
     */
    public InscriptionDto getInscription() {
        return inscription;
    }

    /**
     * @param paramInscription the inscription to set
     */
    public void setInscription(InscriptionDto paramInscription) {
        this.inscription = paramInscription;
    }

    /**
     * @return the error
     */
    public InscriptionException getError() {
        return error;
    }

    /**
     * @param paramError the error to set
     */
    public void setError(InscriptionException paramError) {
        this.error = paramError;
    }

}
