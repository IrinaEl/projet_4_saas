package fr.isika.projet4.business.impl.front;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.business.api.front.IBusinessConnexion;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.SponsorDto;
import fr.isika.projet4.dto.UtilisateurDto;
import fr.isika.projet4.exceptions.ConnexionException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * Classe de business front implementant la classe d'api de
 * {@link IBusinessConnexion}.
 * @author stagiaire
 */
@Service
public class BusinessConnexion implements IBusinessConnexion {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * proxy.
     */
    private IDaoService proxy;

    /**
     * Constructeur du business.
     */
    public BusinessConnexion() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    /**
     * Methode de connexion du front.
     */
    @Override
    public SponsorDto seConnecterFront(String mail, String mdp) throws ConnexionException {
        return proxy.seConnecterFront(mail, mdp);

    }

    /**
     * Methode de connexion du back.
     */
    @Override
    public UtilisateurDto seConnecterBack(String mail, String mdp) throws ConnexionException {

        return proxy.seConnecterBack(mail, mdp);
    }

}
