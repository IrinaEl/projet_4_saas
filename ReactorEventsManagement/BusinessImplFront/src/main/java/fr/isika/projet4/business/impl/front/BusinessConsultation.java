package fr.isika.projet4.business.impl.front;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.isika.projet4.business.api.front.IBusinessConsultation;
import fr.isika.projet4.data.api.service.IDaoService;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.service.utils.ProxyUtils;

/**
 * Classe de business front implementant la classe d'api de
 * IBusinessConsultation.
 * @author stagiaire
 *
 */
@Service
public class BusinessConsultation implements IBusinessConsultation {

    /**
     * Logger.
     */
    private Logger log = Logger.getLogger(getClass());

    /**
     * proxy.
     */
    private IDaoService proxy;

    /**
     * Constructeur du business.
     */
    public BusinessConsultation() {
        try {
            proxy = ProxyUtils.getProxy();
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    /**
     * Methode de connexion du front.
     * @paramId : id de l'événement.
     * @return evenement.
     * @throws EventException exception lié à l'événement.
     */
    @Override
    public EvenementDto getEventById(int paramId) throws EventException {
        return proxy.getEventById(paramId);
    }
}
