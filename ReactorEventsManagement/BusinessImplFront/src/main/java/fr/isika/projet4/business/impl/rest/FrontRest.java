package fr.isika.projet4.business.impl.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.projet4.business.api.front.IBusinessConnexion;
import fr.isika.projet4.business.api.front.IBusinessConsultation;
import fr.isika.projet4.business.api.front.IBusinessInscription;
import fr.isika.projet4.business.impl.wrap.args.ArgumentsConnexion;
import fr.isika.projet4.business.impl.wrap.args.ArgumentsInscriptionActivite;
import fr.isika.projet4.business.impl.wrap.args.ArgumentsInscriptionEvenement;
import fr.isika.projet4.business.impl.wrap.retour.RetourConnexionBack;
import fr.isika.projet4.business.impl.wrap.retour.RetourConnexionFront;
import fr.isika.projet4.business.impl.wrap.retour.RetourConsultation;
import fr.isika.projet4.business.impl.wrap.retour.RetourInscription;
import fr.isika.projet4.exceptions.ConnexionException;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.InscriptionException;

/**
 * Classe de service du business front pour angular.
 * @author stagiaire
 *
 */
@CrossOrigin("*")
@RestController()
@RequestMapping("/front-rest")
public class FrontRest {

    /**
     * Injection du business connexion.
     */
    @Autowired
    private IBusinessConnexion businessConnexion;

    /**
     * Injection du business inscription.
     */
    @Autowired
    private IBusinessInscription businessInscription;

    /**
     * Injection du business consultation.
     */
    @Autowired
    private IBusinessConsultation businessConsultation;

    // -----------
    // CONNEXION
    // -----------

    /**
     *
     * @param cnx les arguments de la methode connexion front.
     * @return {@link RetourConnexionFront}
     */
    @PostMapping(path = "/connexion-sponsor",
                 consumes = "application/json",
                 produces = "application/json")
    public RetourConnexionFront connexionFront(@RequestBody ArgumentsConnexion cnx) {
        RetourConnexionFront retour = new RetourConnexionFront();
        try {
            retour.setUser(businessConnexion.seConnecterFront(cnx.getMail(), cnx.getMdp()));
        } catch (ConnexionException e) {
            retour.setError(e);
        }
        return retour;
    }

    /**
     *
     * @param cnx les arguments de la methode connexion back.
     * @return {@link RetourConnexionBack}
     */
    @PostMapping(path = "/connexion-utilisateur",
                 consumes = "application/json",
                 produces = "application/json")
    public RetourConnexionBack connexionBack(@RequestBody ArgumentsConnexion cnx) {
        RetourConnexionBack retour = new RetourConnexionBack();
        try {
            retour.setUser(businessConnexion.seConnecterBack(cnx.getMail(), cnx.getMdp()));
        } catch (ConnexionException e) {
            retour.setError(e);
        }
        return retour;
    }

    // --------------------
    // INSCRIPTION
    // --------------------
    /**
     *
     * @param ins les arguments de la methode d'inscription activité.
     * @return {@link RetourInscription}
     */
    @PostMapping(path = "/inscription-activite",
                 consumes = "application/json",
                 produces = "application/json")
    public RetourInscription inscriptionActivite(@RequestBody ArgumentsInscriptionActivite ins) {
        RetourInscription retour = new RetourInscription();
        try {
            retour.setInscription(businessInscription.inscriptionActivite(ins.getParamActivite(), ins.getParamInscriptionAct()));
        } catch (InscriptionException e) {
            retour.setError(e);
        }
        return retour;
    }

    /**
     *
     * @param ins les arguments de la methode d'inscription evenement.
     * @return {@link RetourInscription}
     */
    @PostMapping(path = "/inscription-evenement",
                 consumes = "application/json",
                 produces = "application/json")
    public RetourInscription inscriptionEvenement(@RequestBody ArgumentsInscriptionEvenement ins) {
        RetourInscription retour = new RetourInscription();
        try {
            retour.setInscription(businessInscription.inscriptionEvenement(ins.getParamEvenement(), ins.getParamInscriptionEvent()));
        } catch (InscriptionException e) {
            retour.setError(e);
        }
        return retour;
    }

    /**
     * Service front rest.
     * @return return un evenement en fo ction de son id.
     */
    @GetMapping(path = "/consultation-evenement/{id}",
                produces = "application/json")
    public RetourConsultation getEventById(@PathVariable(name = "id") Integer id) {
        RetourConsultation retour = new RetourConsultation();
        try {
            retour.setEvent(businessConsultation.getEventById(id));
        } catch (EventException e) {
            retour.setError(e);
        }
        return retour;

    }
}
