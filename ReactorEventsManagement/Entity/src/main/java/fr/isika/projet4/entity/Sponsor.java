package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe définissant l'entity {@link Sponsor}.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "sponsor")
public class Sponsor extends Utilisateur {

    /**
     * longueur raison sociale.
     */
    private static final int LENGTH = 150;

    /**
     * le logo du sponsor à afficher dans l'évènement.
     */
    @Column(name = "sponsor_logo", nullable = false)
    private String logo;

    /**
     * la raison sociale de l'entreprise sponsor.
     */
    @Column(name = "sponsor_raison_sociale", length = LENGTH)
    private String raisonSociale;

    /**
     * liste de dons faits par le sponsor.
     */
    @OneToMany(mappedBy = "sponsor")
    private List<RevenusSponsoring> listeRevenusSponsoring;

    /**
     * liste d'évènements sponsorisés par le sponsor.
     */
    @ManyToMany
    @JoinTable(name = "evenement_sponsor",
               joinColumns = @JoinColumn(name = "evenement_sponsor_sponsor_id"),
               foreignKey = @ForeignKey(name = "FK_SPONSOR_EVENT"),
               inverseJoinColumns = @JoinColumn(name = "evenement_sponsor_evenement_id"),
               inverseForeignKey = @ForeignKey(name = "FK_EVENT_SPONSOR"))
    private List<Evenement> evenements;

    /**
     * constructeur vide.
     */
    public Sponsor() {
        super();
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param paramLogo the logo to set
     */
    public void setLogo(String paramLogo) {
        logo = paramLogo;
    }

    /**
     * @return the raisonSociale
     */
    public String getRaisonSociale() {
        return raisonSociale;
    }

    /**
     * @param paramRaisonSociale the raisonSociale to set
     */
    public void setRaisonSociale(String paramRaisonSociale) {
        raisonSociale = paramRaisonSociale;
    }

    /**
     * @return the listeRevenusSponsoring
     */
    public List<RevenusSponsoring> getListeRevenusSponsoring() {
        return listeRevenusSponsoring;
    }

    /**
     * @param paramListeRevenusSponsoring the listeRevenusSponsoring to set
     */
    public void setListeRevenusSponsoring(List<RevenusSponsoring> paramListeRevenusSponsoring) {
        listeRevenusSponsoring = paramListeRevenusSponsoring;
    }

    /**
     * @return the evenements
     */
    public List<Evenement> getEvenements() {
        return evenements;
    }

    /**
     * @param paramEvenements the evenements to set
     */
    public void setEvenements(List<Evenement> paramEvenements) {
        evenements = paramEvenements;
    }

}
