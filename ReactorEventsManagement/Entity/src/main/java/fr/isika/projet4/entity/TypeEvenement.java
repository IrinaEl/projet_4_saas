package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Classe définit le type d'un évenement.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "type_evenement")
public class TypeEvenement {

    /**
     * constante lenght.
     */
    private static final int LENGTH = 100;

    /**
     * id du type d'evenement.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_evenement_id")
    private int id;
    /**
     * libelle de type d'évenement.
     */
    @Column(name = "type_evenement_libelle", nullable = false, length = LENGTH)
    private String libelle;

    /**
     * liste des evenements.
     */
    @OneToMany(mappedBy = "typeEvenement")
    private List<Evenement> evenements;

    /**
     * Constructor default.
     */
    public TypeEvenement() {
        super();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(int paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

    /**
     * @return the evenements
     */
    public List<Evenement> getEvenements() {
        return evenements;
    }

    /**
     * @param paramEvenements the evenements to set
     */
    public void setEvenements(List<Evenement> paramEvenements) {
        evenements = paramEvenements;
    }

}
