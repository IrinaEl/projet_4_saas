package fr.isika.projet4.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Classe représentant les différentes factures.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "facture")
public class Facture {

    /**
     * La longueur des Varchar par défaut.
     */
    @Transient
    private static final int VARCHAR_LENGTH = 50;

    /**
     * L'id de la facture.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "facture_id")
    private Integer id;

    /**
     * Le numéro de la facture.
     */
    @Column(name = "facture_numero", nullable = false, length = VARCHAR_LENGTH)
    private String numero;

    /**
     * Le délai de paiment en nombre de jours.
     */
    @Column(name = "facture_delai_paiment")
    private int delaiPaiment;

    /**
     * La date d'émission de la facture.
     */
    @Column(name = "facture_date_creation", nullable = false)
    private Date dateCreation;

    /**
     * Les flux qui alimentent la facture.
     */
    @OneToMany(mappedBy = "facture")
    private List<Flux> flux;

    /**
     * Constructeur par défaut.
     */
    public Facture() {
        // Do nothing because default constructor
    }

}
