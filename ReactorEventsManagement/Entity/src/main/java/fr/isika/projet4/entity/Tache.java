package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * class tache.
 * @author stagiaire
 */
@Entity
@Table(name = "tache")
public class Tache {

    /**
     * attribut id tache.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tache_id")
    private Integer id;

    /**
     * attribut deadline tache.
     */
    @Column(name = "tache_deadline", nullable = false)
    private Integer deadline;

    /**
     * attribut type d'activite lié aux taches.
     */
    @ManyToMany(mappedBy = "taches")
    private List<TypeActivite> typeActivites;

    /**
     * attribut type tache dans taches.
     */
    @ManyToOne
    @JoinColumn(name = "id_type_tache_tache", foreignKey = @ForeignKey(name = "FK_TACHE_TYPE_TACHE"))
    private TypeTache typeTaches;

    /***
     * attribut statut de tâche.
     */
    @ManyToOne
    @JoinColumn(name = "id_statut_tache", nullable = false, foreignKey = @ForeignKey(name = "FK_TACHE_STATUT_TACHE"))
    private StatutTache statut;

    /**
     * attribut benevole affecté à la tâche.
     */
    @ManyToOne
    @JoinColumn(name = "id_benevole", foreignKey = @ForeignKey(name = "FK_TACHE_BENEVOLE"))
    private Ressource benevole;

    /**
     * Default constructor.
     */
    public Tache() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the deadline
     */
    public Integer getDeadline() {
        return deadline;
    }

    /**
     * @param paramDeadline the deadline to set
     */
    public void setDeadline(Integer paramDeadline) {
        deadline = paramDeadline;
    }

    /**
     * @return the typeActivites
     */
    public List<TypeActivite> getTypeActivites() {
        return typeActivites;
    }

    /**
     * @param paramTypeActivites the typeActivites to set
     */
    public void setTypeActivites(List<TypeActivite> paramTypeActivites) {
        typeActivites = paramTypeActivites;
    }

    /**
     * @return the typeTaches
     */
    public TypeTache getTypeTaches() {
        return typeTaches;
    }

    /**
     * @param paramTypeTaches the typeTaches to set
     */
    public void setTypeTaches(TypeTache paramTypeTaches) {
        typeTaches = paramTypeTaches;
    }

    /**
     * @return the statut
     */
    public StatutTache getStatut() {
        return statut;
    }

    /**
     * @param paramStatut the statut to set
     */
    public void setStatut(StatutTache paramStatut) {
        statut = paramStatut;
    }

    /**
     * @return the benevole
     */
    public Ressource getBenevole() {
        return benevole;
    }

    /**
     * @param paramBenevole the benevole to set
     */
    public void setBenevole(Ressource paramBenevole) {
        benevole = paramBenevole;
    }

}
