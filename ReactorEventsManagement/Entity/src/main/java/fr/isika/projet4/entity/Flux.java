package fr.isika.projet4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Classe représentant les flux financiers qui circulent pendant la création et
 * l'organisation d'un {@link Evenement} ou d'une {@link Activite}.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "flux")
@Inheritance(strategy = InheritanceType.JOINED)
public class Flux {

    /**
     * L'id du flux.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "flux_id")
    private Integer id;

    /**
     * Le montant du flux.
     */
    @Column(name = "flux_montant", nullable = false)
    private double montant;

    /**
     * La facture dans laquelle se retrouve le flux.
     */
    @ManyToOne
    @JoinColumn(name = "flux_facture_id",
                foreignKey = @ForeignKey(name = "FK_FLUX_FACTURE"))
    private Facture facture;

    /**
     * L'{@link Activite} liée au flux.
     */
    @ManyToOne
    @JoinColumn(name = "flux_activite_id", foreignKey = @ForeignKey(name = "FK_FLUX_ACTIVITE"))
    private Activite activite;

    /**
     * L'{@link Evenement} lié au flux.
     */
    @ManyToOne
    @JoinColumn(name = "flux_evenement_id", foreignKey = @ForeignKey(name = "FK_FLUX_EVENEMENT"))
    private Evenement evenement;

    /**
     * Constructeur par défaut.
     */
    public Flux() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the montant
     */
    public double getMontant() {
        return montant;
    }

    /**
     * @param paramMontant the montant to set
     */
    public void setMontant(double paramMontant) {
        montant = paramMontant;
    }

    /**
     * @return the facture
     */
    public Facture getFacture() {
        return facture;
    }

    /**
     * @param paramFacture the facture to set
     */
    public void setFacture(Facture paramFacture) {
        facture = paramFacture;
    }

    /**
     * @return the activite
     */
    public Activite getActivite() {
        return activite;
    }

    /**
     * @param paramActivite the activite to set
     */
    public void setActivite(Activite paramActivite) {
        activite = paramActivite;
    }

    /**
     * @return the evenement
     */
    public Evenement getEvenment() {
        return evenement;
    }

    /**
     * @param paramEvenement the evenement to set
     */
    public void setEvenment(Evenement paramEvenement) {
        evenement = paramEvenement;
    }

}
