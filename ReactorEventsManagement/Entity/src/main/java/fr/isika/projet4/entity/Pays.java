package fr.isika.projet4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>Pays est la classe . C'est une entité.</b>
 * <p>
 * Un Pays est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un id.</li>
 * <li>Un libellé.</li>
 * </ul>
 * </p>
 * @author stagiaire
 *
 */
@Entity
@Table(name = "pays")
public class Pays {

    /**
     * Constante length.
     */
    private static final int LENGTH = 50;

    /**
     * L'Id de l'entité pays.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pays_id")
    private Integer id;

    /**
     * libelle fe l'entité pays.
     */
    @Column(name = "pays_libelle", nullable = false, length = LENGTH)
    private String libelle;

    /**
     * Constructeur vide de la classe pays.
     */
    public Pays() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

}
