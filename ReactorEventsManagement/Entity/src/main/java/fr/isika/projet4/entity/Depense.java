package fr.isika.projet4.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Type de {@link Flux} représentant le montant réglé ou à régler.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "depense")
public class Depense extends Flux {

    /**
     * Date à laquelle la dépense a été émise.
     */
    @Column(name = "depense_date_sortie")
    private Date dateSortie;

    /**
     * {@link Ressource} à laquelle cette dépense est liée.
     */
    @ManyToOne
    @JoinColumn(name = "depense_ressource_id",
                nullable = false,
                foreignKey = @ForeignKey(name = "FK_DEPENSE_RESSOURCE"))
    private Ressource ressource;

}
