package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author stagiaire class entity Type d'activite.
 *
 */
@Entity
@Table(name = "type_activite")
public class TypeActivite {

    /**
     * constante lenght.
     */
    @Transient
    private static final int LENGTH_INTITULE = 100;

    /**
     * constante lenght.
     */
    @Transient
    private static final int LENGTH_DESCRIPTION = 2500;

    /**
     * attribut id et generation des attributs de la table type d'activite.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_activite_id")
    private Integer id;

    /**
     * attribut intitule et generation des attributs de la table type d'activite.
     */
    @Column(name = "type_activite_intitule", nullable = false, length = LENGTH_INTITULE)
    private String intitule;

    /**
     * attribut description et generation des attributs de la table type d'activite.
     */
    @Column(name = "type_activite_description", nullable = false, length = LENGTH_DESCRIPTION)
    private String description;

    /**
     * attribut liste de taches dans type d'activite.
     */
    @ManyToMany
    @JoinTable(name = "type_activite_tache",
               joinColumns = @JoinColumn(name = "id_type_activite"),
               inverseJoinColumns = @JoinColumn(name = "id_tache"))
    private List<Tache> taches;

    /**
     * Constructor default.
     */
    public TypeActivite() {
        super();
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

    /**
     * @return the taches
     */
    public List<Tache> getTaches() {
        return taches;
    }

    /**
     * @param paramTaches the taches to set
     */
    public void setTaches(List<Tache> paramTaches) {
        taches = paramTaches;
    }

}
