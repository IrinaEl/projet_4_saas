package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe définissant le RA.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "responsable_activite")
public class ResponsableActivite extends Utilisateur {

    /**
     * liste d'activités affectées au responsable d'activité.
     */
    @OneToMany(mappedBy = "responsableActivite")
    private List<Activite> listeActivites;

    /**
     * constructeur vide lié à l'entity Responsable Activite.
     */
    public ResponsableActivite() {
        super();
    }

    /**
     * @return the listeActivites
     */
    public List<Activite> getListeActivites() {
        return listeActivites;
    }

    /**
     * @param paramListeActivites the listeActivites to set
     */
    public void setListeActivites(List<Activite> paramListeActivites) {
        listeActivites = paramListeActivites;
    }

}
