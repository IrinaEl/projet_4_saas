package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Classe représentant les différentes ressources utilisées pour organiser un
 * {@link Evenement} ou une {@link Activite}.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "ressource")
@Inheritance(strategy = InheritanceType.JOINED)
public class Ressource {

    /**
     * La longueur des Varchar par défaut.
     */
    @Transient
    private static final int VARCHAR_LENGTH = 50;

    /**
     * La longueur des Varchar revue pour les mails.
     */
    @Transient
    private static final int MAIL_LENGTH = 200;

    /**
     * L'id de la ressource.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ressource_id", nullable = false)
    private Integer id;

    /**
     * Le montant à payer pour ladite ressource.
     */
    @Column(name = "ressource_prix")
    private double prix;

    /**
     * Le nombre de ressources utilisées avec cette société.
     */
    @Column(name = "ressource_quantite")
    private double quantite;

    /**
     * La raison sociale de la ressource (nom de société ou personne morale), ou
     * alors nom/prénom pour les bénévoles.
     */
    @Column(name = "ressource_raison_social", nullable = false, length = VARCHAR_LENGTH)
    private String raisonSociale;

    /**
     * Le numéro d'identification de la société.
     */
    @Column(name = "ressource_numero_rcs", length = VARCHAR_LENGTH)
    private String numeroRCS;

    /**
     * Le numéro de téléphone de la ressource.
     */
    @Column(name = "ressource_numero_telephone", nullable = false, length = VARCHAR_LENGTH)
    private String numeroTelephone;

    /**
     * Le mail de la ressource.
     */
    @Column(name = "ressource_mail", nullable = false, length = MAIL_LENGTH)
    private String mail;

    /**
     * La liste d'{@link Activite} auxquelles la ressource est affectée.
     */
    @ManyToMany(mappedBy = "ressources")
    private List<Activite> activites;

    /**
     * La liste d'{@link Evenement} auxquels la ressource est affectée.
     */
    @ManyToMany(mappedBy = "ressources")
    private List<Evenement> evenements;

    /**
     * la liste de taches affectées à un bénévole.
     */
    @OneToMany(mappedBy = "benevole")
    private List<Tache> taches;

    /**
     * La liste d'{@link Adresse} auxquelles la ressource est affectée.
     */
    @ManyToMany
    @JoinTable(name = "adresses_ressources",
               joinColumns = @JoinColumn(name = "adresses_ressources_ressources_id"),
               foreignKey = @ForeignKey(name = "FK_RESSOURCE_ADRESSE"),
               inverseJoinColumns = @JoinColumn(name = "adresses_ressources_adresse_id"),
               inverseForeignKey = @ForeignKey(name = "FK_ADRESSE_RESSOURCES"))
    private List<Adresse> adresses;

    /**
     * Constructeur par défaut.
     */
    public Ressource() {
        // Do nothing because default constructor
    }

    /**
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the raisonSociale
     */
    public String getRaisonSociale() {
        return raisonSociale;
    }

    /**
     * @return the numeroRCS
     */
    public String getNumeroRCS() {
        return numeroRCS;
    }

    /**
     * @return the numeroTelephone
     */
    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @return the quantite
     */
    public double getQuantite() {
        return quantite;
    }

    /**
     * @return the evenements
     */
    public List<Evenement> getEvenements() {
        return evenements;
    }

    /**
     * @return the adresses
     */
    public List<Adresse> getAdresses() {
        return adresses;
    }

    public List<Tache> getTaches() {
        return taches;
    }

}
