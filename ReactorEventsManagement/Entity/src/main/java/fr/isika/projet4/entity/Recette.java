package fr.isika.projet4.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Classe représentant l'entité {@link Recette}.
 * @author stagiaire
 */
@Entity
@Table(name = "recette")
@Inheritance(strategy = InheritanceType.JOINED)
public class Recette extends Flux {

    /**
     * La date d'entree de l'entité {@link Recette}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "recette_date_entree", nullable = false)
    private Date dateEntree;

    /**
     * Default constructor.
     */
    public Recette() {
        super();
    }

    /**
     * @return the dateEntree
     */
    public Date getDateEntree() {
        return dateEntree;
    }

    /**
     * @param paramDateEntree the dateEntree to set
     */
    public void setDateEntree(Date paramDateEntree) {
        dateEntree = paramDateEntree;
    }

}
