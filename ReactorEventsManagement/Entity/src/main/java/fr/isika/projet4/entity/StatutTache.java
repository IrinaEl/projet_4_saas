package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * classe représentant l'entité persistente du statut de la tâche.
 * @author stagiaire
 *
 */

@Entity
@Table(name = "statut_tache")
public class StatutTache {

    /**
     * nb.
     */
    private static final int NB_VARCHAR = 50;

    /**
     * identifiant du statut.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "statut_tache_id")
    private Integer id;

    /**
     * l'intitulé du statut (1 - en cours, 2 - terminée).
     */
    @Column(name = "statut_tache_intitule", nullable = false, length = NB_VARCHAR)
    private String intitule;

    /**
     * liste de tâche ayant ce statut.
     */
    @OneToMany(mappedBy = "statut")
    private List<Tache> taches;

    /**
     *
     */
    public StatutTache() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the taches
     */
    public List<Tache> getTaches() {
        return taches;
    }

    /**
     * @param paramTaches the taches to set
     */
    public void setTaches(List<Tache> paramTaches) {
        taches = paramTaches;
    }

}
