package fr.isika.projet4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>Code Postal est la classe . C'est une entité.</b>
 * <p>
 * Un code postal est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un id.</li>
 * <li>Un numéro.</li>
 * </ul>
 * </p>
 * @author stagiaire
 *
 */
@Entity
@Table(name = "code_postal")
public class CodePostal {

    /**
     * Constante length.
     */
    private static final int LENGTH = 50;

    /**
     * Id de code postal.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "code_postal_id")
    private Integer id;

    /**
     * numero du code postal.
     */
    @Column(name = "code_postal_numero", nullable = false, length = LENGTH)
    private String numero;

    /**
     * constructeur vide de l'entité code postal.
     */
    public CodePostal() {
        // Do nothing because default constructor
    }

}
