package fr.isika.projet4.entity;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Classe représentant l'entité {@link RevenusSponsoring}.
 * @author stagiaire
 */

@Entity
@Table(name = "revenus_sponsoring")
public class RevenusSponsoring extends Recette {

    /**
     * Le sponsor de l'entité {@link RevenusSponsoring}.
     */
    @ManyToOne
    @JoinColumn(name = "revenus_sponsoring_sponsor", nullable = false, foreignKey = @ForeignKey(name = "FK_revenus_sponsoring_sponsor"))
    private Sponsor sponsor;

    /**
     * Default constructor.
     */
    public RevenusSponsoring() {
        super();
    }

    /**
     * @return the sponsor
     */
    public Sponsor getSponsor() {
        return sponsor;
    }

    /**
     * @param paramSponsor the sponsor to set
     */
    public void setSponsor(Sponsor paramSponsor) {
        sponsor = paramSponsor;
    }

}
