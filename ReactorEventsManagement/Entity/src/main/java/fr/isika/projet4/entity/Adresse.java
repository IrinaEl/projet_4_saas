package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <b>Adresse est la classe . C'est une entité.</b>
 * <p>
 * Une adresse est caractérisée par les informations (attributs) suivantes :
 * <ul>
 * <li>Un id.</li>
 * <li>Un numéro de voie.</li>
 * <li>Un type de voie.</li>
 * <li>Une ville.</li>
 * <li>Un pays.</li>
 * </ul>
 * </p>
 * @author stagiaire
 *
 */
@Entity
@Table(name = "adresse")
public class Adresse {

    /**
     * Constante length.
     */
    private static final int LENGTH = 50;

    /**
     * L'ID d'adresse.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adresse_id")
    private Integer id;

    /**
     * Le numéro de voie associé à l'adresse.
     */
    @Column(name = "adresse_numero_voie", nullable = false, length = LENGTH)
    private String numeroVoie;

    /**
     * Le nom de voie associé à l'adresse.
     */
    @Column(name = "adresse_nom_voie", nullable = false, length = LENGTH)
    private String nomVoie;

    /**
     * Le type de la voie associé à l'adresse.
     */
    @ManyToOne
    @JoinColumn(name = "adresse_type_voie_id", nullable = false)
    private TypeVoie typeVoie;

    /**
     * La ville associée à la voie associé à l'adresse.
     */
    @ManyToOne
    @JoinColumn(name = "adresse_ville_id", nullable = false)
    private Ville ville;

    /**
     * Les évenements associés à l'adresse.
     */
    @OneToMany
    private List<Evenement> evenements;

    /**
     * Les ressources associées à l'adresse.
     */
    @ManyToMany(mappedBy = "adresses")
    private List<Ressource> ressources;

    /**
     * Le constructeur vide de l'entité adresse.
     */
    public Adresse() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the numeroVoie
     */
    public String getNumeroVoie() {
        return numeroVoie;
    }

    /**
     * @param paramNumeroVoie the numeroVoie to set
     */
    public void setNumeroVoie(String paramNumeroVoie) {
        numeroVoie = paramNumeroVoie;
    }

    /**
     * @return the nomVoie
     */
    public String getNomVoie() {
        return nomVoie;
    }

    /**
     * @param paramNomVoie the nomVoie to set
     */
    public void setNomVoie(String paramNomVoie) {
        nomVoie = paramNomVoie;
    }

    /**
     * @return the typeVoie
     */
    public TypeVoie getTypeVoie() {
        return typeVoie;
    }

    /**
     * @param paramTypeVoie the typeVoie to set
     */
    public void setTypeVoie(TypeVoie paramTypeVoie) {
        typeVoie = paramTypeVoie;
    }

    /**
     * @return the ville
     */
    public Ville getVille() {
        return ville;
    }

    /**
     * @param paramVille the ville to set
     */
    public void setVille(Ville paramVille) {
        ville = paramVille;
    }

    /**
     * @return the evenements
     */
    public List<Evenement> getEvenements() {
        return evenements;
    }

    /**
     * @param paramEvenements the evenements to set
     */
    public void setEvenements(List<Evenement> paramEvenements) {
        evenements = paramEvenements;
    }

    /**
     * @return the ressources
     */
    public List<Ressource> getRessources() {
        return ressources;
    }

    /**
     * @param paramRessources the ressources to set
     */
    public void setRessources(List<Ressource> paramRessources) {
        ressources = paramRessources;
    }

}
