package fr.isika.projet4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * Classe définissant l'entité Utilisateur.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "utilisateur")
@Inheritance(strategy = InheritanceType.JOINED)
public class Utilisateur {

    /**
     * longueur des lenght.
     */
    private static final int LENGTH_UTIL = 50;

    /**
     * id de l'{@link Utilisateur}.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "utilisateur_id")
    private Integer idUtil;

    /**
     * mail de l'{@link Utilisateur}.
     */
    @Column(name = "utilisateur_mail", length = LENGTH_UTIL, nullable = false)
    private String mailUtil;

    /**
     * mdp de l'{@link Utilisateur}.
     */
    @Column(name = "utilisateur_mdp", length = LENGTH_UTIL, nullable = false)
    private String mdpUtil;

    /**
     * nom de l'{@link Utilisateur}.
     */
    @Column(name = "utilisateur_nom", length = LENGTH_UTIL, nullable = false)
    private String nomUtil;

    /**
     * prenom de l'{@link Utilisateur}.
     */
    @Column(name = "utilisateur_prenom", length = LENGTH_UTIL, nullable = false)
    private String prenomUtil;

    /**
     * constructeur vide lié à l'entity Utilisateur.
     */
    public Utilisateur() {
        // Constructeur connard
    }

    /**
     * @return the idUtil
     */
    public Integer getIdUtil() {
        return idUtil;
    }

    /**
     * @param paramIdUtil the idUtil to set
     */
    public void setIdUtil(Integer paramIdUtil) {
        idUtil = paramIdUtil;
    }

    /**
     * @return the mailUtil
     */
    public String getMailUtil() {
        return mailUtil;
    }

    /**
     * @param paramMailUtil the mailUtil to set
     */
    public void setMailUtil(String paramMailUtil) {
        mailUtil = paramMailUtil;
    }

    /**
     * @return the mdpUtil
     */
    public String getMdpUtil() {
        return mdpUtil;
    }

    /**
     * @param paramMdpUtil the mdpUtil to set
     */
    public void setMdpUtil(String paramMdpUtil) {
        mdpUtil = paramMdpUtil;
    }

    /**
     * @return the nomUtil
     */
    public String getNomUtil() {
        return nomUtil;
    }

    /**
     * @param paramNomUtil the nomUtil to set
     */
    public void setNomUtil(String paramNomUtil) {
        nomUtil = paramNomUtil;
    }

    /**
     * @return the prenomUtil
     */
    public String getPrenomUtil() {
        return prenomUtil;
    }

    /**
     * @param paramPrenomUtil the prenomUtil to set
     */
    public void setPrenomUtil(String paramPrenomUtil) {
        prenomUtil = paramPrenomUtil;
    }

}
