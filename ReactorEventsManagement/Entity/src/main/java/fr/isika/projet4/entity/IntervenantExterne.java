package fr.isika.projet4.entity;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Un type de {@link Ressource} (perssonne ou société) qui participe à un
 * {@link Evenement} ou à une {@link Activite}.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "intervenant_externe")
public class IntervenantExterne extends Ressource {

    /**
     * Le genre en fonction de la profession de l'intervenant.
     * {@link TypeIntervenant}
     */
    @ManyToOne
    @JoinColumn(name = "intervenant_externe_type_intervenant_id",
                nullable = false,
                foreignKey = @ForeignKey(name = "FK_INTERVENANT_EXTERNE_TYPE_INTERVENANT"))
    private TypeIntervenant typeIntervenant;

    /**
     * Constructeur par défaut.
     */
    public IntervenantExterne() {
        // Do nothing because default constructor
    }

    /**
     * @return the typeIntervenant
     */
    public TypeIntervenant getTypeIntervenant() {
        return typeIntervenant;
    }

    /**
     * @param paramTypeIntervenant the typeIntervenant to set
     */
    public void setTypeIntervenant(TypeIntervenant paramTypeIntervenant) {
        typeIntervenant = paramTypeIntervenant;
    }

}
