package fr.isika.projet4.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * class définit entitie Evenement.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "evenement")
public class Evenement {

    /**
     * constante lenght.
     */
    private static final int LENGTH = 100;

    /**
     * constante lenght.
     */
    private static final int LENGTH_DESCRIPTION = 2500;

    /**
     * id de evenement.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "evenement_id")
    private int id;

    /**
     * date debut de l'évenement.
     */
    @Column(name = "evenement_date_debut")
    private Date dateDebut;

    /**
     * date fin de l'évenement.
     */
    @Column(name = "evenement_date_fin")
    private Date dateFin;

    /**
     * intitule de l'évenement.
     */
    @Column(name = "evenement_intitule", nullable = false, length = LENGTH)
    private String intitule;

    /**
     * description de l'evenement.
     */
    @Column(name = "evenement_description", nullable = false, length = LENGTH_DESCRIPTION)
    private String description;

    /**
     * le prix de l'evenement.
     */
    @Column(name = "evenement_prix")
    private Double prix;

    /**
     * budget de l'evenement.
     */
    @Column(name = "evenement_budget")
    private Double budget;

    /**
     * liste d'inscriptions de l'evenement.
     */
    @OneToMany(mappedBy = "evenement")
    private List<Inscription> inscriptions;

    /**
     * attribut liste de ressources dans evenement.
     */
    @ManyToMany
    @JoinTable(name = "evenement_ressource",
               joinColumns = @JoinColumn(name = "evenement_ressource_ressource_id"),
               foreignKey = @ForeignKey(name = "FK_EVENEMENT_RESSOURCE"),
               inverseJoinColumns = @JoinColumn(name = "evenement_ressource_evenement_id"),
               inverseForeignKey = @ForeignKey(name = "FK_RESSOURCE_EVENEMENT"))
    private List<Ressource> ressources;

    /**
     * attribut liste de lead poles dans evenement.
     */
    @ManyToMany
    @JoinTable(name = "evenement_leadpole",
               joinColumns = @JoinColumn(name = "evenement_leadpole_id"),
               inverseJoinColumns = @JoinColumn(name = "leadpole_evenement_id"))
    private List<LeadPole> leadPoles;

    /**
     * attribut liste de sponsors dans evenement.
     */
    @ManyToMany(mappedBy = "evenements")
    private List<Sponsor> sponsors;

    /**
     * attribut adresse dans evenement.
     */
    @ManyToOne
    @JoinColumn(name = "evenement_adresse_id",
                foreignKey = @ForeignKey(name = "FK_EVENEMENT_ADRESS"))
    private Adresse adresse;

    /**
     * attribut liste des activités dans evenement.
     */
    @OneToMany(mappedBy = "evenement")
    private List<Activite> activites;

    /**
     * attribut de type d'evenement.
     */
    @ManyToOne
    @JoinColumn(name = "evenement_type_evenment_id",
                foreignKey = @ForeignKey(name = "FK_EVENEMENT_TYPE_EVENMENT"))
    private TypeEvenement typeEvenement;

    /**
     * constructeur vide.
     */
    public Evenement() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the dateDebut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * @param paramDateDebut the dateDebut to set
     */
    public void setDateDebut(Date paramDateDebut) {
        dateDebut = paramDateDebut;
    }

    /**
     * @return the dateFin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * @param paramDateFin the dateFin to set
     */
    public void setDateFin(Date paramDateFin) {
        dateFin = paramDateFin;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

    /**
     * @return the prix
     */
    public Double getPrix() {
        return prix;
    }

    /**
     * @param paramPrix the prix to set
     */
    public void setPrix(Double paramPrix) {
        prix = paramPrix;
    }

    /**
     * @return the budget
     */
    public Double getBudget() {
        return budget;
    }

    /**
     * @param paramBudget the budget to set
     */
    public void setBudget(Double paramBudget) {
        budget = paramBudget;
    }

    /**
     * @return the inscriptions
     */
    public List<Inscription> getInscriptions() {
        return inscriptions;
    }

    /**
     * @return the leadPoles
     */
    public List<LeadPole> getLeadPoles() {
        return leadPoles;
    }

    /**
     * @param paramLeadPoles the leadPoles to set
     */
    public void setLeadPoles(List<LeadPole> paramLeadPoles) {
        leadPoles = paramLeadPoles;
    }

    /**
     * @return the adresse
     */
    public Adresse getAdresse() {
        return adresse;
    }

    /**
     * @param paramAdresse the adresse to set
     */
    public void setAdresse(Adresse paramAdresse) {
        adresse = paramAdresse;
    }

    /**
     * @return the typeEvenement
     */
    public TypeEvenement getTypeEvenement() {
        return typeEvenement;
    }

    /**
     * @param paramTypeEvenement the typeEvenement to set
     */
    public void setTypeEvenement(TypeEvenement paramTypeEvenement) {
        typeEvenement = paramTypeEvenement;
    }

    /**
     * @return the activites
     */
    public List<Activite> getActivites() {
        return activites;
    }

    /**
     * @param paramActivites the activites to set
     */
    public void setActivites(List<Activite> paramActivites) {
        activites = paramActivites;
    }

}
