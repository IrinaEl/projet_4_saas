package fr.isika.projet4.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * class definit entitie Inscription.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "inscription")
public class Inscription {

    /**
     * constante length.
     */
    private static final int LENGTH = 100;

    /**
     * id de {@link Inscription}.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inscription_id")
    private Integer id;

    /**
     * le nom de l'inscripteur.
     */
    @Column(name = "inscription_nom", nullable = false, length = LENGTH)
    private String nom;

    /**
     * prenom de l'inscripteur.
     */
    @Column(name = "inscription_prenom", nullable = false, length = LENGTH)
    private String prenom;

    /**
     * date naissance de l'inscripteur.
     */
    @Column(name = "inscription_date_naissance", nullable = false)
    private Date dateNaissance;

    /**
     * mail de l'inscripteur.
     */
    @Column(name = "inscription_mail", nullable = false, length = LENGTH)
    private String mail;

    /**
     * montant regle de l'inscription.
     */
    @Column(name = "inscription_montant_regle", nullable = true)
    private double montantRegle;

    /**
     * montant a regle pour inscription.
     */
    @ManyToOne
    @JoinColumn(name = "inscription_evenement_id",
                nullable = false,
                foreignKey = @ForeignKey(name = "FK_INSCRIPTION_EVENEMENT"))
    private Evenement evenement;

    /**
     * attribut liste de activites dans une inscription.
     */
    @ManyToMany(mappedBy = "inscriptions")
    private List<Activite> activites;

    /**
     * constructeur vide.
     */
    public Inscription() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param paramNom the nom to set
     */
    public void setNom(String paramNom) {
        nom = paramNom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param paramPrenom the prenom to set
     */
    public void setPrenom(String paramPrenom) {
        prenom = paramPrenom;
    }

    /**
     * @return the dateNaissance
     */
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * @param paramDateNaissance the dateNaissance to set
     */
    public void setDateNaissance(Date paramDateNaissance) {
        dateNaissance = paramDateNaissance;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param paramMail the mail to set
     */
    public void setMail(String paramMail) {
        mail = paramMail;
    }

    /**
     * @return the montantRegle
     */
    public double getMontantRegle() {
        return montantRegle;
    }

    /**
     * @param paramMontantRegle the montantRegle to set
     */
    public void setMontantRegle(double paramMontantRegle) {
        montantRegle = paramMontantRegle;
    }

    /**
     * @return the evenement
     */
    public Evenement getEvenement() {
        return evenement;
    }

    /**
     * @param paramEvenement the evenement to set
     */
    public void setEvenement(Evenement paramEvenement) {
        evenement = paramEvenement;
    }

    /**
     * @return the activites
     */
    public List<Activite> getActivites() {
        return activites;
    }

    /**
     * @param paramActivites the activites to set
     */
    public void setActivites(List<Activite> paramActivites) {
        activites = paramActivites;
    }

}
