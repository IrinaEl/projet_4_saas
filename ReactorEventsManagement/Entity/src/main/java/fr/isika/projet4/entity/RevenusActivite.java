package fr.isika.projet4.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe représentant l'entité {@link RevenusActivite}.
 * @author stagiaire
 */

@Entity
@Table(name = "revenus_activite")
public class RevenusActivite extends Recette {

    /**
     * Default constructor.
     */
    public RevenusActivite() {
        super();
    }

}
