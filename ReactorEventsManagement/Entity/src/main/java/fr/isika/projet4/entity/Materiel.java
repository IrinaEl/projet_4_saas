package fr.isika.projet4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Type de {@link Ressource} qui peut être utilisée pour un {@link Evenement} ou
 * pour une {@link Activite}.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "materiel")
public class Materiel extends Ressource {

    /**
     * La longueur des Varchar par défaut.
     */
    @Transient
    private static final int VARCHAR_LENGTH = 50;

    /**
     * Le nom du matériel.
     */
    @Column(name = "materiel_intitule", nullable = false, length = VARCHAR_LENGTH)
    private String intitule;

    /**
     * Constructeur par défaut.
     */
    public Materiel() {
        super();
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

}
