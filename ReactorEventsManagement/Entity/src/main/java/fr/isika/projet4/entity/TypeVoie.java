package fr.isika.projet4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>TypeVoie est la classe . C'est une entité.</b>
 * <p>
 * Un TypeVoie est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un id.</li>
 * <li>Un libellé.</li>
 * </ul>
 * </p>
 * @author stagiaire
 *
 */
@Entity
@Table(name = "type_voie")
public class TypeVoie {

    /**
     * Constante length.
     */
    private static final int LENGTH = 50;

    /**
     * L'ID de TypeVoie.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_voie_id")
    private Integer id;

    /**
     * Le libellé de TypeVoie.
     */
    @Column(name = "type_voie_libelle", nullable = false, length = LENGTH)
    private String libelle;

    /**
     * Contructeur vide.
     */
    public TypeVoie() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

}
