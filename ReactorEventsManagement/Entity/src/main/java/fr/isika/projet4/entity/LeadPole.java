package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Classe définissant l'entity {@link Utilisateur}.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "lead_pole")
public class LeadPole extends Utilisateur {

    /**
     * liste d'évènements affectés au lead pole.
     */
    @ManyToMany(mappedBy = "leadPoles")
    private List<Evenement> listeEvenements;

    /**
     * constructeur vide lié à l'entity Lead Pole.
     */
    public LeadPole() {
        super();
    }

    /**
     * @return the listeEvenements
     */

    public List<Evenement> getListeEvenements() {
        return listeEvenements;
    }

    /**
     * @param paramListeEvenements the listeEvenements to set
     */
    public void setListeEvenements(List<Evenement> paramListeEvenements) {
        listeEvenements = paramListeEvenements;
    }

}
