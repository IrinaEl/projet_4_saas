package fr.isika.projet4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * <b>Ville est la classe . C'est une entité.</b>
 * <p>
 * Une ville est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un id.</li>
 * <li>Un libellé.</li>
 * <li>Un pays.</li>
 * </ul>
 * </p>
 * @author stagiaire
 *
 */
@Entity
@Table(name = "ville")
public class Ville {

    /**
     * Constante length.
     */
    private static final int LENGTH = 50;

    /**
     * L'ID de ville.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ville_id")
    private Integer id;

    /**
     * Le libellé de la ville.
     */
    @Column(name = "ville_libelle", nullable = false, length = LENGTH)
    private String libelle;

    /**
     * pays associé à ville. c'est une clé étrangère de l'entité ville.
     */
    @ManyToOne
    @JoinColumn(name = "ville_pays_id", nullable = false)
    private Pays pays;

    /**
     * Constructeur vide de l'entité ville.
     */
    public Ville() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param paramLibelle the libelle to set
     */
    public void setLibelle(String paramLibelle) {
        libelle = paramLibelle;
    }

    /**
     * @return the pays
     */
    public Pays getPays() {
        return pays;
    }

    /**
     * @param paramPays the pays to set
     */
    public void setPays(Pays paramPays) {
        pays = paramPays;
    }

}
