package fr.isika.projet4.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe définissant l'entity {@link MembreAdministration}.
 * @author stagiaire
 *
 */
@Entity
@Table(name = "membre_administration")
public class MembreAdministration extends Utilisateur {

    /**
     * constructeur vide lié à l'entity Membre de l'admnistration..
     */
    public MembreAdministration() {
        super();
    }
}
