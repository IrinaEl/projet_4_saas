package fr.isika.projet4.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe représentant l'entité {@link RevenusDons}.
 * @author stagiaire
 */

@Entity
@Table(name = "revenus_dons")
public class RevenusDons extends Recette {

    /**
     * Default constructor.
     */
    public RevenusDons() {
        super();
    }

}
