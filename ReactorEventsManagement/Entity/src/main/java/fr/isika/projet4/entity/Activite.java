package fr.isika.projet4.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Class Activite.
 * @author stagiaire
 */
@Entity
@Table(name = "activite")
public class Activite {

    /**
     * constante lenght.
     */
    @Transient
    private static final int LENGTH_INTITULE = 100;

    /**
     * constante lenght.
     */
    @Transient
    private static final int LENGTH_DESCRIPTION = 2500;

    /**
     * attribut id activite.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "activite_id")
    private Integer id;

    /**
     * Attribut date de debut.
     */
    @Column(name = "activite_date_debut", nullable = false)
    private Date dateDebut;

    /**
     * Attribut date de fin.
     */
    @Column(name = "activite_date_fin", nullable = false)
    private Date dateFin;

    /**
     * Attribut intitule activite.
     */
    @Column(name = "activite_intitule", nullable = false, length = LENGTH_INTITULE)
    private String intitule;

    /**
     * Attribut description axctivite.
     */
    @Column(name = "activite_description", nullable = false, length = LENGTH_DESCRIPTION)
    private String description;

    /**
     * Attribut nombe de place pour l'activite.
     */
    @Column(name = "activite_nombre_place", nullable = false)
    private Integer nombrePlace;

    /**
     * Attribut prix de l'activite.
     */
    @Column(name = "activite_prix", nullable = false)
    private double prix;

    /**
     * Attribut budget de l'activite.
     */
    @Column(name = "activite_budget", nullable = false)
    private double budget;

    /**
     * attribut type d'activite.
     */
    @ManyToOne
    @JoinColumn(name = "id_type_activite_activite", foreignKey = @ForeignKey(name = "FK_ACTIVITE_TYPE_ACTIVITE"))
    private TypeActivite typeActivite;

    /**
     * attribut responsable d'activite.
     */
    @ManyToOne
    @JoinColumn(name = "id_responsable_activite_activite", foreignKey = @ForeignKey(name = "FK_ACTIVITE_RESPONSABLE_ACTIVITE"))
    private ResponsableActivite responsableActivite;

    /**
     * attribut evenement.
     */
    @ManyToOne
    @JoinColumn(name = "id_evenement_activite", foreignKey = @ForeignKey(name = "FK_ACTIVITE_EVENEMENT"))
    private Evenement evenement;

    /**
     * attribut liste d'inscriptions à une activite.
     */
    @ManyToMany
    @JoinTable(name = "activite_inscription",
               joinColumns = @JoinColumn(name = "activite_inscription_inscription_id"),
               foreignKey = @ForeignKey(name = "FK_ACTIVITE_INSCRIPTION"),
               inverseJoinColumns = @JoinColumn(name = "activite_inscription_activite_id"),
               inverseForeignKey = @ForeignKey(name = "FK_INSCRIPTION_ACTIVITE"))
    private List<Inscription> inscriptions;

    /**
     * attribut liste de ressources dans une activite.
     */
    @ManyToMany
    @JoinTable(name = "activite_ressource",
               joinColumns = @JoinColumn(name = "activite_ressource_activite_id"),
               foreignKey = @ForeignKey(name = "FK_ACTIVITE_RESSOURCE"),
               inverseJoinColumns = @JoinColumn(name = "activite_ressource_ressource_id"),
               inverseForeignKey = @ForeignKey(name = "RESSOURCE_ACTIVITE"))
    private List<Ressource> ressources;

    /**
     * attribut flux.
     */
    @OneToMany(mappedBy = "activite")
    private List<Flux> flux;

    /**
     * Default Constructor.
     */
    public Activite() {
        // Do nothing because default constructor
    }

    /**
     * @return the dateDebut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * @param paramDateDebut the dateDebut to set
     */
    public void setDateDebut(Date paramDateDebut) {
        dateDebut = paramDateDebut;
    }

    /**
     * @return the dateFin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * @param paramDateFin the dateFin to set
     */
    public void setDateFin(Date paramDateFin) {
        dateFin = paramDateFin;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

    /**
     * @return the nombrePlace
     */
    public Integer getNombrePlace() {
        return nombrePlace;
    }

    /**
     * @param paramNombrePlace the nombrePlace to set
     */
    public void setNombrePlace(Integer paramNombrePlace) {
        nombrePlace = paramNombrePlace;
    }

    /**
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * @param paramPrix the prix to set
     */
    public void setPrix(double paramPrix) {
        prix = paramPrix;
    }

    /**
     * @return the budget
     */
    public double getBudget() {
        return budget;
    }

    /**
     * @param paramBudget the budget to set
     */
    public void setBudget(double paramBudget) {
        budget = paramBudget;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the typeActivite
     */
    public TypeActivite getTypeActivite() {
        return typeActivite;
    }

    /**
     * @param paramTypeActivite the typeActivite to set
     */
    public void setTypeActivite(TypeActivite paramTypeActivite) {
        typeActivite = paramTypeActivite;
    }

    /**
     * @return the responsableActivite
     */
    public ResponsableActivite getResponsableActivite() {
        return responsableActivite;
    }

    /**
     * @param paramResponsableActivite the responsableActivite to set
     */
    public void setResponsableActivite(ResponsableActivite paramResponsableActivite) {
        responsableActivite = paramResponsableActivite;
    }

    /**
     * @return the evenement
     */
    public Evenement getEvenement() {
        return evenement;
    }

    /**
     * @param paramEvenement the evenement to set
     */
    public void setEvenement(Evenement paramEvenement) {
        evenement = paramEvenement;
    }

    /**
     * @return the inscriptions
     */
    public List<Inscription> getInscriptions() {
        return inscriptions;
    }

}
