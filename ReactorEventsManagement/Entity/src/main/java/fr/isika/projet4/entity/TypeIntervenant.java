package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Le genre de {@link IntervenantExterne} qui peuvent participer à un
 * {@link Evenement} ou à une {@link Activite}.
 * <ul>
 * <li>Bénévole</li>
 * <li>Conférencier</li>
 * <li>Vendeur</li>
 * <li>etc.</li>
 * </ul>
 * @author stagiaire
 *
 */
@Entity
@Table(name = "type_intervenant")
public class TypeIntervenant {

    /**
     * La longueur des Varchar par défaut.
     */
    @Transient
    private static final int VARCHAR_LENGTH = 50;

    /**
     * La longueur des Varchar par défaut.
     */
    @Transient
    private static final int TEXT_LENGTH = 5000;

    /**
     * L'id de l'intervenant.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_intervenant_id")
    private Integer id;

    /**
     * Le genre d'intervenant.
     */
    @Column(name = "type_intervenant_intitule", nullable = false, length = VARCHAR_LENGTH)
    private String intitule;

    /**
     * La description d'un genre d'intervenant, peut être utile pour un type
     * d'intervenant atypique.
     */
    @Column(name = "type_intervenant_description", length = TEXT_LENGTH)
    private String description;

    /**
     * Liste des intervenants correspondant à ce type.
     */
    @OneToMany(mappedBy = "typeIntervenant")
    private List<IntervenantExterne> intervenantsExternes;

    /**
     * Constructeur par défaut.
     */
    public TypeIntervenant() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

    /**
     * @return the intervenantsExternes
     */
    public List<IntervenantExterne> getIntervenantsExternes() {
        return intervenantsExternes;
    }

    /**
     * @param paramIntervenantsExternes the intervenantsExternes to set
     */
    public void setIntervenantsExternes(List<IntervenantExterne> paramIntervenantsExternes) {
        intervenantsExternes = paramIntervenantsExternes;
    }

}
