package fr.isika.projet4.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * class type tache.
 * @author stagiaire
 */
@Entity
@Table(name = "type_tache")
public class TypeTache {

    /**
     * constante lenght.
     */
    @Transient
    private static final int LENGTH_INTITULE = 100;

    /**
     * constante lenght.
     */
    @Transient
    private static final int LENGTH_DESCRIPTION = 2500;

    /**
     * attribut identifiant type de tache.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_tache_id")
    private Integer id;

    /**
     * attribut intitule type de tache.
     */
    @Column(name = "type_tache_intitule", nullable = false, length = LENGTH_INTITULE)
    private String intitule;

    /**
     * attribut description type de tache.
     */
    @Column(name = "type_tache_description", nullable = false, length = LENGTH_DESCRIPTION)
    private String description;

    /**
     * attribut list de taches dans type de taches.
     */
    @OneToMany(mappedBy = "typeTaches")
    private List<Tache> taches;

    /**
     * Default constructor.
     */
    public TypeTache() {
        // Do nothing because default constructor
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(Integer paramId) {
        id = paramId;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param paramIntitule the intitule to set
     */
    public void setIntitule(String paramIntitule) {
        intitule = paramIntitule;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(String paramDescription) {
        description = paramDescription;
    }

    /**
     * @return the taches
     */
    public List<Tache> getTaches() {
        return taches;
    }

    /**
     * @param paramTaches the taches to set
     */
    public void setTaches(List<Tache> paramTaches) {
        taches = paramTaches;
    }

}
