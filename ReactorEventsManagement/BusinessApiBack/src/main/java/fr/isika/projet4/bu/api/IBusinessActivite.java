package fr.isika.projet4.bu.api;

import java.util.List;

import fr.isika.projet4.dto.ActiviteDto;
import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.ActiviteException;

/**
 * Classe définissant les services métier relatifs au DTO '{@link ActiviteDto}.
 * @author stagiaire
 *
 */
public interface IBusinessActivite {

    /**
     * Méthode permettant de récupérer une lsite d'{@link ActiviteDto} affectées à
     * un RA.
     * @param idRa - l'identifiant du
     *             {@link fr.isika.projet4.dto.ResponsableActiviteDto} dont les
     *             activités sont recherchées
     * @return - une {@link List d'{@link ActiviteDto} affectées à un
     *         {@link fr.isika.projet4.dto.ResponsableActiviteDto}
     * @throws ActiviteException lorsque :
     *                           <ul>
     *                           <li>Acucune activité n'est affectée au
     *                           {@link fr.isika.projet4.dto.ResponsableActiviteDto}</li>
     *                           <li>L serveur de données n'est pas accessibles</li>
     *                           </ul>
     */

    List<ActiviteDto> getActivitesByRa(Integer idRa) throws ActiviteException;

    /**
     * Méthode permettant de récupérer la liste de tout les {@link ActviteDto} pour
     * un {@link EvenementDto}.
     * @param idEvent - L'id de l'évenmenet concerné.
     * @return
     *         <ul>
     *         <li>La liste des {@link ActiviteDto}</li>
     *         </ul>
     * @throws ActiviteException
     *                           <ul>
     *                           <li>Si l'évenement n'a pas encore une
     *                           {@link ActiviteDto}</li>
     *                           <li>Si l'unité de persistence est hors-service</li>
     *                           </ul>
     */
    List<ActiviteDto> getAllActiviteByEvent(Integer idEvent) throws ActiviteException;

    /**
     * déclaration de la méthode métier permettant de récupérer l'activité par son
     * identifiant.
     * @param idActivite - l'identifiant de l'activité à récupérer.
     * @return - le dto {@link ActiviteDto}
     * @throws ActiviteException exception si l'activité n'existe pas
     */
    ActiviteDto getActiviteById(Integer idActivite) throws ActiviteException;

    /**
     * Méthode permet d'ajouter un Responsable Activite à une Activite.
     * @param responsableActiviteDto responsable Dto concerné
     * @param activiteDto            une activitéDto
     * @return une nouvelle activiteDto
     * @throws ActiviteException
     *                           <ul>
     *                           <li>si ResponsableActivite existe déjà
     *                           <li>
     *                           <li>si ResponsableActiviteDto est null</li>
     *                           <li>Si l'unité de persistence est hors-service</li>
     *                           </ul>
     */
    ActiviteDto addResponsableActivity(ResponsableActiviteDto responsableActiviteDto, ActiviteDto activiteDto) throws ActiviteException;
    ActiviteDto affecterRessource(Integer idRessource, Integer idActivite) throws ActiviteException;
}
