package fr.isika.projet4.bu.api;

import java.util.List;

import fr.isika.projet4.dto.ResponsableActiviteDto;
import fr.isika.projet4.exceptions.RAException;

/**
 * classe déclarant les méthodes métier relatives au dto.
 * {@link ResponsableActiviteDto}
 * @author Irina El
 *
 */
public interface IBusinessResponsableActivite {

    /**
     * méthode métier permettant de récupérer le dto {@link ResponsableActiviteDto}
     * par son identifiant.
     * @param idRa - l'identifiant du {@link ResponsableActiviteDto} recherché
     * @return le dto {@link ResponsableActiviteDto}
     * @throws RAException en cas de retour null
     */
    ResponsableActiviteDto getRaById(Integer idRa) throws RAException;

    /**
     * Méthode permet de récupérer une liste de {@link ResponsableActiviteDto}.
     * @return une liste de ResponsableActiviteDto
     * @throws RAException en cas de retour null
     */
    List<ResponsableActiviteDto> getAllResponsableActivite() throws RAException;
}
