package fr.isika.projet4.bu.api;

import java.util.List;

import fr.isika.projet4.dto.IntervenantExterneDto;
import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.exceptions.RessourceException;

/**
 * classe déclarant les méthodes métier relatives au dto ressource.
 * @author stagiaire
 *
 */
public interface IBusinessRessource {

    /**
     * déclaration de la méthode métier permettant de récupérer la liste de tous les
     * bénévoles.
     * @param idTypeIntervenant identifiant du type d'intervenant correspondant au
     *                          bénévole.
     * @return une lsite de tous les bénévoles.
     * @throws RessourceException si liste vide
     */
    List<IntervenantExterneDto> getAllBenevoles() throws RessourceException;
    /**
     * déclaration de la méthode métier permettant de récupérer la liste des ressources affectées à une activité.
     * @param idActivite id
     * @return liste ressources
     * @throws RessourceException si échec
     */
    List<RessourceDto> getRessourceByActivite(Integer idActivite) throws RessourceException;
    /**
     * déclaration de la méthode métier permettant de récupérer la liste de toutes les ressources.
     * @param idActivite id
     * @return liste ressources
     * @throws RessourceException si échec
     */
    List<RessourceDto> getAllRessources() throws RessourceException;


}
