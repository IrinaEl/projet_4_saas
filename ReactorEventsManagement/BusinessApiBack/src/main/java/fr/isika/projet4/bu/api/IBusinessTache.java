package fr.isika.projet4.bu.api;

import java.util.List;

import fr.isika.projet4.dto.RessourceDto;
import fr.isika.projet4.dto.TacheDto;
import fr.isika.projet4.exceptions.TacheException;

/**
 * Classe définissant les services métier relatifs au DTO {@link TacheDto}.
 * @author stagiaire
 *
 */
public interface IBusinessTache {

    /**
     * méthode permettant de récupérer une liste de {@link TacheDto}.
     * @param idActivite - identifiant de l'activité dont les tâches sont
     *                   recherchées
     * @return une liste de {@link TacheDto}
     * @throws TacheException - exception en cas de liste vide.
     */
    List<TacheDto> getTachesByActivite(Integer idActivite) throws TacheException;

    /**
     * méthode permettant d'affecter un bénévole à une tâche.
     * @param tache    - identifiant de la tâche à laquelle le bénévole sera affecté
     * @param benevole - bénévole à affecter
     * @return {@link TacheDto} avec le bénévole affecté
     * @throws TacheException - exception retournée en cas d'échec
     */
    TacheDto affecterBenevole(TacheDto tache, RessourceDto benevole) throws TacheException;
}
