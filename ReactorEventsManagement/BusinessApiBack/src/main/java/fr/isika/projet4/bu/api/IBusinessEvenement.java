package fr.isika.projet4.bu.api;

import java.util.List;

import fr.isika.projet4.dto.EvenementDto;
import fr.isika.projet4.dto.MembreAdministrationDto;
import fr.isika.projet4.exceptions.EventException;
import fr.isika.projet4.exceptions.LeadPoleException;
import fr.isika.projet4.exceptions.RAException;

/**
 * Classe de définition des services métier pour un {@link EvenementDto}.
 * @author ludwig
 *
 */
public interface IBusinessEvenement {

    /**
     * Méthode permettant de récupérer la liste des {@link EvenementDto} en cours
     * de création ou créés, pour un responsable d'activité.
     * @param idRA - L'id du responsable d'activité concerné.
     * @return
     *         <ul>
     *         <li>La liste des {@link EvenementDto}</li>
     *         </ul>
     * @throws RAException
     *                     <ul>
     *                     <li>Si le responsable d'activité n'est affecté à aucun
     *                     {@link EvenementDto}</li>
     *                     <li>Si l'unité de persistence est hors-service
     *                     </ul>
     */
    List<EvenementDto> getByRA(int idRA) throws RAException;

    /**
     * Méthode permettant de récupérer la liste de tout les {@link EvenementDto}
     * en cours pour le {@link MembreAdministrationDto}.
     * @return
     *         <ul>
     *         <li>La liste des {@link EvenementDto}</li>
     *         </ul>
     * @throws EventException
     *                        <ul>
     *                        <li>S'il y'a aucun évenement pour le moment</li>
     *                        <li>Si l'unité de persistence est hors-service</li>
     *                        </ul>
     */
    List<EvenementDto> getAll() throws EventException;

    /**
     * Méthode permettant de récupérer la liste des {@link EvenementDto} en cours
     * de création ou créés, pour un LeadPole.
     * @param leadPoleID - L'id du LeadPole concerné.
     * @return
     *         <ul>
     *         <li>La liste des {@link EvenementDto}</li>
     *         </ul>
     * @throws LeadPoleException
     *                           <ul>
     *                           <li>Si le LeadPole n'est affecté à aucun
     *                           {@link EvenementDto}</li>
     *                           <li>Si l'unité de persistence est hors-service
     *                           </ul>
     */
    List<EvenementDto> getEvenementsByLeadPoleID(int leadPoleID) throws LeadPoleException;

    /**
     * Méthode récupérant tous les {@link EvenementDto} même si la date de fin
     * est supérieure à la date du jour.
     * @return Une liste d'{@link EvenementDto}.
     * @throws EventException
     *                        <ul>
     *                        <li>S'il y'a aucun évenement pour le moment</li>
     *                        <li>Si l'unité de persistence est hors-service</li>
     *                        </ul>
     */
    List<EvenementDto> getAllDoneEvents() throws EventException;
}
