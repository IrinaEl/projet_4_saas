import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RetourConnexionFront } from './interfaces/retour/retour-connexion-front';
import { RetourConnexionBack } from './interfaces/retour/retour-connexion-back';
import { RetourConsultationEvent } from './interfaces/retour/retour-consultation-event';
import { RetourInscription } from './interfaces/retour/retour-inscription';
import { ArgsInscritpion } from './interfaces/arguments/args-inscritpion-event';
import { RetourListeRessources } from './ServicesRest/retours-back/retour-liste-ressources';
import { ArgsInscritionAct } from './interfaces/arguments/args-inscrition-act';

@Injectable({
  providedIn: 'root'
})
export class Projet4Service {
  baseURL: string = "http://localhost:18080/BusinessImplFront/restInProjet/front-rest/";

  constructor(private service: HttpClient) { }

  serviceConnexionFront(mail: String, mdp: String): Observable<RetourConnexionFront> {
    return this.service.post<RetourConnexionFront>(this.baseURL + "connexion-sponsor",
      { "mail": mail, "mdp": mdp })
  }
  serviceConnexionBack(mail: String, mdp: String): Observable<RetourConnexionBack> {
    console.log(mail)
    console.log(mdp)
    return this.service.post<RetourConnexionBack>(this.baseURL + "connexion-utilisateur",
      { "mail": mail, "mdp": mdp })
  }
  getEventById(id: number): Observable<RetourConsultationEvent>{
    console.log(id)
    return this.service.get<RetourConsultationEvent>(this.baseURL + "consultation-evenement/" + id)
  }

  inscriptionActivite(argInscriptionAct : ArgsInscritionAct): Observable<RetourInscription>{
    return this.service.post<RetourInscription>(this.baseURL + "inscription-activite", argInscriptionAct);
  }
  inscriptionEvenement(argInscription : ArgsInscritpion): Observable<RetourInscription>{
    return this.service.post<RetourInscription>(this.baseURL + "inscription-evenement", argInscription);
  }
}
