import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackArcheRaComponent } from './back-arche-ra.component';

describe(' BackArcheRaComponent', () => {
  let component:  BackArcheRaComponent;
  let fixture: ComponentFixture< BackArcheRaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  BackArcheRaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent( BackArcheRaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

