import { Component, OnInit, Input } from '@angular/core';
import { ServicesBackService } from 'src/app/ServicesRest/back/services-back.service';
import { ActiviteDto } from 'src/app/interfaces/activite-dto';
import { TacheDto } from '../../../interfaces/tache-dto';
import { ArgAffecterBenevole } from '../../../ServicesRest/param-complexes/arg-affecter-benevole';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { RessourceDto } from '../../../interfaces/ressource-dto';
import { ActivatedRoute } from '@angular/router';
import { ArgAffecterRes } from 'src/app/ServicesRest/param-complexes/arg-affecter-res';


@Component({
  selector: 'app-gestion-activite',
  templateUrl: './gestion-activite.component.html',
  styleUrls: ['./gestion-activite.component.sass']
})
export class GestionActiviteComponent implements OnInit {

  //------------------ATTRIBUTS DU COMPOSANT
  heading: string = "";
  subheading: string = "";
  icon = "pe-7s-wallet icon-gradient bg-plum-plate";
  idActivite: number;
  activite: ActiviteDto = null;
  taches: TacheDto[];
  //pour radio button
  benevoles: RessourceDto[];
  @Input() selectedIdBenevole: number;
  selectedTache: TacheDto;
  argsAffecter: ArgAffecterBenevole;
  //pour liste prestataire
  prestataires: RessourceDto[];
  allPrestataires: RessourceDto[];
  argsAffecterRes: ArgAffecterRes;
  @Input() selectedIdRessource: number;

  //------------ATTRIBUTS JS----------------------------------------------------

  closeResult: string;

  //__________________METHODES DU COMPOSANT

  constructor(private service: ServicesBackService, private modalService: NgbModal, private route: ActivatedRoute) {
    route.params.subscribe(val => {
      this.ngOnInit(); //abonnement au changement de param du routerlink dans sidebar, observation du changement de param
    });
  }

  getActiviteById(): void {
    this.service.getActiviteByid(this.idActivite).subscribe(
      data => {
        this.activite = data.activite;
        this.heading = this.activite.intitule;
        this.subheading = this.activite.description;
       // this.activite = this.enleverTrucsCheum(this.activite);
        
        let tmp: any = JSON.stringify(this.activite);
        console.log(tmp);
        tmp = decodeURIComponent(escape(tmp));
        console.log(tmp);
        this.activite = JSON.parse(tmp);


      }
    )
  }

  getTachesByActivite(): void {
    this.service.getTachesByActivite(this.idActivite).subscribe(
      data => {
        this.taches = data.taches;
        this.taches = this.enleverTrucsCheum(this.taches);
       
        let tmp: any = JSON.stringify(this.taches);
        console.log(tmp);
        tmp = decodeURIComponent(escape(tmp));
        console.log(tmp);
        this.taches = JSON.parse(tmp);
      }
    )
  }

  getRessourcesByActivite(): void {
    this.service.getRessourcesByActivite(this.idActivite).subscribe(
      data => {
        this.prestataires = data.ressources;
        this.prestataires = this.enleverTrucsCheum(this.prestataires);

        let tmp: any = JSON.stringify(this.prestataires);
        console.log(tmp);
        tmp = decodeURIComponent(escape(tmp));
        console.log(tmp);
        this.prestataires = JSON.parse(tmp);
      }
    )
  }

  getAllBenevoles(): void {
    this.service.getAllBenevoles().subscribe(
      data => {
        this.benevoles = data.benevoles;
        this.benevoles = this.enleverTrucsCheum(this.benevoles);

        let tmp: any = JSON.stringify(this.benevoles);
        console.log(tmp);
        tmp = decodeURIComponent(escape(tmp));
        console.log(tmp);
        this.benevoles = JSON.parse(tmp);
      }
    )
  }


  getAllRessources(): void {
    this.service.getAllRessources().subscribe(
      data => {
        this.allPrestataires = data.ressources;
        this.allPrestataires = this.enleverTrucsCheum(this.allPrestataires);

        let tmp: any = JSON.stringify(this.allPrestataires);
        console.log(tmp);
        tmp = decodeURIComponent(escape(tmp));
        console.log(tmp);
        this.allPrestataires = JSON.parse(tmp);
      }
    )
  }

  setTache(tache: TacheDto) {
    console.log(tache)
    this.selectedTache = tache;
  }

  affecterBenevole(): void {
    console.log(this.selectedIdBenevole)
    console.log(this.selectedTache.id)
    this.argsAffecter =
      {
        benevole: {
          id: this.selectedIdBenevole
        },
        tache: {
          id: this.selectedTache.id
        }
      };

    this.service.affecterBenevole(this.argsAffecter).subscribe(
      data => {
        this.selectedTache = data.tache;
        this.getTachesByActivite();
      }
    )

  }

  affecterRessource(): void {
    this.argsAffecterRes =
      {
        idRessource: this.selectedIdRessource,
        idActivite: this.idActivite

      };
    console.log(this.argsAffecterRes)
    this.service.affecterRessource(this.argsAffecterRes).subscribe(
      data => {
        this.activite = data.activite;
        this.getRessourcesByActivite();
      }
    )
  }


  verifierType(re: RessourceDto): boolean {
    if (re.type == "IntervenantExterneDto") {
      return true;
    }
    return false;

  }




  //------------pour le prbm d'encodage

  enleverTrucsCheum(objet: any) {

    let tmp: any = JSON.stringify(objet);
    console.log(tmp);
    tmp = decodeURIComponent(escape(tmp));
    console.log(tmp);
    objet = JSON.parse(tmp);
    return objet;
  }


  //____________________METHODES JS

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openSmall(content) {
    this.modalService.open(content, {
      size: 'sm'
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.idActivite = +this.route.snapshot.paramMap.get("idAct");
    this.getActiviteById();
    //this.activite = JSON.parse(localStorage.getItem("keyActivite"));
    console.log(this.idActivite)

    this.getTachesByActivite();
    this.getRessourcesByActivite();

  }

}
