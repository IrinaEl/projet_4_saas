import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsRaComponent } from './stats-ra.component';

describe('StatsRaComponent', () => {
  let component: StatsRaComponent;
  let fixture: ComponentFixture<StatsRaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsRaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsRaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
