import { Component, OnInit } from '@angular/core';
import { EvenementDto } from 'src/app/interfaces/evenement-dto';
import { ServicesBackService } from 'src/app/ServicesRest/back/services-back.service';

@Component({
  selector: 'app-stats-ra',
  templateUrl: './stats-ra.component.html',
  styleUrls: ['./stats-ra.component.sass']
})
export class StatsRaComponent implements OnInit {

  evenementsEnCours: EvenementDto[];
  evenementsFinis: EvenementDto[];

  constructor(private service: ServicesBackService) { }

  ngOnInit() {
    this.service.getAllEvenement().subscribe(
      data => this.evenementsEnCours = data.evenements
    )
  }

}
