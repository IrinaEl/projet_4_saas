import { Component, OnInit, Input } from '@angular/core';
import { ServicesBackService } from 'src/app/ServicesRest/back/services-back.service';
import { ActiviteDto } from 'src/app/interfaces/activite-dto';
import { EvenementDto } from 'src/app/interfaces/evenement-dto';
declare var $: any;
@Component({
  selector: 'app-evenement-accordeon',
  templateUrl: './evenement-accordeon.component.html',
  styleUrls: ['./evenement-accordeon.component.sass']
})
export class EvenementAccordeonComponent implements OnInit {
  @Input()
  event: EvenementDto;
  activites: ActiviteDto[];
  constructor(private service: ServicesBackService) { }

  ngOnInit() {
    this.getActivitesByEvent(this.event.id);

  }
  getActivitesByEvent(idEvent: number) {
    this.service.getActiviteByEvent(idEvent).subscribe(data => { this.activites = data.activites; 
      let tmp: any = JSON.stringify(this.activites);
      tmp = decodeURIComponent(escape(tmp));
      this.activites = JSON.parse(tmp);});
  }
}