import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvenementAccordeonComponent } from './evenement-accordeon.component';

describe('EvenementAccordeonComponent', () => {
  let component: EvenementAccordeonComponent;
  let fixture: ComponentFixture<EvenementAccordeonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvenementAccordeonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvenementAccordeonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});