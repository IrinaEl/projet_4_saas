import { Component, OnInit } from '@angular/core';
import { ServicesBackService } from 'src/app/ServicesRest/back/services-back.service';
import { EvenementDto } from 'src/app/interfaces/evenement-dto';
import { ActiviteDto } from 'src/app/interfaces/activite-dto';
import { Router, NavigationEnd } from '@angular/router';
import { ResponsableActiviteDto } from 'src/app/interfaces/responsable-activite-dto';
declare var $: any;
@Component({
  selector: 'app-evenements',
  templateUrl: './evenements.component.html',
  styleUrls: ['./evenements.component.sass']
})
export class EvenementsComponent implements OnInit {
  events: EvenementDto[];
  activites: ActiviteDto[];
  //idRA: number;
  ra:ResponsableActiviteDto;

  constructor(private service: ServicesBackService, private router: Router) {

  }

  getByRA(): void {
    this.service.getByRA(this.ra.id).subscribe(
      data => {
        this.events = data.events;       
        setTimeout(() => { this.style() }, 1);
        let tmp: any = JSON.stringify(this.events);
        tmp = decodeURIComponent(escape(tmp));
        this.events = JSON.parse(tmp);
      });
  }

  style() {
    let accordion = $("app-evenements .accordion");
    let button = $("app-evenements button");
    let tabpanel = $("app-evenements .card-body");
    for (let i = 0; i < accordion[0].children.length; i++) {
      accordion[0].children[i].style.marginBottom = "50px";
      accordion[0].children[i].children[0].style.padding = "0px"
    }
    for (let i = 0; i < button.length; i++) {
      button[i].style.textDecoration = "none";
      button[i].className = button[i].className + " box";
      button[i].style.width = "100%";
    }
    for (let i = 0; i < tabpanel.length; i++) {
      tabpanel[i].className = tabpanel[i].className + " box-children";
    }
  }

  ngOnInit() {
    this.ra = JSON.parse(localStorage.getItem("user"))
    this.getByRA();
  }

}
