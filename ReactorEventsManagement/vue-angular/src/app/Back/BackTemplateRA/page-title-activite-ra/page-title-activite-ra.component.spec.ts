import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTitleActiviteRaComponent } from './page-title-activite-ra.component';

describe('PageTitleActiviteRaComponent', () => {
  let component: PageTitleActiviteRaComponent;
  let fixture: ComponentFixture<PageTitleActiviteRaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTitleActiviteRaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTitleActiviteRaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
