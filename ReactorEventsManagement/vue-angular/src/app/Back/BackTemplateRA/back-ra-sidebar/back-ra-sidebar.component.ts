import { Component, OnInit, HostListener } from '@angular/core';
import { ThemeOptions } from '../../../theme-options';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ServicesBackService } from '../../../ServicesRest/back/services-back.service';
import { ActiviteDto } from '../../../interfaces/activite-dto';
import { ResponsableActiviteDto } from '../../../interfaces/responsable-activite-dto';

@Component({
  selector: 'app-back-ra-sidebar',
  templateUrl: './back-ra-sidebar.component.html',
  styleUrls: ['./back-ra-sidebar.component.sass']
})
export class BackRaSidebarComponent implements OnInit {
  public extraParameter: any;
 
  activites:ActiviteDto[] =[];
 // idRa:number;
  ra:ResponsableActiviteDto;
  selectedActivite:ActiviteDto;

  constructor(public globals: ThemeOptions, private activatedRoute: ActivatedRoute, private service: ServicesBackService) {

  }
   //------------pour le prbm d'encodage----------------------
  enleverTrucsCheum(objet: any) {

    let tmp: any = JSON.stringify(objet);
    console.log(tmp);
    tmp = decodeURIComponent(escape(tmp));
    console.log(tmp);
    objet = JSON.parse(tmp);
    return objet;
  }
  //-----------------------------------------------

  getActivitesByResp():void {
    this.service.getActivitesByRa(this.ra.id).subscribe(
      data => {
      this.activites = data.activites;
      this.activites = this.enleverTrucsCheum(this.activites);
      }
    )
  }

  // getRaById():void {
  //   this.service.getRaById(this.idRa).subscribe(
  //     data => {
  //     this.ra = data.responsable;
  //     this.ra = this.enleverTrucsCheum(this.ra);
  //     }
  //   )
  // }

  setSelectedAtivite(activite : ActiviteDto) : void {
    this.selectedActivite = activite;
    console.log(activite)
   // localStorage.setItem("keyActivite", JSON.stringify(this.selectedActivite));
  }

  @select('config') public config$: Observable<any>;

  private newInnerWidth: number;
  private innerWidth: number;
  activeId = 'dashboardsMenu';

  toggleSidebar() {
    this.globals.toggleSidebar = !this.globals.toggleSidebar;
  }

  sidebarHover() {
    this.globals.sidebarHover = !this.globals.sidebarHover;
  }

  ngOnInit() {
    setTimeout(() => {
      this.innerWidth = window.innerWidth;
      if (this.innerWidth < 1200) {
        this.globals.toggleSidebar = true;
      }
    });
    
    this.extraParameter = this.activatedRoute.snapshot.firstChild.data.extraParameter;
    this.ra = JSON.parse(localStorage.getItem("user"))
    // this.idRa=11;
    // this.getRaById();
    this.getActivitesByResp();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.newInnerWidth = event.target.innerWidth;

    if (this.newInnerWidth < 1200) {
      this.globals.toggleSidebar = true;
    } else {
      this.globals.toggleSidebar = false;
    }

    
  }
}