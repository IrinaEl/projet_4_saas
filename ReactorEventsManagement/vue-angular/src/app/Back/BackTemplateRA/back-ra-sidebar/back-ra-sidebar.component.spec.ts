import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackRaSidebarComponent } from './back-ra-sidebar.component';

describe('BackSidebarComponent', () => {
  let component: BackRaSidebarComponent;
  let fixture: ComponentFixture<BackRaSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackRaSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackRaSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});