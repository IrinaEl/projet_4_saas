import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiviteLpEvenementComponent } from './activite-lp-evenement.component';

describe('ActiviteLpEvenementComponent', () => {
  let component: ActiviteLpEvenementComponent;
  let fixture: ComponentFixture<ActiviteLpEvenementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiviteLpEvenementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiviteLpEvenementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
