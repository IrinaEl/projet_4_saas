import { Component, OnInit, Input } from '@angular/core';
import { EvenementDto } from '../../../interfaces/evenement-dto';
import { ActiviteDto } from '../../../interfaces/activite-dto';
import { ServicesBackService } from '../../../ServicesRest/back/services-back.service';
import { ResponsableActiviteDto } from '../../../interfaces/responsable-activite-dto';
import { ArgAddResponsasbleActivity } from '../../../ServicesRest/param-complexes/arg-add-responsasble-activity';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
declare var $: any;
@Component({
  selector: 'app-activite-lp-evenement',
  templateUrl: './activite-lp-evenement.component.html',
  styleUrls: ['./activite-lp-evenement.component.sass']
})
export class ActiviteLpEvenementComponent implements OnInit {
  @Input() event: EvenementDto;
  activites: ActiviteDto[];
  @Input() selectedIdResponsableActivite: number;
  ras: ResponsableActiviteDto[] = null;
  selectedResponsableActivite:ResponsableActiviteDto;
  selectedActivite: ActiviteDto;
  argsAdd: ArgAddResponsasbleActivity = null;
  closeResult: string;

  constructor(private service: ServicesBackService,private modalService: NgbModal) { }

  ngOnInit() {
    this.getActiviteByEvent(this.event.id);
  }
  getActiviteByEvent(idEvent: number): void{
    this.service.getActiviteByEvent(idEvent).subscribe(
      data =>{this.activites=data.activites;
      let tmp: any =JSON.stringify(this.activites);
      tmp =decodeURIComponent(escape(tmp));
      this.activites=JSON.parse(tmp);
      setTimeout(()=>{this.style()}, 10)});
  }
  
  getAllResponsableActivite(): void {

    console.log("coucou")
    this.service.getAllResponsableActivite().subscribe(
      data => {
        console.log(data)
        this.ras = data.responsableActivites
        console.log(this.ras)
      //  this.responsableActivites = this.enleverTrucsCheum(this.responsableActivites);
      }
    )    
  }

  setResponsableActivite(ra:ResponsableActiviteDto){
    console.log(ra)
    this.selectedResponsableActivite=ra;
  }

  addResponsableActivity(): void {
   console.log(this.selectedIdResponsableActivite)
   console.log(this.selectedActivite.id)
   this.argsAdd =
   {
    raDto:{
      id: this.selectedIdResponsableActivite
    },
    activiteDto:{
      id: this.selectedActivite.id
    }
   };
   this.service.addResponsableActivity(this.argsAdd).subscribe(
     data => {
       this.selectedActivite = data.activiteDto;
       this.getActiviteByEvent;
     }
   )
  }

  enleverTrucsCheum(objet: any) {

    let tmp: any = JSON.stringify(objet);
    console.log(tmp);
    tmp = decodeURIComponent(escape(tmp));
    console.log(tmp);
    objet = JSON.parse(tmp);
    return objet;
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  style(){
    let accordion=$("app-activite-lp-evenement .accordion");
    let button=$("app-activite-lp-evenement button");
    for(let i = 0;  i < accordion[0].children.length; i++){
      accordion[0].children[i].style.marginBottom ="50px";
    }
    for(let i = 0; i < button.length; i++){
      button[i].style.textDecoration = "none";
      button[i].className = button[i].className + " box";
      button[i].style.width = "100%";
    }
  }

}
