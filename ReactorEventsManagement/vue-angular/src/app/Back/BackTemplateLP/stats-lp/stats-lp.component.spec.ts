import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsLpComponent } from './stats-lp.component';

describe('StatsLpComponent', () => {
  let component: StatsLpComponent;
  let fixture: ComponentFixture<StatsLpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsLpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsLpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
