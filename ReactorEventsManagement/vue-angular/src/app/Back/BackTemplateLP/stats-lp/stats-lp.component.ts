import { Component, OnInit } from '@angular/core';
import { ServicesBackService } from 'src/app/ServicesRest/back/services-back.service';
import { EvenementDto } from 'src/app/interfaces/evenement-dto';

@Component({
  selector: 'app-stats-lp',
  templateUrl: './stats-lp.component.html',
  styleUrls: ['./stats-lp.component.sass']
})
export class StatsLpComponent implements OnInit {

  evenementsEnCours: EvenementDto[];
  evenementsFinis: EvenementDto[];

  constructor(private service: ServicesBackService) { }

  ngOnInit() {
    this.service.getAllEvenement().subscribe(
      data => this.evenementsEnCours = data.evenements
    )
  }

}
