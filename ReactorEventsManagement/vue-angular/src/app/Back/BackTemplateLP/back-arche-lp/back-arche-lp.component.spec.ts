import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackArcheLpComponent } from './back-arche-lp.component';

describe('BackArcheLpComponent', () => {
  let component: BackArcheLpComponent;
  let fixture: ComponentFixture<BackArcheLpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackArcheLpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackArcheLpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
