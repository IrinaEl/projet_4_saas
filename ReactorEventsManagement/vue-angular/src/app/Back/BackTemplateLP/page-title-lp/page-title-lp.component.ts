import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-page-title-lp',
  templateUrl: './page-title-lp.component.html',
  styleUrls: ['./page-title-lp.component.sass']
})
export class PageTitleLpComponent implements OnInit {

 @Input() heading;
 @Input() subheading;
 @Input() icon;

  ngOnInit() {
  }

}
