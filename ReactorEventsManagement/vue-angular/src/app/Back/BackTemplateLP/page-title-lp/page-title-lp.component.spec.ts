import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTitleLpComponent } from './page-title-lp.component';

describe('PageTitleLpComponent', () => {
  let component: PageTitleLpComponent;
  let fixture: ComponentFixture<PageTitleLpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTitleLpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTitleLpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
