import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackLpSidebarComponent } from './back-lp-sidebar.component';

describe('BackLpSidebarComponent', () => {
  let component: BackLpSidebarComponent;
  let fixture: ComponentFixture<BackLpSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackLpSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackLpSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
