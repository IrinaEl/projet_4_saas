import { Component, OnInit, HostListener } from '@angular/core';
import { ThemeOptions } from '../../../theme-options';
import { select } from '@angular-redux/store';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ResponsableActiviteDto } from 'src/app/interfaces/responsable-activite-dto';
import { LeadPoleDto } from 'src/app/interfaces/lead-pole-dto';

@Component({
  selector: 'app-back-lp-sidebar',
  templateUrl: './back-lp-sidebar.component.html',
  styleUrls: ['./back-lp-sidebar.component.sass']
})
export class BackLpSidebarComponent implements OnInit {

  lp:LeadPoleDto;

  public extraParameter: any;

  constructor(public globals: ThemeOptions, private activatedRoute: ActivatedRoute) {

  }

  @select('config') public config$: Observable<any>;

  private newInnerWidth: number;
  private innerWidth: number;
  activeId = 'dashboardsMenu';

  toggleSidebar() {
    this.globals.toggleSidebar = !this.globals.toggleSidebar;
  }

  sidebarHover() {
    this.globals.sidebarHover = !this.globals.sidebarHover;
  }


  ngOnInit() {
    setTimeout(() => {
      this.innerWidth = window.innerWidth;
      if (this.innerWidth < 1200) {
        this.globals.toggleSidebar = true;
      }
    });

    this.extraParameter = this.activatedRoute.snapshot.firstChild.data.extraParameter;
this.lp = JSON.parse(localStorage.getItem("user"));
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.newInnerWidth = event.target.innerWidth;

    if (this.newInnerWidth < 1200) {
      this.globals.toggleSidebar = true;
    } else {
      this.globals.toggleSidebar = false;
    }

  }
}

