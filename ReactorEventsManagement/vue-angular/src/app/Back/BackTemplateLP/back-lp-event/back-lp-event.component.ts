import { Component, OnInit } from '@angular/core';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { EvenementDto } from '../../../interfaces/evenement-dto';
import { ServicesBackService } from '../../../ServicesRest/back/services-back.service';
import { LeadPoleDto } from 'src/app/interfaces/lead-pole-dto';
declare var $: any;
@Component({
  selector: 'app-back-lp-event',
  templateUrl: './back-lp-event.component.html',
  styleUrls: ['./back-lp-event.component.sass']
})
export class BackLpEventComponent implements OnInit {
  //idLp: number = 15;
  lp:LeadPoleDto;
  heading = 'VOS ÉVÉNEMENTS';
  subheading = "Retrouvez ici les événements qui vous ont été affectés";
  icon = 'class="vsm-icon pe-7s-date';
  public isCollapsed = false;
  
  public beforeChange($event: NgbPanelChangeEvent) {

    if ($event.panelId === 'preventchange-2') {
      $event.preventDefault();
    }

    if ($event.panelId === 'preventchange-3' && $event.nextState === false) {
      $event.preventDefault();
    }
  };
  
  events: EvenementDto[] = [] ;
  
  constructor(private service: ServicesBackService) { }

  getEvenementsByLeadPoleID(leadPoleID: number): void{
    this.service.getEvenementsByLeadPoleID(this.lp.id).subscribe(
      data => {this.events=data.evenements;
        setTimeout(() => {this.style()},10);
      let tmp: any = JSON.stringify(this.events);
      this.events=JSON.parse(tmp);
},
      errorCode => console.log(errorCode)
    )
  }

  style() {
    let accordion = $("app-back-lp-event .accordion");
    let button = $("app-back-lp-event button");
    
    for (let i = 0; i < accordion[0].children.length; i++) {
      accordion[0].children[i].style.marginBottom = "50px";
    }
    for (let i = 0; i < button.length; i++) {
      button[i].style.textDecoration = "none";
      button[i].className = button[i].className + " box";
      button[i].style.width="100%";
    }

  }

  ngOnInit() {
    this.lp = JSON.parse(localStorage.getItem("user"))
    this.getEvenementsByLeadPoleID(this.lp.id);
  }

}
