import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackLpEventComponent } from './back-lp-event.component';

describe('BackLpEventComponent', () => {
  let component: BackLpEventComponent;
  let fixture: ComponentFixture<BackLpEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackLpEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackLpEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
