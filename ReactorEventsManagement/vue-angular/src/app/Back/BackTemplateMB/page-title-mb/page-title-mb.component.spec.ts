import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTitleMbComponent } from './page-title-mb.component';

describe('PageTitleMbComponent', () => {
  let component: PageTitleMbComponent;
  let fixture: ComponentFixture<PageTitleMbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTitleMbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTitleMbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
