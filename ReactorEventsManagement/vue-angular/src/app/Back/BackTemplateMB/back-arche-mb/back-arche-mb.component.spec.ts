import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackArcheMbComponent } from './back-arche-mb.component';

describe('BackArcheMbComponent', () => {
  let component: BackArcheMbComponent;
  let fixture: ComponentFixture<BackArcheMbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackArcheMbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackArcheMbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

