import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackMbCreerEventComponent } from './back-mb-creer-event.component';

describe('BackMbCreerEventComponent', () => {
  let component: BackMbCreerEventComponent;
  let fixture: ComponentFixture<BackMbCreerEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackMbCreerEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackMbCreerEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
