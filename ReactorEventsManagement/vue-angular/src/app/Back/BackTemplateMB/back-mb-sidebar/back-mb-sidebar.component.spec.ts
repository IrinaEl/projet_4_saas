import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackMbSidebarComponent } from './back-mb-sidebar.component';

describe('BackMbSidebarComponent', () => {
  let component: BackMbSidebarComponent;
  let fixture: ComponentFixture<BackMbSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackMbSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackMbSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});