import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiviteByEvenementComponent } from './activite-by-evenement.component';

describe('ActiviteByEvenementComponent', () => {
  let component: ActiviteByEvenementComponent;
  let fixture: ComponentFixture<ActiviteByEvenementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiviteByEvenementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiviteByEvenementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});