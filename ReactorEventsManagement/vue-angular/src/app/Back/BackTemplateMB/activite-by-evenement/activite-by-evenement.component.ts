import { Component, OnInit, Input } from '@angular/core';
import { ServicesBackService } from '../../../ServicesRest/back/services-back.service';
import { EvenementDto } from '../../../interfaces/evenement-dto';
import { ActiviteDto } from '../../../interfaces/activite-dto';
declare var $: any;
@Component({
  selector: 'app-activite-by-evenement',
  templateUrl: './activite-by-evenement.component.html',
  styleUrls: ['./activite-by-evenement.component.sass']
})
export class ActiviteByEvenementComponent implements OnInit {

  @Input() event: EvenementDto;
  activites: ActiviteDto[];
  constructor(private service: ServicesBackService) { }

  ngOnInit() {
    this.getActiviteByEvent(this.event.id);
  }
  getActiviteByEvent(idEvent: number) {
    this.service.getActiviteByEvent(idEvent).subscribe(
      data => {this.activites = data.activites;
        let tmp: any = JSON.stringify(this.activites);
        tmp = decodeURIComponent(escape(tmp));
        this.activites = JSON.parse(tmp);
      });
  } 

  
}
