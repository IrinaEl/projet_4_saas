import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackMbEventComponent } from './back-mb-event.component';

describe('BackMbEventComponent', () => {
  let component: BackMbEventComponent;
  let fixture: ComponentFixture<BackMbEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackMbEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackMbEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});