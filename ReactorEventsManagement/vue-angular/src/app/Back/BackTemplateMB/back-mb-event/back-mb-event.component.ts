import { Component, OnInit } from '@angular/core';
import {NgbPanelChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import{EvenementDto} from 'src/app/interfaces/evenement-dto'
import { ServicesBackService } from '../../../ServicesRest/back/services-back.service';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-back-mb-event',
  templateUrl: './back-mb-event.component.html',
  styleUrls: ['./back-mb-event.component.sass']
})
export class BackMbEventComponent implements OnInit {

  heading = 'ÉVÉNEMENTS A VENIR';
  subheading = 'Retrouvez ici tous vos évènements à venir';
  icon = 'vsm-icon pe-7s-date';
 
  events: EvenementDto[] = [] ;
  
  constructor(private service: ServicesBackService,private router: Router) { }
  getAllEvent(): void {
    this.service.getAllEvenement().subscribe(
      data => {this.events = data.evenements;
        console.log(this.events)
        setTimeout(() => { this.style() }, 1);
          let tmp: any = JSON.stringify(this.events);
          tmp = decodeURIComponent(escape(tmp));
          this.events = JSON.parse(tmp);
          
        } ,
      errorCode => console.log(errorCode)
    )
  }

  style() {
    console.log("ezrgft")
    let accordion = $("app-back-mb-event .accordion");
    let button = $("app-back-mb-event button");
    let tabpanel = $("app-back-mb-event .card-body");
    console.log(accordion[0])
    for (let i = 0; i < accordion[0].children.length; i++) {
      accordion[0].children[i].style.marginBottom = "50px";
      accordion[0].children[i].children[0].style.padding = "0px"
    }
    for (let i = 0; i < button.length; i++) {
      button[i].style.textDecoration = "none";
      button[i].className = button[i].className + " box";
      button[i].style.width = "100%";
    }
    for (let i = 0; i < tabpanel.length; i++) {
      tabpanel[i].className = tabpanel[i].className + " box-children";
    }

  }

  ngOnInit() {
    this.getAllEvent();
  }

}