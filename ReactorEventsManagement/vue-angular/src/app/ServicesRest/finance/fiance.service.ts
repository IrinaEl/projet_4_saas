import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RetourSomme } from '../../interfaces/retour/retour-somme';
import { RetourCount } from '../../interfaces/retour/retour-count';

@Injectable({
  providedIn: 'root'
})
export class FianceService {
  baseURL: string = "http://localhost:18080/BusinessImplFinance/restInProjet/finance-rest/";

  constructor(private http: HttpClient) { }

  serviceSommeRecetteParEvenement(id: number): Observable<RetourSomme> {
    return this.http.get<RetourSomme>(this.baseURL + "somme-recette/" + id);
  }

  serviceSommeDepenseParEvenement(id: number): Observable<RetourSomme> {
    return this.http.get<RetourSomme>(this.baseURL + "somme-depense/" + id);
  }

  serviceNbRevActParEvenement(id: number): Observable<RetourCount> {
    return this.http.get<RetourCount>(this.baseURL + "nb-rev-act/" + id);
  }

  serviceNbRevSponsoParEvenement(id: number): Observable<RetourCount> {
    return this.http.get<RetourCount>(this.baseURL + "nb-rev-sponso/" + id)
  }
}
