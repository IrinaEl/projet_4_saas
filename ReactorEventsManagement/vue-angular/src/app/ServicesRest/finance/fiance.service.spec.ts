import { TestBed } from '@angular/core/testing';

import { FianceService } from './fiance.service';

describe('FianceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FianceService = TestBed.get(FianceService);
    expect(service).toBeTruthy();
  });
});
