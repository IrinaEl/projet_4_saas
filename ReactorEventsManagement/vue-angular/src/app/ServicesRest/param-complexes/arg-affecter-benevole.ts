import { TacheDto } from '../../interfaces/tache-dto';
import { RessourceDto } from '../../interfaces/ressource-dto';

export interface ArgAffecterBenevole {
    tache:TacheDto,
    benevole:RessourceDto
}
