import { ResponsableActiviteDto } from '../../interfaces/responsable-activite-dto';
import { ActiviteDto } from '../../interfaces/activite-dto';

export interface ArgAddResponsasbleActivity {
    raDto:ResponsableActiviteDto,
    activiteDto:ActiviteDto
}
