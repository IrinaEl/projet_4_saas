import { TestBed } from '@angular/core/testing';

import { ServicesBackService } from './services-back.service';

describe('ServicesBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicesBackService = TestBed.get(ServicesBackService);
    expect(service).toBeTruthy();
  });
});
