import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RetourActivitesByRa } from '../retours-back/retour-activites-by-ra';
import { RetourRaById } from '../retours-back/retour-ra-by-id';
import { RetourActiviteById } from '../retours-back/retour-activite-by-id';
import { RetourEventsByRa } from '../retours-back/retour-events-by-ra';
import { RetourActiviteByEvent } from '../retours-back/retour-activite-by-event';
import { RetourListeTaches } from '../retours-back/retour-liste-taches';
import { RetourEvent } from '../retours-back/retour-event';
import { RetourUneTache } from '../retours-back/retour-une-tache';
import { ArgAffecterBenevole } from '../param-complexes/arg-affecter-benevole';
import { RetourListeBenevoles } from '../retours-back/retour-liste-benevoles';
import { RetourGetEvenementsByLeadPoleID } from '../retours-back/retour-get-evenements-by-lead-pole-id';
import { RetourGetAllResponsableActivite } from '../retours-back/retour-get-all-responsable-activite';
import { RetourAddResponsableActivity } from '../retours-back/retour-add-responsable-activity';
import { ResponsableActiviteDto } from '../../interfaces/responsable-activite-dto';
import { ActiviteDto } from '../../interfaces/activite-dto';
import { ArgsInscritionAct } from 'src/app/interfaces/arguments/inscrition-act';
import { RetourInscription } from 'src/app/interfaces/retour/retour-inscription';
import { ArgsInscritpion } from 'src/app/interfaces/arguments/args-inscritpion-event';
import { RetourListeRessources } from '../retours-back/retour-liste-ressources';
import { ArgAffecterRes } from '../param-complexes/arg-affecter-res';
import { ArgAddResponsasbleActivity } from '../param-complexes/arg-add-responsasble-activity';

@Injectable({
  providedIn: 'root'
})
export class ServicesBackService {
  baseUrl: string = "http://localhost:18080/BusinessImplBack/p4/back-rest/"

  constructor(private http: HttpClient) { }

  getActivitesByRa(idRa: number): Observable<RetourActivitesByRa> {
    return this.http.get<RetourActivitesByRa>(
      this.baseUrl + "getActivitesByRa/" + idRa);
  }

  getRaById(idRa: number): Observable<RetourRaById> {
    return this.http.get<RetourRaById>(
      this.baseUrl + "getRaById/" + idRa);

  }

  getActiviteByid(idActivite: number): Observable<RetourActiviteById> {
    return this.http.get<RetourActiviteById>(
      this.baseUrl + "getActiviteById/" + idActivite);

  }

  getByRA(idRA: number): Observable<RetourEventsByRa> {
    return this.http.get<RetourEventsByRa>(this.baseUrl + "getByRA/" + idRA);
  }

  getActiviteByEvent(idEvent: number): Observable<RetourActiviteByEvent> {
    return this.http.get<RetourActiviteByEvent>(this.baseUrl + "getActivitesByEvent/" + idEvent);
  }
  getTachesByActivite(idActivite: number) : Observable<RetourListeTaches> {
    return this.http.get<RetourListeTaches>(this.baseUrl +  "getTachesByActivite/" + idActivite);
  }

  getAllEvenement(): Observable<RetourEvent> {
    return this.http.get<RetourEvent>(this.baseUrl+ "getAllEvent");
  }
  affecterBenevole(argAff : ArgAffecterBenevole):Observable<RetourUneTache>{
    return this.http.post<RetourUneTache>(this.baseUrl + "affecterBenevole", argAff);
  }
  affecterRessource(argsAffRess:ArgAffecterRes):Observable<RetourActiviteById>{
    return this.http.post<RetourActiviteById>(this.baseUrl + "affecterRessource", argsAffRess);
  }
  getAllBenevoles(): Observable<RetourListeBenevoles>{
    return this.http.get<RetourListeBenevoles>(this.baseUrl + "getAllBenevoles");
  }
  getEvenementsByLeadPoleID(leadPoleID: number): Observable<RetourGetEvenementsByLeadPoleID>{
    return this.http.get<RetourGetEvenementsByLeadPoleID>(this.baseUrl +"getEvenementsByLeadPoleID/"+leadPoleID);
  }
  inscriptionActivite(argInscriptionAct : ArgsInscritionAct): Observable<RetourInscription>{
    return this.http.post<RetourInscription>(this.baseUrl + "inscription-activite", argInscriptionAct);
  }

  getAllRessources():Observable<RetourListeRessources> {
    return this.http.get<RetourListeRessources>(this.baseUrl + "getAllRessources");
  }

  getAllResponsableActivite(): Observable<RetourGetAllResponsableActivite>{
    return this.http.get<RetourGetAllResponsableActivite>(this.baseUrl+"getAllResponsableActivite");
  }

  getRessourcesByActivite(idAct:Number):Observable<RetourListeRessources>{
      return this.http.get<RetourListeRessources>(this.baseUrl + "getRessourcesByActivite/" + idAct)
  }
  addResponsableActivity(argAdd: ArgAddResponsasbleActivity):Observable<RetourAddResponsableActivity>{
    return this.http.post<RetourAddResponsableActivity>(this.baseUrl+"addResponsableActivity", argAdd);
  }
  inscriptionEvenement(argInscription : ArgsInscritpion): Observable<RetourInscription>{
    return this.http.post<RetourInscription>(this.baseUrl + "inscription-evenement", argInscription);
  }

  getAllDoneEvents(): Observable<RetourEvent> {
    return this.http.get<RetourEvent>(this.baseUrl + "/get-all-events")
  }
}