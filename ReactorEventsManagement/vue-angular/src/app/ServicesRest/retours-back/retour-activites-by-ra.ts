import { ActiviteDto } from '../../interfaces/activite-dto';

export interface RetourActivitesByRa {
    exception?:any,
    activites?:ActiviteDto[]
}
