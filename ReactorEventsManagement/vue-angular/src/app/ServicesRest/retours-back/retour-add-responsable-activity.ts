import { ActiviteDto } from '../../interfaces/activite-dto';

export interface RetourAddResponsableActivity {
    activiteException?: any;
    activiteDto?:ActiviteDto
}
