import { RessourceDto } from 'src/app/interfaces/ressource-dto';

export interface RetourListeRessources {
    exception?:any,
    ressources?:RessourceDto[]
}
