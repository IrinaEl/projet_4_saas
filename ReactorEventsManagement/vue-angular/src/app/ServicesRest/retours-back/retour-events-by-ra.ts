import { EvenementDto } from 'src/app/interfaces/evenement-dto';

export interface RetourEventsByRa {
    exception?: any,
    events?: EvenementDto[]
}
