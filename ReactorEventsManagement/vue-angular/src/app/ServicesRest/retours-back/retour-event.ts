import {EvenementDto} from '../../interfaces/evenement-dto'

export interface RetourEvent {
    error?: any,
    evenements?: EvenementDto[];
}
