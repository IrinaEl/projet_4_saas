import { ResponsableActiviteDto } from '../../interfaces/responsable-activite-dto';

export interface RetourGetAllResponsableActivite {
    raException?:any;
    responsableActivites?:ResponsableActiviteDto[]
}
