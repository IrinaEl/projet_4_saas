import { ResponsableActiviteDto } from 'src/app/interfaces/responsable-activite-dto';

export interface RetourRaById {
    exception?:any,
    responsable?:ResponsableActiviteDto
}
