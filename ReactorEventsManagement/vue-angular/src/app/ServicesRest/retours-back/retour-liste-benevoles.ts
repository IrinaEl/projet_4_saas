import { RessourceDto } from '../../interfaces/ressource-dto';

export interface RetourListeBenevoles {
    exception?:any,
    benevoles?:RessourceDto[]
}
