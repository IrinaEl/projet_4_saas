import { EvenementDto } from '../../interfaces/evenement-dto';

export interface RetourAllEvents {
    exception?:any,
    evenements?:EvenementDto[]
}
