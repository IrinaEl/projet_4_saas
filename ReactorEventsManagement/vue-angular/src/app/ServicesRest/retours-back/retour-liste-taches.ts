import { TacheDto } from '../../interfaces/tache-dto';

export interface RetourListeTaches {
    exception?:any,
    taches?:TacheDto[]
}
