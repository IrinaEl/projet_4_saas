import { EvenementDto } from '../../interfaces/evenement-dto';

export interface RetourGetEvenementsByLeadPoleID {
    exception?: any,
    evenements?: EvenementDto[]
}
