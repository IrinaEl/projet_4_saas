import { TacheDto } from '../../interfaces/tache-dto';

export interface RetourUneTache {
    tache?:TacheDto,
    exception?:any
}
