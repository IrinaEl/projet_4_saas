import { ActiviteDto } from 'src/app/interfaces/activite-dto';

export interface RetourActiviteByEvent {
    exception?:any,
    activites?:ActiviteDto[]
}
