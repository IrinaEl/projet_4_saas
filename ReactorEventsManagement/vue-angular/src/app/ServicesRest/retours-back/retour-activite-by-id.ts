import { ActiviteDto } from 'src/app/interfaces/activite-dto';

export interface RetourActiviteById {
    exception?:any,
    activite?:ActiviteDto
}
