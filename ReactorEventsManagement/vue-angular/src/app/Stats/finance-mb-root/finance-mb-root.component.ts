import { Component, OnInit } from '@angular/core';
import { EvenementDto } from '../../interfaces/evenement-dto';
import { ServicesBackService } from '../../ServicesRest/back/services-back.service';

@Component({
  selector: 'app-finance-mb-root',
  templateUrl: './finance-mb-root.component.html',
  styleUrls: ['./finance-mb-root.component.sass']
})
export class FinanceMbRootComponent implements OnInit {
  evenementsEnCours: EvenementDto[];
  evenementsFinis: EvenementDto[];

  constructor(private service: ServicesBackService) { }

  ngOnInit() {
    this.service.getAllDoneEvents().subscribe(
      data => this.evenementsEnCours = data.evenements
    )
  }

}
