import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceMbRootComponent } from './finance-mb-root.component';

describe('FinanceMbRootComponent', () => {
  let component: FinanceMbRootComponent;
  let fixture: ComponentFixture<FinanceMbRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceMbRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceMbRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
