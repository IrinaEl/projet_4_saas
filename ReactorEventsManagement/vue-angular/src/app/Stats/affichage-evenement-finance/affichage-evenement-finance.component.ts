import { Component, OnInit, Input } from '@angular/core';
import { EvenementDto } from '../../interfaces/evenement-dto';
import { FianceService } from '../../ServicesRest/finance/fiance.service';
import { ChartOptions, ChartType, plugins } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-affichage-evenement-finance',
  templateUrl: './affichage-evenement-finance.component.html',
  styleUrls: ['./affichage-evenement-finance.component.sass']
})
export class AffichageEvenementFinanceComponent implements OnInit {
  @Input() benef: number = 0;
  @Input() recette: number = 0;
  @Input() depense: number = 0;
  @Input() event: EvenementDto;
  nbRevAct: number;
  nbRevSponso: number;

  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    plugins: {
      labels: {
        render: 'percentage',
        precision: 2,
        arc: true
      },
    } 
  };
  public pieChartLabels: Label[] = ['Revenus Activité(s)', 'Revenus Sponsoring'];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(242,31,232,0.7)', 'rgba(220,40,120,0.7)', 'rgba(0,0,255,0.3)'],
    },
  ];

  constructor(private service: FianceService) {  }
  
  ngOnInit() {
    this.remplissageStats()
    let tmp: any = JSON.stringify(this.event);
    tmp = decodeURIComponent(escape(tmp));
    this.event = JSON.parse(tmp);
  }
  
  ngDoCheck() {
    this.calcul()
  }

  remplissageStats(): void {
    this.service.serviceSommeRecetteParEvenement(this.event.id).subscribe(
      data => {
        this.recette = data.somme
        console.log("recette : " + this.recette)
      }
    )
    this.service.serviceSommeDepenseParEvenement(this.event.id).subscribe(
      data => {
        this.depense = data.somme
        console.log("depense : " + this.depense)
      }
    )
    this.service.serviceNbRevActParEvenement(this.event.id).subscribe(
      data => {
        this.nbRevAct = data.nombreCompte
      }
    )
    this.service.serviceNbRevSponsoParEvenement(this.event.id).subscribe(
      data => {
        this.nbRevSponso = data.nombreCompte
        console.log("activité : " + this.nbRevAct)
        console.log("sponso : " + this.nbRevSponso)
      }
    )
  }
  
  calcul(): void {
    console.log("recette : " + this.recette)
    console.log("depense : " + this.depense)
    this.benef = this.recette - this.depense;
    console.log("benef : " + this.benef)
    this.pieChartData = [this.nbRevAct, this.nbRevSponso]
    console.log(this.nbRevAct)
    console.log(this.nbRevSponso)
  }
}
