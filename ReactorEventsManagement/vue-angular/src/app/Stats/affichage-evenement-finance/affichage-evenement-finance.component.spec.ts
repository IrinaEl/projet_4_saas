import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageEvenementFinanceComponent } from './affichage-evenement-finance.component';

describe('AffichageEvenementFinanceComponent', () => {
  let component: AffichageEvenementFinanceComponent;
  let fixture: ComponentFixture<AffichageEvenementFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageEvenementFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageEvenementFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
