import { Component, OnInit, Input } from '@angular/core';
import { EvenementDto } from '../../../../interfaces/evenement-dto';
import { Route, Router, ActivatedRoute } from '@angular/router'
import { Projet4Service } from '../../../../projet-4.service';
import { ServicesBackService } from '../../../../ServicesRest/back/services-back.service';
import { ActiviteDto } from '../../../../interfaces/activite-dto';

@Component({
  selector: 'app-page-consultation-event',
  templateUrl: './page-consultation-event.component.html',
  styleUrls: ['./page-consultation-event.component.sass']
})
export class PageConsultationEventComponent implements OnInit {

  evenement: EvenementDto = null;
  activite: ActiviteDto[];
  idEv:number;

  constructor(private route: ActivatedRoute, private service: Projet4Service, private serviceBack: ServicesBackService) { }

  getEventById() : void {
    this.service.getEventById(this.idEv).subscribe(
      data => {
        this.evenement = data.event;
        
      }
    )
    this.serviceBack.getActiviteByEvent(this.idEv).subscribe(
      data => {
        this.activite = data.activites;
        let tmp: any = JSON.stringify(this.activite);
        tmp=decodeURIComponent(escape(tmp));
        this.activite = JSON.parse(tmp)
      }
    )
  }
  ngOnInit() {
    this.idEv = +this.route.snapshot.paramMap.get("idEvent");
    console.log(this.idEv);
    this.getEventById();
  //   this.serviceBack.getActiviteByEvent(idEvent).subscribe(
  //     data => {
  //       this.activite = data.activites;
  //     }
  //   );
   }

}
