import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageConsultationEventComponent } from './page-consultation-event.component';

describe('PageConsultationEventComponent', () => {
  let component: PageConsultationEventComponent;
  let fixture: ComponentFixture<PageConsultationEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageConsultationEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageConsultationEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
