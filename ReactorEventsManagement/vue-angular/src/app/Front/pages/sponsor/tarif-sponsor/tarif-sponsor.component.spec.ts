import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarifSponsorComponent } from './tarif-sponsor.component';

describe('TarifSponsorComponent', () => {
  let component: TarifSponsorComponent;
  let fixture: ComponentFixture<TarifSponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarifSponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarifSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
