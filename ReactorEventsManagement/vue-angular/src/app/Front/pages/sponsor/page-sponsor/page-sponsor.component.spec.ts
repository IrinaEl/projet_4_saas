import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSponsorComponent } from './page-sponsor.component';

describe('PageSponsorComponent', () => {
  let component: PageSponsorComponent;
  let fixture: ComponentFixture<PageSponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
