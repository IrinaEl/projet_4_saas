import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDispoSponsorsComponent } from './event-dispo-sponsors.component';

describe('EventDispoSponsorsComponent', () => {
  let component: EventDispoSponsorsComponent;
  let fixture: ComponentFixture<EventDispoSponsorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDispoSponsorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDispoSponsorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
