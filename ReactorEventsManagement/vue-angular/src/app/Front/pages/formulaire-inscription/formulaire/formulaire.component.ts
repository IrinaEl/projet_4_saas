import { Component, OnInit, Input } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { EvenementDto } from '../../../../interfaces/evenement-dto';
import { ActivatedRoute } from '@angular/router';
import { ServicesBackService } from '../../../../ServicesRest/back/services-back.service';
import { Projet4Service } from '../../../../projet-4.service';
import { ActiviteDto } from '../../../../interfaces/activite-dto';
import { RetourInscription } from '../../../../interfaces/retour/retour-inscription';
import { InscriptionDto } from '../../../../interfaces/inscription-dto';
import { ArgsInscritpion } from '../../../../interfaces/arguments/args-inscritpion-event';
import { NgForm } from '@angular/forms';
import { ArgsInscritionAct } from '../../../../interfaces/arguments/args-inscrition-act';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.sass']
})
export class FormulaireComponent implements OnInit {
  @Input() nom: string
  @Input() prenom: string
  @Input() dateNaissance: Date
  @Input() mail: string
  evenement:EvenementDto
  activite:ActiviteDto
  idEvent : number = +this.route.snapshot.paramMap.get("id")
  args: ArgsInscritionAct = null;
  argsEvent: ArgsInscritpion= null;
  constructor(private route: ActivatedRoute, private service: Projet4Service, private serviceBack: ServicesBackService) { }
  
  recupEvent(): void {

    this.service.getEventById(this.idEvent).subscribe(
     
      data => {
        this.evenement = data.event;
        let tmp: any = JSON.stringify(this.evenement);
        tmp=decodeURIComponent(escape(tmp));
        this.evenement = JSON.parse(tmp);
      }
    );
  }
  recupAct(): void {
  this.serviceBack.getActiviteByid(this.idEvent).subscribe(
    data => {
      this.activite = data.activite;
      let tmp: any = JSON.stringify(this.activite);
      tmp=decodeURIComponent(escape(tmp));
      this.activite = JSON.parse(tmp);
    }
  );
  }

  inscriptionEvenement(): void {
    this.argsEvent.paramEvent = this.evenement
    let inscription: InscriptionDto
    inscription.mail = this.mail
    inscription.nom = this.nom
    inscription.prenom = this.prenom
    inscription.dateNaissance = this.dateNaissance
    this.argsEvent.paramInscriptionEvenement = inscription

    this.service.inscriptionEvenement(this.argsEvent).subscribe(
      
    );
  }
  inscriptionAct(): void {
    this.args.activite = this.activite
    let inscription: InscriptionDto
    inscription.mail = this.mail
    inscription.nom = this.nom
    inscription.prenom = this.prenom
    inscription.dateNaissance = this.dateNaissance
    this.args.inscriptionAct = inscription

    this.service.inscriptionActivite(this.args).subscribe(
      
    );
  }
  ngOnInit() {
    let idEvent = +this.route.snapshot.paramMap.get("idEvent");
    console.log(idEvent);
    this.service.getEventById(idEvent).subscribe(
      data => {
        this.evenement = data.event;
        let tmp: any = JSON.stringify(this.evenement);
        tmp=decodeURIComponent(escape(tmp));
        this.evenement = JSON.parse(tmp);
    }
  );
  }
}


