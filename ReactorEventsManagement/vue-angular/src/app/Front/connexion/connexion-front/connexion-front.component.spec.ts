import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnexionFrontComponent } from './connexion-front.component';

describe('ConnexionFrontComponent', () => {
  let component: ConnexionFrontComponent;
  let fixture: ComponentFixture<ConnexionFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnexionFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnexionFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
