import { Component, OnInit, Input } from '@angular/core';
import { SponsorDto } from '../../../interfaces/sponsor-dto';
import { ConnexionException } from '../../../interfaces/exception/connexion-exception';
import { Projet4Service } from '../../../projet-4.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion-front',
  templateUrl: './connexion-front.component.html',
  styleUrls: ['./connexion-front.component.sass']
})
export class ConnexionFrontComponent implements OnInit {

  user: SponsorDto
  error: ConnexionException
  message: String
  @Input() mail:String
  @Input() mdp:String
  
  constructor(private service: Projet4Service,
    private router: Router) { }

    methodeConnexionFront(): void {
    console.log(this.mail);
    console.log(this.mdp);
      this.service.serviceConnexionFront(this.mail, this.mdp).subscribe(
        data => {
          if (data.error) {
            this.message = data.error.message
          } else {
            this.user = data.user;
            if (data.user) {
              this.router.navigate(['/sponsor']);
              localStorage.setItem("sponsor", JSON.stringify(this.user));
        }
      }
      }
      )
    }

  ngOnInit() {
  }

}
