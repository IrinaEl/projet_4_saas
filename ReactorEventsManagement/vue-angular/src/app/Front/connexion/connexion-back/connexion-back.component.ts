import { Component, OnInit, RootRenderer, Input } from '@angular/core';
import { Projet4Service } from '../../../projet-4.service';
import { RetourConnexionBack } from '../../../interfaces/retour/retour-connexion-back';
import { UtilisateurDto } from '../../../interfaces/utilisateur-dto';
import { ConnexionException } from '../../../interfaces/exception/connexion-exception';
import { SponsorDto } from '../../../interfaces/sponsor-dto';
import { LeadPoleDto } from '../../../interfaces/lead-pole-dto';
import { MembreAdministrationDto } from '../../../interfaces/membre-administration-dto';
import { ResponsableActiviteDto } from '../../../interfaces/responsable-activite-dto';
import { rootReducer } from '../../../ThemeOptions/store';
import { RouterLink, Router } from '@angular/router';
import { ServicesBackService } from '../../../ServicesRest/back/services-back.service';

@Component({
  selector: 'app-connexion-back',
  templateUrl: './connexion-back.component.html',
  styleUrls: ['./connexion-back.component.sass']
})
export class ConnexionBackComponent implements OnInit {

  user: UtilisateurDto
  error: ConnexionException
  message: String = ""
  @Input() mail: String
  @Input() mdp: String

  constructor(private service: Projet4Service,
    private router: Router) { }

  methodeConnexionBack(): void {
    console.log(this.mail);
    console.log(this.mdp);
    this.service.serviceConnexionBack(this.mail, this.mdp).subscribe(
      data => {
        if (data.error) {
          this.message = data.error.message
        } else {
          this.user = data.user;
          if (this.user.type == "ResponsableActiviteDto") {
            this.router.navigate(['/back/stats-ra']);
          }
          if (this.user.type == "LeadPoleDto") {
            this.router.navigate(['/backlp/stats-lp']);
        }
        if (this.user.type == "MembreAdministrationDto") {
          this.router.navigate(['/backmb/finance']);
        }
        localStorage.setItem("user", JSON.stringify(this.user))
      }
  });
}

  ngOnInit() {

  }

}
