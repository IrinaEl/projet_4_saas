import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-bandeau-countdown',
  templateUrl: './bandeau-countdown.component.html',
  styleUrls: ['./bandeau-countdown.component.sass']
})
export class BandeauCountdownComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(() => {
      $("#clock").countdown("2019/12/31", function (event) {
        $(this).html(event.strftime("<div>%D <span>Jours</span></div> <div>%H <span>Heures</span></div> <div>%M <span>Minutes</span></div> <div>%S <span>Secondes</span></div>"));
      });
    });
   
  }

}
