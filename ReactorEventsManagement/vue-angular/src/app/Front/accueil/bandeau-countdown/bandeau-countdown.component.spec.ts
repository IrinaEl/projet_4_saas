import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BandeauCountdownComponent } from './bandeau-countdown.component';

describe('BandeauCountdownComponent', () => {
  let component: BandeauCountdownComponent;
  let fixture: ComponentFixture<BandeauCountdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BandeauCountdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BandeauCountdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
