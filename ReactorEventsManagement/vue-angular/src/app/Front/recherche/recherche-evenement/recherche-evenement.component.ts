import { Component, OnInit } from '@angular/core';
import { EvenementDto } from '../../../interfaces/evenement-dto';
import { ServicesBackService } from '../../../ServicesRest/back/services-back.service';

@Component({
  selector: 'app-recherche-evenement',
  templateUrl: './recherche-evenement.component.html',
  styleUrls: ['./recherche-evenement.component.sass']
})
export class RechercheEvenementComponent implements OnInit {

  evenements: EvenementDto[];
  selectedEvent:EvenementDto;

  getAllEvenement(): void {
    this.serviceBack.getAllEvenement().subscribe(
      data=>{
        this.evenements = data.evenements;
        let tmp: any = JSON.stringify(this.evenements);
        tmp=decodeURIComponent(escape(tmp));
        this.evenements = JSON.parse(tmp);
        console.log(this.evenements + " ca marche ?");
        
      }
    )
  }

  setSelectedEvent(event:EvenementDto): void {
    this.selectedEvent=event;
  }
  
  constructor(private serviceBack: ServicesBackService) { }

  ngOnInit() {
  this.getAllEvenement();
    
  }
}

