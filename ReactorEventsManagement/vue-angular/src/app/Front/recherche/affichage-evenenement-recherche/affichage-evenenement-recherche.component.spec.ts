import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageEvenenementRechercheComponent } from './affichage-evenenement-recherche.component';

describe('AffichageEvenenementRechercheComponent', () => {
  let component: AffichageEvenenementRechercheComponent;
  let fixture: ComponentFixture<AffichageEvenenementRechercheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageEvenenementRechercheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageEvenenementRechercheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
