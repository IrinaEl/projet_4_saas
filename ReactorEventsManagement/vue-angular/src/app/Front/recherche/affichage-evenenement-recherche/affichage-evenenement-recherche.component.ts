import { Component, OnInit, Input } from '@angular/core';
import { EvenementDto } from '../../../interfaces/evenement-dto';

@Component({
  selector: 'app-affichage-evenenement-recherche',
  templateUrl: './affichage-evenenement-recherche.component.html',
  styleUrls: ['./affichage-evenenement-recherche.component.sass']
})
export class AffichageEvenenementRechercheComponent implements OnInit {

  selectedEvent:EvenementDto;
  @Input() evenement: EvenementDto;

  constructor() { }

  setSelectedEvent(evenement:EvenementDto):void {
    this.selectedEvent = evenement;
  }

  ngOnInit() {
  
  }

}
