import { Component, OnInit } from '@angular/core';
import { UtilisateurDto } from '../../../interfaces/utilisateur-dto';

@Component({
  selector: 'app-header-front',
  templateUrl: './header-front.component.html',
  styleUrls: ['./header-front.component.sass']
})
export class HeaderFrontComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}
 