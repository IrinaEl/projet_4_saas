import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-front-root',
  templateUrl: './front-root.component.html',
  styleUrls: ['./front-root.component.scss']
})
export class FrontRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
