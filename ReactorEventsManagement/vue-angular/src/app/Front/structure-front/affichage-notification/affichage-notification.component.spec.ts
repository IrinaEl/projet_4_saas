import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageNotificationComponent } from './affichage-notification.component';

describe('AffichageNotificationComponent', () => {
  let component: AffichageNotificationComponent;
  let fixture: ComponentFixture<AffichageNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
