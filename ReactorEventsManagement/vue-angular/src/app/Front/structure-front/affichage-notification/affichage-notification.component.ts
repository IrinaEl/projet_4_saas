import { Component, OnInit } from '@angular/core';
import { ActiviteDto } from '../../../interfaces/activite-dto';
import { EvenementDto } from '../../../interfaces/evenement-dto';

@Component({
  selector: 'app-affichage-notification',
  templateUrl: './affichage-notification.component.html',
  styleUrls: ['./affichage-notification.component.sass']
})
export class AffichageNotificationComponent implements OnInit {

  activite: ActiviteDto;
  evenement: EvenementDto;
  
  constructor() { }

  ngOnInit() {
  }

}
