export interface UtilisateurDto {
    id: number,
    mail?: string,
    mdp?: string,
    nom?: string,
    prenom?: string,
    type?: string
}
