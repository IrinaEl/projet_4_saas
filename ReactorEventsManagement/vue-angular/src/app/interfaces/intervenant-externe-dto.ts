import { RessourceDto } from './ressource-dto';
import { TypeIntervenantDto } from './type-intervenant-dto';

export interface IntervenantExterneDto extends RessourceDto {
    typeIntervenant: TypeIntervenantDto
}
