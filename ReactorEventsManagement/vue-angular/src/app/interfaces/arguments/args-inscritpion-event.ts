import { InscriptionDto } from '../inscription-dto';
import { EvenementDto } from '../evenement-dto';

export interface ArgsInscritpion {
    paramInscriptionEvenement ?: InscriptionDto
    paramEvent? : EvenementDto
}
