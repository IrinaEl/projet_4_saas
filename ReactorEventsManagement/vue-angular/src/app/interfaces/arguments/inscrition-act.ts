import { ActiviteDto } from '../activite-dto';
import { InscriptionDto } from '../inscription-dto';

export interface ArgsInscritionAct {
    inscriptionAct : InscriptionDto
    activite : ActiviteDto
}
