import { UtilisateurDto } from './utilisateur-dto';

export interface MembreAdministrationDto extends UtilisateurDto {
}
