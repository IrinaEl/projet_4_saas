import { FactureDto } from './facture-dto';
import { ActiviteDto } from './activite-dto';
import { EvenementDto } from './evenement-dto';

export interface FluxDto {
    id: number,
    montant: number,
    facture?: FactureDto,
    activite?: ActiviteDto,
    evenement?: EvenementDto
}
