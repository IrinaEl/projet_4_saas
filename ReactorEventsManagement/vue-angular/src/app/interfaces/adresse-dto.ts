import { TypeVoieDto } from './type-voie-dto';
import { VilleDto } from './ville-dto';

export interface AdresseDto {
    id: number,
    numeroVoie: string,
    typeVoie: TypeVoieDto,
    nomVoie: string,
    ville: VilleDto
}
