export interface InscriptionException {
    message: string;
}
