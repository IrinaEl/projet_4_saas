import { FluxDto } from './flux-dto';
import { RessourceDto } from './ressource-dto';

export interface DepenseDto extends FluxDto {
    dateSortie: Date,
    ressource: RessourceDto
}
