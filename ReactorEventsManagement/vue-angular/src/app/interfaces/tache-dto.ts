import { TypeTacheDto } from './type-tache-dto';
import { StatutTacheDto } from './statut-tache-dto';
import { RessourceDto } from './ressource-dto';

export interface TacheDto {
    id: number,
    deadline?: number,
    typeTaches?: TypeTacheDto
    statut?:StatutTacheDto
    benevole?:RessourceDto
}
