import { UtilisateurDto } from '../utilisateur-dto';
import { ConnexionException } from '../exception/connexion-exception';

export interface RetourConnexionBack {

    user: UtilisateurDto,
    error: ConnexionException
}
