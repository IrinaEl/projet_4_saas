import { SponsorDto } from '../sponsor-dto';
import { ConnexionException } from '../exception/connexion-exception';

export interface RetourConnexionFront {
    user:SponsorDto,
    error:ConnexionException
}
