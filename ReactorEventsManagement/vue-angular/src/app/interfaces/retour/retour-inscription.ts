import { InscriptionDto } from '../inscription-dto';
import { InscriptionException } from '../exception/inscription-exception';

export interface RetourInscription {
    inscription?: InscriptionDto
    error?: InscriptionException
}
