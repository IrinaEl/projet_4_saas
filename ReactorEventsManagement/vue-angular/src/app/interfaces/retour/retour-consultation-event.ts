import { EvenementDto } from '../evenement-dto';
import { EvenementException } from '../exception/evenement-exception';

export interface RetourConsultationEvent {
    event?:EvenementDto
    error?:EvenementException
}
