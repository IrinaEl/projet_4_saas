import { RessourceDto } from './ressource-dto';

export interface MaterielDto extends RessourceDto {
    intitule: string
}
