import { AdresseDto } from './adresse-dto';
import { TypeEvenementDto } from './type-evenement-dto';

export interface EvenementDto {
    id: number,
    dateDebut?: Date,
    dateFin?: Date,
    intitule: string,
    description: string,
    prix?: number,
    budget?: number,
    adresse?: AdresseDto,
    typeEvenement: TypeEvenementDto
}
