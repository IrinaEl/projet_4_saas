import { EvenementDto } from './evenement-dto';

export interface InscriptionDto {
    id?: number,
    nom: string,
    prenom: string,
    dateNaissance: Date,
    mail: string,
    montantRegle?: number,
    evenement?: EvenementDto
}
