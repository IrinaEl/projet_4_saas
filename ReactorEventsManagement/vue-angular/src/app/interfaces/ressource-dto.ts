export interface RessourceDto {
    id: number,
    prix?: number,
    quantite?: number,
    raisonSociale?: string,
    numeroRCS?: string,
    numeroTelephone?: string,
    mail?: string,
    type?:string
}
