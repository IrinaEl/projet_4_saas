import { UtilisateurDto } from './utilisateur-dto';

export interface SponsorDto extends UtilisateurDto {
    logo: string,
    raisonSociale: string
}
