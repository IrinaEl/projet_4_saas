export interface TypeIntervenantDto {
    id: number,
    intitule: string,
    description?: string
}
