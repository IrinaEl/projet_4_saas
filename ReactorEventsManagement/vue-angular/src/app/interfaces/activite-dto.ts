import { TypeActiviteDto } from './type-activite-dto';
import { ResponsableActiviteDto } from './responsable-activite-dto';
import { EvenementDto } from './evenement-dto';

export interface ActiviteDto {
    id: number,
    dateDebut?: Date,
    dateFin?: Date,
    intitule?: string,
    description?: string,
    nombrePlace?: number,
    prix?: number,
    budget?: number,
    typeActivite?: TypeActiviteDto,
    responsableActivite?: ResponsableActiviteDto,
    evenement?: EvenementDto
}
