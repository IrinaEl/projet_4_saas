import { PaysDto } from './pays-dto';

export interface VilleDto {
    id: number,
    libelle: string,
    pays: PaysDto
}
