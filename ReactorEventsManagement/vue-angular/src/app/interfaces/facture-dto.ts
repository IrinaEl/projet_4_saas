export interface FactureDto {
    id: number,
    numero: string,
    delaiPaiement?: number,
    dateCreation: Date
}
