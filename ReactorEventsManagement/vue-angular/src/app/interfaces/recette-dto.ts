import { FluxDto } from './flux-dto';

export interface RecetteDto extends FluxDto {
    dateEntree: Date
}
