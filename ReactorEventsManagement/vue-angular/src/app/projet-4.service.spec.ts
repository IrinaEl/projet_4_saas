import { TestBed } from '@angular/core/testing';

import { Projet4Service } from './projet-4.service';

describe('Projet4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Projet4Service = TestBed.get(Projet4Service);
    expect(service).toBeTruthy();
  });
});
