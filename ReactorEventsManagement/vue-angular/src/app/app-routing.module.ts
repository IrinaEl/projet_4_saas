import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaseLayoutComponent } from './Layout/base-layout/base-layout.component';
import { PagesLayoutComponent } from './Layout/pages-layout/pages-layout.component';

// DEMO PAGES

// Dashboards

import { AnalyticsComponent } from './DemoPages/Dashboards/analytics/analytics.component';

// Pages

import { ForgotPasswordBoxedComponent } from './DemoPages/UserPages/forgot-password-boxed/forgot-password-boxed.component';
import { LoginBoxedComponent } from './DemoPages/UserPages/login-boxed/login-boxed.component';
import { RegisterBoxedComponent } from './DemoPages/UserPages/register-boxed/register-boxed.component';

// Elements

import { StandardComponent } from './DemoPages/Elements/Buttons/standard/standard.component';
import { DropdownsComponent } from './DemoPages/Elements/dropdowns/dropdowns.component';
import { CardsComponent } from './DemoPages/Elements/cards/cards.component';
import { ListGroupsComponent } from './DemoPages/Elements/list-groups/list-groups.component';
import { TimelineComponent } from './DemoPages/Elements/timeline/timeline.component';
import { IconsComponent } from './DemoPages/Elements/icons/icons.component';

// Components

import { AccordionsComponent } from './DemoPages/Components/accordions/accordions.component';
import { TabsComponent } from './DemoPages/Components/tabs/tabs.component';
import { CarouselComponent } from './DemoPages/Components/carousel/carousel.component';
import { ModalsComponent } from './DemoPages/Components/modals/modals.component';
import { ProgressBarComponent } from './DemoPages/Components/progress-bar/progress-bar.component';
import { PaginationComponent } from './DemoPages/Components/pagination/pagination.component';
import { TooltipsPopoversComponent } from './DemoPages/Components/tooltips-popovers/tooltips-popovers.component';

// Tables

import { TablesMainComponent } from './DemoPages/Tables/tables-main/tables-main.component';

// Widgets

import { ChartBoxes3Component } from './DemoPages/Widgets/chart-boxes3/chart-boxes3.component';

// Forms Elements

import { ControlsComponent } from './DemoPages/Forms/Elements/controls/controls.component';
import { LayoutComponent } from './DemoPages/Forms/Elements/layout/layout.component';

// Charts

import { ChartjsComponent } from './DemoPages/Charts/chartjs/chartjs.component';

// À nous !
import { IrinaComponent } from './Irina/irina/irina.component';
import { BackArcheRaComponent } from './Back/BackTemplateRA/back-arche-ra/back-arche-ra.component';
import { BackArcheMbComponent } from './Back/BackTemplateMB/back-arche-mb/back-arche-mb.component';
import { BackMbEventComponent } from './Back/BackTemplateMB/back-mb-event/back-mb-event.component';
import { BackMbCreerEventComponent } from './Back/BackTemplateMB/back-mb-creer-event/back-mb-creer-event.component';
import { BackArcheLpComponent } from './Back/BackTemplateLP/back-arche-lp/back-arche-lp.component';
import { BackLpEventComponent } from './Back/BackTemplateLP/back-lp-event/back-lp-event.component';
import { FrontRootComponent } from './Front/structure-front/front-root/front-root.component';
import { ConnexionFrontComponent } from './Front/connexion/connexion-front/connexion-front.component';
import { ConnexionBackComponent } from './Front/connexion/connexion-back/connexion-back.component';
import { AccueilComponent } from './Front/accueil/accueil/accueil.component';
import { PageSponsorComponent } from './Front/pages/sponsor/page-sponsor/page-sponsor.component';
import { PageConsultationEventComponent } from './Front/pages/consultation-event/page-consultation-event/page-consultation-event.component';
import { FinanceMbRootComponent } from './Stats/finance-mb-root/finance-mb-root.component';
import { EvenementsComponent } from './Back/BackTemplateRA/evenements/evenements.component';
import { GestionActiviteComponent } from './Back/BackTemplateRA/gestion-activite/gestion-activite.component';
import { FormulaireComponent } from './Front/pages/formulaire-inscription/formulaire/formulaire.component';
import { StatsLpComponent } from './Back/BackTemplateLP/stats-lp/stats-lp.component';
import { StatsRaComponent } from './Back/BackTemplateRA/stats-ra/stats-ra.component';
import { PopUpComponent } from './Front/pages/formulaire-inscription/pop-up/pop-up/pop-up.component';

const routes: Routes = [

  //Nos routes à nous qu'on a fait nous-mêmes, toutes seules comme des grandes !
  { path: 'backmb', component: BackArcheMbComponent, 
  children: [
    { path: 'finance', component: FinanceMbRootComponent, data: { extraParameter: 'dashboardsMenu'}},
    { path: 'creerEvent', component: BackMbCreerEventComponent, data: { extraParameter: 'dashboardsMenu'}},
    { path: 'event', component: BackMbEventComponent, data: { extraParameter: 'dashboardsMenu'}}

  ]},

    { path: '',
    component: FrontRootComponent,
    children: [
      { path: '', component: AccueilComponent },
      { path: 'front-connect', component: ConnexionFrontComponent },
      { path: 'back-connect', component: ConnexionBackComponent },
      { path: 'sponsor', component: PageSponsorComponent},
      { path: 'consultation-event/:idEvent', component: PageConsultationEventComponent},
      { path: 'formulaire/:id', component : FormulaireComponent},
      {path : 'pop-up', component : PopUpComponent}
    ] },



  { path: 'back',
    component: BackArcheRaComponent,
    children: [

      
      { path: 'events', component:EvenementsComponent, data: {extraParameter:'dashboardsMenu'}},
      { path: 'gestion-activite/:idAct', component: GestionActiviteComponent,  data: { extraParameter: 'dashboardsMenu' } },
      { path: 'stats-ra', component: StatsRaComponent,  data: { extraParameter: 'dashboardsMenu' } }
    ] },
  { path: 'backlp', component:BackArcheLpComponent, 
    children: [
      {path: 'event', component: BackLpEventComponent, data: {extraParameter: 'dashbordsMenu'}},
      {path: 'stats-lp', component: StatsLpComponent, data: {extraParameter: 'dashbordsMenu'}}
    
    ]},

  // Des trucs pas à nous, qu'on a pas fait toutes seules
  {
    path: 'template',
    component: BaseLayoutComponent,
    children: [

  // Dashboads

  { path: '', component: AnalyticsComponent, data: { extraParameter: 'dashboardsMenu' } },

  // Elements

  { path: 'elements/buttons-standard', component: StandardComponent, data: { extraParameter: 'elementsMenu' } },
  { path: 'elements/dropdowns', component: DropdownsComponent, data: { extraParameter: 'elementsMenu' } },
  { path: 'elements/icons', component: IconsComponent, data: { extraParameter: 'elementsMenu' } },
  { path: 'elements/cards', component: CardsComponent, data: { extraParameter: 'elementsMenu' } },
  { path: 'elements/list-group', component: ListGroupsComponent, data: { extraParameter: 'elementsMenu' } },
  { path: 'elements/timeline', component: TimelineComponent, data: { extraParameter: 'elementsMenu' } },

  // Components

  { path: 'components/tabs', component: TabsComponent, data: { extraParameter: 'componentsMenu' } },
  { path: 'components/accordions', component: AccordionsComponent, data: { extraParameter: 'componentsMenu' } },
  { path: 'components/modals', component: ModalsComponent, data: { extraParameter: 'componentsMenu' } },
  { path: 'components/progress-bar', component: ProgressBarComponent, data: { extraParameter: 'componentsMenu' } },
  { path: 'components/tooltips-popovers', component: TooltipsPopoversComponent, data: { extraParameter: 'componentsMenu' } },
  { path: 'components/carousel', component: CarouselComponent, data: { extraParameter: 'componentsMenu' } },
  { path: 'components/pagination', component: PaginationComponent, data: { extraParameter: 'componentsMenu' } },

  // Tables

  { path: 'tables/bootstrap', component: TablesMainComponent, data: { extraParameter: 'tablesMenu' } },

  // Widgets

  { path: 'widgets/chart-boxes-3', component: ChartBoxes3Component, data: { extraParameter: 'pagesMenu3' } },

  // Forms Elements

  { path: 'forms/controls', component: ControlsComponent, data: { extraParameter: 'formElementsMenu' } },
  { path: 'forms/layouts', component: LayoutComponent, data: { extraParameter: 'formElementsMenu' } },

  // Charts

  { path: 'charts/chartjs', component: ChartjsComponent, data: { extraParameter: '' } },

]

  },
{
  path: '',
    component: PagesLayoutComponent,
      children: [

        // User Pages

        { path: 'pages/login-boxed', component: LoginBoxedComponent, data: { extraParameter: '' } },
        { path: 'pages/register-boxed', component: RegisterBoxedComponent, data: { extraParameter: '' } },
        { path: 'pages/forgot-password-boxed', component: ForgotPasswordBoxedComponent, data: { extraParameter: '' } },
      ]
},
{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
