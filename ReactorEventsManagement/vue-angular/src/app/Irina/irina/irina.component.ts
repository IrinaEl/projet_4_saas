import { Component, OnInit } from '@angular/core';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-irina',
  templateUrl: './irina.component.html',
  styleUrls: ['./irina.component.sass']
})
export class IrinaComponent implements OnInit {


  public isCollapsed = false;

  public beforeChange($event: NgbPanelChangeEvent) {

    if ($event.panelId === 'preventchange-2') {
      $event.preventDefault();
    }

    if ($event.panelId === 'preventchange-3' && $event.nextState === false) {
      $event.preventDefault();
    }
  };







  constructor() { }

  ngOnInit() {
  }

}
