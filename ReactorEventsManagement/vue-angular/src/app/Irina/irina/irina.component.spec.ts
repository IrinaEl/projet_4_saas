import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IrinaComponent } from './irina.component';

describe('IrinaComponent', () => {
  let component: IrinaComponent;
  let fixture: ComponentFixture<IrinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IrinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IrinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
