import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgReduxModule} from '@angular-redux/store';
import {NgRedux, DevToolsExtension} from '@angular-redux/store';
import {rootReducer, ArchitectUIState} from './ThemeOptions/store';
import {ConfigActions} from './ThemeOptions/store/config.actions';
import {AppRoutingModule} from './app-routing.module';
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';

import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';

// BOOTSTRAP COMPONENTS

import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {ChartsModule} from 'ng2-charts';

// LAYOUT

import {BaseLayoutComponent} from './Layout/base-layout/base-layout.component';
import {PagesLayoutComponent} from './Layout/pages-layout/pages-layout.component';
import {PageTitleComponent} from './Layout/Components/page-title/page-title.component';

// HEADER

import {HeaderComponent} from './Layout/Components/header/header.component';
import {SearchBoxComponent} from './Layout/Components/header/elements/search-box/search-box.component';
import {UserBoxComponent} from './Layout/Components/header/elements/user-box/user-box.component';

// SIDEBAR

import {SidebarComponent} from './Layout/Components/sidebar/sidebar.component';
import {LogoComponent} from './Layout/Components/sidebar/elements/logo/logo.component';


// FOOTER

import {FooterComponent} from './Layout/Components/footer/footer.component';

// DEMO PAGES

// Dashboards

import {AnalyticsComponent} from './DemoPages/Dashboards/analytics/analytics.component';

// Pages

import {ForgotPasswordBoxedComponent} from './DemoPages/UserPages/forgot-password-boxed/forgot-password-boxed.component';
import {LoginBoxedComponent} from './DemoPages/UserPages/login-boxed/login-boxed.component';
import {RegisterBoxedComponent} from './DemoPages/UserPages/register-boxed/register-boxed.component';

// Elements

import {StandardComponent} from './DemoPages/Elements/Buttons/standard/standard.component';
import {DropdownsComponent} from './DemoPages/Elements/dropdowns/dropdowns.component';
import {CardsComponent} from './DemoPages/Elements/cards/cards.component';
import {ListGroupsComponent} from './DemoPages/Elements/list-groups/list-groups.component';
import {TimelineComponent} from './DemoPages/Elements/timeline/timeline.component';
import {IconsComponent} from './DemoPages/Elements/icons/icons.component';

// Components

import {AccordionsComponent} from './DemoPages/Components/accordions/accordions.component';
import {TabsComponent} from './DemoPages/Components/tabs/tabs.component';
import {CarouselComponent} from './DemoPages/Components/carousel/carousel.component';
import {ModalsComponent} from './DemoPages/Components/modals/modals.component';
import {ProgressBarComponent} from './DemoPages/Components/progress-bar/progress-bar.component';
import {PaginationComponent} from './DemoPages/Components/pagination/pagination.component';
import {TooltipsPopoversComponent} from './DemoPages/Components/tooltips-popovers/tooltips-popovers.component';

// Tables

import {RegularComponent} from './DemoPages/Tables/regular/regular.component';
import {TablesMainComponent} from './DemoPages/Tables/tables-main/tables-main.component';

// Widgets

import {ChartBoxes3Component} from './DemoPages/Widgets/chart-boxes3/chart-boxes3.component';

// Forms Elements

import {ControlsComponent} from './DemoPages/Forms/Elements/controls/controls.component';
import {LayoutComponent} from './DemoPages/Forms/Elements/layout/layout.component';

// Charts

import {ChartjsComponent} from './DemoPages/Charts/chartjs/chartjs.component';

// Chart.js Examples

import {LineChartComponent} from './DemoPages/Charts/chartjs/examples/line-chart/line-chart.component';
import {BarChartComponent} from './DemoPages/Charts/chartjs/examples/bar-chart/bar-chart.component';
import {ScatterChartComponent} from './DemoPages/Charts/chartjs/examples/scatter-chart/scatter-chart.component';
import {RadarChartComponent} from './DemoPages/Charts/chartjs/examples/radar-chart/radar-chart.component';
import {PolarAreaChartComponent} from './DemoPages/Charts/chartjs/examples/polar-area-chart/polar-area-chart.component';
import {BubbleChartComponent} from './DemoPages/Charts/chartjs/examples/bubble-chart/bubble-chart.component';
import {DynamicChartComponent} from './DemoPages/Charts/chartjs/examples/dynamic-chart/dynamic-chart.component';
import {DoughnutChartComponent} from './DemoPages/Charts/chartjs/examples/doughnut-chart/doughnut-chart.component';
import {PieChartComponent} from './DemoPages/Charts/chartjs/examples/pie-chart/pie-chart.component';
import { IrinaComponent } from './Irina/irina/irina.component';
import { FrontRootComponent } from './Front/structure-front/front-root/front-root.component';
import { BackArcheRaComponent } from './Back/BackTemplateRA/back-arche-ra/back-arche-ra.component';
import { BackRaSidebarComponent } from './Back/BackTemplateRA/back-ra-sidebar/back-ra-sidebar.component';
import { BackArcheMbComponent } from './Back/BackTemplateMB/back-arche-mb/back-arche-mb.component';
import { BackMbSidebarComponent } from './Back/BackTemplateMB/back-mb-sidebar/back-mb-sidebar.component';
import { BackMbEventComponent } from './Back/BackTemplateMB/back-mb-event/back-mb-event.component';
import { BackArcheLpComponent } from './Back/BackTemplateLP/back-arche-lp/back-arche-lp.component';
import { BackLpSidebarComponent } from './Back/BackTemplateLP/back-lp-sidebar/back-lp-sidebar.component';
import { BackMbCreerEventComponent } from './Back/BackTemplateMB/back-mb-creer-event/back-mb-creer-event.component';
import { BackLpEventComponent } from './Back/BackTemplateLP/back-lp-event/back-lp-event.component';
import { HeaderFrontComponent } from './Front/structure-front/header-front/header-front.component';
import { FooterFrontComponent } from './Front/structure-front/footer-front/footer-front.component';
import { ConnexionFrontComponent } from './Front/connexion/connexion-front/connexion-front.component';
import { ConnexionBackComponent } from './Front/connexion/connexion-back/connexion-back.component';
import { BandeauCountdownComponent } from './Front/accueil/bandeau-countdown/bandeau-countdown.component';
import { AccueilComponent } from './Front/accueil/accueil/accueil.component';
import { RechercheEvenementComponent } from './Front/recherche/recherche-evenement/recherche-evenement.component';
import { AffichageEvenenementRechercheComponent } from './Front/recherche/affichage-evenenement-recherche/affichage-evenenement-recherche.component';
import { AffichageNotificationComponent } from './Front/structure-front/affichage-notification/affichage-notification.component';
import { PageSponsorComponent } from './Front/pages/sponsor/page-sponsor/page-sponsor.component';
import { TarifSponsorComponent } from './Front/pages/sponsor/tarif-sponsor/tarif-sponsor.component';
import { EventDispoSponsorsComponent } from './Front/pages/sponsor/event-dispo-sponsors/event-dispo-sponsors.component';
import { PageConsultationEventComponent } from './Front/pages/consultation-event/page-consultation-event/page-consultation-event.component';
import { AffichageEvenementFinanceComponent } from './Stats/affichage-evenement-finance/affichage-evenement-finance.component';
import { FinanceMbRootComponent } from './Stats/finance-mb-root/finance-mb-root.component';
import { EvenementsComponent } from './Back/BackTemplateRA/evenements/evenements.component';
import { GestionActiviteComponent } from './Back/BackTemplateRA/gestion-activite/gestion-activite.component';
import { PageTitleActiviteRaComponent } from './Back/BackTemplateRA/page-title-activite-ra/page-title-activite-ra.component';
import { EvenementAccordeonComponent } from './Back/BackTemplateRA/evenement-accordeon/evenement-accordeon.component';
import { PageTitleMbComponent } from './Back/BackTemplateMb/page-title-mb/page-title-mb.component';
import { RouterModule } from '@angular/router';
import { ActiviteByEvenementComponent } from './Back/BackTemplateMb/activite-by-evenement/activite-by-evenement.component';
import { PageTitleLpComponent } from './back/backtemplatelp/page-title-lp/page-title-lp.component';
import { ActiviteLpEvenementComponent } from './back/backtemplatelp/activite-lp-evenement/activite-lp-evenement.component';
import { StatsLpComponent } from './Back/BackTemplateLP/stats-lp/stats-lp.component';
import { StatsRaComponent } from './Back/BackTemplateRA/stats-ra/stats-ra.component';
import { FormulaireComponent } from './Front/pages/formulaire-inscription/formulaire/formulaire.component';
import { PopUpComponent } from './Front/pages/formulaire-inscription/pop-up/pop-up/pop-up.component';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [

    // LAYOUT

    AppComponent,
    BaseLayoutComponent,
    PagesLayoutComponent,
    PageTitleComponent,

    // HEADER

    HeaderComponent,
    SearchBoxComponent,
    UserBoxComponent,

    // SIDEBAR

    SidebarComponent,
    LogoComponent,

    // FOOTER

    FooterComponent,

    // DEMO PAGES

    // Dashboards

    AnalyticsComponent,

    // User Pages

    ForgotPasswordBoxedComponent,
    LoginBoxedComponent,
    RegisterBoxedComponent,

    // Elements

    StandardComponent,
    IconsComponent,
    DropdownsComponent,
    CardsComponent,
    ListGroupsComponent,
    TimelineComponent,

    // Components

    AccordionsComponent,
    TabsComponent,
    CarouselComponent,
    ModalsComponent,
    ProgressBarComponent,
    PaginationComponent,
    TooltipsPopoversComponent,

    // Tables

    RegularComponent,
    TablesMainComponent,

    // Dashboard Boxes

    ChartBoxes3Component,

    // Form Elements

    ControlsComponent,
    LayoutComponent,

    // CHARTS

    ChartjsComponent,

    // Chart.js Examples

    LineChartComponent,
    BarChartComponent,
    DoughnutChartComponent,
    RadarChartComponent,
    PieChartComponent,
    PolarAreaChartComponent,
    DynamicChartComponent,
    BubbleChartComponent,
    ScatterChartComponent,
    IrinaComponent,
    FrontRootComponent,
    BackArcheRaComponent,
    BackRaSidebarComponent,
    BackArcheMbComponent,
    BackMbSidebarComponent,
    BackMbEventComponent,
    BackArcheLpComponent,
    BackLpSidebarComponent,
    BackMbCreerEventComponent,
    BackLpEventComponent,
    HeaderFrontComponent,
    FooterFrontComponent,
    ConnexionFrontComponent,
    ConnexionBackComponent,
    BandeauCountdownComponent,
    AccueilComponent,
    RechercheEvenementComponent,
    AffichageEvenenementRechercheComponent,
    AffichageNotificationComponent,
    PageSponsorComponent,
    TarifSponsorComponent,
    EventDispoSponsorsComponent,
    PageConsultationEventComponent,
    AffichageEvenementFinanceComponent,
    FinanceMbRootComponent,
    EvenementsComponent,
    GestionActiviteComponent,
    PageTitleActiviteRaComponent,
    EvenementAccordeonComponent,
    PageTitleMbComponent,
    PageTitleLpComponent,
    ActiviteByEvenementComponent,
    ActiviteLpEvenementComponent,
    FormulaireComponent,
    StatsLpComponent,
    StatsRaComponent,
    PopUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgReduxModule,
    CommonModule,
    LoadingBarRouterModule,

    // Angular Bootstrap Components

    PerfectScrollbarModule,
    NgbModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    // Charts

    ChartsModule,
  ],
  providers: [
    {
      provide:
      PERFECT_SCROLLBAR_CONFIG,
      // DROPZONE_CONFIG,
      useValue:
      DEFAULT_PERFECT_SCROLLBAR_CONFIG,
      // DEFAULT_DROPZONE_CONFIG,
    },
    ConfigActions,
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private ngRedux: NgRedux<ArchitectUIState>,
              private devTool: DevToolsExtension) {

    this.ngRedux.configureStore(
      rootReducer,
      {} as ArchitectUIState,
      [],
      [devTool.isEnabled() ? devTool.enhancer() : f => f]
    );

  }
}
